---
typora-root-url: images
---

# JVM

# 一、JVM探索

- JVM的理解？JAVA 8虚拟机和之前的变化更新？
- 什么是OOM？什么是栈溢出？怎么分析
- JVM的常用调优参数？
- 内存快照如何抓取？怎么分析Dump文件？
- 类加载器

# 二、JVM的位置

# 三、JVM的体系结构

<img src="/../images/JVM结构.png" style="zoom: 50%;" />

​	JVM完整架构图：

<img src="/../images/JVM架构.png" style="zoom: 67%;" />

# 四、类加载器

​	作用：加载`Class` 文件

<img src="/../images/类加载器2.png" style="zoom: 67%;" />

​	类的加载指的是：将类的.class文件中的`二进制数据` 读入到内存中，将其放在`运行时数据区` 的 `方法区`内，然后在`堆区`创建一个这个类的java.lang.Class对象，用来封装**类在方法区中类的对象**。

​	JVM的`类加载`是通过`ClassLoader`及其子类来完成的，类的层次关系和加载顺序可以由下图来描述：

<img src="/../images/类加载器.gif" style="zoom:67%;" />

​	加载过程中会：先检查类**是否被已加载**，检查顺序是**自底向上**。

- 从Custom ClassLoader到BootStrap ClassLoader逐层检查，只要**某个classloader已加载**就视为已加载此类，保证此类只所有ClassLoader**加载一次**。而加载的顺序是自顶向下，也就是**由上层来逐层尝试**加载此类。

---

**类加载器的层次结构：**

- 启动（Bootstrap）类加载器
- 扩展（Extension）类加载器
- 系统（System）类加载器 或 （APP）应用类加载器

```java
// s：某个对象
s.getClass().getClassLoader();	# 获得s使用的类加载器
```

---

**若报：Null异常**

​	原因：**调用不到类加载器**

---

# 五、双亲委派机制

## 1、概念

​		双亲委派模式是在Java 1.2后引入的，其工作原理的是：

		- 如果一个类加载器收到了类加载请求，它**并不会自己先去加载**，而是把这个请求**委托给父类的加载器**去执行；
		- 如果父类加载器还**存在其父类加载器**，则进一步**向上委托**，依次递归；
		- 请求最终将**到达顶层的启动类加载器**，如果父类加载器**可以完成类加载任务**，**就成功返回**；
		- 倘若父类加载器**无法完成此加载任务**，**子加载器才会尝试**自己去加载；
		- 这就是双亲委派模式，即每个儿子都很懒，每次有活就丢给父亲去干，直到父亲说这件事我也干不了时，儿子自己想办法去完成

## 2、优势

​	采用双亲委派模式的是好处是：

  - Java类随着它的类加载器一起具备了一种**带有优先级的层次关系**，通过这种层级关系可以**避免类的重复加载**；

  - 当父亲已经加载了该类时，就**没有必要**子ClassLoader再加载一次；

  - 其次是考虑到安全因素，java**核心api中定义类型**不会被随意替换；

    （假设通过网络传递一个名为java.lang.Integer的类，通过双亲委托模式传递到启动类加载器，而启动类加载器在核心Java API发现这个名字的类，发现该类已被加载，并不会重新加载网络传递的过来的java.lang.Integer，而直接返回已加载过的Integer.class，这样便可以防止核心API库被随意篡改）

# 六、沙箱安全机制

- 沙箱机制就是：将java代码**限定在虚拟机特定的运行范围**。
- 沙箱主要显示系统资源访问
- 所有的Java程序运行在指定沙箱，可以定制安全策略。

![](/../images/沙箱安全机制.png)

----

**组成沙箱的基本组件**：

- 字节码校验器
  - 确保文件遵循Java语言规范
- 类加载器
  - 防止恶意代码去干涉善意的代码 //双亲委派

# 七、Native

![](/../images/Native.png)

​	**native关键字**说明其修饰的方法是：一个**原生态方法**，**方法对应的实现**：不在当前文件，而是在**用其他语言（如C和C++）实现的文件中**。

​	Java语言本身：**不能对操作系统底层**进行**访问和操作**，但是可以通过`JNI接口`**调用其他语言**来**实现对底层的访问**，实际上就是java就是**在不同的平台上**调用**不同的native方法**实现对操作系统的访问的。

# 八、PC寄存器

​	PC寄存器又叫：**程序计数器**（Program Counter Register）；

​	每个线程**启动的时候**都会创建一个PC寄存器，PC寄存器里**保存有当前正在执行的**  `JVM`指令的地址。

​	程序计数器：是线程私有的，就是一个指针，指向：方法区中的方法字节码（用来存储**指向下一条指令的地址**,也**即将要执行的指令**代码），由执行引擎读取下一条指令，是一个非常小的内存空间，几乎可以忽略不记。

​	如果执行的是一个Native方法，那这个**计数器是空**的。

# 九、方法区

​	**属于：**永久区的一部分

​	方法区：**被所有线程共享**，所有字段和方法字节码，以及一些特殊的方法，如：构造方法，接口代码也在此定义（所有定义的方法信息都保存在这里——**共享区间**）

​	方法区一般存着：**静态变量**（static），**常量**（final），Class**类信息**，**常量池**。

​	**实例变量**存在：**堆内存中**，与方法区无关。

# 十、栈

​	程序**正在执行的方法**，一定在**栈的顶部**~

​	若**栈满了**，则会抛出：`StackOverflowError` 异常

---

<img src="/../images/无线压栈.png" style="zoom:50%;" />

---

栈、堆、方法区之间的**交互关系**

<img src="/../images/栈堆方法区的关系.png" style="zoom: 50%;" />

# 十一、堆

​	Heap：一个JVM只有一个堆内存，堆内存的大小是可以：调节的。

​	类加载器读取类的文件后，将：实例对象保存到堆中。

---

​	堆内存分为三个区域（**JDK 1.8之前**）：

- 新生区（伊甸园区）
- 养老区
- 永久区

<img src="/../images/堆三个区域.png" style="zoom: 67%;" />

​	若：堆内存存满了（也就是**三个区域都存满**了），则会报：`java.lang.OutOfMemoryError:java heap space`异常

---

​	堆内存分为三个区域（**JDK 1.8之后**）：

<img src="/../images/jdk1.8之后的堆空间.png" style="zoom:50%;" />

​	在JDK 1.8之后，永久存储区 改为：元空间

# 十二、一个对象实例化的过程到内存

​	通过super **初始化父类**内容时，**子类**的成员变量并**未显示初始化**，但默认初始化;

​	等super() 父类初始化完毕后，才进行子类的成员变量显示初始化。

```java
Person person = new Person();
/*
  1. JVM 会读取指定路径下的Person.class文件， 并加载进内存， 并会先加载Person 的父类（如果有直接的父类的情况下）
  2. 在堆内存中开辟空间， 分配地址
  3. 并在对象空间中，对对象中的属性进行默认初始化
  4. 调用对应的构造函数进行初始化
  5. 在构造函数中，第一行会先调用父类中的构造函数进行初始化
  6. 父类初始化完毕后，再对子类属性进行显示初始化
  7. 在进行子类构造函数的特定初始化
  8. 初始化完毕后，将地址值 赋值给引用变量
*/
```

# 十三、永久区

​	JDK 1.8 后，改名：元空间（**逻辑上存在；物流上不存在**）

​	真理：经过研究，99%的对象都是临时对象。

​	这个区域常驻内存，用来存放：JDK 自身携带的Class对象。

​	这个区域不存在：GC，关闭JVM 就会释放这个区域的内存。

---

​	**OOM（Out Of Memory）：**

 - 一般都是启动类，加载了大量的第三方jar包；

 - Tomcat部署了太多的应用；

 - 大量动态生成的反射类；

   直到内存满，出现：OOM

# 十四、堆内存调优

## 1、获取JVM的使用的内存和初始化内存

```java
// 返回虚拟机视图使用的最大内存
long max = Runtime.getRuntime().maxMemory();

// 返回jvm的初始化总内存
long total - Runtime.getRuntime().totalMemory();

System.out.println((max/(double)1024/1024));
System.out.println((total/(double)1024/1024));
```

​	默认情况下：分配的**总内存**是电脑内存的：1/4，而**初始化内存**：1/64。

## 2、扩大堆内存的空间

​	若遇到：**OOM（内存溢出）异常**，可以考虑：**扩大堆内存的空间**，若扩大后还报错，可能是代码问题。

### （1）设置VM配置—PrintGCDetails，打印GC信息

```java
// 指定1024M内存，PrintGCDetails：打印GC垃圾回收信息
-Xms1024m -Xmx1024m -xx:+PrintGCDetails

-Xms：设置初始化内存分配大小  /164
-Xmx：设置最大分配内存，默认  1/4
    
    
-Xmx10240m -Xms10240m -Xmn5120m XxSurvivorRatio=3

-Xmx：最大堆大小
-Xms：初始堆大小
-Xmn：年轻代大小

-XXSurvivorRatio：年轻代中Eden区与Survivor区的大小比值
    
年轻代5120m， Eden：Survivor=3，Survivor区大小=1024m（Survivor区有两个，即将年轻代分为5份，每个Survivor区占一份），总大小为2048m。
```

**步骤：**file——>Edit Confgiuration...

<img src="/../images/自定义JVM内存大小.png" style="zoom:50%;" />

再执行：

```java
// 返回虚拟机视图使用的最大内存
long max = Runtime.getRuntime().maxMemory();

// 返回jvm的初始化总内存
long total - Runtime.getRuntime().totalMemory();

System.out.println((max/(double)1024/1024));
System.out.println((total/(double)1024/1024));
```

结果：`heap`堆

![](/../images/结果.png)

## 3、查看GC的清理方式

设置堆内存大小：

```
-Xms8m -Xmx8m -xx:+PrintGCDetails  
```

写一个程序：

```java
String str = "kuangshen";

while(true){
    str + = new Random().nextInt(777777777)+new Random().nextInt(777777777);
}
```

结果：

![](/../images/GC.png)

## 4、使用JPofiler工具分析OOM原因

​	若出现了OOM故障，如果排除？

	-	能够看到代码第几行出错：内存快照分析工具；MAT，Jprofiler
	-	Dubug

----

### （1）MAT、JPofiler作用

	-	分析Dump（进程的内存镜）内存文件，快速定位内存泄露；
	-	获得堆中的数据
	-	获得大的对象
	-	...

### （2）安装Jpofiler

#### 	①	IDEA中安装Jpofiler插件

<img src="/../images/安装JPofiler.png" style="zoom:50%;" />

#### 	②	安装Jpofilers可视化工具

​	官方地址：https://www.ej-technologies.com/products/jprofiler/overview.html

​	Jpofilers：**性能测试**

<img src="/../images/注册Jpofiler.png" style="zoom:50%;" />

​	无脑下一步，安装后，注册，**注册码**：

<img src="/../images/注册码.png" style="zoom:50%;" />

#### 	③	IDEA中配置可视化工具

![](/../images/选择插件.png)

### （3）使用JPofiler分析

#### 	①	编写一个JAVA程序

```java
byte[] array = new byte[1*1024*1024]; 

public static void main(String[]args){
	ArrayList<Demo03> list = new ArrayList<>();
	int count = 0;
	
	try{
		while(true){
			list.add(new Demo03());  // 问题所在
			count = count + 1;
		}
	}catch(Error e){
		System.out.println("count:"+count);
		e.printStackTrace();
	}
}
```

#### 	②	配置堆内存信息HeapDump

```java
 // 只要报：OutOfMemoryError，就把文件下载下来
-Xmslm -Xms8m -XX:+HeapDumpOutOfMemoryError
```

​	**HeapDump**：从堆中荡出：信息；

<img src="/../images/JV配置.png" style="zoom: 80%;" />

#### 	③	打开dumping head的文件

<img src="/../images/打开JPofiler文件.png" style="zoom:50%;" />

#### 	④	分析内存溢出的原因

​	方式一：**查看大对象**（也就是查看那个对象使用的内存最多）

![](/../images/使用jpofiler1.png)

​	方式二：查看线程，**看是哪行报错**

<img src="/../images/使用jpofiler2.png" style="zoom:50%;" />

# 十五、GC

## 1、GC的作用域

​	**方法区、堆**

![](/../images/GC的作用区域.png)

## 2、常见GC题目

	-	JVM的**内存模型和分区**，每个区放什么？
	-	堆里面的分区有哪些？ **Eden、from、to、老年区**，他们的特点？
	-	GC的算法有哪些？**标记清除法、标记压缩、复制算法、引用算法**
	-	**轻GC** 和 **重 GC**分别在什么时候发生？

## 3、引用计数法

![](/../images/引用计数法.png)

## 4、复制算法

​	主要针对：新生区

​	**谁空谁是：to**

```java
// 自定义某个对象幸存多少次后，被存入：老年区
-XX: -XX:MaxTenuringThreshold=?   
```

![](/../images/复制算法.png)

---

**好处：**

- 没有内存碎片

**坏处：**

- 浪费内存空间，多了一半空间永远是：to （假设对象100%存活）

----

最佳使用场景：**对象存活度较低的情况下**

## 5、标记清除算法

<img src="/../images/标记算法.png" style="zoom:67%;" />

---

- **优点**：不需要额外的空间！
- **缺点**：两次扫描，浪费时间，会产生内存碎片（图**中虚线正方形**：内存碎片）

![](/../images/内存碎片.png)

## 6、标记压缩算法

​	对**标记清理算法进一步优化**，防止了：内存碎片产生

![](/../images/压缩标记.png)

---

## 7、总结

内存效率：

​	复制算法  > 标记清除算法 > 标记压缩算法（时间复杂度）

内存整齐度：

​	复制算法   = 标记压缩算是   > 标记清除算法

内存利用率：

​	标记压缩算法   = 标记清除算法  > 复制算法

# 十六、