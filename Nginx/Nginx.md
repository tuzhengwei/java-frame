---
typora-root-url: images
---

# Nginx

> Nginx下载：hub.daocloud.io/repos/2b7310fb-1a50-48f2-9586-44622a2d1771
>
> Nginx笔记1：https://www.ljxy.pub/article/6
> Nginx笔记2：https://blog.csdn.net/m0_49558851/article/details/107786372

# 一、Nginx

## 1 为什么要学习Nginx

- 问题1：客户端到底要将请求发送给哪台服务器，是服务器1？还是服务2？...
- 问题2：如果所有客户端的请求都发送给了服务器1，服务器2呢？...
- 问题3：客户端发送的请求可能是申请动态资源的，也有申请静态资源的...

![](../images/普通.png)



> 在搭建集群后，使用Nginx做反向代理

![](/../images/Nginx代理.png)

## 2 Nginx介绍

Nginx是由俄罗斯人研发的，应对Rambler的网站并发，并且2004年发布的第一个版本

Nginx的特点

1. 稳定性极强，7*24小时不间断运行(就是一直运行)
2. Nginx提供了非常丰富的配置实例
3. 占用内存小，并发能力强（随便配置一下就是5w+,而tomcat的默认线程池是150）

----

# 二、正/反向代理

## 1 正向代理

1. 正向代理服务是由：客户端设立的
2. 客户端了解：代理服务器和目标服务器
3. 帮助客户实现突破访问权限，提高访问的速度，对目标服务器<button>隐藏客户端的ip地址</button>

![](/../images/正向代理.png)

## 2 反向代理

1. 反向代理服务器是配置：在服务端的
2. 客户端<button>不知道访问</button>的到底是：哪一台服务器
3. 达到负载均衡，并且可以<button>隐藏服务器真正的ip地址</button>

![](/../images/方向代理.png)

# 三、Nginx配置

## 1 安装Nginx

### 1.1 Linux安装

```cmd
[root@localhost ~]# cd /opt/
[root@localhost opt~]# ls
```

**第一步：**创建一个`docker_nginx`文件夹

```cmd
[root@localhost opt~]# mkdir docker_nginx
[root@localhost opt~]# cd docker_nginx
```

**第二步：**创建一个.yml配置文件

```cmd
[root@localhost docker_nginx~]# vi docker-compose.yml
```

将下面配置粘贴到：`docker-compose.yml `中

```yml
version: '3.1'
services: 
  nginx:
    restart: always
    # Nginx镜像地址
    image: daocloud.io/library/nginx:latest
    container_name: nginx
    # 默认端口：80:80
    ports: 
      - 80:80
```

**第三步：**编译配置文件

```cmd
[root@localhost docker_nginx~]# docker-compose up -d
```

**第四步：**访问80端口

![](/../images/默认地址.png)

## 2 Nginx配置文件

**重点：**<button>server模块</button>

**第一步：**进入Nginx配置的内部（ebd02ee738d8）

```cmd
[root@localhost docker_nginx~]# docker ps
[root@localhost docker_nginx~]# docker exec -it ebd bash
# 进入Nginx配置文件
root@ebd02ee738d8:/# cd/etc/nginx
```

```cmd
root@ebd02ee738d8:/etc/nginx# ls
conf.d fastcgi_params ...... nginx.conf
```

**第二步：**打开nginx.conf文件

```cmd
root@ebd02ee738d8:/etc/nginx# cat nginx.conf
```

### 2.1 nginx.conf

```yml
user  nginx;
worker_processes  1;

error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

# 以上同城为全局块
# worker_processes的数值越大，Nginx的并发能力就越强
# error_log代表Nginx错误日志存放的位置
# pid是Nginx运行的一个标识

events {
    worker_connections  1024;
}
# events块
# worker_connections的数值越大，Nginx的并发能力就越强

http {
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    keepalive_timeout  65;

    #gzip  on;
	
	# 引入conf.d目录下的所有.conf文件，例如：default.conf
    include /etc/nginx/conf.d/*.conf;
}

# http块
# include代表引入一个外部文件
# include       /etc/nginx/mime.types;	mime.types中存放着大量媒体类型
# include /etc/nginx/conf.d/*.conf;	 引入了conf.d下以.conf为结尾的配置文件
```

### 2.2 default.conf

**注意！！！**：**default.conf** <button>在**nginx.conf**中被引入</button>

```cmd
root@ebd02ee738d8:/etc/nginx/conf.d# ls
default.conf
root@ebd02ee738d8:/etc/nginx/conf.d# cat default.conf
```

conf.d目录下只有一个default.conf文件，内容如下

```yml
# listen代表Nginx监听的端口号
# server_name代表Nginx接受请求的IP
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

	# 请求资源模块
    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }
	# location块
	# root: 将接受到的请求根据/usr/share/nginx/html去查找静态资源
	# index: 默认去上述的路径中找到index.html或index.htm

    # error_page  404              /404.html;
    # redirect server error pages to the static page /50x.html
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
```

## 3 修改docker-compose文件

### 3.1 关闭容器

```cmd
root@ebd02ee738d8:/etc/nginx/conf.d# exit
exit
[root@localhost docker_nginx~]# docker-compose down
```

### 3.2 修改文件

```cmd
[root@localhost docker_nginx~]# vi docker-compose.yml
```

```yml
version: '3.1'
services: 
  nginx:
    restart: always
    image: daocloud.io/library/nginx:latest
    container_name: nginx
    ports: 
      - 80:80
    volumes:
      - /opt/docker_nginx/conf.d/:/etc/nginx/conf.d
```

### 3.3 重新构建容器

```cmd
[root@localhost docker_nginx~]# docker-compose build
nginx uses an image, skipping
```

### 3.4 重启启动容器

```cmd
[root@localhost docker_nginx~]# docker-compse -up -d
[root@localhost docker_nginx~]# ls
conf.d docker-compse.yml
[root@localhost docker_nginx~]# cd conf.d/
[root@localhost docker_nginx~]# ls
# 后续可再次做扩展
null
```

### 3.5 创建default.conf

```yml
# listen代表Nginx监听的端口号
# server_name代表Nginx接受请求的IP
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

    #charset koi8-r;
    #access_log  /var/log/nginx/host.access.log  main;

	# 请求资源模块
    location / {
        root   /usr/share/nginx/html;
        index  index.html index.htm;
    }
}
```

### 3.6 重启Nginx

```cmd
[root@localhost docker_nginx~]# docker-compose restart
```

浏览器

![](/../images/默认地址.png)

----

# 四、反向代理实现

## 1 准备

1 准备目标服务器

> 配置一个tomcat服务器

```cmd
# 启动服务
[root@localhost] docker_mysql_tomcat] # docker-compose up -d
```

2 创建一个root目录

```cmd
[root@localhost] docker_mysql_tomcat] # cd tomcat_webapps/
[root@localhost] tomcat_webapps] # mkdir ROOT
[root@localhost] tomcat_webapps] # cd ROOT
# 创建一个index.html
[root@localhost] ROOT] # vi index.html
```

```
<h1>Hello Nginx!!!</h1>
```

## 2 客户端访问

> 启动后直接可以IP访问

```
192.168.109:8080
```

![](/../images/hello.png)

---

## 3 Nginx访问

```cmd
# 扩展命名（替换）
[root@localhost conf.d~]# mv defalt.conf default.conf
```

### 3.1 配置访问路径

```cmd
[root@localhost docker_nginx~]# cd conf.d/
[root@localhost conf.d~]# ls
default.conf
# 进入默认配置
[root@localhost conf.d~]# vi default.conf
```

在default.conf输入

```yml
server{
	listen 80;
	server_name localhost;
	
	# 基于反向代理访问到Tomcat服务器
	location / {
		proxy_pass http://192.168.199.109:8080/;
	}
}
```

```cmd
# 重新启动，使配置生效
[root@localhost docker_nginx~]# docker-compose restart
```

### 3.2 访问

直接服务IP访问，不用端口号

# 五 location路径映射

## 1 优先级关系

> 从高到底，高的优先级高

```properties
(location = ) > 
		(location /xxx/yyy/zzz) > 
        	(location ^~) >
        		(location ~,~*) > 
        			(location /起始路径) > 
        				(location /)
```

## 2 匹配关系

```yml
# 1. = 匹配（优先级最低，都匹配）
location / {
	# 匹配，主机名后面不能带任何字符串
	# 例如 www.baidu.com 不能是 www.baidu.com/id=xxx
}
```

```yml
# 2. 通用匹配
location /xxx {
	# 匹配所有以/xxx开头的路径
	# 例如127.0.0.1:8080/xxx	xxx可以为空，为空则和=匹配一样
}
```

```yml
# 3. 正则匹配
location ~ /xxx {
	# 匹配所有以/xxx开头的路径
}
```

```yml
# 4. 匹配开头路径
location ^~ /xxx/xx {
	# 匹配所有以/xxx/xx开头的路径
}
```

```yml
# 5. 匹配结尾路径
location ~* \.(gif/jpg/png)$ {
	# 匹配以.gif、.jpg或者.png结尾的路径
}
```

## 3 案例

```cmd
[root@localhost opt]# cd cocker_nginx/
[root@localhost docker_nginx]# ls
conf.d docker-compose.yml
[root@localhost docker_nginx]# cd conf.d/
[root@localhost conf.d]# ls
default.conf
[root@localhost conf.d]# vi default.conf
```

```yml
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

	location /index {
        proxy_pass http://192.168.199.109:8081/;	# tomcat首页
    }

	location ^~ /CR/ {
        proxy_pass http://192.168.199.109:8080/Login/;	# 前台首页
    }

    location / {
        proxy_pass http://192.168.199.109:8080/adminIndex/;	# 后台首页
    }
}
```

> 重启nginx

```cmd
[root@localhost conf.d]# cd ../
[root@localhost docker_nginx]# docker-compose restart
```



# 六 负载均衡

## 1 轮询

​	根据客户端发起的请求次数，循环交给服务器处理（从上到下，平均分配）

![](/../images/轮播.png)

## 2 权重

​	按服务器处理的<button>速度比例</button>分配各自处理的请求

![](/../images/权重.png)

## 3 ip_hash

​	客户端<button>指定请求的服务器IP地址</button>（仅访问目标服务器）

![](/../images/iphash.png)

## 4 案例

### 4.1 准备三台服务器

<img src="/../images/三台服务器.png" style="zoom:50%;" />

---

```cmd
[root@localhost docker_nginx]# ls
conf.d docker-compose.yml
[root@localhost docker_nginx]# cd conf.d/
[root@localhost conf.d]# vi default.conf
```

### 4.2 轮询案例

​	配置写入：

```yml
# 别名
upstream my_server{
	# 轮询
    server ncthz.top:8080;
    server ncthz.top:8081;
}
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

	location / {
        proxy_pass http://my_server/;	# tomcat首页
    }
}
```

```yml
upstream 名字{
    server ip:端口;
    server 域名:端口;
    ...
    ...
}
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

	location / {
        proxy_pass http://upstream的名字/;	
    }
}
```

重启

```cmd
[root@localhost docker_nginx]# docker-compose restart
```

​	访问（IP不同，访问tomcat版本不同）

<img src="/../images/版本号.png" style="zoom:80%;" />

---

### 4.3 权重案例

​	实现权重的方式：在配置文件中upstream块中<button>加上weight</button>

```yml
upstream my_server{
    server ncthz.top:8080 weight=10;
    server ncthz.top:8081 weight=2;
}
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

	location / {
        proxy_pass http://my_server/;	#tomcat首页
    }
}
```

​	保存——重启后，多次刷新访问地址，权重高的**出现次数**大于权重低的

### 4.4 ip_hash

​	实现ip_hash方式：在配置文件upstream块中加上ip_hash;

```yml
upstream my_server{
	ip_hash;
    server ncthz.top:8080 weight=10;
    server ncthz.top:8081 weight=2;
}
server {
    listen       80;
    listen  [::]:80;
    server_name  localhost;

	location / {
        proxy_pass http://my_server/;	#tomcat首页
    }
}
```

​	仅访问：ncthz.top:8080 服务器

----

# 七 动静分离

## 1 概念

​	Nginx的并发能力公式：

```
// 最终的并发能力
worker_processes * worker_connections / 4|2 = Nginx
```

​	普通请求方式

![](/../images/普通请求方式.png)

​	从图可以看出，每次请求都需要去访问服务器，若访问的是静态资源，**例如：image、html页面等**，每次请求都会加大服务器的压力

<img src="/../images/动静分离.png" style="zoom:67%;" />

​	若访问的是静态资源，则不再请求服务器，直接可以返回

## 2 动态资源代理

```yml
#配置如下
location / {
  proxy_pass 路径;
}
```

## 3 静态资源代理

### 3.1 修改`docker-compose.yml`

![](/../images/静态分离1.png)

```cmd
[root@localhost docker_nginx]# vi docker-compose.yml
```

修改docker-compose.yml添加静态资源数据卷 不同版本的静态资源位置可能不同，可以在三中查看Nginx代理（location块中root后的路径）

```yml
version: '3.1'
services: 
  nginx:
    restart: always
    image: daocloud.io/library/nginx:latest
    container_name: nginx
    ports: 
      - 80:80
    volumes:
      - /opt/docker_nginx/conf.d/:/etc/nginx/conf.d
      - /opt/docker_nginx/img/:/data/img
      - /opt/docker_nginx/html/:/data/html
```

### 3.2 停Nginx并重启 

![](/../images/静态分离2.png)

```cmd
# 关闭
[root@localhost docker_nginx]# docker-compose down
# 启动
[root@localhost docker_nginx]# docker-compose up -d
```

### 3.3 添加HTML和图片

```cmd
[root@localhost img]# cd html/
# 创建一个index.html
[root@localhost html]# vi index.html
```

```html
<h1>Hello Static Resource!</h1>
```

添加图片

```cmd
[root@localhost html]# cd ../
[root@localhost docker_nginx]# cd img/
[root@localhost img]#
...
```

![](/../images/图片.png)

​	（1）随机拖入一张图片到服务器，然后修改名称为：1.jgp

​	（2）将1.jgp移动到指定的img目录下

```cmd
[root@localhost ~]# mv 1.jpg /opt/docker_nginx/img/
```

### 3.4 修改conf.d配置文件

![](/../images/静态分离3.png)

```yml
upstream my_server{
	ip_hash;
    server ncthz.top:8080 weight=10;
    server ncthz.top:8081 weight=2;
}

server {
    listen       80;
    server_name  localhost;

    # 代理到html静态资源
	location /html {
	    # 等同于：data/html/
        root /data;
        # 首页
        index  index.html;
    }
    
    # 代理到img静态资源
    location /img{
    	root /data;
    	autoindex on;
    }
}
```

```yml
# 配置如下
location / {
    root 静态资源路径;
    index 默认访问路径下的什么资源;
    autoindex on; # 代表展示静态资源的全部内容，以列表的形式展开
}
```

### 3.5 重启Nginx并预览

```cmd
[root@localhost docker_nginx]# docker-compose restart
```

<img src="/../images/首页.png" style="zoom:50%;" />

---

​	访问图片（地址：192.168.199.109/img/1.jpg）

<img src="/../images/访问图片.png" style="zoom: 33%;" />

----

​	autoindex作用：代表展示静态资源的全部内容，以<button>列表的形式</button>展开

<img src="/../images/autoimages.png" style="zoom:50%;" />

---



# 八 集群







