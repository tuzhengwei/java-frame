---
typora-root-url: images
---

# Redis

# 1 nosql讲解

## 1.1 单机MySQL的年代

![](../images/单机MYSQL.png)

​	此模式仅适合：数据量小的项目（百万级别及以下）。

​	若数据量趋近于无穷，会出现：

​		（1）单个机器存储不够，查询耗时

​		（2）若建立索引，则单个机器内存不够

​		（3）访问量（读写混合），单个服务器承受不住

## 1.2 memcached（缓存）+MySQL+垂直再分

![](../images/缓存+数据库.png)

​	一个项目，百分之八十都是对数据库进行查询操作。

## 1.3 分库分表+水平拆分+MySQL集群

> 本质：数据库（读、写）
>
> cache：解决了读

（1）`MyISAM`

​	**表锁**（整张表都锁住），高并发下，会严重影响效率

（2）`Innodb`

​	**行锁**

![](../images/分库分表.png)

## 1.4 目前一个项目架构

![](../images/现代.png)

# 2 nosql数据模型

## 2.1 四大数据库介绍

**（1）KV键值对**

Redis

**（2）文档型数据库（bson格式与json一样）**

- MongoDB
  - 一个基于分布式文件存储的数据库，C++编写，主要处理大量的文档
  - 一个介于关系型数据库和非关系型数据库之间的产品！是非关系数据库功能最丰富的数据库
- ConthDB

**（3）列存储数据库**

- HBase
- 分布式文件系统

**（4）图关系数据库**

## 2.2 对比

![](../images/数据库对比.png)



# 3 Redis入门

## 3.1 概述

> Redis是什么？

​	Redis（**Re**mote **Di**ctionary **S**erver )，即远程字典服务。

​	是一个开源的使用ANSI [C语言](https://baike.baidu.com/item/C语言)编写、支持网络、可基于内存亦可持久化的日志型、Key-Value[数据库](https://baike.baidu.com/item/数据库/103728)，并提供多种语言的API。

​	默认端口：6379

---

> Redis能干嘛？

（1）内存存储、持久化（避免内存断电即失）

（2）效率高，可用于高速缓存

（3）发布订阅系统

（4）地理信息分析

（5）计时器、计数器（浏览量）

---

> 特性

（1）多样的数据类型

（2）持久化

（3）集群

（4）事务

（5）...

----

## 3.2 Redis安装-Windows安装

> 步骤1：下载安装包

地址：https://github.com/microsoftarchive/redis/releases/tag/win-3.2.100

> 步骤2：解压并运行

![](../images/windows解压.png)

## 3.3 Reids安装-Linux安装

> **步骤1：下载安装包**

地址：https://redis.io/

---

> **步骤2：上传文件到Linux**

![](../images/上传文件.png)

---

> **步骤3：解压包**

![](../images/Linux解压.png)

```cmd
# 将文件移动到opt目录下
[root@kuangshen kuangshen]# mv redis-5.0.8.tar.gz /opt
```

---

> **步骤4：安装环境**

```cmd
[root@kuangshen ~]# cd /opt/
[root@kuangshen opt]# cd redis-5.0.8
[root@kuangshen redis-5.0.8]# ls
# 安装GCC
[root@kuangshen redis-5.0.8]# yum install gcc-c++
# 查看GCC版本
[root@kuangshen redis-5.0.8]# gcc -v
# 自动配置所需文件
[root@kuangshen redis-5.0.8]# make
[root@kuangshen redis-5.0.8]# make install
```

> **步骤5：redis的默认安装路径，`usr/local/bin`**

![](../images/redis默认路径.png)

> **步骤6：将redis配置文件复制到当前目录下**

![](../images/拷贝配置文件.png)

```cmd
# 创建一个文件夹
[root@kuangshen bin]# mkdir kconfig
# 将配置文件拷贝在所创文件夹下
[root@kuangshen bin]# cp /opt/redis-5.0.8/redis.conf kconfig
[root@kuangshen kconfig]# ls
```

> **步骤7：redis默认不是后台启动的，修改配置文件**

```cmd
[root@kuangshen kconfig]# vim redis.conf
```

<img src="../images/修改redis后台启动.png" style="zoom: 80%;" />

---

> **步骤8：启动**

```cmd
# 查看当前工作目录是那个bin
[root@kuangshen bin]# pwd
/usr/local/bin
```

- 启动redis服务端

```cmd
# 不使用redis自己的配置，使用拷贝的配置文件启动
[root@kuangshen bin]# redis-server kconfig/redis.conf
```

- 启动redis客户端进行测试

```cmd
[root@kuangshen bin]# ls
# 使用redis客户端进行连接
[root@kuangshen bin]# redis-cli -p 6379
127.0.01:6379>ping 
ping
127.0.01:6379>set name tzw  
ok
127.0.01:6379>get nage
# 不存在nage属性，返回：空
(nil)
127.0.01:6379>get name
tzw
127.0.01:6379>keys *  # 查看所有的key
1) "name"
```

> **步骤9：查看redis进程是否启动** 

```cmd
[root@kuangshen ~]# ps -ef|grep redis # 查看进程并过滤redis信息
```

> **步骤10：关闭redis**

```cmd
[root@kuangshen bin]# redis-cli -p 6379
127.0.01:6379>ping 
ping
127.0.01:6379>shutdown
not connected> exit
[root@kuangshen ~]# ps -ef|grep redis # 查看redis是否关闭
```



## 3.4 Linux命令找不到问题

**注意**：若出现<button>命名找不到</button>，则需要修改环境配置

![](../images/linux命名找不到问题.png)

解决方法：

```cmd
# 此文件为系统的每个用户设置环境信息,当用户第一次登录时,该文件被执行
[root@kuangshen /]# vim  /etc/profile 
```

```cmd
JAVA_HOME=/usr/java/jdk1.8.0_221-amd64
CLASSPATH=%JAVA_HOME%/lib:%JAVA_HOME%/jre/lib
export CLASSPATH JAVA_HOME
```

```cmd
# 在/etc/profile中添加环境变量后，是使用source /etc/profile编译后只能在当前终端生效
[root@kuangshen /]# source /etc/profile
# 注意：重新开启一个终端后，该环境变量失效
# 解决方法：重新启动reboot，因为设置的环境变量，并没有真正生效，只是使用source 命令让临时运行而已
```

## 3.5 性能测试

**语法：**

```cmd
redis-benchmark [option] [option value]
```

![](../images/性能测试.png)

> **案例1：同时执行 10000 个请求测试性能**

```cmd
$ redis-benchmark -n 10000  -q
```

> **案例2：测试100个并发连接，100000请求**

![](../images/测试性能.png)

```cmd
[root@kuangshen bin]# redis-benchmark -h localhost -p 6379 -c 100 -n 100000
```

> <button>查看分析结果</button>

![](../images/分析1.png)

## 5.6 启动redis

![](../images/启动.png)

# 4 Redis基础知识

![](../images/进入配置文件.png)

```cmd
# 进入redis配置文件
[root@kuangshen kconfig]# vim redis.conf
```

## 4.1 16个数据库

​	默认使用第0个数据库

![](../images/数据库.png)

> select：切换数据库

```cmd
[root@kuangshen bin]# redis-cli -p 6379
127.0.01:6379>select 3  # 切换数据库
OK
127.0.01:6379[3]>dbsize # 查看数据库大小
127.0.01:6379[3]>keys * # 查看数据库所有的key
127.0.01:6379[3]>flushdb * # 清空当前数据库
127.0.01:6379[3]>flushall * # 清空全部数据库
```

## 4.2 Redis单线程

​	Redis是基于内存操作，它的瓶颈是：机器的内容和网络带宽。

> Redis为什么单线程害这么快？

（1）误区1：高性能的服务器一定是多线程吗？

（2）误区2：多线程（CPU上下文会切换）一定比单线程快？

## 4.3 常用命令

> 常用命令：官网

<img src="../images/常用命令1.png" style="zoom:80%;" />

---

```cmd
127.0.0.1:6379> flushall # 清空
```

```cmd
127.0.0.1:6379> expire name 10  # 设置key的过期时间，单位：秒
127.0.0.1:6379>ttl name # 查看距离删除name的倒计时时间
5
```

```cmd
127.0.0.1:6379>type name  # 查看name的类型
string
```

# 5 五大基本数据类型

## 5.1 Redis-key



## 5.2 String

> **常用命令**

---

> **（1）字符串拼接与获取字长**

```cmd
127.0.0.1:6379>set key1 v1 # 设置一个key1，内容：v1
OK
127.0.0.1:6379>get key1
"v1"
```

```bash
127.0.0.1:6379>keys *
1) "key1"
# 注意：命名是否区分大小写
127.0.0.1:6379>EXISTS key1  # 判定key1是否存在，返回值：1 或 0（可测）
(integer) 1 
127.0.0.1:6379>APPEND key1 "hello"  # 在key1内容后面拼接"hello"
(integer) 7
127.0.0.1:6379>get key1
"v1hello"
```

```bash
127.0.0.1:6379>STRLEN key1   # 查看key1的内容长度
(integer) 7
```

> **（2）步长：i++**
>
> **注意：**String：除了是字符串，也可以是：<button>数字</button>

```bash
127.0.0.1:6379>set view 0   # 初始浏览量：0
127.0.0.1:6379>get views
"0"
```

```cmd
127.0.0.1:6379>incr views  # 自增
(integer) 1
127.0.0.1:6379>incr views
(integer) 2
127.0.0.1:6379>decr views  # 自减
(integer) 1
```

```cmd
127.0.0.1:6379>INCRBY views 10  # 设置自增步长
127.0.0.1:6379>DECRBY views 5 	# 设置递减步长
```

> **（3）字符串范围**

```cmd
127.0.0.1:6379>set key1 "hello world"
127.0.0.1:6379>GETRANGE key1 0 3  # 截取字符串[0,3]
"hell"
127.0.0.1:6379>GETRANGE key1 0 -1  # 截取全部的字符串
```

> **（4）替换**

```cmd
127.0.0.1:6379>set key2 "abcdefg"
127.0.0.1:6379>SETRANGE key2 1 xx  # 从1开始，往后替换
127.0.0.1:6379>get key2
"axxdefg"
```

> **（5）设置过期时间和判断是否存在命令**

```cmd
# setex（set with expire）：设置过期时间，单位：秒
# setnx（set if not exist）：若值不存在，则创建
```

```cmd
127.0.0.1:6379>setex key3 30 "hello"  # 30秒后过期
127.0.0.1:6379>ttl key3
26
```

```cmd
127.0.0.1:6379>setnx mykey "redis"  # 判断mykey是否存在，不存在，则创建
(integer) 1
```

> **（6）设置和获取多个值**

```cmd
127.0.0.1:6379>mset k1 v1 k2 v2 k3 v3 # 同时设置多个值
ok
127.0.0.1:6379>mget k1 k2 k3  # 同时获取多个值
127.0.0.1:6379>msetnx k1 v1 k4 v4 # 原子性操作，要么都成功，要么都失败
```

> **（7）对象**

```cmd
set user:1 
	name: zhangsan
	age:2
}
# 通过key，可以实现一个对象
127.0.0.1:6379>mset user:1:name zhangsan user:1:age 2
127.0.0.1:6379>mget user:1:name user:1:age
1)"zhangsan"
2)"2"
```

> **（8）getset  ： 先get，然后再set**

```cmd
127.0.0.1:6379>getset db redis # 先获取当前数据库，再设置数据库为redis
nil  # 不存在，则返回空
127.0.0.1:6379>getset db mongodb
"redis"
127.0.0.1:6379>get db
"mongodb"
```

## 5.3 List

> redis中，List可以实现栈、队列、阻塞队列

---

> （1）**LPUSH：**将一个或多个值插入到列表的<button>头部（左）</button>

```cmd
127.0.0.1:6379>LPUSH list one
127.0.0.1:6379>LPUSH list two
127.0.0.1:6379>LPUSH list three
127.0.0.1:6379>LRANGE list 0 -1  # 查询列表全部元素
1) "three"
2) "two"
3) "one"
```

> **（2）RPUSH**

```cmd
127.0.0.1:6379>RPUSH list rightr  # 插入元素（右部）
127.0.0.1:6379>LRANGE list 0 -1  # 查询列表全部元素
1) "three"
2) "two"
3) "one"
4) "rightr"  # 新插入的元素，在尾部
```

> **（3）lindex**：区间获取具体的值

```cmd
# 127.0.0.1:6379>LRANGE list[0] 此方法可以测试是否可以获取值
127.0.0.1:6379>lindex list 1   # 获取列表下标为1的元素值
1) "two"
127.0.0.1:6379>lindex list 0
1) "three"
```

> **（4）LPOP**：移除（左边出），并返回移除的值

```cmd
127.0.0.1:6379>LPOP list
"three"
```

> **（5）RPOP**：移除（右边出），并返回移除的值

```cmd
127.0.0.1:6379>RPOP list
"righr"
```

> **（6）lrem：**移除指定个数的值（<button>从后向前</button>移除）

```cmd
127.0.0.1:6379>LRANGE list 0 -1
1) "three"
2) "three"
3) "two"
4) "one"
127.0.0.1:6379>lrem list 1 one  # 移除一个
127.0.0.1:6379>LRANGE list 0 -1
1) "three"
2) "three"
3) "two"
```

> **（7）Llen：**获取list大小

```cmd
127.0.0.1:6379>Llen list #  返回列表的长度
```

> **（8）trim：**修剪，list：截取

```cmd
127.0.0.1:6379>LRANGE mylist 0 -1
1) "three"
2) "three"
3) "two"
4) "one"
# 截取下标1到下标2之间的元素（包含本身），截取后，list本身也改变了
127.0.0.1:6379>ltrim mylist 1 2  
127.0.0.1:6379>LRANGE mylist 0 -1
1) "three"
2) "two"
```

> **（9）rpoplpush：**移除列表最后一个元素，并将它移动到新的列表中

```cmd
127.0.0.1:6379>LRANGE mylist 0 -1
1) "three"
2) "three"
3) "two"
# 移除mylist列表最后一个元素，并将它转移到myotherlist列表中
127.0.0.1:6379>rpoplpush mylist myotherlist
127.0.0.1:6379>LRANGE mylist 0 -1
1) "three"
2) "three"
127.0.0.1:6379>LRANGE myotherlist 0 -1
1) "two"
```

> **（10）let：**将列表中指定下标的值<button>替换为另外一个值</button>，更新操作

```cmd
127.0.0.1:6379>LRANGE mylist 0 -1
1) "three"
127.0.0.1:6379>lset mylist 1 item  # 若不存在，则报错
(error) ERR no such  key
127.0.0.1:6379>lset mylist 0 item  # 下标为0，替换为：item
127.0.0.1:6379>lrange mylist 0 -1
1) "item"
```

> **（11）linsert：**将具体的某个值插入到列表中（before、after）

```cmd
127.0.0.1:6379>LRANGE mylist 0 -1
1) "three"
2) "two"
127.0.0.1:6379>linsert mylist before "two" "one"  # 在two前插入
127.0.0.1:6379>lrange mylist 0 -1
1) "three"
2) "one"
2) "two"
127.0.0.1:6379>linsert mylist after "three" "new"  # 在three后插入
127.0.0.1:6379>lrange mylist 0 -1
1) "three"
2) "new"
3) "one"
4) "two"
```

> <button>总结</button>

- 实际是一个链表，before Node after，left、right都可以插入值
- 若key不存在，则创建新的链表
- 若key存在，则新增内容
- 若移除了所有值，空链表，也代表不存在
- 在两边插入或者改变值，效率最高！中间元素，相对来说效率会低

<button>消息队列（Lpush Rpop）</button>    <button>栈（Lpush  Lpop）</button>

## 5.4 Set

> `set`中的值是不可重读的，且无序

---

> （1）sadd：添加元素

```cmd
127.0.0.1:6379>sadd myset "hello"
127.0.0.1:6379>sadd myset "test"
127.0.0.1:6379>sadd myset "lovekuangshen"
```

> （2）smembers：查看set集合所有值

```cmd
127.0.0.1:6379>smembers myset
1) "hello"
2) "test"
3) "lovekuangshen"
```

> （3）sismemeber ：判断某值是否为存在指定set集合中，存在返回：1

```cmd
127.0.0.1:6379>sismember myset hello
(integer) 1
```

> （4）scard：获取set集合中的内容元素个数

```cmd
127.0.0.1:6379>scard myset
(integer) 4
```

> （5）srem：移除set中指定元素

```cmd
127.0.0.1:6379>srem myset hello # 移除set集合中指定元素
```

> （6）srandmember：随机抽选一个元素，可指定抽选个数

```cmd
127.0.0.1:6379>smembers myset
1) "hello"
2) "test"
3) "lovekuangshen"
127.0.0.1:6379>srandmember myset # 随机抽选（默认一个）
1) "test"
127.0.0.1:6379>srandmember myset 2 # 随机抽选2个
1) "test"
2) "lovekuangshen"
```

> （7）spop：随机删除set集合中的一个元素

```cmd
127.0.0.1:6379>smembers myset
1) "hello"
2) "test"
3) "lovekuangshen"
127.0.0.1:6379>spop myset  # 随机删除一个
1) "test"
127.0.0.1:6379>smembers myset
1) "hello"
2) "lovekuangshen"
```

> （8）smove：将一个指定的值，移动到另外一个set中

```cmd
127.0.0.1:6379>smembers myset
1) "hello"
2) "test"
3) "lovekuangshen"
127.0.0.1:6379>smove myset myset2 "hello" # 将set的hello移到set2中
127.0.0.1:6379>smembers myset
1) "test"
2) "lovekuangshen"
127.0.0.1:6379>smembers myset2
1) "hello"
```

> （9）数字集合类：差集、交集、并集

```cmd
127.0.0.1:6379>smembers key1
1) "a"
2) "b"
3) "c"
127.0.0.1:6379>smembers key2
1) "c"
2) "d"
3) "e"
```

> 差集

```cmd
127.0.0.1:6379>sdiff key1 key2
1) "a"
2) "b"
```

> 交集，适用场景：共同好友

```cmd
127.0.0.1:6379>sinter key1 key2
1) "c"
```

> 并集

```cmd
127.0.0.1:6379>sunion key1 key2
1) "b"
2) "c"
3) "e"
4) "a"
5) "d"
```

## 5.5 Hash

Map集合，key-value！是一个map集合

Hash适用对象的存在

> （1）赋值

```cmd
# set一个具体的key-value
127.0.0.1:6379>hset myhash field1 kuangshen
# 通过key获取value
127.0.0.1:6379>hget myhash field1
"kuangshen"

# set多个具体的key-value
127.0.0.1:6379>hmset myhash field1 hello field world
ok
# 获取多个字段值
127.0.0.1:6379>hmget myhash field1 field2
1) "hello"
2) "world"

# 获取myhash全部的值
127.0.0.1:6379>hgetall myhash
1) "field1"
2) "hello"
3) "field2"
4) "world"
```

> （2）删除

```cmd
# 删除hash指定key字段
127.0.0.1:6379>hdel myhash field1 
(integer) 1
127.0.0.1:6379>hgetall myhash
1) "filed1"
2) "world"
```

> （3）hlen：获取hash表的长度，返回类型：integer

```cmd
127.0.0.1:6379>hmset myhash field1 hello field world
ok
127.0.0.1:6379>hlen myhash
(integer) 2
```

> （4）hexists：判断hash中指定字段是否存在

```cmd
127.0.0.1:6379>hexists myhash field1
(integer) 1
127.0.0.1:6379>hexists myhash field2
(integer) 0
```

> （5）获取所有key 或 value

```cmd
127.0.0.1:6379>hkeys myhash  # 获取hash所有key
1) "field1"
2) "field2"
127.0.0.1:6379>hvals myhash  # 获取hash所有value
1) "world"
2) "hello"
```

> （6）hincrby

```cmd
127.0.0.1:6379>hset myhash field 5
127.0.0.1:6379>hincrby myhash field 1
(integer) 6  # 自增1
127.0.0.1:6379>hincrby myhash field -1
(integer) 5  # 自减1
```

> （7）hsetnx：key存在，则不能更新value

```cmd
127.0.0.1:6379>hsetnx myhash field hello  # field 不存在
(integer) 1
127.0.0.1:6379>hsetnx myhash field world  # field 存在，设置失败
(integer) 0
```

## 5.6 Zset

有序集合

> （1）添加
>

```cmd
127.0.0.1:6379>zadd myset 1 one 	# 设置一个值
127.0.0.1:6379> zadd myset 2 two 3 three	# 设置多个值
127.0.0.1:6379>zrange myset 0 -1	# 0 -1 ：获取所有值
1) "one"
2) "two"
3) "three"
```

> （2）zrangebyscore：排序

```cmd
# 添加三个用户
127.0.0.1:6379>zadd salary 2500	xiaohong
127.0.0.1:6379>zadd salary 5000	xiaohua
127.0.0.1:6379>zadd salary 500	kaixin

# -inf : 负无穷
# +inf : 正无穷
# 按salary升序排序
127.0.0.1:6379>zrangebyscore salary -inf +inf 
1) "kaixin"
2) "xiaohua"
3) "zhangsan"

# 显示全部用户，升序，显示对应数值
127.0.0.1:6379>zrangebyscore salary -inf +inf withscores
1) "kaixin"
2) "500"
3) "xiaohua"
4) "2500"
5) "zhangsan"
6) "5000"

# 显示小于等于2500用户，升序，显示对应数值
127.0.0.1:6379>zrangebyscore salary -inf 2500 withscores
1) "kaixin"
2) "500"
3) "xiaohua"
4) "2500"
```

> （3）移除rem中的元素

```cmd
127.0.0.1:6379>zrange myset 0 -1
1) "kaixin"
2) "xiaohua"
3) "zhangsan"
# 移除key为xiaohua的元素，返回类型：integer：1 or 0
127.0.0.1:6379>zrem myset xiaohua 
127.0.0.1:6379>zrange myset 0 -1
1) "kaixin"
2) "zhangsan"

127.0.0.1:6379>zcard myset # 获取有序集合中的个数
(integer) 2
```





Nosql四大分类

CAP

BASE



三种特殊数据类型

geo

hyperloglog

bitmap

Redis配置详情

Redis持久化

RDB

AOF

Redis事务操作

Redis实现订阅发布

Redis主从复制

Redis哨兵模式

缓存穿透及解决方案

缓存击穿及解决方案

缓存雪崩及解决方案

基础API之Jedis详解操作

SpringBoot集成Redis操作

Redis的实践分析





 