---
typora-root-url: images
---

# 常用Utils

> 所有的Utils，位于：`myutils/配置名.java`

# 一 DateUtils



# 二 JsonUtils



# 三 HttpUtils封装

## 1 导入依赖文件

```xml
<!--httpclient-->
<dependency>
    <groupId>org.apache.httpcomponents</groupId>
    <artifactId>httpclient</artifactId>
    <version>4.5.12</version>
</dependency>
```

## 2 HttpUtils.java

```java
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.util.Map;

/**
 * 封装httpClient
 */
public class HttpUtils {

    /**
     * get请求
     * @param url 请求地址
     */
    public static String get(String url,Map<String,String> params){
        // 参数拼接
        if (params != null && params.size() > 0){
            int flag = 0;
            for (String key : params.keySet()){
                // 第一次拼接参数，使用：？，后续若还有参数，使用：&
                if (flag == 0){
                    url += "?" + key + "=" + params.get(key);
                    flag = 1;
                }else{
                    url += "&" + key + "=" + params.get(key);
                }
            }
        }
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse response = null;
        try {
            // 执行请求
            response = httpclient.execute(httpGet);
            return EntityUtils.toString(response.getEntity());
        }catch (Exception e){
            throw new RuntimeException("get请求失败");
        } finally {
            try {
                if (response != null) {
                    response.close();
                }
                //相当于关闭浏览器
                httpclient.close();
            }catch (Exception e){

            }
        }
    }

    /**
     * post 请求
     */
    public static String post(String url,Map<String,String> params) throws Exception{
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);
        httpPost.addHeader("Content-type","application/json; charset=utf-8");
        httpPost.setHeader("Accept", "application/json");
        if (params != null && params.size() > 0){
            StringEntity stringEntity = new StringEntity(JsonUtils.objectToJoin(params),"utf-8");
            httpPost.setEntity(stringEntity);
        }
        CloseableHttpResponse response = null;
        try {
            // 执行请求
            response = httpclient.execute(httpPost);
            return EntityUtils.toString(response.getEntity(), "UTF-8");
        } finally {
            if (response != null) {
                response.close();
            }
            httpclient.close();
        }
    }
```

## 3 测试

**第一步：**检查是否可以解析url地址，获取json数据

```java
@RequestMapping("/test")
@ResponseBody
public String fun(){
    String url = "https://img-home.csdnimg.cn/data_json/toolbar/toolbar0513.json";
    Map<String,String> params = new HashMap<String,String>();
    return HttpUtils.get(url, params);
}
```

<img src="/../images/httputils.png" style="zoom:50%;" />

---

**第二步：**获取json数据中某个：JsonNode结点的数据

<img src="/../images/JsonNode.png" style="zoom:67%;" />

```java
@RequestMapping("/test")
@ResponseBody
public String fun(){
    String url = "https://img-home.csdnimg.cn/data_json/toolbar/toolbar0513.json";
    Map<String,String> params = new HashMap<String,String>();
    // 解析url地址，获取json数据
    String json = HttpUtils.get(url,params);
    // 解析json数据，获取内部结点
    JsonNode jsonNode = JsonUtils.stringToJsonNode(json);
    // 获取jsonnode为：background 的数据
    String  background= jsonNode.get("background").toString();
    return background;
}
```

> **结果：返回的依然是一个：json数据**
>
> {"default":"","dark":"https://g.csdnimg.cn/common/csdn-toolbar/images/toolbar_bg_dark.png","home":""}

----

**第三步：**再次解析Json数据，获得里面单个元素的值

```java
@RequestMapping("/test")
@ResponseBody
public String fun(){
    String url = "https://img-home.csdnimg.cn/data_json/toolbar/toolbar0513.json";
    Map<String,String> params = new HashMap<String,String>();
    // 解析url地址，获取json数据
    String json = HttpUtils.get(url,params);
    // 解析json数据，获取内部结点
    JsonNode jsonNode = JsonUtils.stringToJsonNode(json);
    // 获取jsonnode为：tbackground的数据
    String  background= jsonNode.get("background").toString();
    // 再次解析
    JsonNode jsonNode1 = JsonUtils.stringToJsonNode(background);
    // 去除双引号
    String dark = jsonNode1.get("dark").toString().replace("\"","");
    return dark;
}
```

> **结果：**https://g.csdnimg.cn/common/csdn-toolbar/images/toolbar_bg_dark.png

# 四 ExcelUtils封装

> 学习视频：https://www.bilibili.com/video/BV1kf4y1i761?p=5&spm_id_from=pageDriver

## 1 导入依赖文件

```xml
<!--Excel-->
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi-ooxml-schemas</artifactId>
    <version>4.0.0</version>
</dependency>
<!-- https://mvnrepository.com/artifact/org.apache.poi/poi-ooxml -->
<dependency>
    <groupId>org.apache.poi</groupId>
    <artifactId>poi-ooxml</artifactId>
    <version>4.0.0</version>
</dependency>
```

## 2 ExcelUtils.java

```java
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Excel导入导出
 */
public class ExcelUtils {
    /**
     * Excel 导出
     * @param response  HttpServletResponse
     * @param header 表头
     * @param keys map的key值
     * @param content 内容数据
     * @param title 表格名字
     * @param sheetName sheet名
     */
    public static void exportExcel(HttpServletResponse response, String[] header,
                                   String[] keys, List<Map<String, Object>> content,
                                   String title, String sheetName) throws Exception{
        title = title + ".xlsx";
        Workbook wb = new SXSSFWorkbook(1000);
        Sheet sheet = wb.createSheet(sheetName);
        Row row = sheet.createRow( 0);
        // 行高
        row.setHeight((short) 700);
        // 列宽
        for (int i = 0; i < header.length; i++) {
            sheet.setColumnWidth(i, 20 * 256);
        }
        for (int i = 0; i < header.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(header[i]);
            cell.setCellStyle(HeaderStyle(wb));
        }
        for (int i = 0; i < content.size(); i++) {
            Map<String, Object> map = content.get(i);
            row = sheet.createRow((int) i + 1);
            row.setHeight((short) 500);
            for (int j = 0; j < keys.length; j++){
                Cell cell = row.createCell(j);
                cell.setCellValue(map.get(keys[j]) == null ? "" : map.get(keys[j]).toString());
                cell.setCellStyle(contentStyle(wb));
            }
        }
//        if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
//            title = new String(title.getBytes("UTF-8"), "ISO8859-1"); // firefox浏览器
//        } else if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
//            title = URLEncoder.encode(title, "UTF-8");// IE浏览器
//        } else if (request.getHeader("User-Agent").toUpperCase().indexOf("CHROME") > 0) {
//            title = new String(title.getBytes("UTF-8"), "ISO8859-1");// 谷歌
//        }
        title = new String(title.getBytes("UTF-8"), "ISO8859-1");
        response.reset();
        response.setContentType("application/octet-stream; charset=utf-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Content-Disposition", "attachment; filename=" + title);
        wb.write(response.getOutputStream());
        response.getOutputStream().close();
    }

    /**
     * Excel 导入
     * @param file 文件
     * @param keys 数据顺序
     */
    public static  List<Map<String, Object>>  importExcel(MultipartFile file, String[] keys) throws Exception{
        Workbook wb = null;
        String fileName = file.getOriginalFilename();
        if (fileName.endsWith("xls")) {
            POIFSFileSystem pois = new POIFSFileSystem(file.getInputStream());
            wb = new HSSFWorkbook(pois);
        } else if (fileName.endsWith("xlsx")) {
            wb = new XSSFWorkbook(file.getInputStream());
        }
        Sheet sheet = wb.getSheetAt(0);
        int rowCount = sheet.getPhysicalNumberOfRows();
        if (sheet.getRow( 1).getPhysicalNumberOfCells() != keys.length){
            throw new RuntimeException("导入的Excel和模板的列不匹配");
        }
        List<Map<String,Object>> result = new ArrayList<>();
        for (int i = 0; i < rowCount - 1; i++) {
            Row row = sheet.getRow(i + 1);
            Map<String,Object> tmp = new HashMap<>();
            for (int j = 0;j < keys.length; j++){
                Cell cell = row.getCell(j);
                // 把类型转行Spring
//                cell.setCellType(CellType.STRING);
                tmp.put(keys[j], cell.getStringCellValue());
            }
            result.add(tmp);
        }
        return result;
    }

    /**
     * 表头样式
     */
    private static CellStyle HeaderStyle(Workbook wb){
        Font font = wb.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short) 11);
        CellStyle cellStyle = commonStyle(wb);
        cellStyle.setFont(font);
        return cellStyle;
    }

    /**
     * 内容样式
     */
    private static CellStyle contentStyle(Workbook wb){
        Font font = wb.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short) 10);
        CellStyle cellStyle = commonStyle(wb);
        cellStyle.setFont(font);
        return cellStyle;
    }

    /**
     * 公共样式
     */
    private static CellStyle commonStyle(Workbook wb){
        CellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);
        style.setVerticalAlignment(VerticalAlignment.CENTER);
        style.setBorderBottom(BorderStyle.THIN);
        style.setBorderLeft(BorderStyle.THIN);
        style.setBorderTop(BorderStyle.THIN);
        style.setBorderRight(BorderStyle.THIN);
        style.setWrapText(true);// 自动换行
        return style;
    }
}
```

## 3 导出Excel表

```java
@RequestMapping("/excel")
@ResponseBody
public String Excel(HttpServletResponse response){
    // 表头
    String[] header = new String[]{"姓名", "年龄"};
    // key
    String[] key = new String[]{"name", "age"};
    // 内容
    List<Map<String, Object>> content = new ArrayList<>();
    Map<String, Object> map1 = new HashMap<>();
    map1.put("name", "小蜜");
    map1.put("age", 23);
    content.add(map1);
    Map<String, Object> map2 = new HashMap<>();
    map2.put("name", "小明");
    map2.put("age", 24);
    content.add(map2);

    try{
        ExcelUtils.exportExcel(response, header,key,content,"文件表","sheet1");
    }catch(Exception e){
        e.printStackTrace();
    }
    return null;
}
```

> 访问：http://localhost:8080/test

<img src="/../images/导出excel.png" style="zoom:50%;" />

## 4 导入Excel表

> post请求，需要传递参数

```Java
@RequestMapping("/importE")
public void IExcel(@RequestBody MultipartFile file)throws Exception{
    String[] keys = new String[]{"name", "age"};
    List<Map<String, Object>> list = ExcelUtils.importExcel(file, keys);
    for(Map<String, Object> map : list){
        for(String key : map.keySet()){
            System.out.print(map.get(key) +  " ");
        }
        System.out.println();
    }
}
```

> 运行：报错，null异常，测试可参考视频里面的方法

# 五 



# 六 搭建文件服务器