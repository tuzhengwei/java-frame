# JAVA字符串常见操作

## 1 StringBuffer与String的相互转换

​	方法：toString

```java
StringBuffer stringBuffer = new StringBuffer(“Hello World.”);
// 调用toString函数将StringBuffer对象转换成String对象
String c = stringBuffer.toString();
```

## 2 String与字符数组的相互转换

​	方法：toCharArray

```java
String c = “Hello World.”;// 创建一个String对象
// 再调用String对象的toCharArray函数转换成字
char[] cd = c.toCharArray();
```

## 3 char数组转换成String

```java
// 方法1：
char[] data = {'a', 'b', 'c'};
String str = new String(data);
// 方法2：调用String类的valueOf函数转换。
String.valueOf(char[] ch);
```

## 4 数字与字符串之间的相互转换



## 5 Java浮点数如何保留小数

​	方法：format

```java
import java.util.Scanner;
public class practice
{
    public static void main(String[] args)
    {
        //write your own codes
        Scanner input = new Scanner(System.in);
        float f = input.nextFloat();
        float c =5*(f-50)/9+10;
        System.out.println("The temperature is " + (String.format("%.2f",c)));
    }
}
```

# IO流复制上传文件

> 文件：记录或存放在一起的数据集合。

## 1 流的类型

**字节流：**

​	由8位的通用字节流，以：字节为基本单位，在：`java.io`包中，对于字节流进行操作的类大部分继承于：`InputStream`（输入字节流）和`OutputStream`（输出字节流）。

---

**字符流：**

​	由16位的`Unicode`字符流，以字符（两个字节）为基本单位，**非常适合处理：字符串和文本**，对于字符流进行操作的类大部分继承：`Reader`（读取流）和`Writer`（写入流）。

## 2 FileOutPutStream

​	**5种重载方式**

- FileOutputStream(**File file**) throws FileNotFoundException

  ​	使用File对象创建文件输出流对象，如果文件打开失败，将抛出异常。

- FileOutputStream(**File file, boolean append**) throws FileNotFoundException

  ​	使用File对象创建文件输出流对象，并由参数append指定是否追加文件内容，**true为追加，false为不追加**，异常情况同上。

- FileOutputStream(**String name**) throws FileNotFoundException

  ​	直接使用**文件名或路径**创建文件输出流对象，异常情况同上。

- FileOutputStream(String name, boolean append) throws FileNotFoundException

  ​	直接使用**文件名或路径**创建文件输出流对象，并由参数append指定是否追加

----

## 3 示例

**示例一**

```java
public class FileOutputStreamDemo1{
    // 在函数内部不进行异常处理，将异常抛出函数外部
    public static void main(String[] args) throws IOException{
        String str = "hello world!";
        // 创建文件对象
        File file = new File("text.txt");
        // 通过文件对象创建文件输出流对象
        // 附加第二个参数：true，追加，默认：不追加
        FileOutputStream fos = new FileOutputStream(file,true);
        // 逐个将字符写入到文件中
        for(int i = 0; i < str.length(); i++ ){
            fos.write(str.charAt(i));
        }
        fos.close();
    }
}
```

**示例二**

```java
public class FileOutputStreamDemo1{
    // 在函数内部不进行异常处理，将异常抛出函数外部
    public static void main(String[] args) throws IOException{
        String str = "hello world!";
        // 通过文件对象创建文件输出流对象
        // 附加第二个参数：true，追加，默认：不追加
        FileOutputStream fos = new FileOutputStream("text.txt");
        // 将字符串转换为：字节数组
        byte[] buffer = str.getBytes();
        // 将字节数组中包含的数据一次性写入到文件中
        fos.write(buffer);
        fos.close();
    }
}
```













# 序列化实体对象