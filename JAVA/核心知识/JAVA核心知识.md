---

typora-root-url: images
---

# JAVA核心知识

# 一、基础篇

## 1、Java父类强制转换子类原则

```java
public class Main {
	public static void main(String[] args) {
//		test1();
		test2();
	}
	
	private static void test1() {
		Fruit fruit = new Fruit();
		Apple apple = new Apple();
		apple = (Apple)fruit;   // 父类强制转换为：子类，报：java.lang.ClassCastException
	}
	
	private static void test2() {
		Fruit fruit = new Apple();  //父类是：子类构造出来的实例，Apple：子类
		Apple apple = new Apple();
		apple = (Apple)fruit;   // 父类强制转换为：子类，报：
	}
	
	static class Fruit{
		
	}
	static class Apple extends Fruit{
		
	}
}
```

---

结果是：

```
test1 ：报类转异常
test2 ：转换正常
```

## 2、switch case 支持的 6 种数据类型

```java
switch(expression){
    case value :
     		//语句      
			break;  
	default	:
...
```

---

**这里的 `expression` 都支持哪些类型呢？**

- 基本数据类型：byte, short, char, int
- 包装数据类型：Byte, Short, Character, Integer
- 枚举类型：Enum
- 字符串类型：String（**Jdk 7**+ 开始支持）

## 3、switch多值匹配骚操作

多个 case 匹配同一段逻辑

```java
private static void test(int value) {
	switch(value) {
		case 1:case 2:case 3:case 4:case 5:
			System.out.println("1");
			break;
	}
}
```
---

**格式化后**：

```java
private static void test(int value) {
	switch(value) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
			System.out.println("1");
			break;
	}
}
```
---

​	提示：JDK 12 后，switch出现了新的写法。

## 4、Java异常机制

​	下面是Java异常类的组织结构，红色区域的异常类表示是：程序需要**显示捕捉或者抛出的**。

![](/../images/异常.png)

### （1）Throwable

​		Throwable是Java**异常的顶级类**，所有的异常都继承于这个类。Error，Exception是异常类的两个大分类。

### （2）Error

​		Error是**非程序异常**，即程序**不能捕获的异常**，一般是编译或者系统性的错误，如OutOfMemorry**内存溢出**异常等。

### （3）Excpetion

​		Exception是程序异常类，由**程序内部产生**。Exception又分为：运行时异常、非运行时异常。

---

​		①	**运行时异常**

​				运行时异常的特点是：Java**编译器不会检查它**，也就是说，当程序中可能出现这类异常，即使没有用try-catch语句捕获它，也没有用throws子句声明抛出它，也会编译通过，运行时异常**可处理或者不处理**。运行时异常一般常出来定义系统的自定义异常，业务根据自定义异常做出不同的处理。

​				常见的运行时异常如：**NullPointException**、ArrayIndexOutOfBoundsException等。

​		②	**非运行时异常**

​				非运行时异常是程序**必须进行处理的异常**，捕获或者抛出，如果**不处理程序就不能编译通过**。如常见的IOException、ClassNotFoundException等。

## 5、通用唯一标识码UUID

​		UUID全称：Universally Unique Identifier，即：**通用唯一识别码**。

​		UUID是由一组**32位数的16进制数字**所构成，是故UUID理论上的总数为：16^32 = **2^128**，约等于3.4 x 10^38。也就是说若每纳秒产生1兆个UUID，要**花100亿年**才会将所有UUID用完。

---

​		UUID的是让**分布式系统中的所有元素**都能有**唯一的辨识信息**，而不需要通过中央控制端来做辨识信息的指定。如此一来，每个人都可以创建不与其它人冲突的UUID。在这样的情况下，就不需考虑数据库创建时的名称重复问题。

---

**UUID由以下几部分的组合：**

- 当前日期和时间，UUID的第一个部分与时间有关，如果你在生成一个UUID之后，过几秒又生成一个UUID，则第一个部分不同，其余相同。
- 时钟序列。
- 全局唯一的IEEE机器识别号，如果有网卡，从网卡MAC地址获得，没有网卡以其他方式获得。

UUID的唯一缺陷：在于生成的**结果串会比较长**。

---

```java
// UUID生成
public static void main(String[] args) {
	System.out.println(UUID.randomUUID());
}
```

## 6、字符串拼接+和concat的区别

```java
public static void main(String[] args) {
	String str1 = "s1";
	System.out.println(str1 + 100);
	System.out.println(str1 + null);
	
	String str2 = "s2";
	str2 = str2.concat("a").concat("bc");
	System.out.println(str2); // s2abc
	System.out.println(str2.concat(null)); //java.lang.NullPointerException

}
```
---

总结：

- `+` ：可以是：字符串或者数字及**其他基本类型数据**，而`concat` **只能接收字符串**。
- `+` ：左右可以为null，`concat` 为会空指针。
- 如果**拼接空字符串**，`concat` 会稍快，在速度上两者可以忽略不计，如果拼接更多字符串建议用StringBuilder。
- 从字节码来看+号编译后就是使用了StringBuiler来拼接，所以**一行+++的语句**就会创建一个StringBuilder，**多条+++语句**就会创建多个，所以为什么建议用StringBuilder的原因。

```java
String str1 = "1"+"2"+"3"; // 内部创建一个StringBuilder
```

## 7、try/finally

网站：https://mp.weixin.qq.com/s?__biz=MzI3ODcxMzQzMw==&mid=2247483767&idx=1&sn=54e60e6d11002deb22db7d7cc7adfac5&scene=21#wechat_redirect

---

总结：

（1）不管try，finally**都会执行**；

（2）在try中return，如果是基本类型，在finally执行前会**把结果保存起来**，即使在finally中有修改也**以try中保存的值**为准，但如果是**引用类型**，修改的属性会**以finally修改后的为准**；

（3）如果try/finally**都有return**，**直接返回finally中的**return。

## 8、Java序列化

​		Java序列化技术正是：**将对象转变成**一串由**二进制字节**组成的**数组**。可以通过将二进制数据**保存到磁盘或者传输网络，**磁盘或者网络**接收者**可以在对象的属类的模板上来**反序列化类的对象**，达到**对象持久化**的目的。

---

​	被`transient` 关键字修饰的变量不再被序列化，静态变量不管是否被：`transient`修饰，**都不能序列化**。

### （1）怎么序列化一个对象？

​	要序列化一个对象，这个对象所在类就必须实现Java序列化的接口：java.io.Serializable

```java
import java.io.Serializable;

public class User implements Serializable{
    ...
}
```

### （2）序列化/反序列化

​	可以**借助**`commons-lang3`（ [ˈkɒmənz]）工具包里面的类实现对象的序列化及反序列化，

```java
import org.apache.commons.lang3.SerializationUtils;

public static void main(String[]args){
    User user = new User();
    user.setUsername("java");
    user.setAddress("china");
    // 序列化
    byte[] bytes = SerializationUtils.serialize(user);
    // 反序列化
    User u = SerializationUtils.deserialize(bytes);
    System.out.println(u);
}
```

### （3）序列化注意事项

- 序列化对象**必须实现序列化接口**。

- 序列化对象里面的**属性是对象**的话**也要实现序列化接口**。

- 类的对象**序列化后**，类的**序列化ID不能轻易修改**，**不然反序列化会失败**。

- 类的对象**序列化后**，类的属性**有增加或者删除不会影响**序列化，只是**值会丢失**。

- 如果**父类序列化了**，子类会**继承父类的序列化**，子类**无需添加**序列化接口。

- 如果**父类没有**序列化，**子类序列化**了，子类中的属性能正常序列化，但**父类的属性会丢失**，**不能**序列化。

- 用Java序列化的二进制字节数据**只能由Java反序列化**，**不能被其他语言**反序列化。如果要进行前后端或者不同语言之间的交互一般需要将对象转变成Json/Xml通用格式的数据，再恢复原来的对象。

  

- 如果某个字段**不想序列化**，在该字段前**加上transient（ [ˈtrænziənt]）关键字**即可。

## 9、为什么byte取值-128~127

​		java设计byte类型为1个字节，**1个字节占8位**，即8bit，这是常识。

​		计算机系统中是用补码来存储的，首位为0表示正数，首位为1表示负数，所以有以下结论：

​			（1）最大的补码用二进制表示为：**01111111** = 127

​			（2）最小的补码用二进制表示为：**10000000** = -128

## 10、3种常见的Class级别的错误

### （1）ClassNotFoundException

​		很明显，这个错误是 `找不到类异常`，即在当前classpath路径下找不到这个类。

### （2）NoClassDefFoundError

​		这是**虚拟机隐式加载类**出现的异常。

​		这个异常继承了Error类，一般发生在**引用的类不存在**，即类、方法或者属性引用了某个类或者接口，如果目标引用不存在就会抛出这个异常。

### （3）ClassCastException

​		**类转换异常**，这个错误一般发生在一个对象强制转换类型的时候，如：将一个String强制转换成Integer就会报这个错。

## 11、void和Void的区别

```java
private void test1(){
    
}
private Void test2(){
    return null;
}
```

---

void

​	用于**无返回值**的方法定义。

Void

​	Void是void的包装方法，和其他基础类型的包装方法不同是Void不能被实例化，Void还可用于一直**返回null**的方法或者返回null的泛型。

## 12、管理Cookie增删改查操作

### （1）属性介绍

| Cookie属性 | 说明                                                         |
| :--------: | ------------------------------------------------------------ |
|    name    | Cookie名称                                                   |
|   value    | Cookie的值                                                   |
|   maxAge   | Cookie的失效时间                                             |
|    path    | Cookie的有效路径， `/`：表示这个路径即该工程下都可以访问该cookie 如果不设置路径，那么只有设置该cookie路径及其子路径可以访问。 |

| maxAge值 | 说明                     |
| :------: | ------------------------ |
|   负数   | 浏览器关闭后cookie就失效 |
|    0     | 马上清理cookie           |
|   正数   | 设置过期时间，单位：/秒  |

### （2）获取所有Cookie

```java
public static Cookie[] getCookies(HttpServletRequest request){
	return request.getCookies();
}
```

​	获取cookie很简单**，直接从request中**获取即可。

### （3）获取指定Cookie

```java
public static Cookie getCookieByName(HttpServletRequest request,String name){
	if(StringUtils.isBlank(name)){
		return null;
	}
	Cookie[] cookies = getCookies(request);
	if(null!=cookies){
		for(Cookie cookie :cookies){
			if(name.equals(cookie.getName())){
				return cookie;
			}
		}
	}
	return null;
}
```

### （4）添加Cookie

```java
public static boolean addCookie(HttpServletResponse response,String name,String value,int maxAge){
	if(StringUtils.isBlank(name)||StringUtils.isBlank(value)){
		return false;
	}
	Cookie cookie = new Cookie(name.trim(),value.trim());
    if(maxAge<=0){
        maxAge = Integer.MAX_VALUE;
    }
    cookie.setMaxAge(maxAge);
    cookie.setPath("/");
    response.addCookie(cookie);
    return true;
}
```

### （5）删除Cookie

```java
public static boolean removeCookie(HttpServletRequest request,HttpServletResponse response,String name){
	if(StringUtils.isBlank(name)){
		return false;
	}
	Cookie[] cookies = getCookies(request);
	if(null!=cookies){
		for(Cookie cookie : cookies){
			if(name.equals(cookie.getName())){
				cookie.setValue(null);
				cookie.setMaxAge(0);
				cookie.setPath("/");
				response.addCookie(cookie);
				return true;
			}
		}
	}
}
```

​	删除cookie，把 `value`设置为null，把 `max-age`设置为0就行了。

---

## 注意

​	编辑操作和删除操作一样，但是需要注意的是修改、删除Cookie时，除value、maxAge之外的所有属性，例如name、path、domain等，都要**与原Cookie完全一样**。否则，浏览器将视为两个不同的Cookie不予覆盖，导致修改、删除失败。

## 13、substring方法在jkd6,7,8中的差异

```java
substring(int beginIndex, int endIndex)
```

### （1）substring有什么用

​		substring返回的是：字符串索引位置beginIndex开始，endIndex-1结束的字符串。

### （2）JDK 6

​		String背后是**由char数组构成**的

​		在JDK6中，String包含三个字段：char value[], int offset, int count，意思很简单。

​		substring被调用时，它会**创建一个新的字符串**，但字符串的值还指向堆中同样的字符数组。它们的区别只是**数量和下标引用不一样**.

---

![](/../images/substring6.png)

​		如果一个**很长的字符串**，但是每次使用substring()，你只需要很小的一部分。这将会导致性能问题，因为只需要一小部分，**却引用了整个**字符数组内容，解决JDK 6 substring的方法：

```java
x = x.substring(x, y)+"";  //拼接，产生一个新字符串
```

### （3）JDK 7

![](/../images/substring7.png)

## 14、Java集合

### （1）Java集合

​	`java.util.Collection` 是一个**集合接口**。它提供了对集合对象进行基本操作的通用接口方法。

​	Set 和List 都继承了Conllection,Map没有.

### （2）集合类型

​	JAVA集合主要**分为三种类型**：**Set**(集),**List**(列表),**Map**(映射)

	-	`Set` 集合：集合元素是**不能重复**的，元素是**没有顺序**的，所以它**不能基于位置**访问元素。
	-	`List` 集合： 集合元素是**可以重复**的，元素是**有顺序**的，所以它**可以基于位置**访问元素。
	-	`Map` ：它包含键值对。Map的键是**不能重复**的，可以保证元素的插入顺序，也可以排序。

### （3）Set

​	Set**并不是无序**的，**传统说的Set无序**是**指HashSet**,它**不能保证**元素的**添加顺序**，更不能保证**自然顺序**，而Set的**其他实现类**是可以实现这两种顺序的。

​	保证元素**添加的顺序**：LinkedHashSet

​	保证元素**自然的顺序**：TreeSet

![](/../images/set.png)

---

**HashSet**

​	HashSet是**基于HashMap**实现的，它**不允许出现**重复元素，不保证集合中元素的顺序，允许包含值为null的元素，但**最多只能有**一个null元素。

---

**TreeSet**

​	TreeSet可以**实现排序等功能**的集合，它在将对象元素添加到集合中时**会自动按照某种比较规则**将其插入到有序的对象序列中，并保证该集合元素按照**“升序”排列**。

---

**LinkedHashSet**

​	具有HashSet的查询速度，且**内部使用链表**维护元素的顺序（**插入的次序**），于是在使用迭代器遍历Set时，结果会**按元素插入的次序**显示。

### （4）List

**ArrayList**

​	内部结构**基于数组实现**，可以对元素进行**随机的访问**，向ArrayList中插入与删除元素的速度慢。

---

**LinkedList**

​	LinkedList 是一个继承于AbstractSequentialList的**双向链表**，可以被当作堆栈、队列或双端队列进行操作。

​	LinkedList是**非同步的**。每个节点除含有元素外，还包含：**向前、向后**的指针。 

​	新建一个LinkedList，生成一个**头节点**（header，就是一个头指针），它的元素为null。 

### （5）Map

![](/../images/Map.png)

​	Map基于：**散列表的实现**，Map 是一种把**键对象和值对象**映射的集合，它的每一个元素都包含一对键对象和值对象。

---

**HashMap**（数组+链表）

​	HashMap底层就是：一个**数组结构**（叫做Entry Table），数组中**的每一项又是一个链表**（叫做Bucket,用于解决hash冲突而设计的）。可以通过构造器设置：`容量capacity` 和 `负载因子load factor`，以**调整容器的性能**。

---

**LinkedHashMap**

​	**类似于**HashMap，但是迭代遍历它时，**取得“键值对”的顺序**是其：**插入次序**，或者是最近最少使用(LRU)的次序，只比HashMap慢一点。而在迭代访问时发而更快，因为它**使用链表维护内部次序**。

---

**TreeMap**

​	基于：**红黑树数据结构**的实现，查看“键”或“键值对”时，它们会被排序(次序由Comparabel或Comparator决定)。TreeMap的特点在于，你**得到的结果是经过排序的**。TreeMap是唯一的带有subMap()方法的Map，它可以返回一个子树。

---

**WeakHashMap**

​	**弱键**(weak key)Map，Map中使用的对象也被允许释放: 这是为解决特殊问题设计的。如果没有map之外的引用指向某个“键”，则此“键”可以被垃圾收集器回收。

---

**IdentifyHashMap**

​	使用==代替equals()对“键”作比较的hash map，专为解决**特殊问题而设计**。

---

**Hashtable**

​	Hashtable与HashMap类似，Hashtable继承自Dictionary类，实现了Map接口，不同的是它**不允许记录的键或者值为空**；和HashMap相比，Hashtable是**线程同步**的，即**任一时刻只有一个线程**能写Hashtable，因此也导致了 Hashtable在写入时会比较慢。而且Hashtable可以通过Enumeration去遍历。

### （6）总结

​		List按对象进入的顺序保存对象，不做排序或编辑操作。

​		Set对每个对象**只接受一次**，并使用自己内部的排序方法(通常，你只关心某个元素**是否属于Set**,而**不关心它的顺序**--否则应该使用List)。

​		Map同样对每个元素保存一份，但这是基于"键"的，Map也有**内置的排序**，因而**不关心元素添加的顺序**。

​		如果添加元素的顺序对你很重要，应该使用 LinkedHashSet/TreeSet或者LinkedHashMap/TreeMap.　

## 15、循环删除List元素、Get/Put的过程

​	网址：https://mp.weixin.qq.com/s?__biz=MzI3ODcxMzQzMw==&mid=2247483843&idx=1&sn=5c30296c3ffaa34ee091ce9a5ae7e871&scene=21#wechat_redirect

---

```java
for(lterator<String> ite = list.iterator();ite.hasNext();){
    String str = ite.next();
    if(str.contains("b")){
        ite.remove();
    }
}
```

## 16、几种线程安全的Map

### （1）HashMap

```java
private Map<String, Object> map = new HashMap<>();  //	线程不安全
```

### （2）HashTable

```java
private Map<String, Object> map = new HashTable<>();  //	线程安全
```

​	HashTable的`get/put方法`**都被**`synchronized关键字`修饰，说明它们是**方法级别阻塞的，**它们占用共享资源锁，所以导致同时**只能一个线程操作**get或者put，而且get/put操作**不能同时执行**，所以这种同步的集合效率非常低，一般不建议使用这个集合。

### （3）SynchronizedMap

```java
private Map<String, Object> map = Collections.synchronizedMap(new HashMap<String, Object>());
```

​	出SynchronizedMap的实现方式是：**加了个对象锁**，每次对HashMap的操作都要**先获取**这个mutex的对象锁才能进入，所以性能也不会比HashTable好到哪里去，也不建议使用。

### （4）ConcurrentHashMap

```java
private Map<String, Object> map = new ConcurrentHashMap<>();
```

​	这个也是最推荐使用的线程安全的Map，也是实现方式最复杂的一个集合，每个版本的实现方式也不一样，在**jdk8之前**是使用：**分段加锁**，分成16个桶，每次只加锁其中一个桶，而在**jdk8**又加入了**红黑树和CAS算法（Compare and Swap ：比较并替换）**来实现。

## 17、HashMap 和 Hashtable 的 6 个区别

（1）Hashtable 是线程安全的，HashMap 不是线程安全的。

​		Hashtable **所有的元素操作**都是 **synchronized** 修饰的，而 HashMap 并没有。

（2）NULL

​		Hashtable 是**不允许**键或值为 null 的，HashMap 的键值则**都可以为** null。

​		**原因：**

​		 Hashtable key 为 null 会**直接抛出空指针异常**，value 为 null 手动抛出空指针异常；而 HashMap 的逻辑**对 null 作了特殊处理**。

（3）实现方式

​		可以看出两者**继承的类不一样**，Hashtable 继承了 Dictionary类，而 HashMap 继承的是 AbstractMap 类。

（4）容量扩容

​		HashMap 的**初始容量**为：16，Hashtable 初始容量为：11，两者的负载因子默认都是：0.75。

（5）迭代器

​		HashMap 中的 Iterator 迭代器是 **fail-fast（快速失败）** 的，而 Hashtable 的 Enumerator 不是 fail-fast 的。

​		所以，**当 **  其他线程   **改变了HashMap 的结构**，如：增加、删除元素，**将会抛出** ConcurrentModificationException 异常，而 Hashtable 则不会。

## 18、StringBuffer 和 StringBuilder 的 3 个区别

### （1）线程安全

​		StringBuffer：线程安全，StringBuilder：线程不安全。

​		因为 StringBuffer 的**所有公开方法**都是` synchronized 修饰`的，而 StringBuilder 并没有 ` synchronized ` 修饰。

### （2）缓冲区

​		StringBuffer **每次获取 toString** 都会**直接使用缓存区的 toStringCache 值**来**构造一个字符串**。

​		而 StringBuilder 则每次都**需要复制一次字符数组**，再构造一个字符串。

### （3）性能

​		既然 StringBuffer 是线程安全的，它的所有公开方法都是同步的，StringBuilder 是**没有对方法加锁同步**的，所以毫无疑问，StringBuilder 的性能要远大于 StringBuffer。

## 19、StringJoiner 基本使用

​	网址：https://mp.weixin.qq.com/s?__biz=MzI3ODcxMzQzMw==&mid=2247514800&idx=1&sn=6fa9606fe0384e32443193e246d6092f&chksm=eb503f86dc27b690377408bf2ff12fdb801cae6f8c723d1e3090ff79e8ed3d6a3d6f1ba36233&scene=21#wechat_redirect

---

​	用来**拼接字符串**的，一般需要**分隔符进行拼接**，如：

```java
StringBuilder sb = new StringBuilder();
sb.append("hello");
sb.append(",");
sb.append("guys");
sb.append(",");
String str = sb.toString();
```

---

```java
StringJoiner stringJoiner = new StringJoiner(",");
stringJoiner.add("hello");
StringJoiner.add("guys");
String str=  StringJoiner.toString();
```

## 20、String创建方式

```java
String str1 = ”abc";
String str2 = new String("abc");
```

```java
String a = "abc";
String b = "abc";
System.out.println(a==b); // true
System.out.println(a.equals(b)); // true
```

​	a,b都指向了：方法区的**同一个字符串**。所以，当同样的一个字符串用""**重复创建时**，**只在方法区创建一次**。

---

```java
String c = new String("abc");
String d = new String("abc");
System.out.println(a==b); // false
System.out.println(a.equals(b)); // true
```

​	用new创建的字符串**每次都会在JVM堆中创建**，所以c,d都对应堆中的两个不同的字符串。

---

![](/../images/string1.png)

## 21、接口和抽象方法的区别

- 抽象类可以有**构造方法**，接口中不能有构造方法；
- 抽象类中可以有**普通成员变量**，接口中没有普通成员变量；
- 抽象类中可以包含**非抽象的普通方法**，接口中的所有方法必须都是抽象的，不能有非抽象的普通方法；
- 抽象类中的抽象方法的访问类型可以是 **public**，protected和（默认类型,虽然eclipse 下不报错，但应该也不行），但接口中的抽象方法只能是 public 类型的，并且默认即为 public abstract 类型。
- 抽象类中可以包含**静态方法**，接口中不能包含静态方法（JAVA8的**新特性**：接口可以有静态方法和默认方法）
- 抽象类和接口中都可以包含**静态成员变量**，抽象类中的静态成员变量的访问类型可以任意，但接口中定义的变量只能是 public static final 类型，并且默认即为 public static final 类型。
- 一个类可以**实现多个接口**，但**只能继承一个抽象类**。

## 22、Java哪些集合类是线程安全的？

**Vector**：就比ArrayList多了个同步化机制（线程安全）。
**Hashtable**：就被HashMap多了个线程安全。
**ConcurrentHashMap**：是一种高效但是线程安全的集合。
**Stack**：栈，也是线程安全的，继承于Vector，已过时，不建议使用。

## 23、Array.copyOf() 

​	 ` Array.copyOf() `：用于**复制指定的数组内容**以达到**扩容的目的**，该方法对不同的基本数据类型都有对应的重载方法。

```java
public static native void arraycopy(Object src,  int  srcPos,
                                        Object dest, int destPos,
                                        int length);
```

---

**参数说明：**
	src：源对象
	srcPos：源数组中的起始位置
	dest：目标数组对象
	destPos：目标数据中的起始位置
	length：要拷贝的数组元素的数量

---

案例：

```java

public class ArraycopyTest{
 
    public static void main(String[] args){
        char[] c1 = new String("123456").toCharArray();
        char[] c2 = new String("abcdef").toCharArray();
        System.arraycopy(c1, 2, c2, 1, 2);
        System.err.println(Arrays.toString(c1));
        System.err.println(Arrays.toString(c2));
    }
```

结果：

```
输出:
[1, 2, 3, 4, 5, 6]
[a, 3, 4, d, e, f]
```



# 二、多线程

## 1、实现java多线程的4种方式

### （1）继承Thread类

​		**概述：**Thread类其实是实现了Runnable接口的一个实例，继承Thread类后需要**重写run方法并通过start方法**启动线程。

​		**缺点：**继承Thread类**耦合性太强了**，因为java只能单继承，所以**不利于扩展**。

---

```java
static class Thread1 extends Thread{
    
    @Override
    public void run(){
       System.out.println("thread1 running...");
    }
}
```

### （2）实现Runnable接口

​		通过**实现Runnable接口并重写run方法**，并把**Runnable**实例**传给Thread对象**；

​		Thread的start方法调用run方法，**再通过**调用Runnable实例的run方法启动线程。

---

```java
static class Thread2 implements Runnable{
    
    @Override
    public void run(){
       System.out.println("thread2 running...");
    }
}
```

### （3）实现Callable接口

​		通过**实现Callable接口并重写call方法**，并把Callable实例传给**FutureTask**对象，再把FutureTask对象**传给Thread对象**。

​		它与Thread、Runnable**最大的不同是**Callable能**返回一个异步处理的结果**：Future对象**并能抛出异常**，而其他两种不能。

---

```java
static class Thread3<T> implements Callable<T>{
    @SuppressWarning("unhecked")
    @Override
    public T call() throws Exception{
        System.out.println("");
        return (T) new User("java",22);
    }
}
```

```java
public static void main(String[]args) throws Exception{
	Thread t1 = new Thread2();
	Thread t2 = new Thread(new thread2);
	
	FuterTask<User> ft = new FutureTask<>(new Thread3());
	Thread t3 = new Thread(ft);
    
    t1.start();
    t2.start();
    t3.start();
    
    ft.get();
}
```

### （4）线程池



## 2、java线程的状态和生命周期

![](/../images/线程状态.png)

---

NEW：毫无疑问表示的是**刚创建的线程**，还**没有开始启动**。

RUNNABLE:  表示线程已经触发start()方式调用，**线程正式启动**，线程处于运行中状态。

BLOCKED：表示**线程阻塞**，等待获取锁，如碰到synchronized、lock等关键字等占用临界区的情况，一旦获取到锁就进行RUNNABLE状态继续运行。

WAITING：表示线程处于**无限制等待状态**，等待一个特殊的事件来**重新唤醒**，如通过wait()方法进行等待的线程等待一个`notify()`或者`notifyAll()`方法，通过join()方法进行等待的线程等待目标线程运行结束而唤醒，一旦通过相关事件唤醒线程，线程就进入了RUNNABLE状态继续运行。

TIMED_WAITING：表示线程进入了一个**有时限的等待**，如sleep(3000)，等待3秒后线程重新进行RUNNABLE状态继续运行。

TERMINATED：表示线程执行完毕后，进行**终止状态**。

## 3、死锁、活锁、饥饿、无锁

​	**死锁：**是多线程中最差的一种情况，多个线程**相互占用对方的资源的锁**，而又相互等对方释放锁，此时若无外力干预，这些线程则一直处理阻塞的假死状态，形成死锁。

---

​	**活锁：**恰恰**与死锁相反**，死锁是大家都拿不到资源都占用着对方的资源，而活锁是**拿到资源却又相互释放不执行**。当多线程中出现了相互谦让，都主动将资源释放给别的线程使用，这样这个资源在多个线程之间跳动而又得不到执行，这就是活锁。

---

​	**饥饿**：一个线程一直占着一个资源不放而**导致其他线程得不到执行**，与死锁不同的是饥饿**在以后一段时间内还是能够得到执行的**，如那个占用资源的线程结束了并释放了资源。

---

​	**无锁：**即**没有对资源进行锁定**，即所有的线程都能访问并修改同一个资源，但同时**只有一个线程能修改成功**。

## 4、如何避免及解决死锁问题

### （1）按顺序加锁

​		每个线程**都按同一个的加锁顺序**这样就不会出现死锁。

### （2）获取锁时限

​		每个获取锁的时候**加上个时限**，如果**超过某个时间就放弃**获取锁之类的。

### （3）死锁检测

​		**按线程间获取锁的关系检测**线程间是否发生死锁，如果发生**死锁就执行一定的策略**，如终断线程或**回滚操作**等。

## 5、线程池

### （1）什么是线程池

​		**线程池：装有线程的池子**，我们可以把要执行的多线程交给线程池来处理，和连接池的概念一样，通过**维护一定数量的线程池**来**达到多个线程的复用**。

### （2）线程池的好处

​		不用线程池，即每个线程**都要通过new Thread(xxRunnable).start()的方式来创建并运行一个线程**；

​		线程少的话这不会是问题，而真实环境可能会**开启多个线程让系统和程序达到最佳效率**，**当线程数达到一定数量   就会耗尽系统的CPU和内存资源**，也会**造成GC频繁收集和停顿**，因为每次创建和销毁一个线程都是要消耗系统资源的，如果为每个任务都创建线程这无疑是一个很大的性能瓶颈。

​		所以，**线程池中的线程复用极大节省了系统资源**，当线程一段时间不再有任务处理时它也会自动销毁，而不会长驻内存。

### （3）线程池核心类

​		在`java.util.concurrent包`中我们能找到线程池的定义，其中`ThreadPoolExecutor`是我们**线程池核心类**：

​		线程池类的主要参数：

<img src="/../images/线程池.png" style="zoom:50%;" />

----

- corePoolSize：线程池的核心大小，也可以理解为最小的**线程池大小**。
- maximumPoolSize：**最大**线程池大小。
- keepAliveTime：**空余线程存活时间**，指的是超过corePoolSize的空余线程达到多长时间才进行销毁。
- unit：销毁时间单位。
- workQueue：存储等待执行线程的**工作队列**。
- threadFactory：创建线程的工厂，一般用默认即可。
- handler：**拒绝策略**，当工作队列、线程池全已满时如何拒绝新任务，默认抛出异常。

---

### （4）线程池工作流程

​	①	如果**线程池中的线程**    **小于corePoolSize时**就会**创建新线程直接执行任务**。

​	②	如果线程池中的线程**大于corePoolSize时就**会暂时**把任务存储到工作队列**workQueue中**等待执行**。

​	③	如果**工作队列workQueue也满时**：当线程数小于最大线程池数maximumPoolSize时就会创建新线程来处理，而线程数大于等于最大线程池数maximumPoolSize时就会**执行拒绝策略**。

---

### （5）工作队列



### （6）线程池分类

​	**①	newFixedThreadPool**

​		**固定线程池**：**核心**线程数和**最大**线程数**固定相等**，而**空闲存活时间为0毫秒**，说明此参数也无意义，工作队列为最大为Integer.MAX_VALUE大小的阻塞队列。

​		当执行任务时，如果线程都很忙，就会丢到工作队列等有空闲线程时再执行，队列满就执行默认的拒绝策略。

-----

​	**②	newCachedThreadPool**

​		**带缓冲线程池**：从构造看核心线程数为0，最大线程数为Integer最大值大小，**超过0个的空闲线程在60秒后销毁**；

​		SynchronousQueue这是一个**直接提交的队列**，意味着每个新任务都会有线程来执行，如果线程池**有可用线程则执行任务，没有的话就创建一个来**执行，线程池中的**线程数不确定**，一般建议**执行速度较快较小**的线程，不然这个**最大线程池边界过大容易造成内存溢出**。

---

​	**③	newSingleThreadExecutor**

​		**线程线程池**：核心线程数和最大线程数**均为1**，空闲线程存活0毫秒同样无意思，意味着**每次只执行一个线程**，**多余的先**存储到**工作队列**，一个一个执行，**保证了线程的顺序执行**。

----

​	**④	newScheduledThreadPool**

​		**调度线程池：**即按一定的周期执行任务，即定时任务，对ThreadPoolExecutor进行了包装而已。

### （7）拒绝策略

​	**①	AbortPolicy**

​		 简单粗暴，**直接抛出拒绝异常**，这也是**默认的**拒绝策略。

----

​	**②	CallerRunsPolicy**

​		 如果**线程池未关闭**，则会在调用者线程中**直接执行新任务**，这会导致主线程**提交线程性能变慢**。

----

​	**③	DiscardPolicy**

​		 表示：**不处理**新任务，即丢弃。

----

​	**④	DiscardOldestPolicy**

​		  **抛弃最老的任务**，就是**从队列取出最老的任务**然后**放入新的任务**进行执行。    

### （8）如何提交线程

​	准备：

```java
ExecutorService es = Executors.newFixedThreadPool(3);  // 定义一个固定大小的线程池
```

​	提交：

```java
es.submit(xxRunnble);  //返回一个Future对象，能在主线程中通过Future的get方法捕获线程中的异常。
es.execute(xxRunnble);  //没有返回值
```

### （9）如何关闭线程池

```java
es.shutdown(); // 不再接受新的任务，之前提交的任务等执行结束再关闭线程池。
```

```java
es.shutdownNow(); //不再接受新的任务，试图停止池中的任务再关闭线程池，返回所有未处理的线程list列表。
```

### （10）自定义线程池



## 6、sleep( ) 和 wait( ) 的区别

### （1）使用限制

​		 **sleep 方法**：可以让让**当前线程休眠**，**时间一到当前线程继续往下执行**，在任何地方都能使用，但需要捕获 InterruptedException 异常。

​		 **wait 方法**：则必须**放在 synchronized 块里面**，同样需要捕获 InterruptedException 异常，并且需要获取对象的锁。

​		而且 wait 还**需要额外的方法**  `notify/ notifyAll `  进行**唤醒**，它们同样需要放在 synchronized 块里面，且获取对象的锁；

​		也可以使用**带时间的 wait(long millis) 方法**，时间一到**，无需其他线程唤醒**，也会重新竞争获取对象的锁继续执行。

---

### （2）使用场景

​		**sleep ：**一般用于**当前线程休眠**，或者轮循暂停操作；

​		**wait ：**则多用于**多线程之间的通信**。

----

### （3）所属类

​		**sleep ：**是 **Thread 类**的静态本地方法；

​		**wait ：**则是 **Object 类**的本地方法。

---

### （4）线程切换

​		sleep ：会**让出 CPU 执行时间**且**强制上下文切换**，而 wait 则不一定，wait 后可能**还是有机会重新竞争到锁**继续执行的。

---

### （5）释放锁

​		wait 可以释放**当前线程对 lock 对象锁的持有**，而 sleep 则不会。

## 7、多线程 start 和 run 方法区别

​	**start方法**：来**启动线程**，真正实现了多线程运行，这时**无需等待run方法体**代码执行完毕而**直接继续执行下面的代码**。

​	start()方法来启动一个线程，这时此线程处于**就绪（可运行）状态，并没有运行**，一旦**得到cpu时间片，就开始执行run()方法**，这里方法 run()称为线程体，它包含了要执行的这个线程的内容，**Run方法运行结束，此线程随即终止**。

----

​	**run方法：**只是类的一个普通方法而已，如果**直接调用Run方法，等于调用了一个普通的同步方法**，达不到多线程运行的异步执行；

​	程序中依然只有主线程这一个线程，其程序**执行路径还是只有一条**，还是要顺序执行，还是要等待run方法体执行完毕后才可继续执行下面的代码，这样就没有达到写线程的目的。

---

## 8、Thread.yield 方法

### （1）概念

​		**yield 即 "谦让"**，也是 Thread 类的方法。它**让掉当前线程 CPU 的时间片，使正在运行中的线程重新变成就绪状态**，并重新竞争 CPU 的调度权（**抢占式**）。它可能会获取到，也有可能被其他线程获取到。

### （2）yield 和 sleep 的异同

​	1）yield, sleep 都能**暂停当前线程**，sleep 可以指定具体休眠的时间，而 yield 则依赖 CPU 的时间片划分。

​	2）yield, sleep 两个在暂停过程中，如已经持有锁，则都不会释放锁资源。

​	3）yield **不能被中断**，而 sleep 则**可以接受中断**。

## 9、join方法

​		`t.join()方法` ：只会**使主线程**(或者说调用t.join()的线程)**进入等待池**并   **等待t线程执行完毕后**才会被唤醒。并**不影响同一时刻**处在运行状态的其他线程。

## 10、Synchronized 有几种用法

### （1）同步普通方法

<img src="/../images/普通方法.png" style="zoom: 67%;" />

​		**同一个实例**   **只有一个线程**能获取锁进入这个方法。

---

### （2）同步静态方法

<img src="/../images/静态方法.png" style="zoom: 67%;" />

​		同步静态方法是类级别的锁，一旦**任何一个线程进入这个方法**，**其他所有线程将无法访问**这个类的**任何同步类锁**的方法。

---

### （3）同步类

<img src="/../images/同步类.png" style="zoom:50%;" />

​		锁住效果和同步静态方法一样，都是**类级别的锁**，同时**只有一个线程**能访问带有**同步类锁**的方法。

### （4）同步this实例

​		<img src="/../images/同步this实例.png" style="zoom: 67%;" />

​		用法**和同步普通方法锁**一样，都是**锁住整个当前实例**，只有**获取到这个实例的锁**才能进入这个方法。

### （5）同步对象实例

<img src="/../images/同步对象实例.png" style="zoom:67%;" />

​	锁住当前实例一样，这里表示锁住整个 LOCK 对象实例，只有**获取到这个 LOCK 实例的锁**才能进入这个方法。

### （6）总结

​		**类锁与实例锁**不相互阻塞，但相同的类锁，相同的当前实例锁，相同的对象锁会相互阻塞。

## 11、synchronized 深度解析

### （1）简介

​	**作用：**

​		保证在**同一时刻**最多**只有一个线程**执行该代码，以**保证并发安全**的效果。

----

​	**地位：**

- `Synchronized`是Java关键字，Java**原生支持**
- 最**基本的**互斥同步手段
- 并发编程的**元老级别**

---

​	**不控制并发**的影响：

​		**测试：**两个线程**同时a++**，猜一下结果

```java
public class SynchronizedTest1 implements Runnable{
	static SynchronizedTest1 st = new SynchronizedTest1();
	static int a = 0;
	/*
	 *  不适用Synchronized，两个线程同时：a++
	 *  结果预期：20000,
	 */
	public static void main(String[] args) throws InterruptedException {
		Thread t1 = new Thread(st);
		Thread t2 = new Thread(st);
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		System.out.println(a);
	}
	@Override
	public void run() {
		for(int i=0;i<10000;i++) {
			a++;
		}
	}
```

**结果：**

​	10958（每次运行结果为：10000左右）

​	**结果预期：**20000

---

### （2）用法

​	**同一个Class对象**，创建多个线程

```java
static SynchronizedTest1 st = new SynchronizedTest1();

Thread t1 = new Thread(st);
Thread t2 = new Thread(st);
```

```java
public class SynchronizedTest1 implements Runnable{
	static SynchronizedTest1 st = new SynchronizedTest1();
	static int a = 0;
	/*
	 *  不适用Synchronized，两个线程同时：a++
	 *  结果预期：20000,
	 */
	public static void main(String[] args) throws InterruptedException {
		Thread t1 = new Thread(st);
		Thread t2 = new Thread(st);
		t1.start();
		t2.start();
		t1.join();
		t2.join();
		System.out.println(a);
	}
```

#### 	-	对象锁

- **代码块形式**：**手动指定**锁对象

---

```java
	@Override
	public void run() {
		synchronized(this) {
			System.out.println("开始执行：" + Thread.currentThread());
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("开始结束：" + Thread.currentThread());
		}
	}
```

**输出结果：**

开始执行：Thread[Thread-1,5,main]
开始结束：Thread[Thread-1,5,main]
开始执行：Thread[Thread-0,5,main]
开始结束：Thread[Thread-0,5,main]

---

- **方法锁形式**：`synchronized` 修饰方法，**锁对象默认为**`this`

---

```java
	@Override
	public synchronized void run() {
		System.out.println("开始执行：" + Thread.currentThread());
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("开始结束：" + Thread.currentThread());
	}
```

**输出结果：**

开始执行：Thread[Thread-1,5,main]
开始结束：Thread[Thread-1,5,main]
开始执行：Thread[Thread-0,5,main]
开始结束：Thread[Thread-0,5,main]

---

#### 	-	类锁

​	**多个Class对象**，分别创建对应的线程。

```java
static SynchronizedTest1 st1 = new SynchronizedTest1();
static SynchronizedTest1 st2 = new SynchronizedTest1();

Thread t1 = new Thread(st1);
Thread t2 = new Thread(st2);
```

​	**概念：**Java类可能有多个对象，但只有一个Class对象

​	**本质：**所谓的类锁，不过是**Class对象的锁**而已

​	**用法和效果：**类锁只能在同一时刻**被一个对象拥有**

---

​	**形式1：**synchronized 加载 `static 方法`上

```java
	public static synchronized void Method() {
		System.out.println("开始执行：" + Thread.currentThread());
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("开始结束：" + Thread.currentThread());
	}
```

​	**形式2：**synchronized**(*.class)代码块**

```java
	public  void method() {
		synchronized(SynchronizedTest1.class) {
			System.out.println("开始执行：" + Thread.currentThread());
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("开始结束：" + Thread.currentThread());
		}
	}
```

### （3）多线程访问同步方法

​	**一个对象**只有一把：对象锁；**所有的对象**只有一把：类锁；

---

- 两个线程同时访问一个对象的相同的synchronized方法
- 两个线程同时访问两个对象的相同的synchronized方法
- 两个线程同时访问两个对象的相同的static的synchronized方法
- 两个线程同时访问同一对象的synchronized方法与非synchronized方法
- 两个线程访问同一对象的不同的synchronized方法
- 两个线程同时访问同一对象的static的synchronized方法与非static的synchronized方法
- 方法抛出异常后，会释放锁吗

---

​	**1、两个线程同时访问一个对象的相同的synchronized方法**

​		  同一实例拥有同一把锁，其他线程必然等待，顺序执行

---

​	**2、两个线程同时访问两个对象的相同的synchronized方法**

​		 不同的实例拥有的锁是不同的，所以不影响，并行执行

---

​	**3、两个线程同时访问两个对象的相同的static的synchronized方法**

​		 静态同步方法，是类锁，**所有实例**是**同一把锁**，其他线程必然等待，顺序执行

---

​	**4、两个线程同时访问同一对象的synchronized方法与非synchronized方法**

​		 非synchronized方法不受影响，并行执行

---

​	**5、两个线程访问同一对象的不同的synchronized方法**

​		 **同一实例**拥有同一把锁，所以顺序执行（说明：锁的是this对象==同一把锁）

---

​	**6、两个线程同时访问同一对象的static的synchronized方法与非static的synchronized方法**

​		 static同步方法是类锁，非static是对象锁，原理上是不同的锁，所以不受影响，并行执行

---

​	**7、方法抛出异常后，会释放锁吗**

​		 会**自动释放锁**，这里区别Lock，Lock：需要显示的释放锁

​		 获取lock锁后发生异常后，线程退出，**Lock锁不释放**

---

**3个核心思想：**

- 一把锁只能**同时被一个线程**获取，没有拿到锁的线程**必须等待**（对应1、5的情景）
- 每个实例都对应有自己的一把锁，**不同的实例之间互不影响**；例外：锁对象是*.class以及synchronized被static修饰的时候，所有对象共用同一把锁（对应2、3、4、6情景）
- 无论是方法正常执行完毕还是方法抛出异常，都会释放锁（对应7情景）

---

**问题：**

​	目前进入到被synchronized修饰的方法里边**调用了非synchronized方法**，是线程安全的吗？

```java
public static void main(String[] args) throws InterruptedException {
	new Thread(()->{
		method1();
	}).start();
	
	new Thread(()->{
		method1();
	}).start();
}

public static synchronized void method1() {
	method2();
}

public static  void method2() {
	System.out.println("开始执行：" + Thread.currentThread());
	try {
		Thread.sleep(3000);
	} catch (Exception e) {
		e.printStackTrace();
	}
	System.out.println("开始结束：" + Thread.currentThread());
}
```
### （4）性质

#### 	-	可重入

​		**可重入：**指某个线程**已经获得某个锁**，可以**再次获取锁**而**不会出现死锁**。

----

​	`synchronized`：

```java
// 演示可重入锁是什么意思，可重入，就是可以重复获取相同的锁，synchronized和ReentrantLock都是可重入的
// 可重入降低了编程复杂性
public class WhatReentrant {
	public static void main(String[] args) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				synchronized (this) {
					System.out.println("第1次获取锁，这个锁是：" + this);
					int index = 1;
					while (true) {
						synchronized (this) {
							System.out.println("第" + (++index) + "次获取锁，这个锁是：" + this);
						}
						if (index == 10) {
							break;
						}
					}
				}
			}
		}).start();
	}
}
```

**结果：**

第1次获取锁，这个锁是：MyTest.SynchronizedTest1$1@6e1ecd8e

第2次获取锁，这个锁是：MyTest.SynchronizedTest1$1@6e1ecd8e

...

---

​	`ReentrantLock`：

```java
public static void main(String[] args) {
	ReentrantLock lock = new ReentrantLock();
	new Thread(new Runnable() {
		@Override
		public void run() {
			try {
				lock.lock();
				System.out.println("第1次获取锁，这个锁是：" + lock);
				int index = 1;
				while (true) {
					try {
						lock.lock();
						System.out.println("第" + (++index) + "次获取锁，这个锁是：" + lock);
						try {
							Thread.sleep(new Random().nextInt(200));
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						
						if (index == 10) {
							break;
						}
					} finally {
						lock.unlock();  //内部try
					}
				}
			} finally {
				lock.unlock(); //外部try
			}
		}
	}).start();
}
```
结果：

第1次获取锁，这个锁是：MyTest.SynchronizedTest1$1@6e1ecd8e

第2次获取锁，这个锁是：MyTest.SynchronizedTest1$1@6e1ecd8e

...

---

​	使用ReentrantLock的**注意点**

​	ReentrantLock 和 synchronized 不一样，需要**手动释放锁**，所以使用 ReentrantLock的时候一定要**手动释放锁**，并且**加锁次数和释放次数要一样**。

---

- 情况1：证明同一方法是可重入的
- 情况2：证明可重入不要求是同一方法
- 情况3：证明可重入不要求是同一类中的

---

#### 	-	不可中段

​	一旦这个锁被别的线程获取了，如果我现在想获得，我**只能选择等待或者阻塞**，直到别的线程释放这个锁，如果别的线程永远不释放锁，那么我只能永远的等待下去。

​	相比之下，**Lock类可以拥有中断的能力**，第一点：如果我觉得我等待的时间太长了，有权中断现在已经获取到锁的线程执行；第二点：如果我觉得我等待的时间太长了不想再等了，也可以退出。

---

### （5）原理

#### 	-	加解锁原理（现象、时机、深入JVM看字节码）

​		每**一个类的实例对应一把锁**，每一个`synchronized`方法都必须**首先获得调用该方法的类的实例的锁**，方能执行，否则就会阻塞，方法**执行完成或者抛出异常**，锁被释放，被阻塞线程才能获取到该锁，执行。

----

#### 	-	可重入原理（加锁次数计数器）

​		**JVM负责跟踪**对象被加锁的次数;

​		线程**第一次**给对象加锁的时候，**计数变为1**，每当这个相同的线程在此对象上再次获得锁时，计数会递增;

​		每当任务离开时，计数递减，当**计数为0的时候，锁被完全释放**;

----

#### 	-	可见性原理（内存模型）

​		`synchronized关键字`实现**可见性**：

​		被synchronized修饰，那么执行完成后，对对象所做的任何修改都要在**释放锁之前**，都要从线程**内存写入到主内存**，所以主内存中的**数据是最新的**。

### （6）缺陷

#### -	效率低

- 锁的**释放情况少**（线程执行完成或者异常情况释放）
- 试图获得锁时**不能设定超时**（只能等待）
- **不能中断**一个**正在试图获得锁的线程**（不能中断）

---

#### -	不够灵活

	-	**加锁和释放的时机比较单一**，每个锁仅有单一的条件（某个对象），可能是不够的
	-	比如：**读写锁**更灵活

---

#### -	无法预判是否成功获取到锁

---

### （7）如何选择Lock或Synchronized

- 如果可以的话，尽量**优先使用java.util.concurrent各种类**（不需要考虑同步工作，不容易出错）
- 优先使用synchronized，这样可以减少编写代码的量，从而可以减少出错率
- **若用到Lock或Condition独有的特性**，才使用Lock或Condition

### （8）总结

**一句话总结synchronized：**

> JVM会自动通过使用`monitor`来**加锁和解锁**，保证了同一时刻只有一个线程可以执行指定的代码，从而保证线程安全，同时具有可重入和不可中断的特性。

## 12、线程安全的List

​	Vector > SynchronizedList > CopyOnWriteArrayList

---

### （1）Vector

​		Vector的**内部实现类似于ArrayList**，Vector也是**基于一个容量能够动态增长的数组**来实现的，该类是JDK1.0版本添加的类，它的很多实现方法都加入了同步语句，因此是**线程安全的**（但Vector其实也只是**相对安全**，有些时候还是要加入同步语句来保证线程的安全）。

### （2）SynchronizedList 

​	java.util.Collections.SynchronizedList

​	**把所有 List 接口的实现类**  转换成   **线程安全的List**，比 Vector 有更好的扩展性和兼容性。

---

```java
// SynchronizedList的构造方法如下：
final List<E> list;

SynchronizedList(List<E> list){
    super(list);
    this.list = list;
}
```

---

```java
// SynchronizedList的部分方法源码如下：
public E get(int index){
    synchronized(mutex){ return list.get(index);}
}
public E set(int index, E element){
     synchronized(mutex){ return list.set(index, element);}
}
public void add(int index,E element){
    synchronized(mutex){list.add}
}
```

​	它**所有方法**都是**带同步对象锁**的，和 Vector 一样，它**不是性能最优的**。

### （3）并发包里面的并发集合类

​	以下两种并发集合，**只适合于读多写少的情况**，因为每次写操作都要**进行集合内存复制**，性能开销很大，如果集合较大，很容易**造成内存溢出**。

```java
java.util.concurrent.CopyOnWriteArrayList
java.util.concurrent.CopyOnWriteArraySet
```

#### -	 CopyOnWriteArrayList

​		CopyOnWrite（简称：COW）：即**复制再写入**，就是在添加元素的时候，**先**把原 List 列表**复制一份**，**再添加**新的元素。

```java
public boolean add(E e){
    // 加锁
    final ReentrantLock lock = this.lock;//获得锁
    lock.lock();
    try{
        // 获取原始集合
        Object[] elements = getArray();
        int len = elements.length;
        
        // 复制一个新集合
        Object[] newElements = Arrays.copyOf(elements, len+1);
        newElements[len] = e;
        
        // 替换原始集合为新集合
        setArray(newElements);
        return true;
    }finally{
        lock.unlock(); // 释放锁
    }
}
```

​	**添加**元素时，**先加锁**，**再进行复制** ，再替换操作，最后再释放锁。

----

​	`get 方法`源码：

```java
public E get(Object[] a,int index){
	return (E)a[index];
}
public E get(int index){
	return get(getArray(),index);
}
```

​	get方法（获取元素）**并没有加锁**

​	好处是，**在高并发情况下**，读取元素时就不用加锁，写数据时才加锁，大大**提升了读取性能**。

---

#### -	CopyOnWriteArraySet

```java
public boolean addIfAbsent(E e){
	Object[] snapshot = getArray();
	return indexOf(e,snapshot,0,sanpshot.length)>=0?false:addIfAbsent(e,snapshot);
}
```

​	CopyOnWriteArrayList 的 addIfAbsent 方法来**去重**的，添加元素的时候判断对象是否已经存在，**不存在才添加进集合**。

---

# 三、JDK 8 新特性

## 1、Lambda表达式

​	上联：左右遇一括号省

​	下联：左侧推断类型省

​	横批：能省则省

---

​	Lambda表达式（外文名：**lambda** [expression](https://baike.sogou.com/lemma/ShowInnerLink.htm?lemmaId=52918064&ss_c=ssc.citiao.link)）是一个**匿名函数**，即**没有函数名的函数**。

​	**介绍：**使用它设计的**代码会更加简洁**。当开发者在编写Lambda表达式时，也会随之被编译成一个：**函数式接口**。

### （1）Lambda语法

​	**一行**执行语句的写法：

```java
(parameters)->expression
```

​	如果有**多行**执行语句，可以加上 `{}`

```java
(parameters)->{statements;}
```

---

**语法格式一：**无参数，无返回值

```java
public void test1(){
 	Runable r = new Runnable(){
        // 局部变量
        int num = 0;// jdk 1.7前，必须是：final
        @Override
        public void run(){
            System.out.println("Hello world!");
        }
    };
	r.run();
}
```

**等价于：**

```java
Runnable r1 = () ->  System.out.println("Hello world!");
r1.run();
```

---

**语法格式二：**有一个参数，并且无返回值

```java
public void test2(){
    Consumer<String> con = (x) -> System.out.println(x);
    con.accept("Lambda语法二");
}
```

---

**语法格式三：**若只有一个参数，小括号可以**省略不写**

```java
public void test2(){
    Consumer<String> con = x -> System.out.println(x);
    con.accept("Lambda语法二");
}
```

---

**语法格式四：**有两个以上的参数，有返回值，并且Lambda体有多条语句

```java
// 原来的匿名内部类
public void lambda1() {
	Comparator<Integer> com = new Comparator<Integer>() {
		@Override
		public int compare(Integer o1, Integer o2) {
			return Integer.compare(o1, o2);
		}
	};
	TreeSet<Integer> ts = new TreeSet<>(com);//添加写好的重写比较方法
}
```
**等价于：**

```java
// Lambda表达式
public void lambda2() {
	Comparator<Integer> com = (x, y) ->{
      	return Integer.compare(x, y);   
    }      
	TreeSet<Integer> ts = new TreeSet<>(com);
}
```
---

**语法格式五：**若Lambda体中只有一条语句，**return 和 大括号**都可以省略

```java
// Lambda表达式
public void lambda2() {
	Comparator<Integer> com = (x, y) ->Integer.compare(x, y);
	TreeSet<Integer> ts = new TreeSet<>(com);
}
```

---

**语法格式六：**Lambda表达式的**参数列表数据类型**可以**省略不写**，因为：**JVM编译器**可以通过：**上下文推断出** 数据类型，即：**类型推断**。

```java
（Integer x,Integer y) -> Integer.compare(x,y);

// 第二个可不写参数，上下文推断（也就是第一个参数）
List<String> list = new ArrayList<>(); 
```

### （2）Lambda基础

​		Java 8 中引入了一个**新的操作符**：**->**  （称为：箭头操作符或Lambda操作符）

​		Lambda表达式拆分为：左侧（**参数列表**）、右侧（需要**执行的功能**，即：Lambda体）

### （3）Lambda认识

#### 	-	准备条件

```java
public class Employee {
	private String name;
	private int age;
	private double salary;
```

---

```java
	public static void main(String[] args) {
		List<Employee> employees = Arrays.asList(
				new Employee("张三",18,9999.0),
				new Employee("李四",38,5555.0),
				new Employee("王五",50,3333.0),
				new Employee("赵六",17,6666.0),
				new Employee("电七",16,7777.0)
		);
	}
```

#### 	-	传统写法

​	**需求1：**获取当前公司中**员工年龄**大于35的员工信息

```java
	public List<Employee> filterages(List<Employee> list){
		List<Employee> emps = new ArrayList<>();
		
		for(Employee emp:list) {
			if(emp.getAge()>=35) {
				emps.add(emp);
			}
		}
		return emps;
	}
```

---

​	**增加**一个需求，则需要**重新写一个方法**：

​	**需求2：**获取当前公司中**员工工资**大于3500的员工信息

```java
public List<Employee> filterSalary(List<Employee> list){
	List<Employee> emps = new ArrayList<>();
	
	for(Employee emp:list) {
		if(emp.getSalary()>=3500) {
			emps.add(emp);
		}
	}
	return emps;
}
```
#### 	-	策略设计模式写法

​	**一个需求一个类，方法通用**

```java
public interface MyPredicate<T> {  // 接口类<泛型>
	public boolean Judge(T t);  
}
```

---

​	**需求1：**获取当前公司中**员工年龄**大于35的员工信息

```java
// 实现接口类，并重新对应的方法
public class FilterAges implements MyPredicate<Employee>{  
	@Override
	public boolean Judge(Employee t) {  // 比较年龄
		return t.getAge() > 35;
	}
}
```

---

```java
public List<Employee> filterEmployee(List<Employee> list,MyPredicate<Employee> mp){
	List<Employee> emps = new ArrayList<>();
	
	for(Employee employee:list) {
		if(mp.Judge(employee)) {  // 如果当前对象的年龄大于35，则符合
			emps.add(employee);
		}
	}
	return emps;
}
```
---

​	**增加一个需求**，改为：增加一个`class` 类，并实现`FilterAges`接口，重写：`Judge` 方法：

​	**需求2：**获取当前公司中**员工工资**大于3500的员工信息

```java
public class FilterSalary implements MyPredicate<Employee>{
	@Override
	public boolean Judge(Employee t) { // 如果当前对象的工资大于3500，则符合
		return t.getSalary() > 3500;
	}
}
```

---

```java
public static void main(String[] args) {
	List<Employee> employees = Arrays.asList(
			new Employee("张三",18,9999.0),
			new Employee("李四",38,5555.0),
			new Employee("王五",50,3333.0),
			new Employee("赵六",17,6666.0),
			new Employee("电七",16,7777.0)
	);
	TestLambda testLambda = new TestLambda();
	List<Employee> list1 = testLambda.filterEmployee(employees,new FilterAges());
	
	for(Employee employee : list1) {
		System.out.println(employee);
	}
	
	System.out.println("-------打印工资大于3500的对象-------------");
	
	List<Employee> list2 = testLambda.filterEmployee(employees,new FilterSalary());
	
	for(Employee employee : list2) {
		System.out.println(employee);
	}
```
### （4）函数式接口

​		**Lambda表达式：**需要**“函数式接口”的支持。**

​		**函数式接口：**接口中**只有一个抽象方法**的接口。可以**使用注解@FunctionalInterface修饰**（检查是否是函数式接口）

---

```java
@FunctionalInterface
public interface MyFun {
	public Integer getValue(Integer num);
}
```

```java
// 对一个数进行运算
public static void main(String[] args) {
    
	Integer num1 = operation(100,(x)->x*x);
	System.out.println(num1); // 10000
    
    Integer num2 = operation(100,(x)->x+200);
	System.out.println(num2); // 300
}

public static Integer operation(Integer num,MyFun mf) {
	return mf.getValue(num);
}
```

### （5）Lambda练习

#### -	重写sort

```java
employees = Arrays.asList(
				new Employee("张三",18,9999.0),
				new Employee("李四",38,5555.0),
				new Employee("王五",50,3333.0),
				new Employee("赵六",17,6666.0),
				new Employee("电七",16,7777.0)
		);

public void test(){
    Collections.sort(employees,(e1,e2)->{
       if(e1.getAge() == e2.getAge()){
           return e1.getName().compareTo(e2.getName);
       }else{
           return Integer.compare(e1.getAge(),e2.getAge())
       } 
    });
}
```

#### -	处理字符串

```java
@FunctionalInterface
public interface MyFunction { // 声明函数式接口
	public String getValue(String str);	
}
```

```java
public class Main {

	public static void main(String[] args) {
		String trim_str = strHandler("\t\t\t 我是谁",(str)->str.trim());
		System.out.println(trim_str); //我是谁
		
		String case_str = strHandler("abcd",(str)->str.toUpperCase());
		System.out.println(case_str); //ABCD
		
		String sub_str = strHandler("abcd",(str)->str.substring(1));
		System.out.println(sub_str);//bcd
	}
	
	// 需求：处理字符串，str：参数，mf：执行的功能
	public static String strHandler(String str,MyFunction mf) {
		return mf.getValue(str);
	}
}
```

#### -	泛型

```java
@FunctionalInterface
public interface MyFunction2<T,R> {
	public R getValue(T t1,T t2);
}
```

```java
public class Main {

	public static void main(String[] args) {
		op(100L,200L,(x,y)->x+y);
		op(100L,200L,(x,y)->x*y); 
	}
	
	// 对于两个Long型数据进行处理
	public static void op(Long l1,Long l2,MyFunction2<Long,Long> mf) {
		System.out.println(mf.getValue(l1, l2));
	}
}
```

## 2、内置的四大核心函数式接口

### （1）Consumer<T> : 消费型接口

```java
void  accept(T t);
```

```java
import java.util.function.Consumer;
public class Main {
	public static void main(String[] args) {
		happy(1000,m -> System.out.println("我消费了"+m+"元"));
        // 我消费了1000.0元
	}
    // 参数1：参数；参数2：执行的功能
	public static   void happy(double money,Consumer<Double> con) {
		con.accept(money);
	}
}
```

### （2）Supplier<T> : 供给型接口

```java
T get();
```

```java
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Main {

	public static void main(String[] args) {
		List<Integer> list = getNumList(10,()->(int)(Math.random()*100));// 随机数
		
		for(Integer li:list) {
			System.out.print(li+" ");
		}
        // 5 19 64 45 33 74 17 79 45 60 
	}
	
	// 产生指定个数的整数，并放入集合中
	public static List<Integer> getNumList(int num,Supplier<Integer> sup){
		List<Integer> list = new ArrayList<>();
		
		for(int i=0;i<num;i++) {
			Integer n = sup.get(); // 获得里面的元素
			list.add(n);
		}
		return list;
	}
}
```

### （3）Function<T,R>：函数型接口

​	T：操作；R：返回的值

```java
R apply(T t);
```

```java
import java.util.function.Function;
public class Main {
	public static void main(String[] args) {
		String trim_str = strHandler("\t\t\t 我是谁",(str)->str.trim());
		System.out.println(trim_str); //我是谁
		
		String case_str = strHandler("abcd",(str)->str.toUpperCase());
		System.out.println(case_str); //ABCD
	}
	//处理字符串
	public static String strHandler(String str,Function<String,String> fun) {
		return fun.apply(str);
	}
}
```

### （4）Predicate<T> ： 断言型接口

```java
boolean test(T t);
```

```java
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Main {

	public static void main(String[] args) {
        // Arrays.asList：将一个数组转换为一个List集合
		List<String> list =  Arrays.asList("Hello","atguigu","Lambda","www");
		List<String>  list_str = filterStr(list,(s)->s.length() > 3); // 筛选字符串长度 > 3
	
		for(String li:list_str) {
			System.out.print(li+" ");//Hello atguigu Lambda 
		}
	}
	
	//将满足条件的字符串，放入集合中
	public static List<String> filterStr(List<String> list,Predicate<String> pre){
		List<String> strList = new ArrayList<>();
		
		for(String str:list) {
			if(pre.test(str)) {
				strList.add(str);
			}
		}
		return strList;
	}
}
```

### （5）其他接口

<img src="/../images/其他接口.png" style="zoom:67%;" />

## 3、方法引用与构造器引用

​	概念：若Lambda体中的内容有**方法已经实现了**，我们可以使用：**方法引用**（Lambda表达式的另外一种表现形式）。

​	主要**三种语法格式**：

​		对象 :: 实例方法名
​		类 :: 静态方法名
​		类 :: 实例方法名

---

### （1）对象 :: 实例方法名

```java
	// 对象::实例方法名
	public static void test1() {
		PrintStream ps1 = System.out; //PrintStream 对象
		Consumer<String> con1 = (x)->ps1.println(x);
		con1.accept("abd"); // abd
		// ---------------------------
		PrintStream ps = System.out;
		Consumer<String> con2 = ps::println;//对象::实例方法名
		con2.accept("abd");// abd
		// ---------------------------
		Consumer<String> con3 = System.out::println;
		con3.accept("abc");// abc
	}
```

### （2）类 :: 静态方法名

```java
// 类::静态方法名
public static void test2() {
	Comparator<Integer> com1 = (x,y)->Integer.compare(x,y);
	Comparator<Integer> com2 = Integer::compare;
}
```
### （3）类 :: 实例方法名

```java
// 类::实例方法名
public static void test3() {
	BiPredicate<String,String> bp1 = (x,y)->x.equals(y);
	
	BiPredicate<String,String> bp2 = String::equals;
}
```
### （4）注意

		-	Lambda体中**调用方法的参数列表与返回值类型**，要与函数式接口中抽象方法的**函数列表和返回值类型保持一致**
		-	若Lambda参数列表中的**第一参数**是：实例方法的**调用者**，而**第二参数**是：是实例方法的参数时，可以使用：`ClassName::method`

### （5）构造器引用

​	**无参构造器**

​	注意：调用的**构造器的参数列表**要与函数式接口中**抽象方法的参数列表**保持一致！！

```java
	//构造器引用
	public static void test4() {
		Supplier<Employee> sup = () -> new Employee();
		// 构造器引用方式（无参）
		Supplier<Employee> sup2 = Employee::new; // 返回值：Employee对象
		Employee emp = sup2.get();//get() 函数式接口方法
		System.out.println(emp);
        // 打印：Employee [name=null, age=0, salary=0.0]
	}
```

---

​	**有参构造器**（一个参数）

```java
public Employee(int age) { 
	this.age = age;
}
```

```java
// 构造器引用
public static void test4() {
	// Function<Integer,Employee> fun = (x)-> new Employee(x);
	// 等价于：
	Function<Integer,Employee> fun2 = Employee::new;//参数：Integer，返回类型：Employee对象
	Employee emp = fun2.apply(101);
	System.out.println(emp);// 	Employee [name=null, age=101, salary=0.0]
}
```

---

​	**有参构造器**（两个参数）

```java
public Employee(String name,int age) {
	this.name = name;
	this.age = age;
}
```

​	*BiFunction<String,Integer,Employee>  ：String，Integer：**参数**，Employee：**返回类型***

```java
//构造器引用
public static void test4() {
	BiFunction<String,Integer,Employee> bf = Employee::new;//对象::实例方法名
	Employee emp = bf.apply("tzw", 100);
	System.out.println(emp);//Employee [name=tzw, age=100, salary=0.0]	
}
```
### （6）数组引用

​	Type :: new

```java
//数组引用
public static void test4() {
   // 创建长度为：Integer长的数组，返回一个String数组
	Function<Integer,String[]> fun1 = (x) ->new String[x];
	String[] strs = fun1.apply(10); //创建一个长度为：10的string数组
	System.out.println(strs.length);
	
	// 上面等价于：
	Function<Integer,String[]> fun2 = String[]::new; //访问构造器
	String[] str2 = fun2.apply(20);
	System.out.println(str2.length);
	
}
```
## 4、Stream API

### （1）简述

​	**概念：**为JAVA 8中处理集合的关键抽象概念，可以对**指定的集合**进行操作，可以执行非常复杂的：**查找、过滤和映射数据**等操作。

​	使用Stream API 对集合数据进行操作，**类似于**使用SQL执行的**数据库查询**。

​	简而言之，Steam API 提供了一种**高效且易于使用**的**处理数据的方式**。

---

<img src="/../images/stream流.png" style="zoom:67%;" />

[集合讲的是：数据；流讲的是：计算！！！]()

注意：

- Stream流自己**不会存储数据**

- Stream流**不会改变源对象**。相反，会**返回一个持有结果**的：新Stream流

- Stream流操作是**延迟执行**的。意味着要等到**需要结果的时候才执行**。

  （简而言之：**执行终止操作，才会执行：中间操作**）

---

### （2）Stream的操作

**创建Stream**

​	一个数据源（集合、数组等），**获取一个流**。

---

**中间操作**

​	一个中间操作链，对数据源的数据进行处理。

​	**多个中间操作**可以连接起来形成**一个流水线**，除非流水线上触发：终止操作，否则：中间操作不会执行任何的处理！！，而在**执行终止操作**的时候，**一次性全部处理**，称为：**惰性求值**。

|        方法         | 描述                                                         |
| :-----------------: | :----------------------------------------------------------- |
| filter(Predicate p) | 接收Lambda，从流中**排除**某些元素                           |
|      distict()      | **筛选**，通过流所生产元素的hashCode()和equals()去除重复元素。 |
| limit(long maxSize) | 截断流，使其元素**不超过**给定数量。                         |
|    skip(long n)     | **跳过**元素，返回一个：扔掉了前n个元素的流。<br/>若流中不足n个，则返回一个：空流。与limit(n)互补 |

---

**终止操作（终端操作）**

​	一个终止操作，执行中间操作链，并产生结果。

### （3）四种获取流的方式

#### -	list.stream

```java
/*
 *  第一种：
* 	可以通过：Collection系列集合提供的：
* 		Stream()或paralleleStream()
*/
List<String> list = new ArrayList();
Stream<String> stream1 = list.stream();
```

#### -	Arrays.stream

```java
/*
 *  第二种：通过Arrays中的静态方法：stream()获取数据流
 */
Employee[] emps = new Employee[10];
Stream<Employee> stream2 = Arrays.stream(emps);
```

#### -	Stream.of

```java
/*
*  第三种：通过Stream类中的静态方法：of()
*/
Stream<String> stream3 = Stream.of("aa","bb","cc");
```

#### -	Stream.iterate

```java
/*
* 第四种：创建无限流：iterate
* 	 例；从0开始，无线递增：2
*/
Stream<Integer> stream4 = Stream.iterate(0, (x)->x+2);
```

```java
stream4.forEach(System.out::println);// 遍历
// limit：中间操作
stream4.limit(10).forEach(System.out::println);// 遍历（限制10个）
```

```java
// 无线流，生成随机数，限制5个
Stream.generate(()->Math.random())
    .limit(5)
    .forEach(System.out::println);
```

### （4）筛选与切片-中间操作

​	注意：**执行：终止操作，才会执行：中间操作**

---

```java
/*
*   筛选与切片
* filter—————接收：Lambda，从流中排除某些元素；
* limit——————截断流，使其元素不超过给定数量
* skip(n)—————跳过元素，返回一个：扔掉了前n个元素的流。
* 			若流中不足n个，则返回一个：空流。与limit(n)互补
* distinct————筛选，通过流所生产元素的hashCode()和equals()去除重复元素
*/
```
准备工作：

```java
public class Main {
	static List<Employee> employees;
	public static void main(String[] args) {
		employees = Arrays.asList(
				new Employee("张三",18,9999.0),
				new Employee("李四",38,5555.0),
				new Employee("王五",50,3333.0),
				new Employee("赵六",17,6666.0),
				new Employee("电七",16,7777.0)
		);
	}
```

---

#### -	filter

```java
public static void test2() {
	// 中间操作
    Stream<Employee> stream = employees.stream()
    	.filter((e)->{
    	System.out.println("中间操作");
    	return e.getAge()>35;
   	 });
    // 终止操作
//		stream.forEach(System.out::println);
	}
```

​	当**未执行**终止操作时，中间操作也**不执行**。

---

```java
public static void test2() {
	// 中间操作
    // 内部迭代：迭代器由：Stream API 完成
	Stream<Employee> stream = employees.stream()
			.filter((e)->{
				System.out.println("中间操作");
				return e.getAge()>35; // 过滤
			});
	// 终止操作
	stream.forEach(System.out::println);
    /*打印：
        中间操作
        中间操作
        Employee [name=李四, age=38, salary=5555.0]
        中间操作
        Employee [name=王五, age=50, salary=3333.0]
        中间操作
        中间操作
    */
}
```
#### -	limit

```java
public static void test2() {
	employees.stream()
		.filter((e)->{
			System.out.println("断路");
			return e.getAge() > 35;
		})
		.limit(2)  // 限制2条，当满足条件，则不继续过滤
		.forEach(System.out::println);
}
/*
打印结果：
    断路
    断路
    Employee [name=李四, age=38, salary=5555.0]
    断路
    Employee [name=王五, age=50, salary=3333.0]
*/
```
#### -	skip(n)

```java
public static void test2() {
	employees.stream()
		.filter((e)->{
			return e.getSalary() > 3500;
		})
		.skip(2) // 跳过前两个
		.forEach(System.out::println);
}
/*
打印结果：
    Employee [name=赵六, age=17, salary=6666.0]
	Employee [name=电七, age=16, salary=7777.0]
*/
```
#### -	distinct

​	**实体类**，**必须重写：**hashCode 和 equals方法

```java
@Override
public int hashCode() {
	final int prime = 31;
	...
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	...
}
```
**更改数据**

```java
employees = Arrays.asList(
    new Employee("张三",18,9999.0),
    new Employee("李四",38,5555.0),
    new Employee("王五",50,5555.0),
    new Employee("电七",17,7777.0),
    new Employee("电七",17,7777.0)
);
```

```java
public static void test2() {
	employees.stream()
		.filter((e)->e.getSalary() > 5000);
		.skip(2) // 跳过前两个
		.distinct() // 去重，必须完全一致
		.forEach(System.out::println);
}
/*
打印结果：
    Employee [name=王五, age=50, salary=5555.0]
    Employee [name=电七, age=17, salary=7777.0]
*/
```
### （5）映射-中间操作

#### -	map

​	接收：Lambda，将**元素转换成**：其他形式或提取信息。接收：一个函数作为参数，该函数会**被应用到每个元素上**，并将其**映射成一个新的元素**。

```
eg:{{aaa},{bbb},{ccc}}
map：将数组的每一个元素加入到流中，例如：{aaa}
```

![](/../images/map映射.png)

---

#### -	flatMap

​	接收：一**个函数**作为参数，将流中的**每个值都换成另一个流**，然后把所有流**连接成一个流**。

```
eg:{{aaa},{bbb},{ccc}}
flatmap：将数组的每一个元素流的元素加入到流中，例如：{aaa}——>{a,a,a}
```

---

```java
List<String> list = Arrays.asList("aaa","bbb","ccc","ddd");
Stream<Character> sm = list.stream()
		.flatMap(Main::filterCharater); //{a,a,a},{b,b,b}
sm.forEach(System.out::println);
/*
打印结果：
    a
    a
    a
    b
    b
    ...
*/
```

```java
public static Stream<Character> filterCharater(String str) {
    List<Character> list = new ArrayList<>();
   	for(Character ch : str.toCharArray()) {
    	list.add(ch);
    }
    return list.stream();
}
```

### （6）排序-中间操作

#### -	自然排序（sorted）

```java
List<String> list = Arrays.asList("aaa","bbb","ccc","ddd");
list.parallelStream()
	.sorted()  // 自然排序
	.forEach(System.out::println);
/*
打印结果：
   	ccc
    aaa
    ddd
    bbb
*/
```

#### -	定制排序（sorted(Comparator com）

```java
public class Main {
	static List<Employee> employees;
	public static void main(String[] args) {
		employees = Arrays.asList(
				new Employee("张三",18,9999.0),
				new Employee("李四",38,5555.0),
				new Employee("王五",50,5555.0),
				new Employee("电七",17,7777.0),
				new Employee("电七",17,7777.0)
		);
		employees.stream()
			.sorted((e1,e2)->{  // 定制排序
				if(e1.getAge()>e2.getAge()) {
					return e1.getName().compareTo(e2.getName());
				}else {
					return e1.getName().compareTo(e2.getName());
				}
			}).forEach(System.out::println);
		
	}
}
```

### （7）查询与匹配-终止操作

|   函数    | 作用                     |
| :-------: | ------------------------ |
| allMatch  | 检查是否匹配所有元素     |
| anyMatch  | 检查是否至少匹配一个元素 |
| noneMatch | 检查是否没有匹配所有元素 |
| findFirst | 返回第一个元素           |
|  findAny  | 返回当前流中的任意元素   |
|   count   | 返回流中元素的总个数     |
|    max    | 返回流中最大值           |
|    min    | 返回流中最小值           |

---

准备条件

```java
public class Employee {
	private String name;
	private int age;
	private double salary;
	private Status Status; // 枚举状态
	public enum Status{
		free,
		busy,
		vocation;
	}
```

```java
public class Main {
	static List<Employee> employees;
	public static void main(String[] args) {
		employees = Arrays.asList(
				new Employee("张三",18,9999.0,Status.free),
				new Employee("李四",38,5555.0,Status.busy),
				new Employee("王五",50,5555.0,Status.vocation),
				new Employee("电七",17,7777.0,Status.free),
				new Employee("电七",17,7777.0,Status.busy)
		);
```

#### -	allMatch

```java
boolean b1 = employees.stream()
				// 匹配 所有的元素状态是否都为：busy
			.allMatch((e)->e.getStatus().equals(Status.busy));
System.out.println(b1); // false
```

#### -	anyMatch

```java
boolean b2 = employees.stream()
				// 匹配 所有的元素状态是否有：busy
			.anyMatch((e)->e.getStatus().equals(Status.busy));
System.out.println(b2); // true
```

#### -	noneMatch

```java
boolean b3 = employees.stream()
				// 匹配 所有的元素是否没有：busy
			.noneMatch((e)->e.getStatus().equals(Status.busy));
System.out.println(b3); // false
```

#### -	findFirst

```java
// 获取排序后的第一个元素
Optional<Employee> op = employees.stream()
			.sorted((e1,e2)->Double.compare(e1.getSalary(), e2.getSalary()))
			.findFirst(); // 返回类型：Optional
System.out.println(op.get());
//Employee [name=李四, age=38, salary=5555.0, Status=busy]
```

#### -	findAny

```java
// 返回状态为：free的元素
Optional<Employee> op2 = employees.stream()
				.filter((e)->e.getStatus().equals(Status.free))
				.findAny();
System.out.println(op2.get());
// Employee [name=张三, age=18, salary=9999.0, Status=free]
```

#### -	count

```java
Long count = employees.stream()
				.count();
System.out.println(count); //5
```

#### -	max

```java
Optional<Employee> op3 = employees.stream()
				.max((e1,e2)->Double.compare(e1.getSalary(), e2.getSalary()));
System.out.println(op3.get());
//Employee [name=张三, age=18, salary=9999.0, Status=free]
```

#### -	min

```java
Optional<Double> op4 = employees.stream()
			.map(Employee::getSalary) // 先把所有对象的工资提取出来
			.min(Double::compare); // 在所有工资找最低
System.out.println(op4.get());	//	5555.0
```

### （8）reduce-规约

​	**概念：**reduce(T identity, BinaryOperator) / reduc(BinaryOperator)

​	简而言之，就是**将流中元素反复结合起来**，得到一个值。

| 方法                            | 作用                                                         |
| ------------------------------- | ------------------------------------------------------------ |
| reduce(T iden,BinaryOperator b) | 可以将流中元素**反复结合起来**，得到一个值<br />返回：T      |
| reduce(BinaryOperator b)        | 可以将流中元素**反复结合起来**，得到一个值<br />返回：Optional<T> |

---

```java
public class Main {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		Integer sum = list.stream()
            	// x：0，y：1（初始）
				.reduce(0, (x,y)->x+y);
		System.out.println(sum); //55
	}
```

![](/../images/规约1.png)

​	若**未指定：x**（起始值），可以返回为：null。

```java
Optional<Double> op = employees.stream()
		.map(Employee::getSalary)
		.reduce(Double::sum);
System.out.println(sum);
```

---

### （9）collect-收集

​		collecct——将流**转换为：其他形式**。接收一个Collector接口的实现，用于给**Stream中元素**做：**汇总**的方法。

```java
public static void init() {
	employees = Arrays.asList(
			new Employee("张三",18,9999.0,Status.free),
			new Employee("李四",38,5555.0,Status.busy),
			new Employee("王五",50,5555.0,Status.vocation),
			new Employee("电七",17,7777.0,Status.free),
			new Employee("电七",17,7777.0,Status.busy)
	);
}
```
---

#### -	转换List

```java
// 将流中的元素转换为：list
List<String> list = employees.stream()
    .map(Employee::getName) // 获取对象的名字存入流中
    .collect(Collectors.toList());
list.forEach(System.out::println);
打印：
    张三
    李四
    王五
    电七
    电七
```

#### -	转换Set

```java
// 将流中的元素转换为：Set
Set<String> set = employees.stream()
    .map(Employee::getName) // 获取对象的名字存入流中
    .collect(Collectors.toSet());
set.forEach(System.out::println);
打印：
    张三
    李四
    王五
    电七
    电七
```
#### -	转换HashSet

```java
// 将流中的元素转换为：HashSet
HashSet<String> hs = employees.stream()
	.map(Employee::getName) // 获取对象的名字存入流中
	.collect(Collectors.toCollection(HashSet::new));
 hs.forEach(System.out::println);
```

#### -	获取流中元素个数

```java
Long count = employees.stream()
			.collect(Collectors.counting());
System.out.println(count); // 5
```

#### -	求工资平均值

```java
Double avg = employees.stream()
			.collect(Collectors.
                     averagingDouble(Employee::getSalary));
System.out.println(avg); // 7332.6
```

#### -	工资总和

```java
Double sum = employees.stream()
				.collect(Collectors.
					summingDouble(Employee::getSalary));
System.out.println(sum);  //36663.0
```

#### -	最大值

```java
Optional<Employee> max = employees.stream()
				.collect(Collectors
                         .maxBy((e1,e2)->Double.compare(e1.getSalary(),e2.getSalary())));
System.out.println(max);
//打印：
//Optional[Employee [name=张三, age=18, salary=9999.0, Status=free]]
```

#### -	最小值

```java
Optional<Double> min = employees.stream()
				.map(Employee::getSalary)
				.collect(Collectors.minBy(Double::compare));
System.out.println(min.get()); // 5555.0
```

#### -	单级分组

```java
// 按状态分组查询，返回：map集合
Map<Status,List<Employee>> map = employees.stream()
			.collect(Collectors.groupingBy(Employee::getStatus));
System.out.println(map);
```

#### -	多级分组

```java
// 先按状态分组，再按年龄分组，返回：map集合
Map<Status,Map<String,List<Employee>>> map = employees.stream()
			.collect(Collectors.groupingBy(Employee::getStatus,Collectors.groupingBy((e)->{
				if(((Employee) e).getAge()<=35) {
					return "青年";
				}else {
					if(((Employee) e).getAge() <= 50) {
						return "中年";
					}else {
						return "老年";
					}
				}
          })));
//{free={青年=[Employee [name=张三, age=18, salary=9999.0, St....
```

#### -	summarizingDouble

```java
DoubleSummaryStatistics dss = employees.stream()
				.collect(Collectors.
                         summarizingDouble(Employee::getSalary));
System.out.println(dss);
/*
DoubleSummaryStatistics{count=5, sum=36663.000000, min=5555.000000, average=7332.600000, max=9999.000000}
*/
```

```java
System.out.println(dss.getMax()); // 直接获取最大值
```

### （10）Strean练习

```java
public class Test {
	static List<Employee> employees;
	public static void main(String[] args) {
		init();
		test1();
	}
	...
	public static void init() {
		employees = Arrays.asList(
				new Employee("张三",18,9999.0,Status.free),
				new Employee("李四",38,5555.0,Status.busy),
				new Employee("王五",50,5555.0,Status.vocation),
				new Employee("电七",17,7777.0,Status.free),
				new Employee("电七",17,7777.0,Status.busy)
		);
	}
}
```

---

#### -	求平方

```java
/*
*  给定一个数组列表，如何返回一个由每个数的平方构造的列表
*  eg：[1,2,3,4,5,6]  返回:[1,4,9,16,25,36]
*/
public static void test1() {
       Integer[] nums = new Integer[] {1,2,3,4,5,6};
       Arrays.stream(nums)
           .map((x)->x*x)
           .forEach(System.out::println);
}
```

#### -	求流中的元素个数

```java
/*
* 怎么样用map 和 reduce 方法数一数 流中有多少个Employee
*/
public static void test2() {
	Optional<Integer> count = employees.stream()
			.map((e)->1) // 永远为真
			.reduce(Integer::sum);
	System.out.println(count.get()); // 5
}
```

### （11）Stream综合练习

```java
// 交易员
public class Trader {
	private String name; // 姓名
	private String city; // 城市
```

```java
// 交易信息
public class Transaction {
	private Trader trader;
	private int date;
	private int price;
```

```java
public class TestTransaction {
	private static List<Transaction> transaction;
	public static void main(String[] args) {
		init();
		test5();
	}
	...
	public static void init() {
		Trader raoul = new Trader("Raoul","Cambridge");
		Trader mario = new Trader("Mario","Milan");
		Trader alan = new Trader("Alan","Cambridge");
		Trader brian = new Trader("Brian","Cambridge");
		
		transaction = Arrays.asList(
			new Transaction(brian,2011,300),
			new Transaction(raoul,2012,1000),
			new Transaction(raoul,2011,400),
			new Transaction(mario,2012,710),
			new Transaction(mario,2012,700),
			new Transaction(alan,2012,950)
		);
	}
```

---

#### - 找出2011年发生的所有交易，并按交易额排序（升序）		

```java
public static void test1() {
	transaction.stream()
		.filter((t)->t.getDate() == 2011)
		.sorted((t1,t2)->Integer.compare(t1.getPrice(), t1.getPrice()))
		.forEach(System.out::println);
	/*
	 * 	Transaction [trader=Trader [place=Brian, name=Cambridge], date=2011, price=300]
		Transaction [trader=Trader [place=Raoul, name=Cambridge], date=2011, price=400]

	*/
}
```

#### -	交易员都在那些不同的城市工作过

```java
public static void test2() {
    transaction.stream()
        .map((t)->t.getTrader().getCity())
        .distinct()
        .forEach(System.out::println);
    /*
		Brian
		Raoul
		Mario
		Alan
	*/
}
```

#### -	查找所有来自剑桥的交易员，并按姓名排序

```java
public static void test3() {
	transaction.stream()
		.filter((t)->t.getTrader().getCity().equals("Cambridge"))
		.map(Transaction::getTrader)
		.sorted((t1,t2) -> t1.getName().compareTo(t2.getName()))
		.distinct()
		.forEach(System.out::println);
}
Trader [place=Alan, city=Cambridge]
Trader [place=Brian, city=Cambridge]
Trader [place=Raoul, city=Cambridge]
```

#### -	返回所有交易员的姓名字符串，按字符顺序排序

```java
public static void test4() {
// 按字母排序
	String str = transaction.stream()
		.map((t)->t.getTrader().getName())
		.sorted()
		.reduce("",String::concat);
	System.out.println(str);
}
AlanBrianMarioMarioRaoulRaoul
```

#### -	有没有交易员是在米兰工作的

```java
public static void test5() {
		boolean b1 = transaction.stream()
			.anyMatch((t)->t.getTrader().getCity().equals("Milan"));
		System.out.println(b1);//true
	}
```

#### -	打印生活在剑桥的交易员的所有交易额

```java
public static void test6() {
		Optional<Integer> sum = transaction.stream()
			.filter((e)->e.getTrader().getCity().equals("Cambridge"))
			.map(Transaction::getPrice)
			.reduce(Integer::sum);
		
		System.out.println(sum.get()); //2650	
	}
```

#### -	所有交易中，最高的交易额是多少

```java
public static void test7() {
		Optional<Integer> max = transaction.stream()
				.map((t)->t.getPrice())
				.max(Integer::compare);
		System.out.println(max); //Optional[1000]
	}
```

#### -	找到交易额最小的交易

```java
public static void test8() {
		Optional<Transaction> op = transaction.stream()
				.min((t1,t2)->Integer.compare(t1.getPrice(),t2.getPrice()));
		System.out.println(op.get());
	}
Transaction [trader=Trader [place=Brian, city=Cambridge], date=2011, price=300]
```

---

## 5、并行流与顺序流

​	并行流概念：把一个内容分成多个数据块，并用不同的线程分别处理每个数据块的流。

---

```
// 并行流与顺序流之间的切换，Stream API
parallel()
sequential()
```

### （1）Fork/Join框架

​	适用场景：**数据量很大的**情况。

​	**简介：**在必要的情况下，将一个大任务，进行**拆分（fork）成若干个小任务**（拆到不可再分），再见一个个的小任务运算的**结果进行join汇总**。

![](/../images/Fork——join.png)

---

### （2）Fork/Join框架与传统线程池的区别

​	采用 ” **工作窃取** “ 模式（work-stealing）。

​	当执行新的任务时，可以将其**拆分分成更小的任务**执行，并将：小任务**添加到：线程队列**中，然后再**从一个随机线程的队列**中：**偷**一个并把它放在**自己的队列**中。

<img src="/../images/fork.png" style="zoom:67%;" />

---

​	相对于一般的线程池实现，`fork/join` 框架的优势体现在对其中包含的任务的处理方式。

---

​	在一般的线程池中，如果一个线程正在执行的任务**由于某些原因无法继续运行**，那么该线程会处于等待状态。

​	而`fork/join` 框架实现中，如果某个子问题由于**等待另外一个子问题的完成**，而无法继续运行，那么处理该子问题的线程会**主动寻找其他尚未运行的子问题**来执行。减少了线程的等待时间，提高了性能。

### （3）演示累加100亿

#### -	传统方式

**准备条件**

```java
// 继承RecursiveTask
public class ForkJoinCalculate extends RecursiveTask<Long> {
    /*
	 * 		serialVersionUID适用于Java的序列化机制。
	 * 		简单来说，Java的序列化机制是通过判断类的serialVersionUID来验证版本一致性的。
	 * 在进行反序列化时，JVM会把传来的字节流中的serialVersionUID与本地相应实体类的serialVersionUID进行比较，
	 * 如果相同就认为是一致的，可以进行反序列化，
	 * 否则就会出现序列化版本不一致的异常，即是InvalidCastException。
	 */
	private static final long serialVersionUID = 12345678997L;
	
	private long start; // 累加的起始数
	private long end; // 累加的终止数
	
	private static final long threshold = 10000; 
	// 构造器
	public ForkJoinCalculate(long start,long end) {
		this.start = start;
		this.end = end;
	}
	
	@Override
	protected Long compute() {
		long length = end - start;
		// 默认：0-10000的累加，若超过10000，则拆分
		if(length <= threshold ) {
			
			long sum = 0;
			
			for(long i= start;i<=end;i++) {
				sum+=i;
			}
			return sum;
		}else { // 超过10000，则进行拆分操作
			long middle = (start + end) / 2;//拆分一半
			// 获取左边的和
			ForkJoinCalculate left = new ForkJoinCalculate(start,middle);// 重新创建一个对象
			left.fork();//拆分子任务，同时压入：线程队列
			
			// 获取右边的和
			ForkJoinCalculate right = new ForkJoinCalculate(middle+1,end);
			right.fork();//拆分子任务，同时压入：线程队列
			
			// 合并值
			return left.join() + right.join();
		}
	}	
}
```

```java
public static void main(String[] args) {
	 test1();
	 test2();
}
```
---

​	顺序累加（**一次性累加完**）

```java
// 普通的for方法（耗费时间为：4082，数据量越大时间越多）
public static void test2() {
    Instant start = Instant.now();//获得运行此刻的时间

    long sum = 0L;
    for(long i=0;i<=10000000000L;i++) {
        sum += i;
    }
    System.out.println(sum);

    Instant end = Instant.now();//获得运行此刻的时间
    System.out.println("耗费时间为："+
                       Duration.between(start, end).toMillis());
}
```

---

​	**拆分**累加，再汇总

```java
// 使用fork拆分累加（耗费时间为：4239，数据量越大时间越少）
public static void test1() {
    Instant start = Instant.now();//获得运行此刻的时间
    ForkJoinPool  pool = new ForkJoinPool();
    // 从0开始，累加：100亿
    ForkJoinTask<Long> task = new ForkJoinCalculate(0,10000000000L);
    //每个子任务处理之后会带一个返回值,最终所有的子任务的返回结果会join(合并)成一个结果.
    Long sum = pool.invoke(task);//提交子任务

    System.out.println(sum);

    Instant end = Instant.now();//获得运行此刻的时间
    System.out.println("耗费时间为："+
                       Duration.between(start, end).toMillis());
}
```

---

#### -	JAVA 8 并行流	

```java
/*
* java 8 顺序流转 ：并行流
*/
public static void test3() {
// rangeClosed生成一个连续的数，0累加到100亿
	long sum = LongStream.rangeClosed(0, 10000000000L)
		.parallel() // 顺序流 转为：并行流
		.reduce(0,Long::sum);
	System.out.println(sum);
}
```

## 6、接口中的默认方法与静态方法

### （1）默认方法

自 JAVA 8之后，**允许接口有默认的方法** 或 静态方法：

```java
public interface MyFun {
	default String getName() {
		return "MyFun";
	}
}
```

---

问题：若**接口和类**都有相同的方法和实现，优先级如何：

---

```java
public class MyClass {
	public String getName() {
		return "MyClass";
	}
}
```

```java
// 继承MyClass和实现MyFun
public class SubClass extends MyClass implements MyFun{
	
}
```

```java
public static void main(String[] args) {
		SubClass sc = new SubClass();
		System.out.println(sc.getName());//MyClass
}
```

---

### （2）接口方法优先级

​	若一个接口中定义了一个默认的方法，而另外一个**父类或接口中**又**定义了一个同名的方法**时：

- **接口与类：** **默认选择父类**的方法。如果一个父类提供了具体的实现，那么**接口中**具有相同名称和参数的默认方法：**被忽略**。

```java
public class SubClass extends MyClass implements MyFun
```

- **接口与接口**：冲突。如果一个父接口提供了一个默认方法，而另一个接口也提供了一个具有相同名称和参数列表的方法（**不管是否为默认方法**），那么必须**覆盖该方法来解决：冲突**。

```java
// 冲突，必须指定实现那个接口的方法
public class SubClass  implements MyFun,MyFun2..
```

---

### （3）静态方法

```java
public interface MyFun {
	public static void show() {
		System.out.println("接口的static方法");
	}
}
```

## 7、新时间日期API

​	`LocalDate`、`LocalTime`、`LocalDateTime`类的实例是：**不可变的对象**，分别表示使用：`ISO-8601`日历系统的日期、时间、日期和时间。它们提供了简单的日期或时间，并**不包含**当前的时间信息，**也不包含**与时区相关的信息。

---

### （1）Instant

​		**时间戳：**以Unix元年：**1970年1月1日：00:00:00** 到某个时间之间的**毫秒值**

```java
Instant ins1 = Instant.now();
System.out.println(ins1);//2021-04-18T13:02:07.250Z

// 增加小时
OffsetDateTime odt = ins1.atOffset(ZoneOffset.ofHours(8));
System.out.println(odt);//2021-04-18T21:02:07.250+08:00

//1618751002849
System.out.println(ins1.toEpochMilli());

// 增加60秒
Instant ins2 = Instant.ofEpochSecond(60);
System.out.println(ins2);//1970-01-01T00:01:00Z
```

### （2）LocalDate

```java
// 获取本地当前的时间
LocalDateTime ldt = LocalDateTime.now();
System.out.println(ldt); //2021-04-18T20:41:16.624

// 指定时间
LocalDateTime ldt1 = LocalDateTime.of(2021,10,19,13,22,33);
System.out.println(ldt1);//2021-10-19T13:22:33

// 日期运算
LocalDateTime ldt2 = ldt.plusYears(2); //加两年，返回：新实例
System.out.println(ldt2);//2023-04-18T20:47:18.118

LocalDateTime ldt3 = ldt.minusYears(2);//减两年，返回：新实例
System.out.println(ldt3);//2019-04-18T20:47:18.118

System.out.println(ldt.getYear()); //2021
System.out.println(ldt.getMonthValue());//4
System.out.println(ldt.getSecond());//5
```
### （3）Duration

​		Duration：计算两个“**时间”之间的间隔**

```java
Instant ins1 = Instant.now();

try {
	Thread.sleep(1000);
} catch (InterruptedException e) {
	e.printStackTrace();
}

Instant ins2 = Instant.now();
// Duration：计算两个“时间”之间的间隔
Duration duration = Duration.between(ins1,ins2);
System.out.println(duration.toMillis()); //1000
```

### （4）Period

​	Duration：计算两个“**日期”之间的间隔**

```java
LocalDate ld1 = LocalDate.of(2015,1,1);
LocalDate ld2 = LocalDate.now();

Period period = Period.between(ld1, ld2);
System.out.println(period); //P6Y3M17D

System.out.println(period.getYears());//6
System.out.println(period.getMonths());//3
System.out.println(period.getDays());//17
```

### （5）TemporalAdjuster

​	**TemporalAdjuster：**时间校正器。例：将日期调正到“下个周日”等操作。

​	**TemporalAdjusters：**该类通过静态方法，提供了大量的常用TemporalAdjuster的实现。

---

```java
// 获取下个周日
LocalDate nextSunday = LocalDate.now().with(
	TemporalAdjusters.next(DayOfWeek.SUNDAY);
);
```

```java
// 自定义：下一个工作日
LocalDateTime ld3 = ld1.with((L)->{
    LocalDateTime ldt2 = (LocalDateTime) L;

    DayOfWeek dow = ldt2.getDayOfWeek(); //获取周日
    System.out.println(dow); //SUNDAY

    if(dow.equals(DayOfWeek.FRIDAY)) {
        return ldt2.plusDays(3);
    }else {
        if(dow.equals(DayOfWeek.SATURDAY)) {
            return ldt2.plusDays(2);
        }else {
            return ldt2.plusDays(1);
        }
    }
});
System.out.println(ld3);//2021-04-19T21:29:01.176
```

### （6）DateTimeFormatter	

​		作用：**格式化时间/日期**

```java
// 日期格式化，可以使用：自带的
DateTimeFormatter dtf1 = DateTimeFormatter.ISO_DATE;
LocalDateTime ldt = LocalDateTime.now();

String strDate = ldt.format(dtf1);
System.out.println(strDate); // 2021-04-18

// 自定义格式
DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss");
String strDate2 = dtf2.format(ldt);
System.out.println(strDate2);//2021年04月18日 21:39:33

// 字符串 转 日期
LocalDateTime  newDate = ldt.parse(strDate2,dtf2);
System.out.println(newDate); //2021-04-18T21:40:18
```

### （7）ZoneDate

​		作用：**设置 某个 时区 的时间**

```java
// 获取全部的时区
Set<String> set = ZoneId.getAvailableZoneIds();
set.forEach(System.out::println);
/*
* 	Australia/Darwin
	Asia/Khandyga
	Asia/Kuala_Lumpur
	...
*/
// 获取指定的地区的时效
LocalDateTime ldt = LocalDateTime.now(ZoneId.of("Europe/Tallinn"));
System.out.println(ldt); //2021-04-18T16:48:33.890

ZonedDateTime zdt = ldt.atZone(ZoneId.of("Asia/Shanghai"));
System.out.println(zdt); //2021-04-18T16:49:40.016+08:00[Asia/Shanghai]
```

## 8、Optional

​	作用：**最大减少**空指针异常。

```
Optional容器类的常用方法：
（1）Optional.of(T t)：创建一个Optional实例
（2）Optional.empty()：创建一个空的Optional实例
（3）Optional.ofNullable(T t)：若t不为null，创建Optional实例，否则创建：空实例
（4）isPresent()：判断是否包含值
（5）orElse(T T)：如果调用对象包含值，返回该值，否则返回：T
（6）orElseGet(Supplier s)：如果调用对象包含值，返回该值，否则返回：s获取的值
（7）map(Function f)：如果有值，对其处理，并返回处理后的Optional，否则返回Optional.empty()
（8）flatMap(Function mapper)：与map类似，要求返回值必须是：Optional
```

---

```java
public static void test1(){
	// 空指针异常，可以快速定位：空异常的位置
	Optional<Employee> op = Optional.of(null);
    // 若需要null对象，可以使用：empty()
	Employee emp = op.get();
	System.out.println(emp);
}
```

---

**例题：**判断一个男人心中是否有：女神

**普通写法：**if...else判断

---

JAVA 8 **使用Optional：**

```java
// 女神类：Godness.class
public class Godness{
	private String name;
	...
}
```

```java
// 新男人类：NewMan.class
public class NewMan{
// 若心中木有女神，则默认：null，使用Optional.empty，避免null异常
	private Optional<Godness> godness = Optional.empty();
    ...
}
```

**测试：**

```java
public static void test2){
	// Optional.ofNullable方法构建对象
	Optional<NewMan> op = Optional.ofNullable(null);
	String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<NewMan> op = Optional.ofNullable(new NewMan());
	String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<Godness> gn = Optional.ofNullable(null);
    String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<Godness> gn = Optional.ofNullable(new Godness("涂老师"));
    String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：涂老师
}
```

```java
public static String getGodnessName(Optional<NewMan> man){
	return man.orElse(new NewMan())//若man为空，则创建
		.getGodness() // 获得女神，若未空，则orElse
		.orElse(new Godness("白"))// 默认女神名字
		.getName();// 获得女神名字
}
```

# 五、Java进阶篇

## 1、初始化顺序

### （1）测试单类的初始化顺序

```java
public class Main {

	public static String staticField = "static field";
	
	static {
		System.out.println(staticField);
		System.out.println("static Block");
	}
	
	private String field = "member field";
			
	/*
	 * 非静态初始块
	 * 	1.普通代码块：直接在{ }中出现的
		2.构造代码块：在构造函数的{}中出现的
		3.静态代码块：static{}中出现的
		4.同步代码块：多线程中出现
	 * 
	 */
	{
		System.out.println(field);
		System.out.println("non-static block");
	}
	
	public Main() {
		System.out.println("constructor");
	}
	
	public static void main(String[] args) {
		new Main();
	}
}
```

```
static field
static Block
member field
non-static block
constructor
```

结论：

​		**静态变量 > 静态初始块 > 成员变量 > 非静态初始块 > 构造器**

---

### （2）类继承的初始化顺序

​	**父类**的静态**变量**和静态**初始块**肯定是**先于子类**加载的，但是同一级别的类，在**类中的顺序**就**决定了它们的初始化顺序**，**而不是变**量**一定会优先于**初始块。

```java
public class Parent {
	private static String parentStaticField = "parent static field";
	
	static {
		System.out.println(parentStaticField);
		System.out.println("parent static block");
	}
	
	private String parentField = "parent member field";
	
	{
		System.out.println(parentField);
		System.out.println("parent non-static block");
	}
	
	public Parent() {
		System.out.println("parent constructor");
	}
}
```

```java
private static String childStaticField = "child static field";
	
	static {
		System.out.println(childStaticField);
		System.out.println("child static block");
	}
	
	private String childField = "child member field";
	
	{
		System.out.println(childField);
		System.out.println("child non-static block");
	}
	
	public Child() {
		System.out.println("child constructor");
	}
	
	public static void main(String[] args) {
		new Child();
	}
}
```

```
parent static field
parent static block
child static field
child static block
parent member field
parent non-static block
parent constructor
child member field
child non-static block
child constructor
```

结论：

**父类静态变量 > 父类静态初始块 > 子类静态变量 > 子类静态初始块 > 父类成员变量 > 父类非静态初始块 > 父类构造器 > 子类成员变量 > 子类非静态初始块 > 子类构造器**

---

## 2、



## 3、

