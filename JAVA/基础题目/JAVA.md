---
typora-root-url: images
---

# JAVA

## 不使用的对象应手动赋值为：NULL

## 1、

## 2、创建对象内存分析

​	第一步：**方法区中**会初始化所有的类属性和方法；

​	第二步：根据方法的优先级依次执行，例如：执行main()方法；

​	第三步：若方法中出现了：**new** 关键字，则在**堆中分配**需要创建对象的内存空间，初始化为默认值，其次将**常量池的值**赋予它。

​	第四步：在main()方法中，使用**对应对象的引用变量名**   **指向堆中**对应的对象的内存地址。

<img src="/../images/对象的创建过程.png" style="zoom: 80%;" />

---

​	注意：**栈中引用的对象**。也就是说：只要堆中的这个对象，在栈中还存在引用，就会被认定是：存活的。

## 3、字符串转换整数

```java
str.charAt(0)
```

## 4、装箱和拆箱

```java
Integer i01 =  59 ;
int  02 =  59 ;
Integer i03 =Integer.valueOf(59);
Integer i04 = new Integer(59);
```

## 5、Java多态实现条件

​	Java实现多态有三个必要条件：继承、重写、向上转型。

## 6、集合介绍

​	Java集合大致可以分为Set、List、Queue和Map四种体系：

- 其中Set代表无序、不可重复的集合；
- List代表有序、重复的集合；
- 而Map则代表具有映射关系的集合；
- Queue体系集合，代表一种队列集合实现。

## 7、Java运算符>>与>>>区别详解

​	>>：带符号右移（相当于除以2）

​	>>>：无符号右移

## 8、关键字

###     （1）transient

​			让某些**被transient关键字修饰的成员属性变量   不被序列化**。

###     （2）instanceof

​		instanceof运算符

​				**前一个操作符是：一个引用变量；**   **后一个操作数通常是：一个类（可以是接口）**

​			   用于判断**前面的对象  是否是   后面的类，或者其子类、实现 类的实例**。如果是返回true，否则返回false。

  	**在判断某个类（接口也可以看成一个特殊的类）的对象是不是   其他类（或接口）的实例，一定要首先进行向上转型，然后才可用instanceof关键字进行判断，这是基本操作规范。**

```java
interface A{
    void say();
}
class B implements A{ 
    public void say()
    {
        System.out.println("B实现的say()方法");
    }
}
class C implements A{
    public void say()
    {
        System.out.println("C实现的say()方法");
    }
}
 
public class TestDemo{
    public static void main(String[] args) {
	    A a= new B();  //接口不能new
	    System.out.println(a instanceof B);   //true;发生了A a= new B();
        System.out.println(a instanceof C);   //false;没有发生A a = new C(); 没有向上转型     
    }
}
```

​	注意：且instanceof左边操作元显式**声明的类型**与右边操作元必须是**同种类**或右边是左边父类的继承关系 

​	若**左操作数为null**， 结果就直接返回false

```java
//false;这是instanceof 特 有 的 规 则 ： 若左操作数为null， 结果就直接返回false， 不再运算右操作数是什么类。
boolean b5 = null instanceof String; 
```

 	 instanceof操作符：**只能用作对象的判断**

```java
//编译不通过;'A'在此处视为基本数据类型char，instanceof操作符只能用作对象的判断
boolean b4 = 'A' instanceof Character; 
```

###     

##     9 Integer的范围

```java
public class Main {
    public static void main(String[] args) {
        Integer i1 = 100;
        Integer i2 = 100;
        Integer i3 = 200;
        Integer i4 = 200;
         
        System.out.println(i1==i2);	 	//true
        System.out.println(i3==i4);		//false
    }
}
```

​	输出结果表明**i1和i2**指向的是：同一个对象；
​	而**i3和i4**指向的是：不同的对象。

```java
//Integer的valueOf方法的具体实现：
public static Interger valueOf(int i){
    if(i>=-128&&i<=IntergerCache.high){
        return IntergerCache.cache[i+128];
    }else{
        return new Interger(i);
    }
```

​	通过**valueOf**方法创建Integer对象的时候，如果数值在**[-128,127]**之间，便**返回指向**IntegerCache.cache中**已经存在的对象**的引用；**否则**创建一个新的Integer对象。

## 10、清理StringBuffer

```java
//方法一：使用delete方法
sb.delete(0,sb.length());
System.out.println(sb.toString());

//方法二： 使用setLength方法
sb.append("abc");
sb.append("def");
System.out.println(sb.toString());
sb.setLength(0);
System.out.println(sb.toString());

//方法三： 新的引用对象，指向一个空
sb.append("555");
sb.append("666");
System.out.println(sb.toString());
sb = new StringBuffer();
System.out.println(sb.toString());
```

## 11、删除列表的元素

```java
List<Integer> list = new ArrayList<Integer>();
list.add(1);
list.add(2);
list.add(3);
list.add(4);
```

**for循环删除（成功删除）：**

```java
for(int i=0;i<list.size();i++) {
	System.out.println(list.get(i));
	list.remove(i+1);
}
```

**foreach删除（删除失败）：**

```java
for(Integer i:list) {
	list.remove(1);// 删除下标为：1
}
```

> at java.util.ArrayList$Itr.next(Unknown Source)

**Iterator删除：**

```java
Iterator<Integer> it = list.iterator();
while(it.hasNext()) {
	//迭代器删除，必须先获取元素，才可以删除
	it.next();
	it.remove();
}
```

> 可以看出，两个操作都会影响元素的个数。 
>
>   当我们使用迭代器或 foreach 遍历时，如果你在 foreach 遍历时，自动调用迭代器的迭代方法，此时在遍历过程中调用了集合的add,remove方法时，modCount就会改变，而迭代器记录的modCount是开始迭代之前的，如果两个不一致，就会报异常，说明有两个线路（线程）同时操作集合。这种操作有风险，为了保证结果的正确性， 避免这样的情况发生，一旦发现modCount与expectedModCount不一致，立即报错。
>
> 　 此类的 iterator 和 listIterator 方法返回的迭代器是快速失败的：在创建迭代器之后，除非通过迭代器自身的 remove 或 add 方法从结构上对列表进行修改， 否则在任何时间以任何方式对列表进行修改， 迭代器都会抛出 ConcurrentModificationException。 因此，面对并发的修改，迭代器很快就会完全失败， 而不是冒着在将来某个不确定时间发生任意不确定行为的风险。

# JAVA链表

## 1、Node类

```java
public class Node {
	public Node next;	//指针（在java中，存的是一个对象）
	public int data;	// 数据域
    public Node(){
        
    }
    public Node(int data){
        this.data = data;
    }
}
```

## 2、单链表的创建

> **注意：**JAVA中是没有头结点的，若需要头结点，则需要自己添加
>
>  Node result = new Node(0);  // 创建一个头结点，data：0
>  result.next = node;    //   result为头结点，是next指向：node链表

```java
public static int i=0; 
```

> 尾插法

```java
public static void create_tree(Node tn,int nums[],int nums_length) {
		while(i<nums_length) {
			Node t1 = new Node();//创建新的结点
			t1.data = nums[i];//设置数据域
			t1.next = null; //next域为：null
			tn.next = t1; // 使末尾结点的next域指向当前结点
			i++;
			create_tree(t1,nums,nums_length);  // 递归创建
		}
	}
```

> 头插法

```java
public static void beforeInsert(Node tn,int nums[],int nums_length) {
		while(i<nums_length) {
			Node t1 = new Node();
			t1.data = nums[i];
			t1.next = tn.next; // 指向首节点
			tn.next = t1; //更新首节点
			i++;
			beforeInsert(t1,nums,nums_length);
		}
	}
```

## 3、哨兵节点：删除链表倒数第N个节点

​	在链表的题目中，十道有九道会用到**哨兵节点**，哨兵节点，其实就是一个**附加在原链表最前面用来简化边界条件的附加节点，它的值域不存储任何东西，只是为了操作方便而引入。**

​	比如原链表为：`a->b->c`，则加了哨兵节点的链表即为：`x->a->b>c`，如下图：

![](/../images/头结点.png)

> ​	那我们为什么需要**引入哨兵节点**呢?
>
> ​	举个例子，比如我们要删除某链表的第一个元素，**常见的删除链表的操作是找到要删元素的前一个元素**，假如我们记为 pre。我们通过：**pre.Next = pre.Next.Next**，但是此时若是删除第一个元素的话，就很难进行。

---

```
给定一个链表: 1->2->3->4->5, 和 n = 2.
当删除了倒数第二个节点后，链表变为 1->2->3->5.
```

> **思路分析**

​	首先我们思考，让我们删除倒数第N个元素，那我们**只要找到倒数第N个元素就可以了**，那怎么找呢？

​	我们**只需要设置两个指针变量，中间间隔N-1元素。当后面的指针遍历完所有元素指向nil时，前面的指针就指向了我们要删除的元素。**如下图所示：

<img src="/../images/删除链表.png" style="zoom:67%;" />

---

​	接下来，我们只要**同时定位**到要**删除的元素 **  和 它的**前1个元素**，通过前面讲过的删除操作，就可以很顺利的完成这道题目啦。

> ## 解题过程

> 1）首先我们**定义好哨兵节点resul**t，指向哨兵节点的目标元素指针left，以及目标指针left的前一个指针pre，此时pre指向null。
> 2）接下来我们开始遍历整个链表。
> 3）当right移动到距离目标元素left的**距离为N-1时**，同时开始移动cur。
> 4）当**链表遍历完之后**，此时right指向null，这时的left就是我们要找的待删除的目标元素。
> 5）最后我们通过pre.Next = pre.Next.Next完成删除操作，就完成了整个解题过程。

<img src="/../images/链表思路.png" style="zoom:67%;" />

```java
public static Node removeNthFromEnd(Node node, int n) {
		// 自己创建一个头结点（data：0），使链表成为：头结点——>1,2,3,4,5,6 或 构建单链表的时候添加头结点
        Node result = new Node(0);
        result.next = node; //result为头结点
        Node pre = null;  // 删除结点的前驱结点
        Node left = result; // 左指针
        Node right = node; // 右指针
        int foot = 1;
        // 当右指针为：null，则删除结点的位置为：left（左指针）
        while (right != null) {
            if (foot >= n) { // 当右指针与左指针之间间隔为：n-1个元素后，左指针开始移动
                pre = left;
                left = left.next;
            }
            right = right.next;
            foot++;
        }
        pre.next = pre.next.next; // 删除结点
        return result.next;
    }
```

# JAVA二叉树





# JAVA基础算法题

## 1、最大公约数和最小倍数

```java
/*
* 题目：输入两个正整数m和n
* 		最大公约数
*/
public static int Get_divisor(int m,int n) {
	if(m<0||n<0) {
		return -1;
	}
	if(n==0) {
		return m;
	}
	return Get_divisor(n,m%n);
}
```

```java
/*
 * 	最小公倍数
*/
public static int get_gcd(int m,int n) {
	while(true) {
		if((m=m%n)==0) {
			return n;
		}
		if((n=n%m)==0) {
			return m;
		}
	}
}
```

## 2、统计字符个数

> 输入一行字符，分别**统计**出其中英文字母、空格、数字和其它字符的个数。  

```java
public static void Get_char_number(String str) {
    int english_number = 0;  // 英文字母
    int space_number = 0; //空格
    int math_number = 0; // 数字
    int other_number = 0;// 其他字符
    char arr[] = str.toCharArray();
    int arr_index = arr.length;
    for(int i=0;i<arr_index;i++) {
    	if(arr[i]>=48&&arr[i]<=57) { // 数字
    		math_number++;
    		continue;
   		}
    	if((arr[i]>=65&&arr[i]<=90)||arr[i]>=97&&arr[i]<=122) { // 字母
    		english_number++;
    		continue;
    	}
    	if(arr[i]==' ') { //空格
    		space_number++;
    		continue;
    	}
    	other_number++; // 其他符号
    }

    	System.out.println("数字："+math_number +"\n 字符："+english_number+
    "\n 空格"+space_number +"\n 其他符号："+other_number);
    }
```

## 3、完数

> 一个数如果恰好等于它的因子之和，这个数就称为 "完数 "。例如：6=1＋2＋3.编程  找出1000以内的所有完数

```java
public static void find_prefect() {
    List<Integer> list = new ArrayList<Integer>();
    for(int i=1;i<=1000;i++) {
    		int s = 0;
    		for(int j=1;j<i;j++) {
    			if(i%j==0) { // 能整除则是因子
    				s+=j;
    			}
   			}
            if(i==s) { // 若是完数
            	list.add(i);
   		 	}
    }
    for(int i=0;i<list.size();i++) {
    	System.out.print(list.get(i)+" ");
    }
}
6 28 496 
```

## 4、无重复三位数

> 有1、2、3、4个数字，能组成多少个互不相同且**无重复数字的三位数**？都是多少？  

```java
public static void main(String[] args) {
	int nums[] = new int[] {1,2,3,4};
	int temp[] = new int[3];
		
	sort_number(nums,0,nums.length-1,temp);
}
```

```java
public static void sort_number(int nums[],int start,int end,int[] temp) {
		if(start == 2) { // 剪枝，但已经有三位数，则输出
			for(int i = 0;i <= start ;i++) {
				System.out.print(nums[i]+" ");
			}
			System.out.println();
		}else {
			for(int i = start; i <= end; i++) {
			swap(nums,start,i);
            sort_number(nums,start+1,end,temp);
			swap(nums,start,i);
		}
	}	
}
```

```java
public static void swap(int nums[],int start,int i) {
	int t = nums[start];
	nums[start] = nums[i];
	nums[i] = t;
}
```

## 5、非十进制转十进制

```java
String str = "1111.111";
float sum = count_ten(str,2);;
```

```java
/*
* 非十进制转十进制
* target：非十进制数，n：需要转换的进制
*/
public static float count_ten(String target,int n) {
	float sum = 0;
	if(target==null) {
		return 0;
	}
	String int_type = "";//记录整数部分
	String float_type = "";//记录小数部分
	if(target.indexOf(".")!=-1) {
		int_type = target.substring(0,target.indexOf("."));
		float_type = target.substring(target.indexOf(".")+1);
	}else {
		int_type = target;
	}
	char ch1[] = int_type.toCharArray(); 
	char ch2[] = float_type.toCharArray();

	sum = get_nums(ch1,ch2,n);  // 转换进制

	return sum;
}
```

```java
/*
*  进制计算，例如：1111.111
*  	ch1：整数部分：1111
*  	ch2：小数部分：111
*/
public static float get_nums(char ch1[],char ch2[],int n) {
	float sum = 0;
	int ch1_length = ch1.length;
	int ch2_length = ch2.length;
	// 整数部分计算
	for(int i=0 ; i < ch1_length ; i++) {
		sum += (ch1[i]-48) * get_int(n,i);
	}
    // 小数部分计算
	for(int i=0 ; i < ch2_length ; i++) {
		sum += (ch2[i]-48) * get_float(n,i);
	}
	return sum;
}
```

```java
// 计算数值的整数部分，类似：2,2*2,2*2*2,....，i：连乘的次数
public static int get_int(int n,int i) {
	int temp = 1;
	for(int j = 0; j < i ; j++) {
		temp *= n;
	}
	return temp;
}
```

```java
// 计算数值的小数部分，类似：1/2,1/2*1/2,1/2*1/2*1/2,....，i：连乘的次数
public static float get_float(int n,int i) {
	float temp = (float) 1.0;
	for(int j = 0; j <= i ; j++) {
		temp *= (1.0/n);
	}
	return temp;
}
```

## 6、求最长递增子串

```
/*
* 求最长递增子串
* 	例：ababc ——> abc
*/
```

```java
String str = "ababccabcde";
String max_str = get_max_str(str);
System.out.println(max_str);
```

```java
public static String get_max_str(String str) {
		if(str.length() == 0) {
			return null;
		}
		
		StringBuffer sb = new StringBuffer();
		int str_length = str.length();
		char[] ch = str.toCharArray();
		String max_str = ch[0]+"";  // 最大递增子串，默认长度：第一个元素
		
		sb.append(ch[0]);  // 初始默认递增长度为：ch[0]
		/*
		 * 循环判断；
		 * 	要注意：若递增字串一直到最后一个，那么，最后的递增字串无法比较，因为循环语句结束了
		 * 	解决办法：
		 * 		循环结束后再继续判断一下
		 */
		for(int i=1;i<str_length;i++) {
			if(ch[i]-ch[i-1]==1) {
				sb.append(ch[i]); // 若是递增，则添加
			}else {
				/*
				 * 若当前递增字串大于之前的字串，则更新
				 * 若两者相等，这里暂时不判断（默认不存在相等的递增字串）
				 */
				if(sb.toString().length()>max_str.length()) {
					max_str = sb.toString();
				}
				sb.setLength(0); // 清空sb
				sb.append(ch[i]); // 以当前元素为：起点
			}
		}
		if(sb.toString().length()>max_str.length()) {
			max_str = sb.toString();
		}
		return max_str;
	}
```

7、



# JAVA算题题

## 1、整数反转

分析：假设有1147483649这个数字，它是小于最大的32位整数2147483647的，但是将这个数字反转过来后就变成了9463847411，这就比最大的32位整数还要大了，这样的数字是没法存到int里面的，所以肯定要返回0(溢出了)。

<img src="/../images/java1.jpg" style="zoom:33%;" />

```java
	public int Reverse() {
		Scanner scan = new Scanner(System.in);
		int a = scan.nextInt();
		int res=0;
		while(a!=0) {
			int end = a%10;	//取尾数
			//判断是否 大于 最大32位整数，如果这个数字比7还大，说明溢出了
            if (res>214748364 || (res==214748364 && end>7)) {
                return 0;
            }
            //判断是否 小于 最小32位整数，是小于8
            if (res<-214748364 || (res==-214748364 && end<-8)) {
                return 0;
            }
			res = res*10+end;
			a = a/10;
		}
		System.out.println(res);
		return res;
	}
```

## 2、判断整数是否回文数

​	 **思路**：将整数**反转**，若反转后相同则是：回文数

```java
public static boolean Reverse(int x) {
		/*
		 *  思路：将整数反转，若反转后相同则是：回文数
		 */
		int b = x;// 记录a的值
		// 负数百分百不是：回文数
		if(x>=0) {
			// 0~9 也是：回文数
			if(x>=0&&x<=9) {
				return true;
			}else {
				int res = 0 ;
				while(x!=0) {
					int end = x%10;	// 取尾数
					res = res*10+end;
					x=x/10;
				}
				if(res==b) {
					return true;
				}
			}
		}
		return false;
	}
```

## 3、合并两个有序数组



## 4、N个数依次入栈，出栈顺序有多少种？

N 个元素的出栈个数记为：f（n），那么对于：1,2,3：

​	f（1） =  1	 #  1

​	f（2） =  2     #  12、21

​	f（3） =  5     #  123、132、213、321、231

---

```
f(4) = f(0)*f(3) + f(1)*f(2) + f(2) * f(1) + f(3)*f(0)  # 0 1 2 3 交叉相乘 3 2 1 0
```

----

```
f(n) = f(0)*f(n-1) + f(1)*f(n-2) + ... + f(n-1)*f(0)
```

## 5、两数之和

​	给定一个包括  n 个整数的**数组  nums**  和 **一个目标值  target**。
​	找出  nums  中的 **任意个数之和 等于 target**，并打印出所有结果的索引，**索引不能重复**。

```java
private static void find_index1(int[] nums,int target) {
		boolean mark[] = new boolean[nums.length]; // 标记数组
		int nums_length = nums.length;
		int count_result=1;
		for(int i=0;i<nums_length;i++) {
			if(nums[i]==target&&mark[i]==false) {
				System.out.println("结果"+(count_result++)+"为："+i);
				mark[i]=true;
			}else {
				for(int j=0;j<nums_length;j++) {
					if(nums[i]+nums[j]==target&&mark[i]==false) {
						System.out.print("结果+"+(count_result++)+"为：");
						System.out.println(i+" : "+j);
						mark[i]=true;
						mark[j]=true;
					}
				}
			}
		}
	}
```

## 6、日期操作

学习网址：https://blog.csdn.net/qq_42446156/article/details/111196098?utm_medium=distribute.pc_relevant.none-task-blog-baidujs_title-1&spm=1001.2101.3001.4242

```java
static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	public static void main(String[] args) throws ParseException {
		String str[][] = {
				{"2020-03-20,2020-03-22"},
				{"2020-05-20,2020-07-22"},
				{"2020-04-20,2020-05-22"},
				{"2020-08-20,2020-09-22"},
		};
		List<String> list = new ArrayList<String>();
		for(int i=0;i<str.length;i++) {
			for(int j=0;j<str[0].length;j++) {
				list.add(str[i][j]);
			}
		}
		String_date(list);	
	}
```

```java
private static void String_date(List<String> list) throws ParseException {
		List<String> free_date = new ArrayList<String>(); // 空余时间
		List<String> busy_date = new ArrayList<String>(); // 交叉时间
		Collections.sort(list);

		int list_length = list.size();

		int i=0;
		while(i<list_length-1) {
			String[] d1 = list.get(i).split(",");
			Date f_startdate = sdf.parse(d1[0]);
			Date f_enddate = sdf.parse(d1[1]);

			String[] d2 = list.get(i+1).split(",");
			Date e_startdate = sdf.parse(d2[0]);
			Date e_endtdate = sdf.parse(d2[1]);
			
			// 若前一个的结束日期小于下一个的开始时间，则：两者间有空余时间
			if(f_enddate.before(e_startdate)) {
				// 空余时间的开始日期
				Date d3 = new Date(f_enddate.getTime()+24*60*60*1000);
				// 空余时间的结束日期
				Date d4 = new Date(e_startdate.getTime()-24*60*60*1000);
				free_date.add(sdf.format(d3)+","+sdf.format(d4)); //添加空余时间
			}else {
				// 若当前前一个的结束日期大于下一个的开始时间，则两者间有交叉时间
				if(f_enddate.after(e_startdate)) {
					// 空余时间的开始日期
					Date d3 = new Date(f_enddate.getTime()-24*60*60*1000);
					// 空余时间的结束日期
					Date d4 = new Date(e_startdate.getTime());
					busy_date.add(sdf.format(d4)+","+sdf.format(d3)); //添加空余时间
				}
			}
			i++;
		}
		System.out.println(free_date.toString());
		System.out.println(busy_date.toString());
	}
```

## 7、N数之和（下标可重复）

​	此方法，结果不全，需要**先对数组进行排序**，为了**避免排序后的数组下标位置错乱**，可以使用`HashMap`集合的key和vlaue映射。

```java
// 主方法
public class N_Sum {
	private static int result_count = 0;
	public static void main(String[] args) {
		int[] nums = {1,5,8,17,29,33,39,11,16};
		int target =33;
		get_index(nums2,0,new int[nums.length],0,target);
	}
    ...
}
```

```java
	/*
	 * 此种方法结果不全，应先对数组进行排序
	 * current_Index：nums的下标
	 * middle_result：记录符号条件的数组下标值
	 */
	private static void get_index(int[] nums, int current_Index,
            int[] middle_result,int result_index,int target) {
		
		// 上一轮操作后，target的值为：0，说明前面的元素累加等于：target
		if(target == 0) { 
			System.out.print("结果为"+(++result_count)+"：");
			StringBuffer sb = new StringBuffer();
			int[] copy_nums = Arrays.copyOf(middle_result, result_index);
			for(int i : copy_nums) {
				System.out.print(i+",");
			}
			System.out.println();
			return;
		}
		// 若当前元素大于target，则直接判断下一个元素
		while(current_Index<nums.length&&nums[current_Index]>target) {
			current_Index++;
		}
		// 递归调用
	   while(current_Index<nums.length&&nums[current_Index]<=target) {
		   middle_result[result_index] = current_Index;
		   get_index(nums,current_Index+1,middle_result,result_index+1,target-nums[current_Index]);
		   current_Index++;
	   }
	}
```

结果：

```java
结果为1：1,3,7,
结果为2：3,8,
结果为3：5,
（缺失结果：0,1,7,8）
```

## 8、N数之和（下标不可重复）

参考：https://blog.csdn.net/dengz_j/article/details/114540948?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522161832685216780264062836%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=161832685216780264062836&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-1-114540948.first_rank_v2_pc_rank_v29&utm_term=%E7%BB%99%E5%AE%9A%E4%B8%80%E4%B8%AA%E5%8C%85%E6%8B%AC++n+%E4%B8%AA%E6%95%B4%E6%95%B0%E7%9A%84%E6%95%B0%E7%BB%84++nums++%E5%92%8C+%E4%B8%80%E4%B8%AA%E7%9B%AE%E6%A0%87%E5%80%BC++target%E3%80%82%E6%89%BE%E5%87%BA++nums++%E4%B8%AD%E7%9A%84+%E4%BB%BB%E6%84%8F%E4%B8%AA%E6%95%B0%E4%B9%8B%E5%92%8C+%E7%AD%89%E4%BA%8E+target%EF%BC%8C%E5%B9%B6%E6%89%93%E5%8D%B0%E5%87%BA%E6%89%80%E6%9C%89%E7%BB%93%E6%9E%9C%E7%9A%84%E7%B4%A2%E5%BC%95%EF%BC%8C%E7%B4%A2%E5%BC%95%E4%B8%8D%E8%83%BD%E9%87%8D%E5%A4%8D%E3%80%82

```java
public class N_Sum {
	private static int result_count = 0;
	public static void main(String[] args) {
		int[] nums = {1,5,8,17,29,33,39,11,16};
		int target =33;
		
		int[] map_key = new int[nums.length];//记录map的key值
		int map_key_index = 0;
		
		// 创建一个map，因为后续需要排序，为了避免下标混乱，使用key来存储下标值
		Map<Integer,Integer> map = new HashMap<Integer,Integer>();
		for(int i=0;i<nums.length;i++) {
			map.put(i, nums[i]);
		}
		// map转换成list进行排序
        List<Map.Entry<Integer, Integer>> list = new ArrayList<Map.Entry<Integer, Integer>>(map.entrySet());
        // 排序
        //然后通过比较器来实现排序
        Collections.sort(list,new Comparator<Map.Entry<Integer,Integer>>() {
            //升序排序
            @Override
            public int compare(Map.Entry<Integer, Integer> o1,
                               Map.Entry<Integer, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
 
        });
		// 遍历map，并获取对应key 存入map_key数组中
        for(Map.Entry<Integer,Integer> mapping:list){
        	map_key[map_key_index] = mapping.getKey();
        	System.out.println(map_key[map_key_index]);
        	map_key_index++;
         }
        
        get_index2(map,0,new int[nums.length],0,target,map_key,new boolean[nums.length]);
	}
    ...
}
```

```java
private static void get_index2(Map<Integer,Integer> maps, int current_Index,
            int[] middle_result,int result_index,int target,int[] map_key,boolean[] mark) {	
		// 上一轮操作后，target的值为：0，说明前面的元素累加等于：target
		if(target == 0) { 
			StringBuffer sb = new StringBuffer();
			int[] copy_nums = Arrays.copyOf(middle_result, result_index);
			// 判断当前符合的元素是否有被使用过
			for(int i : copy_nums) {
				if(mark[i]==false) {
					continue;
				}else {
					return;
				}
			}
			System.out.print("结果为"+(++result_count)+"：");
			for(int i : copy_nums) {
				System.out.print(i+",");
				mark[i] = true;
			}
			System.out.println();
			return;
		}
		// 若当前元素大于target 或者当前元素以及被使用，则直接判断下一个元素
		while(current_Index<maps.size()&&maps.get(map_key[current_Index])>target) {
			current_Index++;
		}
		// 递归调用，maps.get(map_key[current_Index])：用key访问value值
	   while(current_Index<maps.size()&&maps.get(map_key[current_Index])<=target) {
		   middle_result[result_index] = map_key[current_Index];
		   get_index2(maps,current_Index+1,middle_result,result_index+1,target-maps.get(map_key[current_Index]),map_key,mark);
		   current_Index++;
	   }
	}
```

结果：

```java
结果为1：0,1,7,8,
结果为2：5,
```

## 9、长方体最大体积

```
样例输入：6	结果：8（一定要是最大的）
------------------
长宽高都是2，体积为8
输入11，输出45，长宽高3，3，5
```

```java
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		long n  = scan.nextLong();
		long m = getMaxVolume(n);
		System.out.println(m);
	}
	 public static long getMaxVolume (long n) {
	     if(n<0) {
	    	 return 0;
	     }
         // 长、宽、高
	     long z=0, c=0, k=0;
	     long max_value = 0; // 最大值
         // 不确定有多少个满足条件的元素，使用list
	     List<Long> list = new ArrayList<Long>();
	     for(long i=2;i<n;i++) {
	    	 if(is_bool(i)) {
	    		 list.add(i);
	    	 }
	     }
	     Long pi[] = list.toArray(new Long[list.size()]);// 列表转为数组
	     for(int i=0;i<pi.length&&pi[i]<n;i++) {
	    	 for(int j=i;j<pi.length&&pi[i]+pi[j]<n;j++) {
	    		  z = pi[i];
	    		  c = pi[j];
	    		  k = n - z - c;
	    		  if(is_bool(k)) {
	    			  if(max_value>(z*c*k)) {
	    				  continue;
	    			  }else {
	    				  max_value = z * c * k;
	    			  }
	    		  }else {
	    			  continue;
	    		  }
	    	 }
	     }
		 return max_value;
	 }
	 // 质数判断
	 public static boolean is_bool(long a) {
		 if(a<=1) {
			 return false;
		 }
		 for(int i=2;i<a/2;i++) {
			 if(a%i==0) {
				 return false;
			 }
		 }
		 return true;
	 }
}
```

## 10、A在B的前面

```
样例：输入N和字母
3
A B C
输出：
A-B-C A-C-B C-A-B 3
```

```java
import java.util.Arrays;
import java.util.Scanner;
public class Sort_AB {
	private static int count=0;  // 计有多少种满足条件
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int n = scan.nextInt();
		String str_array[] = new String[n];
		for(int i=0;i<n;i++) {
			str_array[i] = scan.next();
		}
		int str_array_index[] = new int[str_array.length];
		for(int i=0;i<str_array_index.length;i++) {
			str_array_index[i]=i;
		}
		Q_sort(str_array,0,str_array_index.length-1);
		System.out.println(count);
	}
	public static void Q_sort(String arr[],int start,int end) {
		if(start==end) {
			int A_index=0;
            // 寻找A的位置
			for(int i=0;i<arr.length;i++) {
				if(arr[i].equals("A")) {
					A_index=i;
					break;
				}
			}
			for(int i=0;i<arr.length;i++) {
				if(arr[i].equals("B")) {
					// 若B 在 A的前面，则结束
					if(i<=A_index) {
						return;
					}
				}
			}
            // 若满足条件，则打印输出
			for(int i=0;i<arr.length;i++) {
				if(i==arr.length-1) {
					System.out.print(arr[i]+" ");
				}else {
					System.out.print(arr[i]+"-");
				}
			}
			count++;
		}else {
            // 全排列
			for(int i=start;i<=end;i++) {
                //1，2，3的全排列这块相当于将其中一个提了出来，下次递归从start+1开始
				swap(arr,start,i);
				Q_sort(arr,start+1,end);
                //这块是复原数组，为了保证下次另外的同级递归使用数组不会出错
                //这块可以通过树来理解，每次回退一步操作，交换回去
				swap(arr,start,i);
			}
		}
	}
    	// 交换
	    public static void swap(String[] array,int i,int j) {
	        String temp = array[i];
	        array[i] = array[j];
	        array[j] = temp;
	    }
}
```

## 11、移除元素

```java
/*
*  移除元素
*  要求：
*  	不可创建多的数组，返回一个int，为：去除目标值后的剩下的元素个数
*  思路：
*  	创建一个left指针，初始：0；
*  	将不是目标值的元素依次插入：原数组（nums[left++] = nums[i]），从下标0开始
*  	最终返回：left的大小（为剩余元素的最大长度）
*/
```

```
输入：nums = [3,2,2,3], val = 3
输出：2, nums = [2,2]
```

```java
public static int removeElement(int[] nums, int val) {
    int nums_len = nums.length;
    int left = 0;
    for(int i=0;i<nums_len;i++) {
    	if(nums[i]!=val) { // 如果当前元素不是val，则放入数组
    		nums[left++] = nums[i];
    	}
    }
    return left;
}
```

## 12、最长公共前缀

```
编写一个函数来查找字符串数组中的最长公共前缀。
如果不存在公共前缀，返回空字符串 ""。
---------------
输入：strs = ["flower","flow","flight"]
输出："fl"
```

```java
/*
*  最长公共前缀
*  思路：
*  	若字符串为0或为null，则直接返回:""
*  	默认最大公共部分为：strs[0]
*  	用每次获得公共部分，循环跟数组的每个元素比较
*  	若两者相比没有交集，则为："" 
*/
```

```java
public static String longestCommonPrefix(String[] strs) {
    if(strs.length==0 ||strs==null) {
        return "";
    }
    String max_Prefix = strs[0];// 最大公共部分，初始：第一个元素
    int strs_len = strs.length;
    for(int i=1;i<strs_len;i++) {
        max_Prefix = CommonPrefix(max_Prefix,strs[i]);
        if(max_Prefix.length()==0) { // 若公共部分为:0，则直接结束
            break;
        }
    }
    return max_Prefix;
}
```

```java
// 求两个字符串公共的部分
public static String CommonPrefix(String str1,String str2) {
    int length = Math.min(str1.length(), str2.length()); 
    int index = 0;//记录公共部分的长度
    while(index<length&&str1.charAt(index)==str2.charAt(index)) {
    	index++;
    }
    return str1.substring(0,index);
 }
```

## 13、股票的最大利润

> 假设把某股票的价格按照时间先后顺序存储在数组中，请问买卖该股票一次可能获得的最大利润是多少？

> ```
> 输入: [7,1,5,3,6,4]
> 输出: 5
> 解释: 在第 2 天（股票价格 = 1）的时候买入，在第 5 天（股票价格 = 6）的时候卖出，最大利润 = 6-1 = 5 。
>      注意利润不能是 7-1 = 6, 因为卖出价格需要大于买入价格。
> ```

> ```
> 输入: [7,6,4,3,1]
> 输出: 0
> 解释: 在这种情况下, 没有交易完成, 所以最大利润为 0。
> ```

```java
/*
思路：用栈来实现，然后每次入栈就判断一下：
		若栈顶的元素小于进栈的元素，则计算两者的差：利润值；
		若大于进栈元素，则出栈，将当前元素入栈，因为栈只有一个元素，故只需要一个变量
*/

public int maxProfit(int[] prices) {
     if(prices.length==0){
         return 0;
     }
     int prices_len = prices.length;
	 int min = prices[0];
	 int max_price = 0;
	 for(int i=1;i<prices_len;i++) {
		if(prices[i]<min) {
			min = prices[i];
		}else {
			int temp = prices[i] - min;
			if(temp>max_price) {
				max_price = temp;
			}
		}
	}
	return max_price;
}
```

