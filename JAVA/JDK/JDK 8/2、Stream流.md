---

typora-root-url: images
---

## 4、Stream API

### （1）简述

​	**概念：**为JAVA 8中处理集合的关键抽象概念，可以对**指定的集合**进行操作，可以执行非常复杂的：**查找、过滤和映射数据**等操作。

​	使用Stream API 对集合数据进行操作，**类似于**使用SQL执行的**数据库查询**。

​	简而言之，Steam API 提供了一种**高效且易于使用**的**处理数据的方式**。

---

<img src="/../images/stream流.png" style="zoom:67%;" />

[集合讲的是：数据；流讲的是：计算！！！]()

注意：

- Stream流自己**不会存储数据**

- Stream流**不会改变源对象**。相反，会**返回一个持有结果**的：新Stream流

- Stream流操作是**延迟执行**的。意味着要等到**需要结果的时候才执行**。

  （简而言之：**执行终止操作，才会执行：中间操作**）

---

### （2）Stream的操作

**创建Stream**

​	一个数据源（集合、数组等），**获取一个流**。

---

**中间操作**

​	一个中间操作链，对数据源的数据进行处理。

​	**多个中间操作**可以连接起来形成**一个流水线**，除非流水线上触发：终止操作，否则：中间操作不会执行任何的处理！！，而在**执行终止操作**的时候，**一次性全部处理**，称为：**惰性求值**。

|        方法         | 描述                                                         |
| :-----------------: | :----------------------------------------------------------- |
| filter(Predicate p) | 接收Lambda，从流中**排除**某些元素                           |
|      distict()      | **筛选**，通过流所生产元素的hashCode()和equals()去除重复元素。 |
| limit(long maxSize) | 截断流，使其元素**不超过**给定数量。                         |
|    skip(long n)     | **跳过**元素，返回一个：扔掉了前n个元素的流。<br/>若流中不足n个，则返回一个：空流。与limit(n)互补 |

---

**终止操作（终端操作）**

​	一个终止操作，执行中间操作链，并产生结果。

### （3）四种获取流的方式

#### -	list.stream

```java
/*
 *  第一种：
* 	可以通过：Collection系列集合提供的：
* 		Stream()或paralleleStream()
*/
List<String> list = new ArrayList();
Stream<String> stream1 = list.stream();
```

#### -	Arrays.stream

```java
/*
 *  第二种：通过Arrays中的静态方法：stream()获取数据流
 */
Employee[] emps = new Employee[10];
Stream<Employee> stream2 = Arrays.stream(emps);
```

#### -	Stream.of

```java
/*
*  第三种：通过Stream类中的静态方法：of()
*/
Stream<String> stream3 = Stream.of("aa","bb","cc");
```

#### -	Stream.iterate

```java
/*
* 第四种：创建无限流：iterate
* 	 例；从0开始，无线递增：2
*/
Stream<Integer> stream4 = Stream.iterate(0, (x)->x+2);
```

```java
stream4.forEach(System.out::println);// 遍历
// limit：中间操作
stream4.limit(10).forEach(System.out::println);// 遍历（限制10个）
```

```java
// 无线流，生成随机数，限制5个
Stream.generate(()->Math.random())
    .limit(5)
    .forEach(System.out::println);
```

### （4）筛选与切片-中间操作

​	注意：**执行：终止操作，才会执行：中间操作**

---

```java
/*
*   筛选与切片
* filter—————接收：Lambda，从流中排除某些元素；
* limit——————截断流，使其元素不超过给定数量
* skip(n)—————跳过元素，返回一个：扔掉了前n个元素的流。
* 			若流中不足n个，则返回一个：空流。与limit(n)互补
* distinct————筛选，通过流所生产元素的hashCode()和equals()去除重复元素
*/
```
准备工作：

```java
public class Main {
	static List<Employee> employees;
	public static void main(String[] args) {
		employees = Arrays.asList(
				new Employee("张三",18,9999.0),
				new Employee("李四",38,5555.0),
				new Employee("王五",50,3333.0),
				new Employee("赵六",17,6666.0),
				new Employee("电七",16,7777.0)
		);
	}
```

---

#### -	filter

```java
public static void test2() {
	// 中间操作
    Stream<Employee> stream = employees.stream()
    	.filter((e)->{
    	System.out.println("中间操作");
    	return e.getAge()>35;
   	 });
    // 终止操作
//		stream.forEach(System.out::println);
	}
```

​	当**未执行**终止操作时，中间操作也**不执行**。

---

```java
public static void test2() {
	// 中间操作
    // 内部迭代：迭代器由：Stream API 完成
	Stream<Employee> stream = employees.stream()
			.filter((e)->{
				System.out.println("中间操作");
				return e.getAge()>35; // 过滤
			});
	// 终止操作
	stream.forEach(System.out::println);
    /*打印：
        中间操作
        中间操作
        Employee [name=李四, age=38, salary=5555.0]
        中间操作
        Employee [name=王五, age=50, salary=3333.0]
        中间操作
        中间操作
    */
}
```
#### -	limit

```java
public static void test2() {
	employees.stream()
		.filter((e)->{
			System.out.println("断路");
			return e.getAge() > 35;
		})
		.limit(2)  // 限制2条，当满足条件，则不继续过滤
		.forEach(System.out::println);
}
/*
打印结果：
    断路
    断路
    Employee [name=李四, age=38, salary=5555.0]
    断路
    Employee [name=王五, age=50, salary=3333.0]
*/
```
#### -	skip(n)

```java
public static void test2() {
	employees.stream()
		.filter((e)->{
			return e.getSalary() > 3500;
		})
		.skip(2) // 跳过前两个
		.forEach(System.out::println);
}
/*
打印结果：
    Employee [name=赵六, age=17, salary=6666.0]
	Employee [name=电七, age=16, salary=7777.0]
*/
```
#### -	distinct

​	**实体类**，**必须重写：**hashCode 和 equals方法

```java
@Override
public int hashCode() {
	final int prime = 31;
	...
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	...
}
```
**更改数据**

```java
employees = Arrays.asList(
    new Employee("张三",18,9999.0),
    new Employee("李四",38,5555.0),
    new Employee("王五",50,5555.0),
    new Employee("电七",17,7777.0),
    new Employee("电七",17,7777.0)
);
```

```java
public static void test2() {
	employees.stream()
		.filter((e)->e.getSalary() > 5000);
		.skip(2) // 跳过前两个
		.distinct() // 去重，必须完全一致
		.forEach(System.out::println);
}
/*
打印结果：
    Employee [name=王五, age=50, salary=5555.0]
    Employee [name=电七, age=17, salary=7777.0]
*/
```
### （5）映射-中间操作

#### -	map

​	接收：Lambda，将**元素转换成**：其他形式或提取信息。接收：一个函数作为参数，该函数会**被应用到每个元素上**，并将其**映射成一个新的元素**。

```
eg:{{aaa},{bbb},{ccc}}
map：将数组的每一个元素加入到流中，例如：{aaa}
```

![](/../images/map映射.png)

---

#### -	flatMap

​	接收：一**个函数**作为参数，将流中的**每个值都换成另一个流**，然后把所有流**连接成一个流**。

```
eg:{{aaa},{bbb},{ccc}}
flatmap：将数组的每一个元素流的元素加入到流中，例如：{aaa}——>{a,a,a}
```

---

```java
List<String> list = Arrays.asList("aaa","bbb","ccc","ddd");
Stream<Character> sm = list.stream()
		.flatMap(Main::filterCharater); //{a,a,a},{b,b,b}
sm.forEach(System.out::println);
/*
打印结果：
    a
    a
    a
    b
    b
    ...
*/
```

```java
public static Stream<Character> filterCharater(String str) {
    List<Character> list = new ArrayList<>();
   	for(Character ch : str.toCharArray()) {
    	list.add(ch);
    }
    return list.stream();
}
```

### （6）排序-中间操作

#### -	自然排序（sorted）

```java
List<String> list = Arrays.asList("aaa","bbb","ccc","ddd");
list.parallelStream()
	.sorted()  // 自然排序
	.forEach(System.out::println);
/*
打印结果：
   	ccc
    aaa
    ddd
    bbb
*/
```

#### -	定制排序（sorted(Comparator com）

```java
public class Main {
	static List<Employee> employees;
	public static void main(String[] args) {
		employees = Arrays.asList(
				new Employee("张三",18,9999.0),
				new Employee("李四",38,5555.0),
				new Employee("王五",50,5555.0),
				new Employee("电七",17,7777.0),
				new Employee("电七",17,7777.0)
		);
		employees.stream()
			.sorted((e1,e2)->{  // 定制排序
				if(e1.getAge()>e2.getAge()) {
					return e1.getName().compareTo(e2.getName());
				}else {
					return e1.getName().compareTo(e2.getName());
				}
			}).forEach(System.out::println);
		
	}
}
```

### （7）查询与匹配-终止操作

|   函数    | 作用                     |
| :-------: | ------------------------ |
| allMatch  | 检查是否匹配所有元素     |
| anyMatch  | 检查是否至少匹配一个元素 |
| noneMatch | 检查是否没有匹配所有元素 |
| findFirst | 返回第一个元素           |
|  findAny  | 返回当前流中的任意元素   |
|   count   | 返回流中元素的总个数     |
|    max    | 返回流中最大值           |
|    min    | 返回流中最小值           |

---

准备条件

```java
public class Employee {
	private String name;
	private int age;
	private double salary;
	private Status Status; // 枚举状态
	public enum Status{
		free,
		busy,
		vocation;
	}
```

```java
public class Main {
	static List<Employee> employees;
	public static void main(String[] args) {
		employees = Arrays.asList(
				new Employee("张三",18,9999.0,Status.free),
				new Employee("李四",38,5555.0,Status.busy),
				new Employee("王五",50,5555.0,Status.vocation),
				new Employee("电七",17,7777.0,Status.free),
				new Employee("电七",17,7777.0,Status.busy)
		);
```

#### -	allMatch

```java
boolean b1 = employees.stream()
				// 匹配 所有的元素状态是否都为：busy
			.allMatch((e)->e.getStatus().equals(Status.busy));
System.out.println(b1); // false
```

#### -	anyMatch

```java
boolean b2 = employees.stream()
				// 匹配 所有的元素状态是否有：busy
			.anyMatch((e)->e.getStatus().equals(Status.busy));
System.out.println(b2); // true
```

#### -	noneMatch

```java
boolean b3 = employees.stream()
				// 匹配 所有的元素是否没有：busy
			.noneMatch((e)->e.getStatus().equals(Status.busy));
System.out.println(b3); // false
```

#### -	findFirst

```java
// 获取排序后的第一个元素
Optional<Employee> op = employees.stream()
			.sorted((e1,e2)->Double.compare(e1.getSalary(), e2.getSalary()))
			.findFirst(); // 返回类型：Optional
System.out.println(op.get());
//Employee [name=李四, age=38, salary=5555.0, Status=busy]
```

#### -	findAny

```java
// 返回状态为：free的元素
Optional<Employee> op2 = employees.stream()
				.filter((e)->e.getStatus().equals(Status.free))
				.findAny();
System.out.println(op2.get());
// Employee [name=张三, age=18, salary=9999.0, Status=free]
```

#### -	count

```java
Long count = employees.stream()
				.count();
System.out.println(count); //5
```

#### -	max

```java
Optional<Employee> op3 = employees.stream()
				.max((e1,e2)->Double.compare(e1.getSalary(), e2.getSalary()));
System.out.println(op3.get());
//Employee [name=张三, age=18, salary=9999.0, Status=free]
```

#### -	min

```java
Optional<Double> op4 = employees.stream()
			.map(Employee::getSalary) // 先把所有对象的工资提取出来
			.min(Double::compare); // 在所有工资找最低
System.out.println(op4.get());	//	5555.0
```

### （8）reduce-规约

​	**概念：**reduce(T identity, BinaryOperator) / reduc(BinaryOperator)

​	简而言之，就是**将流中元素反复结合起来**，得到一个值。

| 方法                            | 作用                                                         |
| ------------------------------- | ------------------------------------------------------------ |
| reduce(T iden,BinaryOperator b) | 可以将流中元素**反复结合起来**，得到一个值<br />返回：T      |
| reduce(BinaryOperator b)        | 可以将流中元素**反复结合起来**，得到一个值<br />返回：Optional<T> |

---

```java
public class Main {
	public static void main(String[] args) {
		List<Integer> list = Arrays.asList(1,2,3,4,5,6,7,8,9,10);
		Integer sum = list.stream()
            	// x：0，y：1（初始）
				.reduce(0, (x,y)->x+y);
		System.out.println(sum); //55
	}
```

![](/../images/规约1.png)

​	若**未指定：x**（起始值），可以返回为：null。

```java
Optional<Double> op = employees.stream()
		.map(Employee::getSalary)
		.reduce(Double::sum);
System.out.println(sum);
```

---

### （9）collect-收集

​		collecct——将流**转换为：其他形式**。接收一个Collector接口的实现，用于给**Stream中元素**做：**汇总**的方法。

```java
public static void init() {
	employees = Arrays.asList(
			new Employee("张三",18,9999.0,Status.free),
			new Employee("李四",38,5555.0,Status.busy),
			new Employee("王五",50,5555.0,Status.vocation),
			new Employee("电七",17,7777.0,Status.free),
			new Employee("电七",17,7777.0,Status.busy)
	);
}
```
---

#### -	转换List

```java
// 将流中的元素转换为：list
List<String> list = employees.stream()
    .map(Employee::getName) // 获取对象的名字存入流中
    .collect(Collectors.toList());
list.forEach(System.out::println);
打印：
    张三
    李四
    王五
    电七
    电七
```

#### -	转换Set

```java
// 将流中的元素转换为：Set
Set<String> set = employees.stream()
    .map(Employee::getName) // 获取对象的名字存入流中
    .collect(Collectors.toSet());
set.forEach(System.out::println);
打印：
    张三
    李四
    王五
    电七
    电七
```
#### -	转换HashSet

```java
// 将流中的元素转换为：HashSet
HashSet<String> hs = employees.stream()
	.map(Employee::getName) // 获取对象的名字存入流中
	.collect(Collectors.toCollection(HashSet::new));
 hs.forEach(System.out::println);
```

#### -	获取流中元素个数

```java
Long count = employees.stream()
			.collect(Collectors.counting());
System.out.println(count); // 5
```

#### -	求工资平均值

```java
Double avg = employees.stream()
			.collect(Collectors.
                     averagingDouble(Employee::getSalary));
System.out.println(avg); // 7332.6
```

#### -	工资总和

```java
Double sum = employees.stream()
				.collect(Collectors.
					summingDouble(Employee::getSalary));
System.out.println(sum);  //36663.0
```

#### -	最大值

```java
Optional<Employee> max = employees.stream()
				.collect(Collectors
                         .maxBy((e1,e2)->Double.compare(e1.getSalary(),e2.getSalary())));
System.out.println(max);
//打印：
//Optional[Employee [name=张三, age=18, salary=9999.0, Status=free]]
```

#### -	最小值

```java
Optional<Double> min = employees.stream()
				.map(Employee::getSalary)
				.collect(Collectors.minBy(Double::compare));
System.out.println(min.get()); // 5555.0
```

#### -	单级分组

```java
// 按状态分组查询，返回：map集合
Map<Status,List<Employee>> map = employees.stream()
			.collect(Collectors.groupingBy(Employee::getStatus));
System.out.println(map);
```

#### -	多级分组

```java
// 先按状态分组，再按年龄分组，返回：map集合
Map<Status,Map<String,List<Employee>>> map = employees.stream()
			.collect(Collectors.groupingBy(Employee::getStatus,Collectors.groupingBy((e)->{
				if(((Employee) e).getAge()<=35) {
					return "青年";
				}else {
					if(((Employee) e).getAge() <= 50) {
						return "中年";
					}else {
						return "老年";
					}
				}
          })));
//{free={青年=[Employee [name=张三, age=18, salary=9999.0, St....
```

#### -	summarizingDouble

```java
DoubleSummaryStatistics dss = employees.stream()
				.collect(Collectors.
                         summarizingDouble(Employee::getSalary));
System.out.println(dss);
/*
DoubleSummaryStatistics{count=5, sum=36663.000000, min=5555.000000, average=7332.600000, max=9999.000000}
*/
```

```java
System.out.println(dss.getMax()); // 直接获取最大值
```

### （10）Strean练习

```java
public class Test {
	static List<Employee> employees;
	public static void main(String[] args) {
		init();
		test1();
	}
	...
	public static void init() {
		employees = Arrays.asList(
				new Employee("张三",18,9999.0,Status.free),
				new Employee("李四",38,5555.0,Status.busy),
				new Employee("王五",50,5555.0,Status.vocation),
				new Employee("电七",17,7777.0,Status.free),
				new Employee("电七",17,7777.0,Status.busy)
		);
	}
}
```

---

#### -	求平方

```java
/*
*  给定一个数组列表，如何返回一个由每个数的平方构造的列表
*  eg：[1,2,3,4,5,6]  返回:[1,4,9,16,25,36]
*/
public static void test1() {
       Integer[] nums = new Integer[] {1,2,3,4,5,6};
       Arrays.stream(nums)
           .map((x)->x*x)
           .forEach(System.out::println);
}
```

#### -	求流中的元素个数

```java
/*
* 怎么样用map 和 reduce 方法数一数 流中有多少个Employee
*/
public static void test2() {
	Optional<Integer> count = employees.stream()
			.map((e)->1) // 永远为真
			.reduce(Integer::sum);
	System.out.println(count.get()); // 5
}
```

### （11）Stream综合练习

```java
// 交易员
public class Trader {
	private String name; // 姓名
	private String city; // 城市
```

```java
// 交易信息
public class Transaction {
	private Trader trader;
	private int date;
	private int price;
```

```java
public class TestTransaction {
	private static List<Transaction> transaction;
	public static void main(String[] args) {
		init();
		test5();
	}
	...
	public static void init() {
		Trader raoul = new Trader("Raoul","Cambridge");
		Trader mario = new Trader("Mario","Milan");
		Trader alan = new Trader("Alan","Cambridge");
		Trader brian = new Trader("Brian","Cambridge");
		
		transaction = Arrays.asList(
			new Transaction(brian,2011,300),
			new Transaction(raoul,2012,1000),
			new Transaction(raoul,2011,400),
			new Transaction(mario,2012,710),
			new Transaction(mario,2012,700),
			new Transaction(alan,2012,950)
		);
	}
```

---

#### - 找出2011年发生的所有交易，并按交易额排序（升序）		

```java
public static void test1() {
	transaction.stream()
		.filter((t)->t.getDate() == 2011)
		.sorted((t1,t2)->Integer.compare(t1.getPrice(), t1.getPrice()))
		.forEach(System.out::println);
	/*
	 * 	Transaction [trader=Trader [place=Brian, name=Cambridge], date=2011, price=300]
		Transaction [trader=Trader [place=Raoul, name=Cambridge], date=2011, price=400]

	*/
}
```

#### -	交易员都在那些不同的城市工作过

```java
public static void test2() {
    transaction.stream()
        .map((t)->t.getTrader().getCity())
        .distinct()
        .forEach(System.out::println);
    /*
		Brian
		Raoul
		Mario
		Alan
	*/
}
```

#### -	查找所有来自剑桥的交易员，并按姓名排序

```java
public static void test3() {
	transaction.stream()
		.filter((t)->t.getTrader().getCity().equals("Cambridge"))
		.map(Transaction::getTrader)
		.sorted((t1,t2) -> t1.getName().compareTo(t2.getName()))
		.distinct()
		.forEach(System.out::println);
}
Trader [place=Alan, city=Cambridge]
Trader [place=Brian, city=Cambridge]
Trader [place=Raoul, city=Cambridge]
```

#### -	返回所有交易员的姓名字符串，按字符顺序排序

```java
public static void test4() {
// 按字母排序
	String str = transaction.stream()
		.map((t)->t.getTrader().getName())
		.sorted()
		.reduce("",String::concat);
	System.out.println(str);
}
AlanBrianMarioMarioRaoulRaoul
```

#### -	有没有交易员是在米兰工作的

```java
public static void test5() {
		boolean b1 = transaction.stream()
			.anyMatch((t)->t.getTrader().getCity().equals("Milan"));
		System.out.println(b1);//true
	}
```

#### -	打印生活在剑桥的交易员的所有交易额

```java
public static void test6() {
		Optional<Integer> sum = transaction.stream()
			.filter((e)->e.getTrader().getCity().equals("Cambridge"))
			.map(Transaction::getPrice)
			.reduce(Integer::sum);
		
		System.out.println(sum.get()); //2650	
	}
```

#### -	所有交易中，最高的交易额是多少

```java
public static void test7() {
		Optional<Integer> max = transaction.stream()
				.map((t)->t.getPrice())
				.max(Integer::compare);
		System.out.println(max); //Optional[1000]
	}
```

#### -	找到交易额最小的交易

```java
public static void test8() {
		Optional<Transaction> op = transaction.stream()
				.min((t1,t2)->Integer.compare(t1.getPrice(),t2.getPrice()));
		System.out.println(op.get());
	}
Transaction [trader=Trader [place=Brian, city=Cambridge], date=2011, price=300]
```

---

## 5、并行流与顺序流

​	并行流概念：把一个内容分成多个数据块，并用不同的线程分别处理每个数据块的流。

---

```
// 并行流与顺序流之间的切换，Stream API
parallel()
sequential()
```

### （1）Fork/Join框架

​	适用场景：**数据量很大的**情况。

​	**简介：**在必要的情况下，将一个大任务，进行**拆分（fork）成若干个小任务**（拆到不可再分），再见一个个的小任务运算的**结果进行join汇总**。

![](/../images/Fork——join.png)

---

### （2）Fork/Join框架与传统线程池的区别

​	采用 ” **工作窃取** “ 模式（work-stealing）。

​	当执行新的任务时，可以将其**拆分分成更小的任务**执行，并将：小任务**添加到：线程队列**中，然后再**从一个随机线程的队列**中：**偷**一个并把它放在**自己的队列**中。

<img src="/../images/fork.png" style="zoom:67%;" />

---

​	相对于一般的线程池实现，`fork/join` 框架的优势体现在对其中包含的任务的处理方式。

---

​	在一般的线程池中，如果一个线程正在执行的任务**由于某些原因无法继续运行**，那么该线程会处于等待状态。

​	而`fork/join` 框架实现中，如果某个子问题由于**等待另外一个子问题的完成**，而无法继续运行，那么处理该子问题的线程会**主动寻找其他尚未运行的子问题**来执行。减少了线程的等待时间，提高了性能。

### （3）演示累加100亿

#### -	传统方式

**准备条件**

```java
// 继承RecursiveTask
public class ForkJoinCalculate extends RecursiveTask<Long> {
    /*
	 * 		serialVersionUID适用于Java的序列化机制。
	 * 		简单来说，Java的序列化机制是通过判断类的serialVersionUID来验证版本一致性的。
	 * 在进行反序列化时，JVM会把传来的字节流中的serialVersionUID与本地相应实体类的serialVersionUID进行比较，
	 * 如果相同就认为是一致的，可以进行反序列化，
	 * 否则就会出现序列化版本不一致的异常，即是InvalidCastException。
	 */
	private static final long serialVersionUID = 12345678997L;
	
	private long start; // 累加的起始数
	private long end; // 累加的终止数
	
	private static final long threshold = 10000; 
	// 构造器
	public ForkJoinCalculate(long start,long end) {
		this.start = start;
		this.end = end;
	}
	
	@Override
	protected Long compute() {
		long length = end - start;
		// 默认：0-10000的累加，若超过10000，则拆分
		if(length <= threshold ) {
			
			long sum = 0;
			
			for(long i= start;i<=end;i++) {
				sum+=i;
			}
			return sum;
		}else { // 超过10000，则进行拆分操作
			long middle = (start + end) / 2;//拆分一半
			// 获取左边的和
			ForkJoinCalculate left = new ForkJoinCalculate(start,middle);// 重新创建一个对象
			left.fork();//拆分子任务，同时压入：线程队列
			
			// 获取右边的和
			ForkJoinCalculate right = new ForkJoinCalculate(middle+1,end);
			right.fork();//拆分子任务，同时压入：线程队列
			
			// 合并值
			return left.join() + right.join();
		}
	}	
}
```

```java
public static void main(String[] args) {
	 test1();
	 test2();
}
```
---

​	顺序累加（**一次性累加完**）

```java
// 普通的for方法（耗费时间为：4082，数据量越大时间越多）
public static void test2() {
    Instant start = Instant.now();//获得运行此刻的时间

    long sum = 0L;
    for(long i=0;i<=10000000000L;i++) {
        sum += i;
    }
    System.out.println(sum);

    Instant end = Instant.now();//获得运行此刻的时间
    System.out.println("耗费时间为："+
                       Duration.between(start, end).toMillis());
}
```

---

​	**拆分**累加，再汇总

```java
// 使用fork拆分累加（耗费时间为：4239，数据量越大时间越少）
public static void test1() {
    Instant start = Instant.now();//获得运行此刻的时间
    ForkJoinPool  pool = new ForkJoinPool();
    // 从0开始，累加：100亿
    ForkJoinTask<Long> task = new ForkJoinCalculate(0,10000000000L);
    //每个子任务处理之后会带一个返回值,最终所有的子任务的返回结果会join(合并)成一个结果.
    Long sum = pool.invoke(task);//提交子任务

    System.out.println(sum);

    Instant end = Instant.now();//获得运行此刻的时间
    System.out.println("耗费时间为："+
                       Duration.between(start, end).toMillis());
}
```

---

#### -	JAVA 8 并行流	

```java
/*
* java 8 顺序流转 ：并行流
*/
public static void test3() {
// rangeClosed生成一个连续的数，0累加到100亿
	long sum = LongStream.rangeClosed(0, 10000000000L)
		.parallel() // 顺序流 转为：并行流
		.reduce(0,Long::sum);
	System.out.println(sum);
}
```

## 6、接口中的默认方法与静态方法

### （1）默认方法

自 JAVA 8之后，**允许接口有默认的方法** 或 静态方法：

```java
public interface MyFun {
	default String getName() {
		return "MyFun";
	}
}
```

---

问题：若**接口和类**都有相同的方法和实现，优先级如何：

---

```java
public class MyClass {
	public String getName() {
		return "MyClass";
	}
}
```

```java
// 继承MyClass和实现MyFun
public class SubClass extends MyClass implements MyFun{
	
}
```

```java
public static void main(String[] args) {
		SubClass sc = new SubClass();
		System.out.println(sc.getName());//MyClass
}
```

---

### （2）接口方法优先级

​	若一个接口中定义了一个默认的方法，而另外一个**父类或接口中**又**定义了一个同名的方法**时：

- **接口与类：** **默认选择父类**的方法。如果一个父类提供了具体的实现，那么**接口中**具有相同名称和参数的默认方法：**被忽略**。

```java
public class SubClass extends MyClass implements MyFun
```

- **接口与接口**：冲突。如果一个父接口提供了一个默认方法，而另一个接口也提供了一个具有相同名称和参数列表的方法（**不管是否为默认方法**），那么必须**覆盖该方法来解决：冲突**。

```java
// 冲突，必须指定实现那个接口的方法
public class SubClass  implements MyFun,MyFun2..
```

---

### （3）静态方法

```java
public interface MyFun {
	public static void show() {
		System.out.println("接口的static方法");
	}
}
```

## 7、新时间日期API

​	`LocalDate`、`LocalTime`、`LocalDateTime`类的实例是：**不可变的对象**，分别表示使用：`ISO-8601`日历系统的日期、时间、日期和时间。它们提供了简单的日期或时间，并**不包含**当前的时间信息，**也不包含**与时区相关的信息。

---

### （1）Instant

​		**时间戳：**以Unix元年：**1970年1月1日：00:00:00** 到某个时间之间的**毫秒值**

```java
Instant ins1 = Instant.now();
System.out.println(ins1);//2021-04-18T13:02:07.250Z

// 增加小时
OffsetDateTime odt = ins1.atOffset(ZoneOffset.ofHours(8));
System.out.println(odt);//2021-04-18T21:02:07.250+08:00

//1618751002849
System.out.println(ins1.toEpochMilli());

// 增加60秒
Instant ins2 = Instant.ofEpochSecond(60);
System.out.println(ins2);//1970-01-01T00:01:00Z
```

### （2）LocalDate

```java
// 获取本地当前的时间
LocalDateTime ldt = LocalDateTime.now();
System.out.println(ldt); //2021-04-18T20:41:16.624

// 指定时间
LocalDateTime ldt1 = LocalDateTime.of(2021,10,19,13,22,33);
System.out.println(ldt1);//2021-10-19T13:22:33

// 日期运算
LocalDateTime ldt2 = ldt.plusYears(2); //加两年，返回：新实例
System.out.println(ldt2);//2023-04-18T20:47:18.118

LocalDateTime ldt3 = ldt.minusYears(2);//减两年，返回：新实例
System.out.println(ldt3);//2019-04-18T20:47:18.118

System.out.println(ldt.getYear()); //2021
System.out.println(ldt.getMonthValue());//4
System.out.println(ldt.getSecond());//5
```
### （3）Duration

​		Duration：计算两个“**时间”之间的间隔**

```java
Instant ins1 = Instant.now();

try {
	Thread.sleep(1000);
} catch (InterruptedException e) {
	e.printStackTrace();
}

Instant ins2 = Instant.now();
// Duration：计算两个“时间”之间的间隔
Duration duration = Duration.between(ins1,ins2);
System.out.println(duration.toMillis()); //1000
```

### （4）Period

​	Duration：计算两个“**日期”之间的间隔**

```java
LocalDate ld1 = LocalDate.of(2015,1,1);
LocalDate ld2 = LocalDate.now();

Period period = Period.between(ld1, ld2);
System.out.println(period); //P6Y3M17D

System.out.println(period.getYears());//6
System.out.println(period.getMonths());//3
System.out.println(period.getDays());//17
```

### （5）TemporalAdjuster

​	**TemporalAdjuster：**时间校正器。例：将日期调正到“下个周日”等操作。

​	**TemporalAdjusters：**该类通过静态方法，提供了大量的常用TemporalAdjuster的实现。

---

```java
// 获取下个周日
LocalDate nextSunday = LocalDate.now().with(
	TemporalAdjusters.next(DayOfWeek.SUNDAY);
);
```

```java
// 自定义：下一个工作日
LocalDateTime ld3 = ld1.with((L)->{
    LocalDateTime ldt2 = (LocalDateTime) L;

    DayOfWeek dow = ldt2.getDayOfWeek(); //获取周日
    System.out.println(dow); //SUNDAY

    if(dow.equals(DayOfWeek.FRIDAY)) {
        return ldt2.plusDays(3);
    }else {
        if(dow.equals(DayOfWeek.SATURDAY)) {
            return ldt2.plusDays(2);
        }else {
            return ldt2.plusDays(1);
        }
    }
});
System.out.println(ld3);//2021-04-19T21:29:01.176
```

### （6）DateTimeFormatter	

​		作用：**格式化时间/日期**

```java
// 日期格式化，可以使用：自带的
DateTimeFormatter dtf1 = DateTimeFormatter.ISO_DATE;
LocalDateTime ldt = LocalDateTime.now();

String strDate = ldt.format(dtf1);
System.out.println(strDate); // 2021-04-18

// 自定义格式
DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss");
String strDate2 = dtf2.format(ldt);
System.out.println(strDate2);//2021年04月18日 21:39:33

// 字符串 转 日期
LocalDateTime  newDate = ldt.parse(strDate2,dtf2);
System.out.println(newDate); //2021-04-18T21:40:18
```

### （7）ZoneDate

​		作用：**设置 某个 时区 的时间**

```java
// 获取全部的时区
Set<String> set = ZoneId.getAvailableZoneIds();
set.forEach(System.out::println);
/*
* 	Australia/Darwin
	Asia/Khandyga
	Asia/Kuala_Lumpur
	...
*/
// 获取指定的地区的时效
LocalDateTime ldt = LocalDateTime.now(ZoneId.of("Europe/Tallinn"));
System.out.println(ldt); //2021-04-18T16:48:33.890

ZonedDateTime zdt = ldt.atZone(ZoneId.of("Asia/Shanghai"));
System.out.println(zdt); //2021-04-18T16:49:40.016+08:00[Asia/Shanghai]
```

## 8、Optional

​	作用：**最大减少**空指针异常。

```
Optional容器类的常用方法：
（1）Optional.of(T t)：创建一个Optional实例
（2）Optional.empty()：创建一个空的Optional实例
（3）Optional.ofNullable(T t)：若t不为null，创建Optional实例，否则创建：空实例
（4）isPresent()：判断是否包含值
（5）orElse(T T)：如果调用对象包含值，返回该值，否则返回：T
（6）orElseGet(Supplier s)：如果调用对象包含值，返回该值，否则返回：s获取的值
（7）map(Function f)：如果有值，对其处理，并返回处理后的Optional，否则返回Optional.empty()
（8）flatMap(Function mapper)：与map类似，要求返回值必须是：Optional
```

---

```java
public static void test1(){
	// 空指针异常，可以快速定位：空异常的位置
	Optional<Employee> op = Optional.of(null);
    // 若需要null对象，可以使用：empty()
	Employee emp = op.get();
	System.out.println(emp);
}
```

---

**例题：**判断一个男人心中是否有：女神

**普通写法：**if...else判断

---

JAVA 8 **使用Optional：**

```java
// 女神类：Godness.class
public class Godness{
	private String name;
	...
}
```

```java
// 新男人类：NewMan.class
public class NewMan{
// 若心中木有女神，则默认：null，使用Optional.empty，避免null异常
	private Optional<Godness> godness = Optional.empty();
    ...
}
```

**测试：**

```java
public static void test2){
	// Optional.ofNullable方法构建对象
	Optional<NewMan> op = Optional.ofNullable(null);
	String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<NewMan> op = Optional.ofNullable(new NewMan());
	String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<Godness> gn = Optional.ofNullable(null);
    String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<Godness> gn = Optional.ofNullable(new Godness("涂老师"));
    String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：涂老师
}
```

```java
public static String getGodnessName(Optional<NewMan> man){
	return man.orElse(new NewMan())//若man为空，则创建
		.getGodness() // 获得女神，若未空，则orElse
		.orElse(new Godness("白"))// 默认女神名字
		.getName();// 获得女神名字
}
```

## 

