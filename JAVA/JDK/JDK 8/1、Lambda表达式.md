---

typora-root-url: images
---

# 三、JDK 8 新特性

## 1、Lambda表达式

​	上联：左右遇一括号省

​	下联：左侧推断类型省

​	横批：能省则省

---

​	Lambda表达式（外文名：**lambda** [expression](https://baike.sogou.com/lemma/ShowInnerLink.htm?lemmaId=52918064&ss_c=ssc.citiao.link)）是一个**匿名函数**，即**没有函数名的函数**。

​	**介绍：**使用它设计的**代码会更加简洁**。当开发者在编写Lambda表达式时，也会随之被编译成一个：**函数式接口**。

### （1）Lambda语法

​	**一行**执行语句的写法：

```java
(parameters)->expression
```

​	如果有**多行**执行语句，可以加上 `{}`

```java
(parameters)->{statements;}
```

---

**语法格式一：**无参数，无返回值

```java
public void test1(){
 	Runable r = new Runnable(){
        // 局部变量
        int num = 0;// jdk 1.7前，必须是：final
        @Override
        public void run(){
            System.out.println("Hello world!");
        }
    };
	r.run();
}
```

**等价于：**

```java
Runnable r1 = () ->  System.out.println("Hello world!");
r1.run();
```

---

**语法格式二：**有一个参数，并且无返回值

```java
public void test2(){
    Consumer<String> con = (x) -> System.out.println(x);
    con.accept("Lambda语法二");
}
```

---

**语法格式三：**若只有一个参数，小括号可以**省略不写**

```java
public void test2(){
    Consumer<String> con = x -> System.out.println(x);
    con.accept("Lambda语法二");
}
```

---

**语法格式四：**有两个以上的参数，有返回值，并且Lambda体有多条语句

```java
// 原来的匿名内部类
public void lambda1() {
	Comparator<Integer> com = new Comparator<Integer>() {
		@Override
		public int compare(Integer o1, Integer o2) {
			return Integer.compare(o1, o2);
		}
	};
	TreeSet<Integer> ts = new TreeSet<>(com);//添加写好的重写比较方法
}
```
**等价于：**

```java
// Lambda表达式
public void lambda2() {
	Comparator<Integer> com = (x, y) ->{
      	return Integer.compare(x, y);   
    }      
	TreeSet<Integer> ts = new TreeSet<>(com);
}
```
---

**语法格式五：**若Lambda体中只有一条语句，**return 和 大括号**都可以省略

```java
// Lambda表达式
public void lambda2() {
	Comparator<Integer> com = (x, y) ->Integer.compare(x, y);
	TreeSet<Integer> ts = new TreeSet<>(com);
}
```

---

**语法格式六：**Lambda表达式的**参数列表数据类型**可以**省略不写**，因为：**JVM编译器**可以通过：**上下文推断出** 数据类型，即：**类型推断**。

```java
（Integer x,Integer y) -> Integer.compare(x,y);

// 第二个可不写参数，上下文推断（也就是第一个参数）
List<String> list = new ArrayList<>(); 
```

### （2）Lambda基础

​		Java 8 中引入了一个**新的操作符**：**->**  （称为：箭头操作符或Lambda操作符）

​		Lambda表达式拆分为：左侧（**参数列表**）、右侧（需要**执行的功能**，即：Lambda体）

### （3）Lambda认识

#### 	-	准备条件

```java
public class Employee {
	private String name;
	private int age;
	private double salary;
```

---

```java
	public static void main(String[] args) {
		List<Employee> employees = Arrays.asList(
				new Employee("张三",18,9999.0),
				new Employee("李四",38,5555.0),
				new Employee("王五",50,3333.0),
				new Employee("赵六",17,6666.0),
				new Employee("电七",16,7777.0)
		);
	}
```

#### 	-	传统写法

​	**需求1：**获取当前公司中**员工年龄**大于35的员工信息

```java
	public List<Employee> filterages(List<Employee> list){
		List<Employee> emps = new ArrayList<>();
		
		for(Employee emp:list) {
			if(emp.getAge()>=35) {
				emps.add(emp);
			}
		}
		return emps;
	}
```

---

​	**增加**一个需求，则需要**重新写一个方法**：

​	**需求2：**获取当前公司中**员工工资**大于3500的员工信息

```java
public List<Employee> filterSalary(List<Employee> list){
	List<Employee> emps = new ArrayList<>();
	
	for(Employee emp:list) {
		if(emp.getSalary()>=3500) {
			emps.add(emp);
		}
	}
	return emps;
}
```
#### 	-	策略设计模式写法

​	**一个需求一个类，方法通用**

```java
public interface MyPredicate<T> {  // 接口类<泛型>
	public boolean Judge(T t);  
}
```

---

​	**需求1：**获取当前公司中**员工年龄**大于35的员工信息

```java
// 实现接口类，并重新对应的方法
public class FilterAges implements MyPredicate<Employee>{  
	@Override
	public boolean Judge(Employee t) {  // 比较年龄
		return t.getAge() > 35;
	}
}
```

---

```java
public List<Employee> filterEmployee(List<Employee> list,MyPredicate<Employee> mp){
	List<Employee> emps = new ArrayList<>();
	
	for(Employee employee:list) {
		if(mp.Judge(employee)) {  // 如果当前对象的年龄大于35，则符合
			emps.add(employee);
		}
	}
	return emps;
}
```
---

​	**增加一个需求**，改为：增加一个`class` 类，并实现`FilterAges`接口，重写：`Judge` 方法：

​	**需求2：**获取当前公司中**员工工资**大于3500的员工信息

```java
public class FilterSalary implements MyPredicate<Employee>{
	@Override
	public boolean Judge(Employee t) { // 如果当前对象的工资大于3500，则符合
		return t.getSalary() > 3500;
	}
}
```

---

```java
public static void main(String[] args) {
	List<Employee> employees = Arrays.asList(
			new Employee("张三",18,9999.0),
			new Employee("李四",38,5555.0),
			new Employee("王五",50,3333.0),
			new Employee("赵六",17,6666.0),
			new Employee("电七",16,7777.0)
	);
	TestLambda testLambda = new TestLambda();
	List<Employee> list1 = testLambda.filterEmployee(employees,new FilterAges());
	
	for(Employee employee : list1) {
		System.out.println(employee);
	}
	
	System.out.println("-------打印工资大于3500的对象-------------");
	
	List<Employee> list2 = testLambda.filterEmployee(employees,new FilterSalary());
	
	for(Employee employee : list2) {
		System.out.println(employee);
	}
```
### （4）函数式接口

​		**Lambda表达式：**需要**“函数式接口”的支持。**

​		**函数式接口：**接口中**只有一个抽象方法**的接口。可以**使用注解@FunctionalInterface修饰**（检查是否是函数式接口）

---

```java
@FunctionalInterface
public interface MyFun {
	public Integer getValue(Integer num);
}
```

```java
// 对一个数进行运算
public static void main(String[] args) {
    
	Integer num1 = operation(100,(x)->x*x);
	System.out.println(num1); // 10000
    
    Integer num2 = operation(100,(x)->x+200);
	System.out.println(num2); // 300
}

public static Integer operation(Integer num,MyFun mf) {
	return mf.getValue(num);
}
```

### （5）Lambda练习

#### -	重写sort

```java
employees = Arrays.asList(
				new Employee("张三",18,9999.0),
				new Employee("李四",38,5555.0),
				new Employee("王五",50,3333.0),
				new Employee("赵六",17,6666.0),
				new Employee("电七",16,7777.0)
		);

public void test(){
    Collections.sort(employees,(e1,e2)->{
       if(e1.getAge() == e2.getAge()){
           return e1.getName().compareTo(e2.getName);
       }else{
           return Integer.compare(e1.getAge(),e2.getAge())
       } 
    });
}
```

#### -	处理字符串

```java
@FunctionalInterface
public interface MyFunction { // 声明函数式接口
	public String getValue(String str);	
}
```

```java
public class Main {

	public static void main(String[] args) {
		String trim_str = strHandler("\t\t\t 我是谁",(str)->str.trim());
		System.out.println(trim_str); //我是谁
		
		String case_str = strHandler("abcd",(str)->str.toUpperCase());
		System.out.println(case_str); //ABCD
		
		String sub_str = strHandler("abcd",(str)->str.substring(1));
		System.out.println(sub_str);//bcd
	}
	
	// 需求：处理字符串，str：参数，mf：执行的功能
	public static String strHandler(String str,MyFunction mf) {
		return mf.getValue(str);
	}
}
```

#### -	泛型

```java
@FunctionalInterface
public interface MyFunction2<T,R> {
	public R getValue(T t1,T t2);
}
```

```java
public class Main {

	public static void main(String[] args) {
		op(100L,200L,(x,y)->x+y);
		op(100L,200L,(x,y)->x*y); 
	}
	
	// 对于两个Long型数据进行处理
	public static void op(Long l1,Long l2,MyFunction2<Long,Long> mf) {
		System.out.println(mf.getValue(l1, l2));
	}
}
```

## 2、内置的四大核心函数式接口

### （1）Consumer<T> : 消费型接口

```java
void  accept(T t);
```

```java
import java.util.function.Consumer;
public class Main {
	public static void main(String[] args) {
		happy(1000,m -> System.out.println("我消费了"+m+"元"));
        // 我消费了1000.0元
	}
    // 参数1：参数；参数2：执行的功能
	public static   void happy(double money,Consumer<Double> con) {
		con.accept(money);
	}
}
```

### （2）Supplier<T> : 供给型接口

```java
T get();
```

```java
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class Main {

	public static void main(String[] args) {
		List<Integer> list = getNumList(10,()->(int)(Math.random()*100));// 随机数
		
		for(Integer li:list) {
			System.out.print(li+" ");
		}
        // 5 19 64 45 33 74 17 79 45 60 
	}
	
	// 产生指定个数的整数，并放入集合中
	public static List<Integer> getNumList(int num,Supplier<Integer> sup){
		List<Integer> list = new ArrayList<>();
		
		for(int i=0;i<num;i++) {
			Integer n = sup.get(); // 获得里面的元素
			list.add(n);
		}
		return list;
	}
}
```

### （3）Function<T,R>：函数型接口

​	T：操作；R：返回的值

```java
R apply(T t);
```

```java
import java.util.function.Function;
public class Main {
	public static void main(String[] args) {
		String trim_str = strHandler("\t\t\t 我是谁",(str)->str.trim());
		System.out.println(trim_str); //我是谁
		
		String case_str = strHandler("abcd",(str)->str.toUpperCase());
		System.out.println(case_str); //ABCD
	}
	//处理字符串
	public static String strHandler(String str,Function<String,String> fun) {
		return fun.apply(str);
	}
}
```

### （4）Predicate<T> ： 断言型接口

```java
boolean test(T t);
```

```java
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class Main {

	public static void main(String[] args) {
        // Arrays.asList：将一个数组转换为一个List集合
		List<String> list =  Arrays.asList("Hello","atguigu","Lambda","www");
		List<String>  list_str = filterStr(list,(s)->s.length() > 3); // 筛选字符串长度 > 3
	
		for(String li:list_str) {
			System.out.print(li+" ");//Hello atguigu Lambda 
		}
	}
	
	//将满足条件的字符串，放入集合中
	public static List<String> filterStr(List<String> list,Predicate<String> pre){
		List<String> strList = new ArrayList<>();
		
		for(String str:list) {
			if(pre.test(str)) {
				strList.add(str);
			}
		}
		return strList;
	}
}
```

### （5）其他接口

<img src="/../images/其他接口.png" style="zoom:67%;" />

## 3、方法引用与构造器引用

​	概念：若Lambda体中的内容有**方法已经实现了**，我们可以使用：**方法引用**（Lambda表达式的另外一种表现形式）。

​	主要**三种语法格式**：

​		对象 :: 实例方法名
​		类 :: 静态方法名
​		类 :: 实例方法名

---

### （1）对象 :: 实例方法名

```java
	// 对象::实例方法名
	public static void test1() {
		PrintStream ps1 = System.out; //PrintStream 对象
		Consumer<String> con1 = (x)->ps1.println(x);
		con1.accept("abd"); // abd
		// ---------------------------
		PrintStream ps = System.out;
		Consumer<String> con2 = ps::println;//对象::实例方法名
		con2.accept("abd");// abd
		// ---------------------------
		Consumer<String> con3 = System.out::println;
		con3.accept("abc");// abc
	}
```

### （2）类 :: 静态方法名

```java
// 类::静态方法名
public static void test2() {
	Comparator<Integer> com1 = (x,y)->Integer.compare(x,y);
	Comparator<Integer> com2 = Integer::compare;
}
```
### （3）类 :: 实例方法名

```java
// 类::实例方法名
public static void test3() {
	BiPredicate<String,String> bp1 = (x,y)->x.equals(y);
	
	BiPredicate<String,String> bp2 = String::equals;
}
```
### （4）注意

		-	Lambda体中**调用方法的参数列表与返回值类型**，要与函数式接口中抽象方法的**函数列表和返回值类型保持一致**
		-	若Lambda参数列表中的**第一参数**是：实例方法的**调用者**，而**第二参数**是：是实例方法的参数时，可以使用：`ClassName::method`

### （5）构造器引用

​	**无参构造器**

​	注意：调用的**构造器的参数列表**要与函数式接口中**抽象方法的参数列表**保持一致！！

```java
	//构造器引用
	public static void test4() {
		Supplier<Employee> sup = () -> new Employee();
		// 构造器引用方式（无参）
		Supplier<Employee> sup2 = Employee::new; // 返回值：Employee对象
		Employee emp = sup2.get();//get() 函数式接口方法
		System.out.println(emp);
        // 打印：Employee [name=null, age=0, salary=0.0]
	}
```

---

​	**有参构造器**（一个参数）

```java
public Employee(int age) { 
	this.age = age;
}
```

```java
// 构造器引用
public static void test4() {
	// Function<Integer,Employee> fun = (x)-> new Employee(x);
	// 等价于：
	Function<Integer,Employee> fun2 = Employee::new;//参数：Integer，返回类型：Employee对象
	Employee emp = fun2.apply(101);
	System.out.println(emp);// 	Employee [name=null, age=101, salary=0.0]
}
```

---

​	**有参构造器**（两个参数）

```java
public Employee(String name,int age) {
	this.name = name;
	this.age = age;
}
```

​	*BiFunction<String,Integer,Employee>  ：String，Integer：**参数**，Employee：**返回类型***

```java
//构造器引用
public static void test4() {
	BiFunction<String,Integer,Employee> bf = Employee::new;//对象::实例方法名
	Employee emp = bf.apply("tzw", 100);
	System.out.println(emp);//Employee [name=tzw, age=100, salary=0.0]	
}
```
### （6）数组引用

​	Type :: new

```java
//数组引用
public static void test4() {
   // 创建长度为：Integer长的数组，返回一个String数组
	Function<Integer,String[]> fun1 = (x) ->new String[x];
	String[] strs = fun1.apply(10); //创建一个长度为：10的string数组
	System.out.println(strs.length);
	
	// 上面等价于：
	Function<Integer,String[]> fun2 = String[]::new; //访问构造器
	String[] str2 = fun2.apply(20);
	System.out.println(str2.length);
	
}
```
## 

