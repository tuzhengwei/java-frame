---

typora-root-url: images
---

## 8、Optional

​	作用：**最大减少**空指针异常。

```
Optional容器类的常用方法：
（1）Optional.of(T t)：创建一个Optional实例
（2）Optional.empty()：创建一个空的Optional实例
（3）Optional.ofNullable(T t)：若t不为null，创建Optional实例，否则创建：空实例
（4）isPresent()：判断是否包含值
（5）orElse(T T)：如果调用对象包含值，返回该值，否则返回：T
（6）orElseGet(Supplier s)：如果调用对象包含值，返回该值，否则返回：s获取的值
（7）map(Function f)：如果有值，对其处理，并返回处理后的Optional，否则返回Optional.empty()
（8）flatMap(Function mapper)：与map类似，要求返回值必须是：Optional
```

---

```java
public static void test1(){
	// 空指针异常，可以快速定位：空异常的位置
	Optional<Employee> op = Optional.of(null);
    // 若需要null对象，可以使用：empty()
	Employee emp = op.get();
	System.out.println(emp);
}
```

---

**例题：**判断一个男人心中是否有：女神

**普通写法：**if...else判断

---

JAVA 8 **使用Optional：**

```java
// 女神类：Godness.class
public class Godness{
	private String name;
	...
}
```

```java
// 新男人类：NewMan.class
public class NewMan{
// 若心中木有女神，则默认：null，使用Optional.empty，避免null异常
	private Optional<Godness> godness = Optional.empty();
    ...
}
```

**测试：**

```java
public static void test2){
	// Optional.ofNullable方法构建对象
	Optional<NewMan> op = Optional.ofNullable(null);
	String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<NewMan> op = Optional.ofNullable(new NewMan());
	String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<Godness> gn = Optional.ofNullable(null);
    String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<Godness> gn = Optional.ofNullable(new Godness("涂老师"));
    String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：涂老师
}
```

```java
public static String getGodnessName(Optional<NewMan> man){
	return man.orElse(new NewMan())//若man为空，则创建
		.getGodness() // 获得女神，若未空，则orElse
		.orElse(new Godness("白"))// 默认女神名字
		.getName();// 获得女神名字
}
```

## 

