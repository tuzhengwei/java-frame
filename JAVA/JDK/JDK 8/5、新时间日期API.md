---

typora-root-url: images
---

## 7、新时间日期API

​	`LocalDate`、`LocalTime`、`LocalDateTime`类的实例是：**不可变的对象**，分别表示使用：`ISO-8601`日历系统的日期、时间、日期和时间。它们提供了简单的日期或时间，并**不包含**当前的时间信息，**也不包含**与时区相关的信息。

---

### （1）Instant

​		**时间戳：**以Unix元年：**1970年1月1日：00:00:00** 到某个时间之间的**毫秒值**

```java
Instant ins1 = Instant.now();
System.out.println(ins1);//2021-04-18T13:02:07.250Z

// 增加小时
OffsetDateTime odt = ins1.atOffset(ZoneOffset.ofHours(8));
System.out.println(odt);//2021-04-18T21:02:07.250+08:00

//1618751002849
System.out.println(ins1.toEpochMilli());

// 增加60秒
Instant ins2 = Instant.ofEpochSecond(60);
System.out.println(ins2);//1970-01-01T00:01:00Z
```

### （2）LocalDate

```java
// 获取本地当前的时间
LocalDateTime ldt = LocalDateTime.now();
System.out.println(ldt); //2021-04-18T20:41:16.624

// 指定时间
LocalDateTime ldt1 = LocalDateTime.of(2021,10,19,13,22,33);
System.out.println(ldt1);//2021-10-19T13:22:33

// 日期运算
LocalDateTime ldt2 = ldt.plusYears(2); //加两年，返回：新实例
System.out.println(ldt2);//2023-04-18T20:47:18.118

LocalDateTime ldt3 = ldt.minusYears(2);//减两年，返回：新实例
System.out.println(ldt3);//2019-04-18T20:47:18.118

System.out.println(ldt.getYear()); //2021
System.out.println(ldt.getMonthValue());//4
System.out.println(ldt.getSecond());//5
```
### （3）Duration

​		Duration：计算两个“**时间”之间的间隔**

```java
Instant ins1 = Instant.now();

try {
	Thread.sleep(1000);
} catch (InterruptedException e) {
	e.printStackTrace();
}

Instant ins2 = Instant.now();
// Duration：计算两个“时间”之间的间隔
Duration duration = Duration.between(ins1,ins2);
System.out.println(duration.toMillis()); //1000
```

### （4）Period

​	Duration：计算两个“**日期”之间的间隔**

```java
LocalDate ld1 = LocalDate.of(2015,1,1);
LocalDate ld2 = LocalDate.now();

Period period = Period.between(ld1, ld2);
System.out.println(period); //P6Y3M17D

System.out.println(period.getYears());//6
System.out.println(period.getMonths());//3
System.out.println(period.getDays());//17
```

### （5）TemporalAdjuster

​	**TemporalAdjuster：**时间校正器。例：将日期调正到“下个周日”等操作。

​	**TemporalAdjusters：**该类通过静态方法，提供了大量的常用TemporalAdjuster的实现。

---

```java
// 获取下个周日
LocalDate nextSunday = LocalDate.now().with(
	TemporalAdjusters.next(DayOfWeek.SUNDAY);
);
```

```java
// 自定义：下一个工作日
LocalDateTime ld3 = ld1.with((L)->{
    LocalDateTime ldt2 = (LocalDateTime) L;

    DayOfWeek dow = ldt2.getDayOfWeek(); //获取周日
    System.out.println(dow); //SUNDAY

    if(dow.equals(DayOfWeek.FRIDAY)) {
        return ldt2.plusDays(3);
    }else {
        if(dow.equals(DayOfWeek.SATURDAY)) {
            return ldt2.plusDays(2);
        }else {
            return ldt2.plusDays(1);
        }
    }
});
System.out.println(ld3);//2021-04-19T21:29:01.176
```

### （6）DateTimeFormatter	

​		作用：**格式化时间/日期**

```java
// 日期格式化，可以使用：自带的
DateTimeFormatter dtf1 = DateTimeFormatter.ISO_DATE;
LocalDateTime ldt = LocalDateTime.now();

String strDate = ldt.format(dtf1);
System.out.println(strDate); // 2021-04-18

// 自定义格式
DateTimeFormatter dtf2 = DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm:ss");
String strDate2 = dtf2.format(ldt);
System.out.println(strDate2);//2021年04月18日 21:39:33

// 字符串 转 日期
LocalDateTime  newDate = ldt.parse(strDate2,dtf2);
System.out.println(newDate); //2021-04-18T21:40:18
```

### （7）ZoneDate

​		作用：**设置 某个 时区 的时间**

```java
// 获取全部的时区
Set<String> set = ZoneId.getAvailableZoneIds();
set.forEach(System.out::println);
/*
* 	Australia/Darwin
	Asia/Khandyga
	Asia/Kuala_Lumpur
	...
*/
// 获取指定的地区的时效
LocalDateTime ldt = LocalDateTime.now(ZoneId.of("Europe/Tallinn"));
System.out.println(ldt); //2021-04-18T16:48:33.890

ZonedDateTime zdt = ldt.atZone(ZoneId.of("Asia/Shanghai"));
System.out.println(zdt); //2021-04-18T16:49:40.016+08:00[Asia/Shanghai]
```

## 8、Optional

​	作用：**最大减少**空指针异常。

```
Optional容器类的常用方法：
（1）Optional.of(T t)：创建一个Optional实例
（2）Optional.empty()：创建一个空的Optional实例
（3）Optional.ofNullable(T t)：若t不为null，创建Optional实例，否则创建：空实例
（4）isPresent()：判断是否包含值
（5）orElse(T T)：如果调用对象包含值，返回该值，否则返回：T
（6）orElseGet(Supplier s)：如果调用对象包含值，返回该值，否则返回：s获取的值
（7）map(Function f)：如果有值，对其处理，并返回处理后的Optional，否则返回Optional.empty()
（8）flatMap(Function mapper)：与map类似，要求返回值必须是：Optional
```

---

```java
public static void test1(){
	// 空指针异常，可以快速定位：空异常的位置
	Optional<Employee> op = Optional.of(null);
    // 若需要null对象，可以使用：empty()
	Employee emp = op.get();
	System.out.println(emp);
}
```

---

**例题：**判断一个男人心中是否有：女神

**普通写法：**if...else判断

---

JAVA 8 **使用Optional：**

```java
// 女神类：Godness.class
public class Godness{
	private String name;
	...
}
```

```java
// 新男人类：NewMan.class
public class NewMan{
// 若心中木有女神，则默认：null，使用Optional.empty，避免null异常
	private Optional<Godness> godness = Optional.empty();
    ...
}
```

**测试：**

```java
public static void test2){
	// Optional.ofNullable方法构建对象
	Optional<NewMan> op = Optional.ofNullable(null);
	String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<NewMan> op = Optional.ofNullable(new NewMan());
	String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<Godness> gn = Optional.ofNullable(null);
    String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：白
    
    Optional<Godness> gn = Optional.ofNullable(new Godness("涂老师"));
    String gn_name = getGodnessName(op);
    System.out.println(gn_name); // 默认：涂老师
}
```

```java
public static String getGodnessName(Optional<NewMan> man){
	return man.orElse(new NewMan())//若man为空，则创建
		.getGodness() // 获得女神，若未空，则orElse
		.orElse(new Godness("白"))// 默认女神名字
		.getName();// 获得女神名字
}
```

## 

