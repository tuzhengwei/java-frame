---
typora-root-url: images
---

# Java全栈系列课程之Linux教学

|           操作            | 作用                                  |
| :-----------------------: | ------------------------------------- |
|       Ctrl+鼠标滚轮       | 放大或缩小字体                        |
| cd in+tab——>cd instan.txt | 若**文件可见**，按住：tab，可自动补全 |

# 未找到命令异常

```
linux 出现-bash: ls: command not found
```

![](/../images/base.png)

**解决方法：**重新**编辑profle文件**，使用/bin/vi /etc/**profile**命令重新编辑（直接使用**ftp**找到profile.txt 编辑）

![](/../images/base解决.png)

```
export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin
```

# 一、JAVA开发之路

​	JavaSE、MySQL、前端（HTML、CSS、JS）、JavaWeb、SSM框架、SpringBoot、Vue、SpringCloud

​	Linux（CentOS 7）

​	消息队列（RabbitMQ、RockeetMQ），缓存（Redis），搜索引擎（ES）

# 二、入门概述

## 1、概念

​	linux诞生了这么多年，以前还喊着如何能取代windows系统，现在这个口号已经小多了，任何事物发展都有其局限性都有其天花板。就如同在国内再搞一个社交软件取代腾讯一样，想想而已基本不可能，因为用户已经习惯于使用微信交流，不是说技术上实现不了解而是老百姓已经习惯了，想让他们不用，即使他们自己不用亲戚朋友还是要用，没有办法的事情。

​	用习惯了windows操作系统，再让大家切换到别的操作系统基本上是不可能的事情，改变一个人已经养成的习惯太难。没有办法深入到普通老百姓的生活中，并不意味着linux就没有用武之地了。

​	在服务器端，**在开发领域**，linux倒是越来越受欢迎，很多程序员都觉得不懂点linux都觉得不好意思，linux在开源社区的地位依然岿然不动。

​	尤其是作为一个后端程序员，是必须要掌握Linux的，因为这都成为了你找工作的**基础门槛**了，所以不得不学习！

----

​	Linux 是一套**免费使用和自由传播的类** Unix 操作系统，是一个基于 POSIX（**可移植操作系统接口**） 和 UNIX 的**多用户、多任务、支持多线程和多 CPU** 的操作系统。

​	Linux 能运行主要的 UNIX 工具软件、应用程序和网络协议。它支持 32 位和 64 位硬件。Linux **继承了 Unix** 以网络为核心的设计思想，是一个**性能稳定的多用户**网络操作系统。

## 2、发行版

​	Linux 的发行版说简单点就是将 **Linux 内核与应用软件**做一个打包。

---

​	**Kali linux：安全渗透测试使用！（安全方面）**

---

<img src="/../images/发行版.png" style="zoom: 50%;" />

​		目前市面上较知名的发行版有：Ubuntu、RedHat、CentOS、Debian、Fedora、SuSE、OpenSUSE、Arch Linux、SolusOS 等。

## 3、Linux 应用领域

​	今天各种场合都有使用各种 Linux 发行版，从嵌入式设备到超级计算机，并且在服务器领域确定了地位；

​	通常服务器使用 **LAMP（Linux + Apache + MySQL + PHP）**或 **LNMP（Linux + Nginx+ MySQL + PHP）**组合。

​	（PHP：超文本预处理器，是一种通用开源脚本语言）

# 三、环境搭建

​	Linux 的安装，安装步骤比较繁琐，现在其实云服务器挺普遍的，价格也便宜，如果直接不想搭建，也可以**直接买一台**学习用用！

​	安装CentOS（虚拟机安装，**耗资源**）

## 1、本地安装

​	①	可以通过镜像进行安装！

​		CentOS镜像地址：https://mirrors.tuna.tsinghua.edu.cn/centos/

​	②	可以使用我已经制作好的镜像！视频中讲解了该种方式！

​	③	安装 VMware 虚拟机软件，然后打开我们的镜像即可使用！

## 2、购买阿里云服务器

​	配置安全组，Xftp（上传文件）：端口21；

​	SSH端口：22；

## 3、Xftp使用

### 1、进入Xftp

<img src="/../images/点开Xftp.png" style="zoom:50%;" />

### 2、使用Xftp

<img src="/../images/Xftp.png" style="zoom:50%;" />

# 四、Linux目录

|           操作            | 作用                              |
| :-----------------------: | --------------------------------- |
|       Ctrl+鼠标滚轮       | 放大或缩小字体                    |
| cd in+tab——>cd instan.txt | 若文件可见，按住：tab，可自动补全 |

## 1、树形目录结构

<img src="/../images/树形目录.png" style="zoom:50%;" />

## 2、使用命令查看

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ ~]# cd /		# 进入根目录
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# ls		# 查看根目录下的所有文件

[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# ls -a		# 查看当前目录下的所有文件和文件夹和隐藏文件

# 查看当前目录下文件和文件夹的详细信息（文件类型 ，用户 ，组 时间 ）
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# ls -l		# 

# ll 密令 查看目录信息
# ll + /绝对路径 查看该路径下的目录或者文件
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# ls -ll	# 
```

结果：

![](/../images/命令查看目录.png)

## 3、目录介绍

|   文件名   | 作用                                                         |
| :--------: | :----------------------------------------------------------- |
|  **/bin**  | bin是Binary的缩写, 这个目录存放着最**经常使用的命令**        |
|  **/dev**  | 存放的是**启动Linux时**使用的一些**核心文件**，包括一些**连接文件**以及**镜像文件** |
| **/home**  | 用户的**主目录**，在Linux中，每个用户都有一个**自己的目录**，一般该目录名是**以用户的账号命名**的 |
| **/media** | linux系统会自动识别一些设备，例如U盘、光驱等等，当识别后，linux会把**识别的设备挂载到这个目录下** |
|  **/opt**  | 给主机**额外安装软件所摆放的目录**。比如你安装一个ORACLE数据库则就可以放到这个目录下。**默认是空的**。 |
| **/root**  | 目录为**系统管理员**，也称作超级权限者的用户主目录           |
| **/sbin**  | s就是Super User的意思，这里存放的是系统管理员使用的**系统管理程序** |
|  **/sys**  | 目录下安装了**2.6内核**中新出现的一个**文件系统 sysfs**      |
|  **/usr**  | 一个非**常重要的目录**，用户的很多**应用程序和文件**都放在这个目录下，类似于windows下的**program files目录** |
|            |                                                              |

|     文件名      | 作用                                                         |
| :-------------: | ------------------------------------------------------------ |
|    **/boot**    | 存放的是**启动Linux时**使用的一些核心文件，包括一些**连接文件以及镜像文件** |
|    **/etc**     | 存放所有的**系统管理**所需要的**配置文件和子目录**           |
|    **/lib**     | 存放着系统最基本的**动态连接共享库**，其作用类似于Windows里的DLL文件 |
| **/lost+found** | 一般情况下是空的，当系统**非法关机后**，这里就存放了一些文件 |
|    **/mnt**     | 系统提供该目录是为了**让用户临时挂载别的文件系统的**，我们可以将光驱挂载在/mnt/上，然后进入该目录就可以**查看光驱**里的内容了 |
|    **/proc**    | 是一个**虚拟的目录**，它是**系统内存的映射**，我们可以通过直接**访问这个目录来获取系统信息**。 |
|    **/run**     | **临时文件系统**，存储系统启动以来的信息。当**系统重启时**，这个目录下的文件应该**被删掉或清除** |
|    **/srv**     | 存放一些**服务启动之后**需要提取的数据                       |
|    **/tmp**     | 存放一些**临时文件**                                         |
|    **/var**     | 存放着在**不断扩充着的东西**，我们习惯将那**些经常被修改的**目录放在这个目录下。包括各种**日志文件** |

- **/usr/bin：** 系统用户使用的应用程序。
- **/usr/sbin：** 超级用户使用的比较高级的管理程序和系统守护程序。
- **/usr/src：** 内核源代码默认的放置目录。

# 五、Linux常用命令

## 1、解压某个文件

```cmd
# 将压缩包放在home目录下
[root@iZ2ze5ovuegpc8xpnw3jsfZ ~]# cd /home 
[root@iZ2ze5ovuegpc8xpnw3jsfZ home]# ls

# 当前home目录下的文件
apache-tomcat-9.0.22 tar.gz

# 解压指定文件
[root@iZ2ze5ovuegpc8xpnw3jsfZ home]# tar -zxvf apache-tomcat-9.0.22 tar.gz

# 查看解压后的文件（先进入到指定目录，然后ls显示）
[root@iZ2ze5ovuegpc8xpnw3jsfZ home]# cd apache-tomcat-9.0.22
[root@iZ2ze5ovuegpc8xpnw3jsfZ apache-tomcat-9.0.22]# ls -ll
```

## 2、关机命令

​	在linux领域内大多用在服务器上，很少遇到关机的操作。毕竟服务器上跑一个服务是永无止境的，除非特殊情况下，不得已才会关机。

​	关闭后，若是阿里云服务器，则需要**在阿里云服务端重新启动**，才可以正常连接。

```cmd
shutdown	# 关机指令
Shutdown-c	# 取消
sync # 将数据由内存同步到硬盘中。
```

```cmd
shutdown # 关机指令，你可以man shutdown 来看一下帮助文档。例如你可以运行如下命令关机：
 
shutdown –h 10 # 这个命令告诉大家，计算机将在10分钟后关机
 
shutdown –h now # 立马关机
 
shutdown –h 20:25 # 系统会在今天20:25关机
 
shutdown –h +10 # 十分钟后关机
 
shutdown –r now # 系统立马重启
 
shutdown –r +10 # 系统十分钟后重启
 
reboot # 就是重启，等同于 shutdown –r now
 
halt # 关闭系统，等同于shutdown –h now 和 poweroff
```

## 3、目录管理命令

**绝对路径**：

​	路径的写法，由根目录 / 写起，例如**：/usr/share/doc 这个目录。*

---

**相对路径**：

​	路径的写法，不是由 / 写起，例如由 /usr/share/doc 要到 /usr/share/man 底下时，可以写成：**cd ../man** 这就是相对路径的写法啦！

​	所有的Linux命令 都可以**组合使用**。

### （1）常见的处理目录的命令

- ls: 列出目录
- cd：切换目录
- pwd：显示目前的目录
- mkdir：创建一个新的目录
- rmdir：删除一个空的目录
- cp: 复制文件或目录
- rm: 移除文件或目录
- mv: 移动文件与目录，或修改文件与目录的名称

### （2）ls

```cmd
[root@www ~]# ls [-aAdfFhilnrRSt] 目录名称
```

选项与参数（可以**组合使用**）：

- -a ：全部的文件，连同隐藏文件( 开头为 . 的文件) 一起列出来(常用)
- -l ：长数据串列出，包含文件的属性与权限等等数据；(常用)

```cmd
[root@www ~]# ls -al ~
```

### （2）cd（切换目录）

​	**绝对**路径： “/”  开头

​	**相对**路径：“../” 开头

```cmd
# 切换到用户目录下
[root@kuangshen /]# cd home  
 
# 使用 mkdir 命令创建 kuangstudy 目录
[root@kuangshen home]# mkdir kuangstudy
 
# 进入 kuangstudy 目录
[root@kuangshen home]# cd kuangstudy
 
# 回到上一级
[root@kuangshen kuangstudy]# cd ..
 
# 回到根目录
[root@kuangshen kuangstudy]# cd /
 
# 表示回到自己的家目录，亦即是 /root 这个目录
[root@kuangshen kuangstudy]# cd ~
```

### （3）pwd（显示目前所在的目录）

​		pwd 是 **Print Working Directory** 的缩写，也就是显示目前所在目录的命令。

```cmd
[root@kuangshen kuangstudy]#pwd [-P]
```

- 选项与参数：**-P** ：显示出确实的路径，而非使用连接(link) 路径。

```cmd
# 单纯显示出目前的工作目录
[root@kuangshen ~]# pwd
/root
 
# 如果是链接，要显示真实地址，可以使用 -P参数
[root@kuangshen /]# cd bin
[root@kuangshen bin]# pwd -P
/usr/bin
```

### （4）mkdir （创建新目录）

​		创建新的目录，使用：mkdir (make directory)

```cmd
mkdir [-mp] 目录名称
```

`touch`  ： 创建文件

`echo` ：输入字符串到文件中

选项与参数：

- -m ：配置文件的权限喔！直接配置，**不需要看默认权限** (umask) 的脸色～
- -p ：帮助你直接将所需要的目录(包含上一级目录)**递归创建**起来！

```cmd
# 进入我们用户目录下
[root@kuangshen /]# cd /home
 
# 创建一个 test 文件夹
[root@kuangshen home]# mkdir test
 
# 创建多层级目录
[root@kuangshen home]# mkdir test1/test2/test3/test4
mkdir: cannot create directory ‘test1/test2/test3/test4’:
No such file or directory  # <== 没办法直接创建此目录啊！
 
# 加了这个 -p 的选项，可以自行帮你创建多层目录！
[root@kuangshen home]# mkdir -p test1/test2/test3/test4
 
# 创建权限为 rwx--x--x 的目录。
[root@kuangshen home]# mkdir -m 711 test2
[root@kuangshen home]# ls -l
drwxr-xr-x 2 root  root  4096 Mar 12 21:55 test
drwxr-xr-x 3 root  root  4096 Mar 12 21:56 test1
drwx--x--x 2 root  root  4096 Mar 12 21:58 test2
```

### （5）rmdir ( 删除空的目录 )

```cmd
 rmdir [-p] 目录名称	# 只能删除：空目录
```

- 选项与参数：**-p ：**连同上一级『空的』目录也一起删除

```cmd
# 看看有多少目录存在？
[root@kuangshen home]# ls -l
drwxr-xr-x 2 root  root  4096 Mar 12 21:55 test
drwxr-xr-x 3 root  root  4096 Mar 12 21:56 test1
drwx--x--x 2 root  root  4096 Mar 12 21:58 test2
 
# 可直接删除掉，没问题
[root@kuangshen home]# rmdir test
 
# 因为尚有内容，所以无法删除！
[root@kuangshen home]# rmdir test1
rmdir: failed to remove ‘test1’: Directory not empty
 
# 利用 -p 这个选项，立刻就可以将 test1/test2/test3/test4 依次删除。
[root@kuangshen home]# rmdir -p test1/test2/test3/test4
```

​	 rm 命令：删除**非空目录**

### （6）cp ( 复制文件或目录 )

```cmd
[root@www ~]# cp [-adfilprsu] 来源档(source) 目标档(destination)
[root@www ~]# cp [options] source1 source2 source3 .... directory
```

选项与参数：

- a：相当 -pdr 的意思，至於 pdr 请参考下列说明；(常用)
- p：连同**文件的属性**一起复制过去，而非使用默认属性(备份常用)；
- d：若来源档为连结档的属性(link file)，则复制**连结档属性**而非文件本身；
- r：**递归持续复制**，用於目录的复制行为；(常用)
- f：为强制(force)的意思，若目标文件已经存在且无法开启，则移除后再尝试一次；
- i：若目标档(destination)已经存在时，在覆盖时会先询问动作的进行(常用)
- l：进行硬式连结(hard link)的连结档创建，而非复制文件本身。
- s：复制成为符号连结档 (symbolic link)，亦即『捷径』文件；
- u：若 destination 比 source 旧才升级 destination ！

```cmd
# 找一个有文件的目录，我这里找到 root目录
[root@kuangshen home]# cd /root
[root@kuangshen ~]# ls
install.sh
[root@kuangshen ~]# cd /home
 
# 复制 root目录下的install.sh 到 home目录下
[root@kuangshen home]# cp /root/install.sh /home
[root@kuangshen home]# ls
install.sh
 
# 若复制的文件名跟目录中的文件名重复（覆盖），加上-i参数，增加覆盖询问？
[root@kuangshen home]# cp -i /root/install.sh /home
cp: overwrite ‘/home/install.sh’? y # n不覆盖，y为覆盖
```

​	若报：cp: omitting directory ‘instal.sh’ 异常，则添加参数：-r

​	使用**cp -r命令**进行复制（注：cp命令默认是**不能复制目录的**，需要加参数 -r）

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ test1]# cp -r /home/instal.sh /test1  
```

### （7）rm ( 移除文件或目录 )

```cmd
rm [-fir] 文件或目录
```

选项与参数：

- -f ：就是 force 的意思，**忽略不存在**的文件，不会出现警告信息；
- -i ：互动模式，在删除前会**询问使用者是否动作**
- -r ：递归删除啊！最常用在目录的删除了！这是**非常危险**的选项！！！

```cmd
# 将刚刚在 cp 的实例中创建的 install.sh删除掉！
[root@kuangshen home]# rm -i install.sh
rm: remove regular file ‘install.sh’? y
# 如果加上 -i 的选项就会主动询问喔，避免你删除到错误的档名！
 
[root@iZ2ze5ovuegpc8xpnw3jsfZ home]# rm -i install.sh # 删除install.sh
rm: remove regular file ‘install.sh’? yes
 
# 尽量不要在服务器上使用  rm -rf /
```

### （8）mv  ( 移动文件与目录，或重命名 )

​	注意：**移动不是复制**，移动后，**原目录的文件**就不存在了

```cmd
[root@www ~]# mv [-fiu] source destination
[root@www ~]# mv [options] source1 source2 source3 .... directory
```

- -f ：force **强制**的意思，如果目标文件已经存在，**不会询问而直接覆盖**；
- -i ：若目标文件 (destination) 已经存在时，就会询问是否覆盖！
- -u ：若目标文件已经存在，且 source **比较新**，才会升级 (**update**)

```cmd
# 复制一个文件到当前目录
[root@kuangshen home]# cp /root/install.sh /home
 
# 创建一个文件夹 test
[root@kuangshen home]# mkdir test
 
# 将复制过来的文件移动到我们创建的目录，并查看
[root@kuangshen home]# mv install.sh test
[root@kuangshen home]# ls
test
[root@kuangshen home]# cd test
[root@kuangshen test]# ls
install.sh
 
# 将文件夹重命名，然后再次查看！
[root@kuangshen test]# cd ..
[root@kuangshen home]# mv test mvtest	# 将test重新命名为：mvtest
[root@kuangshen home]# ls
mvtest
```

## 4、文件属性查找和修改

### （1）文件属性

​	Linux系统是一种典型的**多用户系统**，不同的用户处于不同的地位，拥有不同的权限。为了保护系统的安全性，Linux系统对**不同的用户**访问**同一文件**（包括目录文件）的权限**做了不同的规定**。

---

​	**显示**一个文件的属性以及文件所属的用户和组：

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# ls -l	# 显示一个文件的属性以及文件所属的用户和组
```

<img src="/../images/ls-l.png" style="zoom:67%;" />

实例中:	**d  rwx   r-x   r-x**  19（**文件个数**） root root  2960 Apr  1 14:36 dev

<img src="/../images/命令.png" style="zoom:50%;" />

​	boot文件的第一个属性用"d"表示。"d"在Linux中代表该文件是一个**目录文件**。

​	在Linux中**第一个字符**代表这个文件是**目录、文件或链接文件**等等：

​	①	d ：目录

​	②	-  ：文件

​	③	 l ：链接文档 ( link file )；

​	④	 b ：则表示为**装置文件**里面的可供**储存的接口设备** ( 可随机存取装置 )；

​	⑤	 c ：则表示为装置文件里面的**串行端口设备**，例如键盘、鼠标 ( 一次性读取装置 )。

---

​	接下来的字符中，以三个为一组，且均为**『rwx』** 的**三个参数的组合**

​	其中，[` r `]代表**可读**(read)、[ `w` ]代表**可写**(write)、[` x` ]代表**可执行**(execute)。

​	要注意的是，这三个权限的**位置不会改变**，如果没有权限，就会出现减号[ - ]而已。

​	在以上实例中，boot 文件是一个目录文件，**属主和属组**都为 root。

### （2）文件修改

#### 	①	chgrp：更改文件属组

```cmd
chgrp [-R] 属组名 文件名
```

​	-R：**递归更改**文件属组，就是在更改某个目录文件的属组时，如果加上-R的参数，那么该目录下的所有文件的属组都会更改。

<img src="/../images/chgrp.png" style="zoom:50%;" />

#### 	②	chown：更改文件属主，也可以同时更改文件属组

```cmd
chown [–R] 属主名 文件名
chown [-R] 属主名：属组名 文件名
```

#### 	③	chmod：更改文件9个属性

​	文件的**权限字符**为：『-rwxrwxrwx』， 这九个权限是三个**三个一组**（**属主权限、属组权限、其他用户权限**）的！
​		(其中，我们可以使用**数字来代表各个权限**)

```cmd
chmod [-R] xyz 文件或目录
```

​	Linux**文件属性**有两种设置方法，一种是**数字**（常用），一种是**符号**。

​	Linux文件的基本权限就有九个，分别是：owner/group/others三种身份各有自己的read/write/execute权限。

​	各权限的分数对照表如下：

| r（可读） | w（可写） | x（可执行） |
| :-------: | :-------: | :---------: |
|     4     |     2     |      1      |

​	若：`rwx`   **等价与**：数字：7（4+2+1）

----

​	可读可写不可执行：rw——>6

​	可读可写可执行：rwx——>7

<img src="/../images/权限.png" style="zoom:50%;" />

​	**三组分别为**：属主权限、属组权限、其他用户权限

## 5、文件内容查看

Linux系统中使用以下命令来**查看文件的内容**：

- `cat` 由**第一行开始显示**文件内容
- `tac` 从**最后一行开始**显示，可以看出 tac 是 cat 的倒着写！
- `nl ` **显示行号**
- `more` **一页一页**的显示文件内容
- `less` 与 more 类似，但是比 more 更好的是，他可以**往前翻页**！（上下键）
- `head` 只看头几行

```cmd
[root@kuangshen network-scripts]# head -n 20 csh.login  # 查看指定行数(20行)
```

- tail 只看尾巴几行

` *man [命令]*来查看各个命令的使用文档，如 ：man cp`

**网络配置**目录：`cd/etc/sysconfig/network-sriptes`（CentOS）

`ifconfig`

### （1）语法

```cmd
cat [-AbEnTv]
```

演示都在**network-sriptes目录中**演示：

### （2）cat与tac

```cmd
[root@kuangshen sysconfig]# cd network-scripts	# 进入network-scripts
# 从开头显示
[root@kuangshen network-scripts]# cat ifcfg-eth0  # 查看ifcfg-eth0文件
# 倒着显示
[root@kuangshen network-scripts]# tac ifcfg-eth0  # 查看ifcfg-eth0文件
```

<img src="/../images/1.png" style="zoom:50%;" />

### （3）nl 显示行号

```cmd
# 显示行号
[root@kuangshen network-scripts]# nl ifcfg-eth0
```

<img src="/../images/2.png" style="zoom:50%;" />

### （4）more 翻页显示，按：Q退出

```cmd
# 翻页显示
[root@kuangshen network-scripts]# more ifcfg-eth0
```

<img src="/../images/3.png" style="zoom:50%;" />

### （5）less，翻页显示 和 可查找指定字符串（按n：继续向下查询；N：向上查询）

​	功能比more更好

​	可**查找指定字符串**（向下查询）

<img src="/../images/set1.png" style="zoom: 33%;" />

​	**输入：/** `/set`（**向下查询**） 后面跟需要查找的：字符串名

<img src="/../images/set2.png" style="zoom:33%;" />

​	**输入：？ `?set` （向上查询**） 后面跟需要查找的：字符串名

​	

# 六、Linux 链接概念

​	Linux 链接分两种，一种被称为**硬链接**（Hard Link），另一种被称为**符号链接**（Symbolic Link）。

----

## 1、硬链接

​	硬链接指通过**索引节点**来进行连接。使用命令：`ln`创建

​	在 Linux 的文件系统中，保存在磁盘分区中的文件不管是什么类型都给它**分配一个编号**，称为索引节点号(Inode Index)。在 Linux 中，**多个文件名**指向**同一索引节点**是存在的。

​	比如：A 是 B 的硬链接（A 和 B 都是文件名），则 A 的目录项中的 inode 节点号与 B 的目录项中的 inode 节点号相同，即一个 inode 节点对应两个不同的文件名，两个文件名指向同一个文件，A 和 B 对文件系统来说是完全平等的。**删除其中任何一个**都**不会影响另外一个**的访问。

​	**硬连接的作用**：允许**一个文件**拥有**多个有效路径名**，这样用户就可以建立硬连接到重要文件，以**防止“误删”**的功能。

```cmd
[root@kuangshen home]# ln f1 f2      # 创建f1的  一个硬连接文件 ： f2
```

---

## 2、软连接

​	软链接文件有类似于 **Windows 的快捷方式**。使用命令：`ln -s`创建

​	它实际上是一个特殊的文件。在符号连接中，文件实际上是一个文本文件，其中包含的有另一文件的位置信息。比如：A 是 B 的软链接（A 和 B 都是文件名），A 的目录项中的 **inode 节点号**与 B 的目录项中的 inode 节点号不相同，A 和 B 指向的是两个不同的 inode，继而指向**两块不同的数据块**。

​	 **A 的数据块**中**存放的只是 B 的路径名**（可以根据这个找到 B 的目录项）。A 和 B 之间是**“主从”关系**，如果 B 被删除了，A 仍然存在（因为两个是不同的文件），但**指向的是一个无效的链接**。

```cmd
[root@kuangshen home]# ln -s f1 f3   # 创建f1的一个符号连接文件f3
```

---

## 3、演示

### （1）创建f1、f2、f3文件夹

```cmd
[root@kuangshen /]# cd /home
[root@kuangshen home]# touch f1  # 创建一个测试文件f1
[root@kuangshen home]# ls
f1
[root@kuangshen home]# ln f1 f2      # 创建f1的一个硬连接文件f2
[root@kuangshen home]# ln -s f1 f3   # 创建f1的一个符号连接文件f3
[root@kuangshen home]# ls -li        # -i参数显示文件的inode节点信息
397247 -rw-r--r-- 2 root  root     0 Mar 13 00:50 f1
397247 -rw-r--r-- 2 root  root     0 Mar 13 00:50 f2
397248 lrwxrwxrwx 1 root  root     2 Mar 13 00:50 f3 -> f1
```

### （2）给f1添加字符串

```cmd
[root@kuangshen home]# echo "wo shi shuai ge" >>f1 

[root@kuangshen home]# cat f1  # 查看
```

### （3）删除操作

<img src="/../images/软链接.png" style="zoom:50%;" />

# 七、Vim编辑器

​	Vim是从 vi 发展出来的一个**文本编辑器**。**代码补完、编译及错误跳转**等方便编程的功能特别丰富，在程序员中被广泛使用。

​	简单的来说， vi 是老式的字处理器，不过功能已经很齐全了，但是还是有可以进步的地方。

​	vim 则可以说是程序开发者的一项很好用的工具。

​	所有的 Unix Like 系统都会内建 **vi 文书编辑器**，其他的文书编辑器则不一定会存在。

​	连 vim 的官方网站 (http://www.vim.org) 自己也说 vim 是一个**程序开发工具**而不是文字处理软件

## 1、Vim键盘图

<img src="/../images/VIM.jpg" style="zoom: 50%;" />

​	主要ex命令：（`:w` ：保存）、（`:q` ：退出）、（`:q!`：不保存退出）

​	退出Vim模式：`:wq`：保存退出、`:wq!`：强制保存退出

## 2、三种使用模式

​	基本上 vi/vim 共分为三种模式，分别是**命令模式（Command mode）**，**输入模式（Insert mode）**和**底线命令模式（Last line mode）**。这三种模式的作用分别是：

### （1）命令模式

​	用户刚刚启动： vi/vim，便进入了命令模式。

​	此状态下**敲击键盘动作**会被Vim识别为命令，而非输入字符。

​	比如我们此时按下：`i`，并不会输入一个字符，`i` ：被**当作了一个命令**。

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# cd /home
[root@iZ2ze5ovuegpc8xpnw3jsfZ home]# vim kuangstudy.txt		# 进入命令模式
```

<img src="/../images/new file.png" style="zoom:50%;" />

①	按 `i` 可**进入**输入模式（具体命令可见：下面第二部分）

<img src="/../images/insert.png" style="zoom: 25%;" />

②	按`x` 删除**当前光标**所在处的字符

③	按`:`切换到**底线命令模式**，以在最底一行输入命令。（若是：编辑模式`i`，则先按：`ESC`）

### （2）输入模式

在命令模式下按下i就进入了输入模式。

在输入模式中，可以使用以下按键：

- 字符按键以及Shift组合，输入字符
- `enter`，回车键，换行
- `BACK SPACE`，退格键，删除光标前一个字符
- `DEL`，删除键，删除光标后一个字符
- `方向键`，在文本中移动光标
- `HOME/END`，移动光标到行首/行尾
- `Page Up/Page Down`，上/下翻页
- `Insert`，切换光标为输入/替换模式，光标将变成竖线/下划线
- `ESC`，退出输入模式，**切换到命令模式**

### （3）底线命令模式

在命令模式下（`ESC` 退出后），再按下：`:`（英文冒号）就进入了**底线命令模式**。

底线命令模式可以**输入单个或多个字符**的命令，可用的命令非常多。

在**底线命令模式中**，基本的命令有：

- `q` ：退出程序
- `w`  ：保存文件

按`ESC键`   可随时**退出底线命令模式**

## 3、vim文件介绍

<img src="/../images/VIM文件介绍.png" style="zoom: 50%;" />

## 4、Vim工作模式

<img src="/../images/Vim工作模式.jpg" style="zoom:50%;" />

 - 新建或者编辑已存在的文件

   ```cmd
   [root@iZ2ze5ovuegpc8xpnw3jsfZ home]# vim kuangstudy.txt	
   ```

 - 进入命令模式后，按`i` 进入输入模式

 - 输入内容后，按`ESC` 退出输入模式；然后`:`进入底线命令模式，输入：`:wq`（保存退出）、`:wq!`（强制保存退出）

## 5、移动命令

|            命令            | 作用                                                         |
| :------------------------: | ------------------------------------------------------------ |
|     h 或 向左箭头键(←)     | 光标向**左移动**一个字符                                     |
|     j 或 向下箭头键(↓)     | 光标向**下移动**一个字符                                     |
|     k 或 向上箭头键(↑)     | 光标向**上移动**一个字符                                     |
|     l 或 向右箭头键(→)     | 光标向**右移动**一个字符                                     |
|        [Ctrl] + [f]        | 屏幕『向下』**移动一页**，相当于 [Page Down]按键 (常用)      |
|        [Ctrl] + [b]        | 屏幕『向上』**移动一页**，相当于 [Page Up] 按键 (常用)       |
|        [Ctrl] + [d]        | 屏幕『向下』移动**半页**                                     |
|        [Ctrl] + [u]        | 屏幕『向上』移动**半页**                                     |
|             +              | 光标移动到**非空格符**的下一行                               |
|             -              | 光标移动到**非空格符**的上一行                               |
| n< space><br />移动n个字符 | 那个 n 表示『数字』<br />例如 20 。**按下数字后**，**再按空格键**，光标会**向右**移动这一行的 **n 个字符**。 |
| n< Enter><br />移动到第n行 | n 为数字。光标向**下移动 n 行**(常用)，（先**退出输入模式**） |
|      0 或功能键[Home]      | 这是数字『 0 』：移动到**这一行**的**最前面字符处** (常用)   |
|      $ 或功能键[End]       | 移动到**这一行**的**最后面字符处**(常用)                     |
|             H              | 光标移动到这个屏幕的**最上方那一行**的**第一个字符**         |
|             M              | 光标移动到这个屏幕的**中央那一行**的**第一个字符**           |
|             L              | 光标移动到这个屏幕的**最下方那一行**的**第一个字符**         |
|             G              | 移动到这个档案的**最后一行**(常用)                           |
|             nG             | n 为数字。<br />移动到这个档案的第 n 行。例如 20G 则会移动到这个档案的第 20 行(可配合 :set nu) |
|             gg             | **移动**到这个档案的**第一行**，相当于 1G 啊！(常用)         |

## 6、搜索替换命令

| 命令  | 作用                                                         |
| :---: | ------------------------------------------------------------ |
| /word | 向光标之下**寻找一个名称为 word 的字符串**。<br />例如要在档案内搜寻 vbird 这个字符串，就输入 /vbird 即可！(常用) |
| ?word | 向**光标之上**寻找一个字符串名称为 word 的字符串。           |
|   n   | 这个 n 是英文按键。代表**重复前一个搜寻**的动作。<br />举例来说， 如果刚刚我们执行 /vbird 去向下搜寻 vbird 这个字符串，则按下 n 后，会向下继续搜寻下一个名称为 vbird 的字符串。如果是执行 ?vbird 的话，那么按下 n 则会向上继续搜寻名称为 vbird 的字符串！ |
|   N   | 这个 N 是英文按键。<br />**与 n 刚好相反**，为『反向』进行前一个搜寻动作。例如 /vbird 后，按下 N 则表示『向上』搜寻 vbird 。 |

## 7、删除、复制与粘贴命令

|   命令   | 作用                                                         |
| :------: | ------------------------------------------------------------ |
|   x, X   | 在一行字当中，x 为**向后删除**一个字符 (相当于 [del] 按键)， X 为**向前删除**一个字符(相当于 [backspace] 亦即是退格键) (常用) |
|    nx    | n 为数字，**连续向后删除** n 个字符。举例来说，我要连续删除 10 个字符， 『10x』。 |
|    dd    | **删除游标所在**的那**一整行**(常用)                         |
|   ndd    | n 为数字。**删除光标所在**的**向下** n 行，例如： **20dd** 则是删除 20 行 (常用) |
|   d1G    | 删除光标所在到**第一行**的所有数据                           |
|    dG    | 删除光标所在到**最后一行**的所有数据                         |
|    d$    | 删除游标所在处，到该行的最后一个字符                         |
|    d0    | 那个是数字的 0 ，删除游标所在处，到该行的最前面一个字符      |
|    yy    | **复制游标所在**的那一行(常用)                               |
|   nyy    | n 为数字。**复制光标所在**的**向下 n 行**，例如 20yy 则是复制 20 行(常用) |
|   y1G    | 复制游标所在行**到第一行**的所有数据                         |
|    yG    | 复制**游标所在行**到**最后一行**的所有数据                   |
|    y0    | 复制光标所在的那个字符到**该行行首**的所有数据               |
|    y$    | 复制光标所在的那个字符到**该行行尾**的所有数据               |
|   p, P   | p 为将**已复制的数据** 在光标下一行贴上，P 则为贴在**游标上一行**！<br />举例来说，我目前光标在第 20 行，且已经复制了 10 行数据。则**按下 p 后**， 那 10 行数据会贴在**原本的 20 行之后**，亦即由 21 行开始贴。但如果是**按下 P** 呢？那么原本的第 20 行会**被推到变成 30 行**。(常用) |
|    J     | 将**光标所在行**与**下一行的数据 **  **结合** 成同一行       |
|    c     | **重复删除**多个数据，例如向下删除 10 行，[ 10cj ]           |
|    u     | 复原**前一个**动作。(常用)                                   |
| [Ctrl]+r | 重做**上一个**动作。(常用)                                   |

## 8、编辑模式命令

| 命令  | 作用                                                         |
| :---: | ------------------------------------------------------------ |
| i, I  | **进入输入模式**(Insert mode)：i 为『从目前光标所在处输入』， I 为『在目前所在行的第一个非空格符处开始输入』。(常用) |
| a, A  | 进入输入模式(Insert mode)：a 为『从目前光标所在的下一个字符处开始输入』， A 为『从光标所在行的最后一个字符处开始输入』。(常用) |
| o, O  | 进入输入模式(Insert mode)：这是英文字母 o 的大小写。o 为『在目前光标所在的下一行处输入新的一行』；O 为在目前光标所在处的上一行输入新的一行！(常用) |
| r, R  | 进入**取代模式**(Replace mode)：r 只会取代光标所在的那一个字符一次；R会一直取代光标所在的文字，直到按下 ESC 为止；(常用) |
| [Esc] | 退出编辑模式，**回到一般模式**中(常用)                       |

## 9、底部命令模式命令

|        命名         | 作用                                                         |
| :-----------------: | ------------------------------------------------------------ |
|         :w          | 将编辑的数据**写入**硬盘档案中(常用)                         |
|         :w!         | 若文件属性为『只读』时，**强制写入**该档案。不过，到底能不能写入， 还是跟你对该档案的档案权限有关啊！ |
|         :q          | 离开 vi (常用)                                               |
|         :q!         | 若曾修改过档案，又不想储存，使用 ! 为强制离开不储存档案。    |
|          !          | (!) 在 vi 当中，常常具有『**强制**』的意思～                 |
|         :wq         | 储存后离开，若为 :wq! 则为**强制储存**后离开 (常用)          |
|         ZZ          | 这是大写的 Z 喔！若档案没有更动，则不储存离开，若档案已经被更动过，则储存后离开！ |
|    :w [filename]    | 将编辑的数据储**存成另一个档案**（类似另存新档）             |
|    :r [filename]    | 在编辑的数据中，**读入另一个档案的数据**。亦即将 『filename』 这个档案内容加到游标所在行后面 |
| :n1,n2 w [filename] | 将 n1 到 n2 的内容**储存成** filename 这个档案               |
|     :! command      | 暂**时离开** vi 到**指令行模式下执行** command 的显示结果！例如 『:! ls /home』即可在 vi 当中看 /home 底下以 ls 输出的档案信息！ |
|       :set nu       | **显示行号**，设定之后，会在每一行的前缀显示该行的行号       |
|      :set nonu      | 与 set nu 相反，为**取消行号**！                             |

# 八、账号管理

​	Linux系统是一个多用户多任务的分时操作系统，任何一个要使用系统资源的用户，都必须**首先向系统管理员申请一个账号**，然后以这个账号的身份进入系统。

​	用户的账号一方面可以帮助系统管理员**对使用系统的用户进行跟踪**，并控制他们对系统资源的访问；另一方面也可以帮助用户组织文件，并为用户提供安全性保护。

​	每个用户账号都拥有一个**唯一的用户名和各自的口令**。

​	用户在登录时键入正确的用户名和口令后，就能够进入系统和自己的主目录。

实现用户账号的管理，要完成的工作主要有如下几个方面：

- 用户账号的添加、删除与修改。
- 用户口令的管理。
- 用户组的管理。

---

​	用户账号的管理工作**主要涉及**到：用户账号的添加、修改和删除。

​	添加用户账号就是在系统中创建一个新账号，然后为新账号分配用户号、用户组、主目录和登录Shell等资源。

## 1、查看所有的系统用户

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ home]# cat /etc/passwd
```

<img src="/../images/系统用户.png" style="zoom:50%;" />

## 2、新增用户 useradd

语法：

```
useradd 选项 用户名
```

参数说明：

选项 :

	- c：comment 指定一段**注释性描述**。
	- d：目录 指定用户主目录，如果此目录不存在，则同时使用-m选项，可以**创建主目录**。
	- g： 用户组 **指定**用户**所属的用户组**。
	- G ：用户组，用户组 指定用户**所属的附加组**。
	- m：使用者目录**如不存在则自动建立**。
	- s：Shell文件 指定用户的**登录Shell**。
	- u：用户号 指定用户的用户号，如果同时有-o选项，则可以重复使用其他用户的标识号。

用户名 ：**指定**新账号的登录名。

```cmd
# 此命令创建了一个用户kuangshen，其中-m选项用来为登录名kuangshen产生一个主目录 /home/kuangshen
[root@kuangshen home]# useradd -m kuangshen
```

<img src="/../images/qinjiang文件.png" style="zoom:50%;" />

## 3、删除用户

​	如果一个用户的账号**不再使用，可以从系统中删除**。

​	删除用户账号就是要将**/etc/passwd等系统文件中的该用户记录**删除，必要时还**删除用户的主目录**。

​	删除一个已有的用户账号使用userdel命令，其格式如下：

```
userdel 选项 用户名
```

​	常用的选项是` -r`，它的作用是**把用户的主目录一起删除**。

```cmd
[root@kuangshen home]# userdel -r kuangshen
```

​	此命令删除用户kuangshen在系统文件中（主要是**/etc/passwd, /etc/shadow, /etc/group**等）的记录，**同时删除**用户的主目录。

## 4、修改用户 usermod

​	修改用户账号就是**根据实际情况更改**用户的有关属性，如用户号、主目录、用户组、登录Shell等。

​	修改已有用户的信息使用usermod命令，其格式如下：

```
usermod 选项 用户名
```

​	常用的选项包括-c, -d, -m, -g, -G, -s, -u以及-o等，这些选项的意义**与useradd命令中的选项一样**，可以为用户指定新的资源值。

```cmd
# usermod -s /bin/ksh -d /home/z –g developer kuangshen
```

​	**此命令**：将用户kuangshen的登录Shell修改为ksh，主目录改为/home/z，用户组改为developer。

```cmd
[root@kuangshen home]# usermod -d /home/233 qinjiang
```

<img src="/../images/修改账户.png" style="zoom:50%;" />

## 5、切换用户 su

​	**默认：**root用户

<img src="/../images/介绍.png" style="zoom:50%;" />



（1）切换用户的命令为：su username 【username是你的用户名哦】

（2）从普通用户切换到root用户，还可以使用命令：sudo su

（3）在终端输入exit或logout或使用快捷方式ctrl+d，可以退回到原来用户，其实ctrl+d也是执行的exit命令

（4）在切换用户时，如果想在切换用户之后使用新用户的工作环境，可以在su和username之间加-，例如：【su - root】

​	$ ：表示**普通用户**

​	#：表示**超级用户**，也就是root用户

```cmd
[root@kuangshen home]# su qinjiang
```

<img src="/../images/切换用户命令.png" style="zoom:50%;" />

## 6、退出普通用户

```cmd
[qinjiang@kuangshen home]# exit
```

<img src="/../images/退出普通用户.png" style="zoom:50%;" />

## 7、修改主机名

​	修改完成后，需要**重新启动**。

<img src="/../images/修改主机名.png" style="zoom:50%;" />



## 8、用户的密码设置

​	Linux上输入密码，是**不会显示的**，正常输入就可以了。

<img src="/../images/给qinjiang用户设置密码.png" style="zoom:67%;" />

​	设置好后，新建一个链接

<img src="/../images/输入密码.png" style="zoom:50%;" />

​	注意：只有**Root才有权限**。

## 9、锁定账户

```cmd
passwd -l qinjiang # 锁定之后，这个用户就不能登录了
passwd -d qinjiang # 没有密码也不能登录
```

<img src="/../images/锁账户.png" style="zoom:67%;" />



# 九、用户组管理

​	属主、属组

​	**每个用户都有一个用户组**，系统可以对一个用户组中的所有用户进行集中管理。不同Linux 系统对用户组的规定有所不同，如Linux下的用户属于与它同名的用户组，这个用户组在创建用户时同时创建。

​	用户组的管理涉及用户组的**添加、删除和修改**。组的增加、删除和修改实际上就是对/etc/group文件的更新。

## 1、创建用户组 groupadd

```
groupadd 选项 用户组
```

可以使用的选项有：

- -g GID 指定新用户组的组标识号（GID）。
- -o 一般与-g选项同时使用，表示新用户组的GID可以与系统已有用户组的GID相同。

```cmd
[root@kuangshen ~]# # groupadd -g 101 group2
```

​	此命令向系统中**增加了一个新组**group2，同时指定新组的**组标识号是101**。（若不指定标记号，则**自动自增**）

## 2、查看用户组

```cmd
[root@kuangshen ~]# cat /etc/group
```

<img src="/../images/用户组.png" style="zoom:50%;" />

​	用户组的信息都放在：`etc/group`

## 3、修改用户组 groupmod

```
groupmod 选项 用户组
```

常用的选项有：

- -g GID 为用户组指定新的组标识号。
- -o 与-g选项同时使用，用户组的新GID可以与系统已有用户组的GID相同。
- -n新用户组 将用户组的名字改为新名字

```cmd
# 此命令将组group2的组标识号修改为102。
groupmod -g 102 group2
 
# 将组group2的标识号改为10000，组名修改为group3。
groupmod –g 10000 -n group3 group2
```

## 4、删除用户组 groupdel

```
groupdel 用户组
```

```cmd
# groupdel group1	# 删除用户组
```

## 5、切换组 newgrp

​	如果一个用户同时属于多个用户组，那么用户可以**在用户组之间切换**，以便具有其他用户组的权限。

​	用户可以在登录后，使用命令newgrp**切换到其他用户组**，这个命令的参数就是目的用户组。例如：

```cmd
$ newgrp root
```

​	这条命令将当前用户**切换到root用户组**，前提条件是root用户组确实是该用户的**主组或附加组**。

## 6、文件查看

​	完成用户管理的工作有许多种方法，但是**每一种方法实际上都是对有关的系统文件进行修改**。

​	与用户和用户组相关的信息都存放在一些系统文件中，这些文件包括：/etc/passwd, /etc/shadow, /etc/group等。

---

​	`	/etc/passwd `  :  文件是用户管理工作涉及的**最重要的一个文件**。

​	Linux系统中的每个用户都在`/etc/passwd`  文件中**有一个对应的记录行**，它记录了这个用户的一些基本属性。

​	这个文件对所有用户**都是可读的**。它的内容类似下面的例子：

```cmd
＃ cat /etc/passwd
```

```
# 文件属性查询
root:x:0:0:Superuser:/:
daemon:x:1:1:System daemons:/etc:
bin:x:2:2:Owner of system commands:/bin:
sys:x:3:3:Owner of system files:/usr/sys:
adm:x:4:4:System accounting:/usr/adm:
uucp:x:5:5:UUCP administrator:/usr/lib/uucp:
auth:x:7:21:Authentication administrator:/tcb/files/auth:
cron:x:9:16:Cron daemon:/usr/spool/cron:
listen:x:37:4:Network daemon:/usr/net/nls:
lp:x:71:18:Printer administrator:/usr/spool/lp:
```

​	从上面的例子我们可以看到，`/etc/passwd` 中**一行记录**对应着**一个用户**，每行记录又被冒号(:)分隔为**7个字段**；

​	其格式和具体含义如下：

```
用户名:口令:用户标识号:组标识号:注释性描述:主目录:登录Shell
```

​	这个文件中的每一行都代表一个用户，可以看到用户的主目录在哪里，属于哪一个组。

​	登录口令：把真正加密后的用户口令存放到：`etc/shadow` 文件中，保证了密码的安全性。

# 十、磁盘管理

Linux磁盘管理好坏直接关系到整个系统的性能问题。

Linux磁盘管理常用命令为 df、du。

- df ：**列出**文件系统的**整体磁盘使用量**
- du：**检查**磁盘空间**使用量**

## 1、查看磁盘使用量 df

```
df [-ahikHTm] [目录或文件名]
```

**选项与参数：**

	- -a ：列出所有的文件系统，包括系统特有的 /proc 等文件系统；
	- -k ：以 **KBytes 的容量**显示各文件系统；
	- -m ：以 **MBytes 的容量**显示各文件系统；
	- -h ：以人们较易阅读的 GBytes, MBytes, KBytes 等格式自行显示；
	- -H ：以 M=1000K 取代 M=1024K 的进位方式；
	- -T ：显示文件系统类型, 连同该 partition 的 filesystem 名称 (例如 ext3) 也列出；
	- -i ：不用硬盘容量，而以 inode 的数量来显示

```cmd
[root@kuangshen~]# df 
[root@kuangshen~]# df -h  # 磁盘容量以：M显示
```

<img src="/../images/查看磁盘.png" style="zoom:50%;" />

```cmd
# 将系统内所有的文件系统列出来！
# 在 Linux 底下如果 df 没有加任何选项
# 那么默认会将系统内所有的 (不含特殊内存内的文件系统与 swap) 都以 1 Kbytes 的容量来列出来！
[root@kuangshen /]# df
Filesystem     1K-blocks    Used Available Use% Mounted on
devtmpfs          889100       0    889100   0% /dev
tmpfs             899460     704    898756   1% /dev/shm
tmpfs             899460     496    898964   1% /run
tmpfs             899460       0    899460   0% /sys/fs/cgroup
/dev/vda1       41152812 6586736  32662368  17% /
tmpfs             179896       0    179896   0% /run/user/0
```

```cmd
# 将系统内的所有特殊文件格式及名称都列出来
[root@kuangshen /]# df -aT
Filesystem     Type        1K-blocks    Used Available Use% Mounted on
sysfs          sysfs               0       0         0    - /sys
proc           proc                0       0         0    - /proc
devtmpfs       devtmpfs       889100       0    889100   0% /dev
securityfs     securityfs          0       0         0    - /sys/kernel/security
tmpfs          tmpfs          899460     708    898752   1% /dev/shm
devpts         devpts              0       0         0    - /dev/pts
tmpfs          tmpfs          899460     496    898964   1% /run
tmpfs          tmpfs          899460       0    899460   0% /sys/fs/cgroup
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/systemd
pstore         pstore              0       0         0    - /sys/fs/pstore
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/freezer
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/cpuset
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/hugetlb
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/blkio
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/net_cls,net_prio
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/memory
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/pids
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/cpu,cpuacct
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/devices
cgroup         cgroup              0       0         0    - /sys/fs/cgroup/perf_event
configfs       configfs            0       0         0    - /sys/kernel/config
/dev/vda1      ext4         41152812 6586748  32662356  17% /
systemd-1      -                   -       -         -    - /proc/sys/fs/binfmt_misc
mqueue         mqueue              0       0         0    - /dev/mqueue
debugfs        debugfs             0       0         0    - /sys/kernel/debug
hugetlbfs      hugetlbfs           0       0         0    - /dev/hugepages
tmpfs          tmpfs          179896       0    179896   0% /run/user/0
binfmt_misc    binfmt_misc         0       0         0    - /proc/sys/fs/binfmt_misc
```

```cmd
# 将 /etc 底下的可用的磁盘容量以易读的容量格式显示
[root@kuangshen /]# df -h /etc
Filesystem      Size  Used Avail Use% Mounted on
/dev/vda1        40G  6.3G   32G  17% /
```

## 2、查看文件使用容量 du

​	Linux du命令也**是查看使用空间**的，但是与df命令不同的是Linux du命令是**对文件和目录磁盘使用的空间的查看**，还是和df命令有一些区别的，这里介绍Linux du命令。

```
du [-ahskm] 文件或目录名称
```

选项与参数：

	- -a ：列出**所有的文件与目录容量**，因为默认仅统计目录底下的文件量而已。
	- -h ：以人们较易读的容量格式 (G/M) 显示；
	- -s ：列出总量而已，而不列出每个各别的目录占用容量；
	- -S ：不包括子目录下的总计，与 -s 有点差别。
	- -k ：以 KBytes 列出容量显示；
	- -m ：以 MBytes 列出容量显示；

```cmd
[root@kuangshen home]# du
```

<img src="/../images/查看文件容量.png" style="zoom:50%;" />

```cmd
[root@kuangshen /]# du -sm /*
```

<img src="/../images/查看整个电脑的文件使用情况.png" style="zoom:50%;" />

​	通配符` *` 来**代表每个目录**。

​	与 df 不一样的是，du 这个命令其实会**直接到文件系统内**去搜寻所有的文件数据。

## 3、磁盘挂载与卸除 u/mount

​	**根文件系统之外**的**其他文件**要想能够被访问，都必须**通过“关联”至  根文件系统上的某个目录**来实现，此关联操作即为“**挂载**”，此目录即为“挂载点”,解除此关联关系的过程称之为“卸载”

​	Linux 的**磁盘挂载**使用mount命令，**卸载**使用umount命令。

**磁盘挂载：**

```cmd
[root@kuangshen /]# mount /dev/kuangshen /mnt/kuangshen
```

<img src="/../images/挂载.png" style="zoom:50%;" />

```cmd
# 将 /dev/hdc6 挂载到 /mnt/hdc6 上面！
[root@www ~]# mkdir /mnt/hdc6
[root@www ~]# mount /dev/hdc6 /mnt/hdc6
[root@www ~]# df
Filesystem           1K-blocks      Used Available Use% Mounted on
/dev/hdc6              1976312     42072   1833836   3% /mnt/hdc6
```

**卸载：**

```
umount [-fn] 装置文件名或挂载点
```

选项与参数：

- -f ：强制卸除！可用在类似网络文件系统 (NFS) 无法读取到的情况下；

- -n ：不升级 /etc/mtab 情况下卸除。

  卸载`/dev/hdc6`

```cmd
[root@www ~]# umount /dev/hdc6
```

# 十一、进程管理

## 1、基本概念

（1）在Linux中，每一个程序都有自己的一个进程（Id号）

（2）每一个进程，都有一个父进程

（3）进程有两种存在方式：前台、后台运行

（4）服务：后台；程序：前台

## 2、命令 ps



​		`	-a`  ：显示当前 终端运行的所有进程信息

​		`-u`  ：以用户的信息显示进程

​		`-x`  ：显示后台运行进程的参数

```cmd
# ps -aux 查看所有进程
ps -aux | grep 需要查看的服务
```

 `|` ：管道符

 `grep` ：查找文件中符合条件的字符串

eg：

```cmd
[root@kuangshen~]# ps -aux | grep mysql		# mysql相关信息
[root@kuangshen~]# ps -aux | grep java		# java相关信息
```

## 3、查看父进程 ps -ef

```cmd
[root@kuangshen~]# ps -ef | grep mysql	
```

​	看父进程，一般通过：**目录树结构**来查看~

```cmd
[root@kuangshen~]# pstree -pu
```

<img src="/../images/查看父进程.png" style="zoom: 33%;" />

## 4、结束进程

```
kill id（进程的id）
```

<img src="/../images/id.png" style="zoom:50%;" />

```cmd
[root@kuangshen~]# kill -9
```

# 十二、环境安装

安装软件有三种方式：

- rpm
- 解压缩
- yun在线安装

## 1、JDK安装

### （1）下载JDK rpm

网址：https://www.oracle.com/java/technologies/javase-downloads.html

免费：https://blog.csdn.net/jzycloud/article/details/114123530

<img src="/../images/上传JDK.png" style="zoom: 50%;" />

### （2）解压JDK到root目录下

### （3）检查当前环境是否存在JAVA环境

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# java -version
-bash: java: command not found
```

### （4）安装JDK

```cmd
# rpm -qa | grep jdk  # 检测JDK版本信息

# rpm -e --nodeps jdk_	# 强制卸载

# rpm -ivk rmp包   #卸载为了即可安装JDK
```

---

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ ~]# ls
install.sh  jdk1.8.0_162  jdk-8u281-linux-x64.rpm
```

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ ~]# rpm -ivh jdk-8u221-linux-x64.rpm		# 安装jdk
```

<img src="/../images/jdk安装成功.png" style="zoom:50%;" />

### （5）配置环境变量

​		配置环境变量文件：`/etc/profile`

**第一步：**进入配置文件中

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ ~]# vim /etc/profile
```

<img src="/../images/进入环境配置文件.png" style="zoom:50%;" />

**第二步：**按`i` 进入输入模式

**第三步：**在最下方配置：

​	先在`/usr/java/` 查看JDK路径：

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ local]# cd /usr
[root@iZ2ze5ovuegpc8xpnw3jsfZ usr]# ls
bin  etc  games  include  java  lib  lib64  libexec  local  sbin  share  src  tmp
[root@iZ2ze5ovuegpc8xpnw3jsfZ usr]# cd java
[root@iZ2ze5ovuegpc8xpnw3jsfZ java]# ls
default  jdk1.8.0_221-amd64  jdk1.8.0_281-amd64  latest
[root@iZ2ze5ovuegpc8xpnw3jsfZ java]# cd jdk1.8.0_221-amd64/		# 查看jdk1.8.0_221-amd64文件
[root@iZ2ze5ovuegpc8xpnw3jsfZ jdk1.8.0_221-amd64]# ls
bin        javafx-src.zip  README.html
COPYRIGHT  jre             THIRDPARTYLICENSEREADME-JAVAFX.txt
include    LICENSE         THIRDPARTYLICENSEREADME.txt
```

 `jdk1.8.0_221-amd64/`  ：为环境配置路径

​	然后在`/etc/profile` 文件下输入：

​	（配置环境变量，跟windows安装环境变量一样）

```vim
JAVA_HOME=/usr/java/jdk1.8.0_221-amd64  # java根路径
CLASSPATH=%JAVA_HOME%/lib;%JAVA_HOME%/jre/lib # class类文件
PATH=$JAVA_HOME/bin;$JAVA_HOME/jre/bin  # 环境变量文件
export PATH CLASSPATH JAVA_HOME  # 将三个文件导出
```

<img src="/../images/环境配置.png" style="zoom: 67%;" />

​	`:wq`  保存退出

​	按配置文件生效：`source/etc/profile`

```cmd
[root@kuangshen~]# java  # 查看是否配置成功
```

## 2、发布项目

### （1）将spring项目打开jar包

​	将jar**移动到**Linux的root目录下

### （2）查看是否开启9000端口

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# firewall-cmd --list-ports
20/tcp 21/tcp 22/tcp 80/tcp 8888/tcp 39000-40000/tcp
```

无9000端口，执行下方命令开启：

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# firewall-cmd --zone=public --add-port=9000/tcp --permanent
```

添加端口后，需要**重启防火墙**：

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# firewall-cmd --list-ports
20/tcp 21/tcp 22/tcp 80/tcp 8888/tcp 39000-40000/tcp
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# systemctl restart firewalld.service  # 重启
[root@iZ2ze5ovuegpc8xpnw3jsfZ /]# firewall-cmd --list-ports
20/tcp 21/tcp 22/tcp 80/tcp 8888/tcp 39000-40000/tcp 9000/tcp
```

### （3）运行jar包

​	① 执行jar包的命令

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ ~]# java -jar demo-0.0.1-SNAPSHOT.jar 
```

​	②  将jar程序设置成**后台运行**，并且将**标准输出**的日志重定向至文件consoleMsg.log。

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ ~]#  nohup java -jar demo-0.0.1-SNAPSHOT.jar  >consoleMsg.log 2>&1 &
```



### （4）浏览

​	http://8.140.163.117:9000/  （公网访问）

## 3、Tomcat安装

### （1）准备Tomcat安装包

​	安装好了Java环境后我们可以测试下Tomcat！准备好Tomcat的安装包！

### （2）解压

​	 将文件移动到`/usr/tomcat/`下，并解压！

### （3）开启端口：8080

### （4）启动和关闭

```cmd
# 执行：startup.sh  -->启动tomcat
# 执行：shutdown.sh -->关闭tomcat
./startup.sh
./shutdown.sh
```

<img src="/../images/tomcat启动.png" style="zoom:50%;" />

## 4、确保Linux的防火墙端口是开启的，如果是阿里云，需要保证阿里云的安全组策略是开放的！

```cmd
# 查看firewall服务状态
systemctl status firewalld
 
# 开启、重启、关闭、firewalld.service服务
# 开启
service firewalld start
# 重启
service firewalld restart
# 关闭
service firewalld stop
 
# 查看防火墙规则
firewall-cmd --list-all    # 查看全部信息
firewall-cmd --list-ports  # 只看端口信息
 
# 开启端口
开端口命令：firewall-cmd --zone=public --add-port=80/tcp --permanent
重启防火墙：systemctl restart firewalld.service
 
命令含义：
--zone #作用域
--add-port=80/tcp  #添加端口，格式为：端口/通讯协议
--permanent   #永久生效，没有此参数重启后失效
```

## 5、Docker（yum安装）

​	前提：**虚拟机一定要联网**

​	官网安装参考手册：https://docs.docker.com/install/linux/docker-ce/centos/

### （1）确定你是CentOS7及以上版本

```cmd
[root@192 Desktop]# cat /etc/redhat-release
CentOS Linux release 7.2.1511 (Core)
```

### （2）yum安装gcc相关（需要确保 虚拟机可以上外网 ）

```cmd
[root@kuangshen bin]# yum -y install gcc
[root@kuangshen bin]# yum -y install gcc-c++
```

### （3）卸载旧版本

```cmd
[root@kuangshen bin]# yum -y remove docker docker-common docker-selinux docker-engine

# 官网版本
[root@kuangshen bin]# yum remove docker \
            docker-client \
            docker-client-latest \
            docker-common \
            docker-latest \
            docker-latest-logrotate \
            docker-logrotate \
            docker-engine
```

### （4）安装需要的软件包

```cmd
[root@kuangshen bin]# yum install -y yum-utils device-mapper-persistent-data lvm2
```

### （5）设置stable镜像仓库

```cmd
# 错误
[root@kuangshen bin]# yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
## 报错
[Errno 14] curl#35 - TCP connection reset by peer
[Errno 12] curl#35 - Timeout
 
# 正确推荐使用国内的
[root@kuangshen bin]# yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
```

### （6）更新yum软件包索引

```cmd
yum makecache fast
```

### （7）安装Docker CE

```cmd
yum -y install docker-ce docker-ce-cli containerd.io
```

### （8）启动docker

```cmd
systemctl start docker
```

### （9）测试

```cmd
docker version
 
docker run hello-world
 
docker images
```

## 6、MySQL安装

网址：https://blog.csdn.net/yoohhwz/article/details/93489175?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522161741282516780266278877%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=161741282516780266278877&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-1-93489175.first_rank_v2_pc_rank_v29&utm_term=Linux%E9%9C%80%E8%A6%81%E9%83%A8%E7%BD%B2%E6%95%B0%E6%8D%AE%E5%BA%93%E5%90%97

​	mysql安装目录在`/usr/local`下，然后通过Xftp **上传**mysql安装包

### （1）检查是否已经存在Mysql进程

​		使用`ps -ef | grep mysqld `命令







## 7、总结

​	上传完项目后，需要**购买自己的域名**，备案解析过去；

​	域名解析后，如果端口是：80（http）或者443（https），可以**直接访问**；若是8080或者9000，则需要Apcahe或者Niginx做一下代理。

