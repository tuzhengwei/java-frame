---
typora-root-url: images
---

# 一、MyBatis简介

学习网站：https://www.bilibili.com/video/BV1NE411Q7Nx?p=32（狂神说）

## 1、什么是MyBatis

​		**SSM**：Mybatis+Spring+SpringMVC
​		MyBatis本是一个apache的一个开源项目iBatis（Internet+abatis），是一个基于java的持久层框架。
​		MyBatis 是一款优秀的持久层框架，它支持**自定义 SQL**、**存储过程以**及**高级映射**。MyBatis **免除**了几乎所有的 **JDBC 代码**以及**设置参数**和**获取结果集**的工作。MyBatis 可以通过简单的 **XML 或注解**来配置和映射原始类型、接口和 Java POJO（Plain Old Java Objects，普通老式 Java 对象）为数据库中的记录。

## 2、怎么导入MyBatis

### 2.1  使用Maven仓库

​		Maven 仓库是项目中依赖的第三方库，这个库所在的位置叫做仓库。
​		在 Maven 中，任何一个依赖、插件或者项目构建的输出，都可以称之为构件。Maven 仓库能帮助我们管理构件（主要是JAR），它就是放置所有JAR文件（WAR，ZIP，POM等等）的地方。
​		在pom.xml文件中写入：导入依赖文件

```xml
<dependency>
  <groupId>org.mybatis</groupId>
  <artifactId>mybatis</artifactId>
  <version>x.x.x</version>
</dependency>
```

## 3、持久化（动词）

数据持久化

	- 持久化就是将程序的数据在持久状态和瞬时状态转化的过程
	- 内存：断电即失
	- 实际（JDBC）、IO文件持久化

为什么需要持久化？
    内存太贵、不能让某些对象、数据丢失

## 4、持久层

Dao层、Service层、Controller层

（Dao层叫数据访问层，属于一种比较底层，比较基础的操作，可以具体到对于某个表或某个实体的增删改查，其*Dao层的作用*是对数据库的访问进行封装，从而不涉及业务）

- 完成持久化工作的代码块
- 层界限十分明显

## 5、为什么需要MyBatis？

- 帮助程序员将数据存入到数据库中；
- 简单易用
- sql与代码分离，提高了可维护性
- 提供映射标签，支持对象与数据库的orm（对象关系映射）字段关系映射
- 提供xml标签，支持编写动态sql

# 二、第一个Mybatis程序

## 1、搭建数据库

```mysql
create database 'mybatis';

use 'mybatis';

创建表
CREATE TABLE user_table(
	id INT(10) NOT NULL PRIMARY KEY,
	name VARCHAR(20) DEFAULT NULL,
	pwd VARCHAR(20) DEFAULT NULL
)ENGINE=INNODB DEFAULT CHARSET=utf8;


插入三条数据
insert into user_table(id,name,pwd) values
(1,'狂神','123456'),
(2,'张三','456789'),
(3,'李四','789123')
```

## 2、创建项目（IDEA）

### 2.1	创建一个Maven项目

```
GroupId：com.example
Artifactld：mybatis-study01
----------------------------------
project name：mybatis-study01（注意加上‘-’，否则会报错）
```

### 2.2	删除src目录，使之成为一个：父工程

### 2.3	导入Maven依赖

```xml
<!-- 导入相关依赖-->
<dependencies>
    <!-- 导入mysql依赖-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.47</version>
    </dependency>

    <!-- 导入mybatis依赖-->
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis</artifactId>
        <version>3.5.2</version>
    </dependency>

    <!-- 导入junit依赖-->
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
    </dependency>
</dependencies>
```

### 2.4	new Module（Maven文件）

​		子模块名称：mybaitsson01

<img src="/../images/子模块结构.png" alt="子模块结构" style="zoom:33%;" />

## 3	子模块从 XML 中构建 SqlSessionFactory

​	从 XML 文件中构建 SqlSessionFactory 的实例

<img src="/../images/项目整体结构（第一个Mybaits项目）.png" alt="项目整体结构（第一个Mybaits项目）" style="zoom:33%;" />

### 3.1	创建xml文件

​			在子模块的resources文件下创建一个：mybatis-config.xml文件（File文件）

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">

<!-- configuration 核心配置文件 -->
<configuration>
    <!-- environments 多环境配置  default：默认选择的环境-->
    <environments default="development">
        <environment id="development">
            <transactionManager type="JDBC"/>
            <dataSource type="POOLED">
                <!-- ${driver} ：数据库驱动 -->
                <property name="driver" value="${driver}"/>
                <!-- ${url} ：数据库地址 -->
                <property name="url" value="${url}"/>
                <!-- ${username} ：数据库用户 -->
                <property name="username" value="${username}"/>
                <!-- ${password} ：数据库密码 -->
                <property name="password" value="${password}"/>
            </dataSource>
        </environment>
    </environments>
    
    <!-- 每一个Mapper.xml都需要在Mybatis核心配置文件中注册  -->
    <mappers>
        <!--<mapper resource="org/mybatis/example/BlogMapper.xml"/>-->
        <mapper resource="com/kuang/dao/UserMapper.xml"></mapper>
    </mappers>

</configuration>
```

​		例如：

```xml
 ...
	<property name="driver" value="com.mysql.jdbc.Driver"/>
    <!--
    jdbc:mysql://localhost:3306/mybatis?useSSL=true&amp;userUnicode=true&amp;
    mybatis：数据库； useSSL安全连接；userUnicode：使用中文；characterEncoding：使用编码
    多个值使用：&amp连接，中间使用：分号隔开
    -->
    <property name="url" value="jdbc:mysql://localhost:3306/mybatis?useSSL=true&amp;userUnicode=true&amp;characterEncoding=utf-8"/>
    <property name="username" value="root"/>
    <property name="password" value="123456"/>
 ...
```

### 3.2	创建SqlSessionFactory并获取Sqlsession

**作用域（Scope）和生命周期**

​		**SqlSessionFactoryBuilder：**可以被实例化、使用和丢弃，一旦创建了 SqlSessionFactory，就不再需要它了。
​		**SqlSessionFactory：**一旦被创建就应该在应用的运行期间一直存在，没有任何理由丢弃它或重新创建另一个实例。
​		**SqlSession：**不是线程安全的，因此是不能被共享的，每次收到 HTTP 请求，就可以打开一个 SqlSession，返回一个响应后，就关闭它。

![作用域（Scope）和生命周期](/../images/作用域（Scope）和生命周期.png)

​		从 XML 文件中构建 SqlSessionFactory 的实例非常简单，建议使用类路径下的资源文件进行配置。 
​		但也可以使用任意的输入流（InputStream）实例，比如用文件路径字符串或 file:// URL 构造的输入流。MyBatis 包含一个名叫 Resources 的工具类，它包含一些实用方法，使得从类路径或其它位置加载资源文件更加容易。

```java
/* org/mybatis/example/mybatis-config.xml：xml文件的路径 */
String resource = "org/mybatis/example/mybatis-config.xml";
/* 将配置好的xml文件加载进来 */
InputStream inputStream = Resources.getResourceAsStream(resource);
/*  使用SqlSessionFactoryBuilder构建 */
SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
```

​		**在java文件下创建包（package）：**
​			com.kuang.dao/utils：

| 包名  | 简述                     |
| :---: | ------------------------ |
|  dao  | 数据库操作对象包（接口） |
| pojo  | 实体类包                 |
| utils | 配置类包                 |

<img src="/../images/子模块结构1.png" alt="子模块结构1" style="zoom:33%;" />

​		在utils包下编写一个：MybatisUtils.java文件
​		（**获取：SqlSessionFactory**以及用SqlSessionFactory**获取**sqlSession）

```java
package com.kuang.utils;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * 从 SqlSessionFactory 中获取 SqlSession
 */
public class MybatisUtils {

     private static SqlSessionFactory  sqlSessionFactory;
    /**
     * 从 XML 中构建 SqlSessionFactory 必须写入静态块，已启动就自动加载
     */
    static{
        try {
            /**
             *  使用Mybatis第一步：
             *      构建SqlSessionFactory对象
             */
            String resource = "mybatis-config.xml";     // 加载xml文件
            InputStream inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *       使用Mybatis第二步：
     *              从 SqlSessionFactory 中获取 SqlSession。
     *      SqlSession 提供了在数据库执行 SQL 命令所需的所有方法。
     * 你可以通过 SqlSession 实例来直接执行已映射的 SQL 语句。
     */
    public static SqlSession getSqlSession(){
        /**
         * 使用sqlSessionFactory 来打开sqlSession
         */
        return sqlSessionFactory.openSession(); 
    }
}

```

## 4	 编写代码

### 4.1	实体类

​		在包：pojo包下创建一个跟数据库中：user表一样的java实体类。

```java
public class User {

    private int id;
    private String name;
    private String pwd;
    ...
    get与set方法
```

### 4.2	Dao接口

​		在包：dao下创建一个数据库操作的**Dao接口**，此处的Mapper为：操作用户表的接口类

```java
/**
 *  数据库操作对象，一般类名为：...Mapper
 */
public interface Mapper {
    	// 抽象方法，获得“查询全部用户”的结果
		List<User> getUserList();	
}
```

### 4.3	接口实现类（Dao包下的接口）

​	在Java的Dao目录下创建一个File文件（UserMapper.xml），通过 **XML** 定义一个语句来**操作数据库**。

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="org.mybatis.example.BlogMapper">
  <select id="selectBlog" resultType="Blog">
    select * from Blog where id = #{id}
  </select>
</mapper>
```

​	例如：UserMapper.xml（配置文件），类似于：Mapper接口的实现类

```xml
 <!--
    namespace : 绑定一个对应的Dao/Mapper接口
    这个功能类似：实现了Mapper接口（如：public class UserDao implements  Mapper）
    -->
<mapper namespace="com.kuang.dao.Mapper">
    <!--
      这步类似于：
       public List<User> getUserList(){
        /* 执行SQL语句 */
       String sql = "select * from mybatis.user";
       /* 获得查询的结果集 */
       return null;
		---------------
        select ：查询语句
        id：对应方法的方法名
        resultType：返回类型		com.kuang.pojo.User：user实体类的路径
		parameterType：参数的类型
    -->
    <select id="getUserList" resultType="com.kuang.pojo.User" parameterType="">
        <!-- mybatis.user_table ：数据库.表名 -->
      select * from mybatis.user_table
    </select>
</mapper>
```

项目结构
![测试和主程序目录要一致](/../images/测试和主程序目录要一致.png)

## 5	测试（juints）

​		（注意：test与main**目录一致**，方便测试）

------------------------------------------------------------

注意：
		若报这个**异常**，原因是Mybatis核心配置文件中**<mappers>**未注册。

 <!--Cause: org.apache.ibatis.builder.BuilderException: Error parsing SQL Mapper Configuration. Cause: java.io.IOException: Could not find resource org/mybatis/example/BlogMapper.xml-->

原因：org.apache.ibatis网站.builder.BuilderException异常：分析SQL映射程序配置时出错。原因：java.io.IOException异常：找不到资源org/mybatis/example/BlogMapper.xml

```xml
<!-- 每一个Mapper.xml都需要在Mybatis核心配置文件中注册  -->
<mappers>
    <!--<mapper resource="org/mybatis/example/BlogMapper.xml"/>-->
    <mapper resource="com/kuang/dao/UserMapper.xml"></mapper>
</mappers>
```

-----

## 6	xml在java文件目录下无法匹配问题	

若还报错，若配置文件放到java文件下，会导致：maven的约定大于配置，无法被导出或者生效（java文件下的配置，Maven无法识别），下列文件直接放入主核心配置文件：pom.xml（若是有多个项目，主、字的pom.xml文件都放置）

```xml
    <!-- 手动配置自动过滤 -->
    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>true</filtering>
            </resource>
            <!-- 加载 java文件下的配置文件 -->
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>true</filtering>
            </resource>
        </resources>
    </build>
```

测试程序：UserDaoTest.java

```java
@Test
public void test(){
    /**
     * 第一步：获取SqlSession对象
     *      调用 MybatisUtils 里面的getSqlSession方法
     */
    SqlSession sqlSession = MybatisUtils.getSqlSession();

    /**
     * 第二步：执行Sql
     *         getMapper：获取Mapper接口对象，然后用接口对象去访问它的实现类（UserMapper.xml)
     */
   Mapper userMapper =  sqlSession.getMapper(Mapper.class);
   List<User> userlist = userMapper.getUserList();

   for(User user :userlist){
       System.out.println(user.toString());
   }

    /**
     * 关闭sqlSession
     */
    sqlSession.close();
}
```

测试结果：ctrl+shift+i

<img src="/../images/测试结果（第一个Mybaits项目）.png" alt="测试结果（第一个Mybaits项目）" style="zoom: 50%;" />

## 7	官方建议写法

​		**try...catch...finally**

```java
@Test
public void test(){
    /**
     * 第一步：获取SqlSession对象
     *      调用 MybatisUtils 里面的getSqlSession方法
     */
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    try{
        /**
         * 第二步：执行Sql
         *      方法一： getMapper：获取Mapper接口对象，然后用接口对象去访问它的实现类（UserMapper.xml)
         */
        Mapper userMapper =  sqlSession.getMapper(Mapper.class);
        List<User> userlist = userMapper.getUserList();
        /**
         * 第二步：执行Sql
         *      方法二： 通过 SqlSession 实例来直接执行已映射的 SQL 语句
         */
        List<User> userlist1 = sqlSession.selectList("com.kuang.dao.Mapper.getUserList");

        for(User user :userlist){
            System.out.println(user.toString());
        }
    }catch(Exception e){
        e.printStackTrace();
    }finally {
        /**
         * 若出现问题，则立即关闭sqlSession
         * 关闭sqlSession
         */
        sqlSession.close();
    }
}
```

# 三、CRUD

目录：resources/com/kuang/dao/**XXXMapper.xml**

## 1、namespace

​		namespace中的包名要跟Dao/mapper接口的包名一致（项目结构目录：**可见上述图片**）。

```xml
<!-- 类似：实现接口类（Mapper） -->
<mapper namespace="com.kuang.dao.Mapper">
    <select id="getUserList" resultType="com.kuang.pojo.User">
      select * from mybatis.user_table
    </select>
</mapper>
```

## 2、select

|     属性      | 解析                             |
| :-----------: | :------------------------------- |
|      id       | 对应namespace中的方法名；        |
|  resultType   | Sql语句执行的返回值；            |
| parameterType | 参数类型（方法传入的参数类型）； |

```java
/**
 *  数据库操作对象，一般类名为：Mapper（接口）
 */
public interface Mapper {
    // 抽象方法，用于存储获取的数据库值
    List<User> getUserList();

    // 根据ID查询用户（在实现类中，通过传递指定的ID参数来查询用户
    User getUserById(int id);

    // 增加用户（在实现类中，通过传递：新增用户信息 来新增用户
    void addUser(User user);

    // 修改用户
    void updateUser(User user);

    // 删除用户（根据ID来删除）
    void deleteUser(int id);
}
```

在对应的**xml文件**中，**实现**对应的抽象方法。

### 2.1	查找

```xml
<!-- 查询指定ID的用户信息 -->
<select id="getUserById" parameterType="int" resultType="com.kuang.pojo.User">
    select * from mybatis.user_table where id = #{id};
</select>
```

### 2.2	修改

**（注意标签：update，不是select）**

```xml
<!-- 修改用户 删除和修改不需要：resultType="com.kuang.pojo.User"返回值 -->
<update id="updateUser" parameterType="com.kuang.pojo.User">
    update mybatis.user_table set name=#{name},pwd=#{pwd} where id=#{id};
</update>
```

### 2.3	删除

**（注意标签：delete，不是select）**

```xml
<!-- 
查询指定ID的用户信息，因参数只有一个，故直接在Sql中写对应的参数名，来获取值，多参使用：Map 
-->
<delete id="deleteUser" parameterType="int">
 	delete from mybatis.user_table where id=#{id};
</delete>
```

### 2.4	新增

**（注意标签：insert，不是select）**

```xml
<!--
新增用户信息（对象的属性，可以直接取出来..values (#{name},#{pwd}) ，而不是：user.name 
-->
<insert id="addUser" parameterType="com.kuang.pojo.User" >
    insert into mybatis.user_table(name,pwd) values (#{name},#{pwd});
</insert>
```

### 2.5	测试

```java
// 删除用户
@Test
public void deleteUser(){
    // 获取SqlSession对象
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    try {
        /*
        	getMapper：获取Mapper接口对象;
        然后用 “接口对象” 去访问它的 “实现类” （UserMapper.xml)
        */
        Mapper usermapper = sqlSession.getMapper(Mapper.class);
		// 调用已经实现好的抽象方法
        usermapper.deleteUser(5);
        // 提交事务
        sqlSession.commit();
        sqlSession.close();
    }catch (Exception e){
        e.printStackTrace();
    }finally {//若出现问题，则立即关闭sqlSession
        sqlSession.close();
    }
}
```

## 3、Map传参

​		若实体类的参数较多，或者数据库中的表、字段，应当优先考虑使用Map。

```java
/**
 *  数据库操作对象，一般类名为：Mapper
 */
public interface Mapper {
    // 若参数过多，可以使用Map来进行传参
    void addUser2(Map<String,Object> map);
}
```

```xml
<!-- #{}的值为：Map的键值对（K,V）K的值，即：直接在Sql中取出key即可 -->
<insert id="addUser2" parameterType="map">
    insert into mybatis.user_table(name,pwd) values (#{username},#{password});
</insert>
```

测试：新增用户

```java
	@Test
    public void Adduser(){
        SqlSession sqlsession = MybatisUtils.getSqlSession();
        try {
            // 用接口对象去访问它的实现类（UserMapper.xml)
            Mapper usermapper = sqlsession.getMapper(Mapper.class);

            Map<String,Object> map = new HashMap<String,Object>();
            map.put("username","李四四");
            map.put("password","222222");
            
            usermapper.addUser2(map);
            // 提交事务
            sqlsession.commit();
            sqlsession.close();
        }catch (Exception e){
            e.printStackTrace();
        }finally{
            sqlsession.close();
        }
    }
```

​		Map传递参数（parameterType="map"），直接在sql中**取出key**即可；

```
 insert into mybatis.user_table(name,pwd) values (#{username},#{password});
```

​		**对象**传递参数，直接在sql中**取**对象的属性即可；

```xml
insert into mybatis.user_table(name,pwd) values (#{name},#{pwd});
```

​		只有**一个基本类型**的参数情况下， 可以直接在sql中取到；

```xml
select * from mybatis.user_table where id = #{id};
```

## 4、模糊查询

可以同时在java和sql中写入：**通配符：%**

```java
/**
 *  数据库操作对象，一般类名为：Mapper
 */
public interface Mapper {
    // 模糊查询
    List<User> getMoneyList(String name);
}
```

### 4.1 在java代码执行的时候，**传递通配符：%...%**

```xml
UserMapper.xml（使用Maper抽象方法的xml文件） 
<!-- 模糊查询多个用户，参数只有一个，因此可以直接用：#{name} -->
 <select id="getMoneyList" resultType="com.kuang.pojo.User">
     select * from mybatis.user_table where name like #{name};
</select>
```

```java
@Test
// 模糊查询用户信息
public void getMoneyList(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    try {
        Mapper  mapper = sqlSession.getMapper(Mapper.class);
        // %张%，使用通配符：%...%
        List<User> userlist =  mapper.getMoneyList("%张%");
        for(User user:userlist){
            System.out.println(user);
        }
        sqlSession.close();
        ...
```

### 4.2 Sql中拼接使用通配符！

```xml
 <!-- 模糊查询多个用户，参数只有一个，因此可以直接用：#{name} -->
 <select id="getMoneyList" resultType="com.kuang.pojo.User">
     select * from mybatis.user_table where name like "%"#{name}"%";
</select>
```

```java
	...	
		Mapper  mapper = sqlSession.getMapper(Mapper.class);
        // java中未使用通配符
        List<User> userlist =  mapper.getMoneyList("张");
	...
```

----

打印结果：

​	User{id=2, name='张三', pwd='456789'}
​	User{id=4, name='张三', pwd='111111'}

# 四、配置解析

## 1	核心配置文件

​		resources目录下的：**mybatis-config.xml**（影响MyBatis行为的设置和属性信息）。

​	配置文档的**顶层结构**如下：

- configuration（配置）
  - [properties（属性）](https://mybatis.org/mybatis-3/zh/configuration.html#properties)
  - [settings（设置）](https://mybatis.org/mybatis-3/zh/configuration.html#settings)
  - [typeAliases（类型别名）](https://mybatis.org/mybatis-3/zh/configuration.html#typeAliases)
  - [typeHandlers（类型处理器）](https://mybatis.org/mybatis-3/zh/configuration.html#typeHandlers)
  - [objectFactory（对象工厂）](https://mybatis.org/mybatis-3/zh/configuration.html#objectFactory)
  - [plugins（插件）](https://mybatis.org/mybatis-3/zh/configuration.html#plugins)
  - environments（环境配置）
    - environment（环境变量）
      - transactionManager（事务管理器）
      - dataSource（数据源）
  - [databaseIdProvider（数据库厂商标识）](https://mybatis.org/mybatis-3/zh/configuration.html#databaseIdProvider)
  - [mappers（映射器）](https://mybatis.org/mybatis-3/zh/configuration.html#mappers)

## 2	环境配置**（environment）**


​			MyBatis可以配置成适应多环境；（不过注意：尽管可以配置多个环境，但是每个SqlessionFactory实例只能选中一种环境）

在 MyBatis 中有两种类型的事务管理器：
 <transactionManager type=""[JDBC|MANAGED]"，默认：JDBC

```xml
<environments default="development">
    <environment id="development">
        <transactionManager type="JDBC"/>
        ...
```

## 3	属性（properties）

****

​		可以通过properties属性来实现引用配置文科，这些属性都是可以外部配置且可动态替换的，即可在java属性文件中配置，也可以通过properties元素的子元素来传递。【db.properties】

**第一步：**
		在resources文件下创建一个：db.properties

```properties
driver=com.mysql.jdbc.Driver
url=jdbc:mysql://localhost:3306/mybatis?useSSL=true&userUnicode=true&characterEncoding=utf-8
username=root
password=123456
```

**第二步：**
		在主配置文件（**resources/mybatis-config.xml**）中引入：db.properties

----

提示：

![标签必须按照顺序来创建](/../images/标签必须按照顺序来创建.png)

---

```xml
<configuration>

    <!-- 引入外部配置文件，resource：配置文件的路径  -->
    <properties resource="db.properties"></properties>

    <environments default="development">
        <environment id="development">
     ...
```

	-	可以直接引入外部文件
	-	可以在其中增加一些属性配置
	-	如果两个文件有同一个字段，**优先使用**外部配置文件。

## 4	类型别名（typeAliases）

	-	类型别名就是为java类型设置的一个：**短名字**；
	-	存在的意义：仅在于减少类完全限定名的冗余。

#### ① 方法一：直接起别名（适合实体类比较少）

**第一步：**
		在**主配置文件**（**resources/mybatis-config.xml**）中创建<typeAliases>标签
     <u>（注意标签顺序）</u>
<img src="/../images/别名1.png" alt="别名1" style="zoom: 50%;" />

```xml
<!-- 可以给：实体类 起别名，标签：typeAlias -->
<typeAliases>
    <!--对：com.kuang.pojo.User的User实体，起名为：User -->
    <typeAlias type="com.kuang.pojo.User" alias="User"/>
    
    <typeAlias alias="Blog" type="domain.blog.Blog"/>
  	<typeAlias alias="Comment" type="domain.blog.Comment"/>
</typeAliases>
```

​		指定一个包名，MyBatis会在**包名下**搜索需要的Java Bean，比如：
**扫描实体类包**，它的默认别名就为这个类的：类名，首字母小写。

**第二步：**在**实体类**对应的xml文件下

<img src="/../images/别名1（附）.png" alt="别名1（附）" style="zoom:50%;" />		

在UserMapper.xml

```xml
<select id="getUserList" resultType="com.kuang.pojo.User">
  select * from mybatis.user_table;
</select>
```

​		改为：

```xml
<select id="getUserList" resultType="User">
  select * from mybatis.user_table;
</select>
```

#### ② 方法二：扫描指定包下的实体（适合实体类比较多）

**第一步：**
		在核心配置文件中，扫描指定包

```xml
 <!--扫描指定包下的实体，标签：package -->
<typeAliases>
     <package name="com.kuang.pojo"></package>
</typeAliases>
```

**第二步：**

```xml
<!--resultType的值：大小写都可以，建议：小写-->
<select id="getUserList" resultType="User">
  select * from mybatis.user_table;
</select>
```

#### ③ 方法三：使用注解来起别名

**第一步：**
		在实体类中添加注解：**@Alias("hello")**  ，注意：注解别名**必须**：小写

```java
@Alias("hello")     // 默认：小写
public class User {
    private int id;
    private String name;
    private String pwd;
```

**第二步：**
		在对应的实体类xml文件中

```xml
<select id="getUserList" resultType="hello">
  select * from mybatis.user_table;
</select>
```



## 5、设置（settings）

​		这是 MyBatis 中极为重要的调整设置，它们会**改变** MyBatis 的运行时行为。 重点看以下三个：

|     **设置名**     | 描述                                                         |     有效值      | 默认值 |
| :----------------: | ------------------------------------------------------------ | :-------------: | :----: |
|    cacheEnabled    | 全局性地开启或关闭所有映射器配置文件中已配置的任何缓存。     |  true \| false  |  true  |
| lazyLoadingEnabled | 延迟加载的全局开关。当开启时，所有关联对象都会延迟加载。 特定关联关系中可通过设置 <br />`fetchType` 属性来覆盖该项的开关状态。 |  true \| false  | false  |
|      logImpl       | 指定 MyBatis 所用日志的具体实现，未指定时将自动查找。        | SLF4J LOG4J ... |   无   |

一个配置完整的 settings 元素的示例如下：

```xml
<settings>
  <setting name="cacheEnabled" value="true"/>
  <setting name="lazyLoadingEnabled" value="true"/>
    ...
```



## 6、插件（plugins）

​		MyBatis 允许你在映射语句执行过程中的某一点进行拦截调用。



## 7、映射器（Mappers）

​		配置会告诉 MyBatis 去哪里找映射文件，剩下的细节就应该是每个 SQL 映射文件。**推荐使用：方法一**

### ①	方式一：类路径

​		使用相对于**类路径的资源**引用；

```xml
<mappers>
  <mapper resource="org/mybatis/builder/AuthorMapper.xml"/>
  <mapper resource="org/mybatis/builder/BlogMapper.xml"/>
  <mapper resource="org/mybatis/builder/PostMapper.xml"/>
</mappers>
```

### ②	方式二：class

​		使用映射器接口实现类的完全限定类名

```xml
<mappers>
  <mapper class="org.mybatis.builder.AuthorMapper"/>
  <mapper class="org.mybatis.builder.BlogMapper"/>
  <mapper class="org.mybatis.builder.PostMapper"/>
</mappers>
```

​		注意：
​				接口和他的实现xml文件名必须同名；
​				接口和他的实现xml文件必须在同一个包下；

### ③	方式三：包

​		将包内的映射器接口实现全部注册为映射器

```xml
<mappers>
  <package name="org.mybatis.builder"/>
</mappers>
```

​		注意：
​				接口和他的实现xml文件名必须同名；
​				接口和他的实现xml文件必须在同一个包下；

## 8、生命周期和作用域

​		生命周期和作用域十分重要，错误的使用会导致非常严重的：并发问题。如图：

<img src="/../images/Mybatis生命周期.png" alt="Mybatis生命周期" style="zoom:50%;" />

**SqlSessionFactoryBuilder：**

​		一旦创建了 SqlSessionFactory，就不再需要它。用完就立即关闭。

**SqlSessionFactory**：
		可以理解为：**数据库连接池**。

​		一旦被创建就应该在应用的运行期间**一直存在**，没有任何理由丢弃它或重新创建另一个实例。

​		最简单的就是使用：**单例模式**或者**静态单例模式**。

**SqlSession**：
		理解为：连接到连接池的**一个请求**。

​		SqlSession 的实例不是线程安全的，因此是不能被共享的，，所以它的最佳的作用域是请求或方法作用域。

​		用完就立即关闭。

**SqlSessionFactory与Sqlsession关系：**

<img src="/../images/SqlSessionFactory与Sqlsession关系.png" alt="SqlSessionFactory与Sqlsession关系" style="zoom:50%;" />

​		解析：SqlSession用完就立即关闭，释放数据池，避免占用内存。
​		图中每个：Mapper表示一个**具体的业务**或功能。



# 五、解决属性名和字段名不一致的问题

数据库中的字段名为：id、name、pwd

创建一个实体类（User），属性：id、name、password

```java
public class User {
    private int id;
    private String name;
    private String password;		// 与数据库的pwd字段名不一致
}
```

测试：User user = userMapper.getUserById(1);	//查找id为1的用户

输出：User{id=1, name='狂神', password='null'}	 // password没有找到

**解决办法：**

## 1、起别名

**pwd as password**

```xml
<!-- 查询指定ID的用户信息，因参数只有一个，故直接在Sql中写对应的参数名，来获取值 -->
<select id="getUserById" parameterType="int" resultType="com.kuang.pojo.User">
    select id,name,pwd as password from mybatis.user_table where id = #{id};
</select>
```

## 2、resultMap

结果集映射

|  id  | name |     pwd      |
| :--: | :--: | :----------: |
|  id  | name | namepassword |

​	结果集映射（那个**属性不一致**，**就**映射那个属性）
​	以下方法**不适合**复杂的sql语句。

```xml
<mapper namespace="com.kuang.dao.Mapper">
    <!-- 
		结果集映射（那个属性不一致，就映射那个属性），id="UserMap"与resultMap="UserMap"相同 
 	-->
    <resultMap id="UserMap" type="User">
        <!-- column：数据库中的字段，property：实体类中的属性 -->
        <!--<result column="id" property="id"></result>-->
        <!--<result column="name" property="name"></result>-->
        <result column="pwd" property="password"></result>
    </resultMap>
<!--------------------------------------------------------------------->
    <!-- 查询指定ID的用户信息，因参数只有一个，故直接在Sql中写对应的参数名，来获取值 -->
    <select id="getUserById" resultMap="UserMap">
        select * from mybatis.user_table where id = #{id};
    </select>

</mapper>
```

​		`		resultMap` 元素是 MyBatis 中最重要最强大的元素。它可以让你从 90% 的 JDBC `ResultSets` 数据**提取代码中**解放出来。

​		ResultMap 的设计思想是，对简单的语句做到**零配置**，对于**复杂一点**的语句，只需要**描述**语句之间的关系就行了。



# 六、日志

在主文件**resources/mybatis-config.xml**中使用<settings>

若数据库操作出现了异常，需要**排错**，可以用到：日志

Mybatis所有日志：

 SLF4J | LOG4J | LOG4J2 | JDK_LOGGING | COMMONS_LOGGING | STDOUT_LOGGING | NO_LOGGING

## 1、默认日志STDOUT_LOGGING

​		STDOUT_LOGGING标准的日志输出
​		注意：**字母大小写、是否有多的空格符号**

```xml
<settings>
    <setting name="logImpl" value="STDOUT_LOGGING"/>
</settings>
```

测试输出：

```
Opening JDBC Connection		// 打开JDBC
Created connection 641853239.
Setting autocommit to false on JDBC Connection [com.mysql.jdbc.JDBC4Connection@2641e737]
==>  Preparing: select * from mybatis.user_table where id = ?; 
==> Parameters: 1(Integer)
<==    Columns: id, name, pwd
<==        Row: 1, 狂神, 123456
<==      Total: 1
User{id=1, name='狂神', password='123456'}
Resetting autocommit to true on JDBC Connection [com.mysql.jdbc.JDBC4Connection@2641e737]
Closing JDBC Connection [com.mysql.jdbc.JDBC4Connection@2641e737]
Returned connection 641853239 to pool.	// 返回结果
```

## 2、log4j日志

​		Log4j是Apache的一个[开放源代码](https://baike.sogou.com/lemma/ShowInnerLink.htm?lemmaId=269184&ss_c=ssc.citiao.link)项目，通过使用Log4j，我们可以控制日志信息输送的目的地是控制台、文件、GUI组件。
​		我们也可以控制每一条日志的输出格式。

### ①	第一步：导入log4j依赖

**pom.xml**

```xml
<!--https://logging.apache.org/log4j/2.x/maven-artifacts.html -->
<dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-api</artifactId>
      <version>2.8.2</version>
</dependency>
<dependency>
      <groupId>org.apache.logging.log4j</groupId>
      <artifactId>log4j-core</artifactId>
      <version>2.8.2</version>
</dependency>
```

### ②	第二步：创建一个log4j.properties文件

```properties
#将等级为DEBUG的日志信息输出到console和file这两个目的地，console和file的定义在下面的代码
log4j.rootLogger=DEBUG,console,file
#-----------------------------------------------------------
#控制台输出的相关设置
log4j.appender.console = org.apache.log4j.ConsoleAppender
log4j.appender.console.Target = System.out
log4j.appender.console.Threshold=DEBUG
log4j.appender.console.layout = org.apache.log4j.PatternLayout
# 格式
log4j.appender.console.layout.ConversionPattern=[%c]-%m%n	
#-----------------------------------------------------------
#文件输出的相关设置
log4j.appender.file = org.apache.log4j.RollingFileAppender
# 输出文件的地址
log4j.appender.file.File=./log/kuang.log
log4j.appender.file.MaxFileSize=10mb
log4j.appender.file.Threshold=DEBUG
log4j.appender.file.layout=org.apache.log4j.PatternLayout
log4j.appender.file.layout.ConversionPattern=[%p][%d{yy-MM-dd}][%c]%m%n
#-----------------------------------------------------------
#日志输出级别
log4j.logger.org.mybatis=DEBUG
log4j.logger.java.sql=DEBUG
log4j.logger.java.sql.Statement=DEBUG
log4j.logger.java.sql.ResultSet=DEBUG
log4j.logger.java.sql.PreparedStatement=DEBUG
```

③	第三步：配置log4j为日志的实现

```xml
<settings>
    <setting name="logImpl" value="LOG4J"/>
</settings>
```

④	测试（以上程序有**异常**，具体可以见SpringBoot笔记的log4j）

# 七、分页

原因：减少数据的处理量。

## 1、Mybatis-Limit-基于SQL

```sql
语法：
SELECT * FROM user_table limit startIndex,pageSize;
SELECT * FROM user-table limit 3;  # [0,3]
```

currentPage(**startIndex**) = (currentPage - 1) *pageSize;

①	接口

```java
// 使用limit分页查询
List<User> getUserLimit(Map<String,Integer> map);
```

②	Mapper.xml

​		实现接口的方法（UserMapper.xml）

```xml
<mapper namespace="com.kuang.dao.Mapper">
<!-- 结果集映射  -->
<resultMap id="UserMap" type="User">
    <result column="pwd" property="password"></result>
</resultMap>
```

```xml
<!--分页查询，parameterType：参数，resultMap：结果集名-->
<select id="getUserLimit" resultMap="UserMap" parameterType="map">
    select * from mybatis.user_table limit #{startIndex},#{pageSize};
</select>
```

③	测试

```java
@Test
public void getUserLimit(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    Mapper mapper = sqlSession.getMapper(Mapper.class);

    // 使用Map来进行传参
    HashMap<String,Integer> map = new HashMap<String,Integer>();
    map.put("startIndex",0);
    map.put("pageSize",2);

    List<User> userlist= mapper.getUserLimit(map);

    for(User user :userlist){
        System.out.println(user.toString());
    }
    sqlSession.close();
}
```

## 2、RowBounds分页-基于JAVA

（不建议使用）

①	在接口类中编写抽象方法

```java
// 使用RowBounds分页(无参，分页使用java代码来实现)
List<User> getUserBoundsLimit();
```

②	在对应的xml文件中实现抽象方法

```xml
<!-- 分页查询（基于JAVA），resultMap：结果集映射 -->
<select id="getUserBoundsLimit" resultMap="UserMap">
    select * from mybatis.user_table;
</select>
```

③	测试类中编写主要的java分页代码

```java
//基于RowBounds分页
@Test
public void getUserBoundLimit(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();

    // RowBounds实现（给定起始位和结束位）
    RowBounds rowBounds = new RowBounds(1,2);

    // 直接访问抽象接口中的抽象方法，而不是.xml文件（RowBounds的构造方法）
    List<User> userlist = sqlSession.selectList("com.kuang.dao.Mapper.getUserBoundsLimit",null,rowBounds);

    for(User user :userlist){
        System.out.println(user);
    }
    sqlSession.close();
}
```

## 3、分页插件

​		学习网址：https://pagehelper.github.io/

# 八、注解开发

## 1、 面向接口编程

​		根本原因：**解耦**，可扩展，提高复用，分层开发，上层不用管具体的实现，大家都遵守共同的标准，使得开发变得容易，规范性更好。

​		**接口的理解**：
​			① 定义（规范、约束）与实现（名实分离的原则）的分离；
​			② 接口的本身就反映了系统设计人员对系统的抽象理解；
​			③ 接口有两类：
​					一是对一个个体的抽象，它可对应为一个抽象体（abstract class）
​					二是对一个个体某一方面的抽象，即形成一个抽象面（interface）

## 2、使用注解访问SQL

​		本质：使用**反射机制**
​		使用注解来映射简单语句会使代码显得更加简洁，但对于**稍微复杂一点的**语句，Java 注解不仅力不从心，还会让你本就复杂的 SQL 语句更加混乱不堪。 因此，如果你需要做一些很复杂的操作，**最好用 XML** 来映射语句。

①	接口（Mapper.interface）

```java
// 使用注解来调用SQL语句
@Select("select * from mybatis.user_table")
List<User> getUsers();
```

②	在核心配置文件注册（mybatis-config.xml）

```xml
<!-- 每一个Mapper.xml都需要在Mybatis核心配置文件中注册  -->
<mappers>
    <!-- 使用注解来调用SQL，则需要绑定对应的接口类（interface） -->
    <mapper class="com.kuang.dao.Mapper"></mapper>
</mappers>
```

③	测试

```java
// 使用注解
@Test
public void zhujiegetUser(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    Mapper mapper = sqlSession.getMapper(Mapper.class);
    // 使用接口类 去 调用实现类（xml文件）
    List<User> userlist = mapper.getUsers();

    for(User user :userlist){
        System.out.println(user);
    }
}
```



## 3、Mybatis执行流程

<img src="/../images/Mybatis执行流程.png" alt="Mybatis执行流程" style="zoom:50%;" />

## 4、CRUD

​		可以在工具类创建的时候实现自动提交事务。

### （1）select

​		①	接口（Mapper.java（**interface**））

使用：**Param**注解来指定参数

```java
/**
 * 使用注解来实现：查询
 */
@Select("select * from user_table where id=#{id}")
// 注意，@Param("id")写入的变量名要跟SQL对应id=#{id}
User Z_getUserById(@Param("id") int id);
```

​		②	编写java代码

 调用：Z_getUserById抽象方法，**无需在**xml文件中实现

```java
@Test
public void zhujiegetUser(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    Mapper mapper = sqlSession.getMapper(Mapper.class);
	// 调用：Z_getUserById抽象方法，无需在xml文件中实现
    User userlist = mapper.Z_getUserById(1);

    System.out.println(userlist);
}
```

### （2）insert

​		①	接口（Mapper.java（**interface**））

```java
/**
 * 注解实现SQL：插入
 */
@Insert("insert into user_table(id,name,pwd) values (#{id},#{name},#{password})")
int Z_insertUser(User user);	// 抽象方法
```

​		②	编写java代码

```java
    // 使用注解
    @Test
    public void zhujiegetUser(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        Mapper mapper = sqlSession.getMapper(Mapper.class);

	    int a = mapper.Z_insertUser(new User(5,"tzw","44444"));
	    // 注意：插入、修改、删除需要提交事务
        sqlSession.commit();
        sqlSession.close();
    }
```

### （3）update

```java
/**
 * 注解实现SQL：修改
 */
@Update("update user_table set name=#{name},pwd=#{password} where id=#{id}")
int Z_updateUser(User user);
```

### （4）delete

```java
/**
 * 注解实现SQL：删除
 */
@Delete("delete from user_table where id=#{uid}")
int Z_deleteUser(@Param("uid") int id);
```

## 5、@Param()注解

- 基本类型的参数或者String类型，需要加上；
- **引用类型**不用加；
- 只有**一个**基本类型的话，不用加；
- 在**SQL中引用**的就是@Param()中设定的属性名；

## 6、#{} 与${}区别

​		将传入的数据都**当成**一个字符串，会对自动传入的数据加一个双引号。如：order by #user_id#，如果传入的值是111,那么解析成sql时的值为order by "111", 如果传入的值是id，则解析成的sql为order by "id"。

​	将传入的数据**直接显示**生成在sql中。如：order by $user_id$，如果传入的值是111,那么解析成sql时的值为order by user_id, 如果传入的值是id，则解析成的sql为order by id.

# 九、Lombok

## 1、介绍

​	一个java库，可以自动构建相关方法，不用再编辑get和set方法，用一个**注释就可以自动生成**。

```java
Project Lombok is a java library that automatically plugs into your editor and build tools, spicing up your java.
/*Never write another getter or equals method again*/, with one annotation your class has a fully featured builder, Automate your logging variables, and much more.
```

重点：

- java library
- plugs
- build tools
- with one annotation

## 2、Lombok的使用

​		①	导入相关依赖**（pom.xml）**

```xml
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.16.18</version>
    <scope>provided</scope>
</dependency>
```

​		②	添加IDE工具对Lombok的支持
​		File——>settings——>Plugins——>搜索：Lombok

​		③	点击File-- Settings设置界面，开启 AnnocationProcessors：（**可省略此步**）
​				File——>settings——>Build,Execution,Deployment——>Compiler——>Annotation Processors——（选择对应的项目）——>选中Enable annotation processing

<img src="/../images/Lombok开启.png" alt="Lombok开启" style="zoom: 25%;" />



## 3、lombok常用注解

```java
@FieldNameConstants
@ToString
@EqualsAndHashCode
// 有参/无参构造器
@AllArgsConstructor, @RequiredArgsConstructor and @NoArgsConstructor
@Log, @Log4j, @Log4j2, @Slf4j, @XSlf4j, @CommonsLog, @JBossLog, @Flogger, @CustomLog
@Data		// 常用，可以自动生成：get/set/toString...
@Builder
@SuperBuilder
@Singular
@Delegate
@Value
@Accessors
@Wither
@With
@SneakyThrows
@val
@var
experimental @var
@UtilityClass
Lombok config system
Code inspections
Refactoring actions (lombok and delombok)
```

## 4、data注解

​	直接在**实体类上**加注解即可。

<img src="/../images/lombok_data注解.png" alt="lombok_data注解" style="zoom: 50%;" />

​		data注解可以**自动构建**，set、get、equals、toString()等方法。



## 5、Lombok的优缺点

**优点:**
		1、能通过注解的形式**自动生成构造器**、getter/setter. equals、 hashcode、 toString等方法, 提高了-定的开发效率
		2、让代码变得简洁，不过多的去关注相应的方法
		3、属性做修改时，也简化了维护为这些属性所生成的getter/setter方法等
**缺点:**
		1、**不支持**多种参数构造器的重载
		2、虽然省去了手动创建getter/setter方法的麻烦,但大大降低了源代码的可读性和完整性，**降低了**阅读源代码的舒适度



## 6、总结

​		Lombok虽然有很多优点，但Lombok更类似于一种IDE插件，项目**也需要**依赖相应的jar包。Lombok依赖jar包是 因为编译时要用它的注解，为什么说它又类似插件?因为在使用时，eclipse或IntelliJ IDEA都需要安装相应的插件，在编译器编译时通过操作AST (抽象语法树)改变字节码生成，变响的就是说它在改变java语法。它不像spring的依赖注入或者mybatis的ORM-样是运行时的特性,而是编译时的特性。这里我个人最感觉不爽的地方就是对插件的依赖!因为Lombok只是省去了一些人工生成代码的麻烦，但IDE都有**快捷键来协助**生成getter/setter等方法，也非常方便。

----

知乎上有位大神发表过对Lombok的一些看法:
	这是一种低级趣味的插件，**不建议使用**。JAVA发展到今天，各种插件层出不穷,如何甄别各种插件的优劣?能从架构上优化你的设计的，能提高应用程序性能的，实现高度封装可扩展的....像1ombok这种， 像这种插件,已经不仅仅是插件了，改变了你如何编写源码，事实上，少去了的代码，你写上去又如何?如果JAVA家族到处充斥这样的东西， 那只不过是一坨披着金属颜色的屎，迟早会被其它的语言取代。



# 十、多对一处理（association）

**见项目的：mybatis04**

## 1、建数据库

```sql
CREATE TABLE teacher (
  id INT(10) NOT NULL,
  name VARCHAR(30) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO teacher(id, name) VALUES (1, '秦老师'); 


CREATE TABLE student (
  id INT(10) NOT NULL,
  name VARCHAR(30) DEFAULT NULL,
  tid INT(10) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY fktid (tid),
  CONSTRAINT fktid FOREIGN KEY (tid) REFERENCES teacher(id)
) ENGINE=INNODB DEFAULT CHARSET=utf8; 

INSERT INTO student(id, name, tid) VALUES ('1', '小明', '1');
INSERT INTO student(id, name, tid) VALUES ('2', '小红', '1'); 
INSERT INTO student(id, name, tid) VALUES ('3', '小张', '1'); 
INSERT INTO student(id, name, tid) VALUES ('4', '小李', '1'); 
INSERT INTO student(id, name, tid) VALUES ('5', '小王', '1');
```

## 2、复杂查询环境搭建

### **①	导入lombok（pom.xml）**

```xml
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.16.18</version>
    <scope>provided</scope>
</dependency>
```

### **②	新建实体类：Teacher、Student，并使用注解：Data**

```java
@Data
public class Teacher {
    private int id;
    private String name;
}
```

```java
@Data
public class Student {
    private int id;
    private String name;
    //学生需要关联一个老师；
    private Teacher teacher;
}
```

### ③	建立Mapper接口（**interface**）

```java
/**
 * 对应的实现类，在resources/com.kuang/TeacherMapper.xml
 */
public interface TeacherMapper {
    // 查询老师
    @Select("select * from Teacher")
    List<Teacher> getTeacher();
}
```

```java
/**
 * 对应的实现类，在resources/com.kuang/StudentMapper.xml
 */
public interface StudentMapper {

}
```

### ④	建立Mapper.xml文件

​		（目录：**resources/com/kuang/Dao/**）

- ​		注意：**接口对应的实习类（xml文件）目录与名字一致**
  <img src="/../images/java目录与resources目录一致.png" alt="java目录与resources目录一致" style="zoom: 33%;" />

  **StudentMapper.xml**

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<!-- 跟对应的接口类绑定（StudentMapper） -->
<mapper namespace="com.kuang.dao.StudentMapper">

</mapper>
```

​	**TeacherMapper.xml**

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<!-- 跟对应的接口类绑定（TeacherMapper） -->
<mapper namespace="com.kuang.dao.TeacherMapper">
    
</mapper>
```

### ⑤ 绑定XXXMapper接口或者文件

​	目录：**resources/mybatis-config.xml**

```xml
 <!--可以给：实体类 起别名，这样在Mapper.xml文件中可以直接写：实体名-->
<typeAliases>
    <package name="com.kuang.pojo"></package>
</typeAliases>
```

```xml
<!--类绑定（xml和interface必须同名，而且必须在同一个包下 -->
<!--<mapper class="com.kuang.dao.TeacherMapper"></mapper>-->
```
```xml
<!-- 每一个Mapper.xml都需要在Mybatis核心配置文件中注册  -->
<mappers>
    <!--  
		注意：创建资源目录的时，需要取消：compact middle packages选项（点击项目旁边一个“设置”符号） 		-->
    <mapper resource="com/kuang/dao/TeacherMapper.xml"/>
    <mapper resource="com/kuang/dao/StudentMapper.xml"/>
</mappers>
```

### ⑥	测试

​	使用**注解的方式**来调用：SQL语句

```java
@Test
public void getTeacher(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    // 获得对应的接口类，然后用接口类去访问实现了的：方法
    TeacherMapper teacherMapper = sqlSession.getMapper(TeacherMapper.class);
    List<Teacher>  t = teacherMapper.getTeacher();
    for(Teacher teacher:t){
        System.out.println(teacher.toString());
    }
    sqlSession.close();
}
```

## 3、按照查询嵌套处理

实现**多表查询**：

```sql
select s.id,s,name,t.name from student s,teacher t where s.tid=t.id;
```

实现的思路：

```
思路：
   1、查询所有的学生信息（将查询的结果放入resultMap中）
   2、若查询的结果中存在对象（Teacher），则再次调用指定的查询SQL语句
   3、这次的查询语句，可根据查询出来的学生的：tid 字段属性值，寻找对应的老师
```

①	在对应的接口中创建一个抽象方法

```java
public interface StudentMapper {
    // 查询所有的学生信息，以及对应老师的信息
    public List<Student> getStudent();
}
```

②	在对应的StudentMapper.xml文件编写对应的语句

**重点：**

```xml
"column"：查询的字段（与数据库中的字段对应）
<resultMap>:
	里面的column对应的表中的字段，需要对应；
	而property：则是给定的变量名，用于解决，属性名和字段名冲突
<association>:
	select：用于调用某个SQL语句，并获得执行的结果（若有返回值）
    column：执行select对应的SQL时，也会将column对应的值传递过去，作为SQL的参数
    resultMap：为查询返回的结果集
```

```xml
<select id="getStudent" resultMap="StudentTeacher">
    select * from student;
</select>
<!--------------------------------------------------------------------->
<!-- 返回的结果，使用Map 结果集映射  -->
<resultMap id="StudentTeacher" type="Student">
    <!-- 只需要映射：字段值，则只需要使用：<result>标签 -->
    <result property="id" column="id"/>
    <result property="name" column="name"/>
    <!--
 针对于复杂的属性，需要单独处理，对象使用: <association> 标签，集合使用：<collection>标签
    -->
    <!--
    javaType：对象，property：实体类中的属性 变量名，column：字段名
    select：与对应的<select><insert>...标签的 id 值对应，这里再次调用某个SQL语句
    -->
    <association property="teacher" column="tid" javaType="Teacher" select="getTeacher"/>
</resultMap>
<!--------------------------------------------------------------------->
<select id="getTeacher" resultType="Teacher">
    select * from teacher  where id=#{id};
</select>
```

重点：根据**查询的学生信息**的tid字段，来嵌套查询对应的teacher表的信息

```xml
 <association property="teacher" column="tid" javaType="Teacher" select="getTeacher"/>
```

③	测试

```java
@Test
public void getTeacher(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    // 获得对应的接口类，然后用接口类去访问实现了的：方法
    StudentMapper studentMapper = sqlSession.getMapper(StudentMapper.class);
    List<Student>  s = studentMapper.getStudent();
    for(Student student:s){
        System.out.println(student.toString());
    }
    sqlSession.close();
}
```

测试结果：多表查询

Student(id=1, name=小明, teacher=Teacher(id=1, name=秦老师))
Student(id=2, name=小红, teacher=Teacher(id=1, name=秦老师))
Student(id=3, name=小张, teacher=Teacher(id=1, name=秦老师))
Student(id=4, name=小李, teacher=Teacher(id=1, name=秦老师))
Student(id=5, name=小王, teacher=Teacher(id=1, name=秦老师))

## 4、按照结果嵌套处理（方法二）

直接在对应的StudentMapper.xml文件中：

```xml
<!--方法一：按照结果集来嵌套处理-->
<select id="getStudent2" resultMap="StudentTeacher2">
    select s.id sid,s.name sname,t.name tname
    from student s,teacher t
    where s.tid=t.id;
</select>
<!--------------------------------------------------------------------->
<!-- 注意：column的值，因为已经使用了别名，所以需要跟别名一致 -->
<resultMap id="StudentTeacher2" type="Student">
    <result property="id" column="sid"/>
    <result property="name" column="sname"/>
    <association property="teacher" javaType="Teacher">
        <result property="name" column="tname"/>
    </association>
</resultMap>
```

# 十一、一对多处理（collection）

主要实现：

```sql
select s.id sid,s.name sname,t.name tname,t.id tid
from student s,teacher t
where s.tid=t.id and t.id =#{tid}	//#{tid}，传递的参数
```

## 1、搭建环境

​		环境跟多对一处理一样，不过需要**更改**实体类

```java
@Data
public class Student {
    private int id;
    private String name;
    private int tid;    // 外键，关联teacher表的id
}
```

​	

```java
@Data
public class Teacher {
    private int id;
    private String name;
    // 一个老师拥有多个学生
    private List<Student> students;
}
```

## 2、按照结果嵌套处理

①	抽象方法

```java
public interface TeacherMapper {
    // 获取指定老师下的所有学生及老师的信息（根据结果集来嵌套查询）
    Teacher getTeacher(@Param("tid") int id);
}
```

②	xml文件编写

​	**先使用select查询**满足条件的所有信息，然后在使用resultMap进行结果集映射处理。

```xml
<!-- 按结果嵌套查询。t.id =#{tid} ：表示需要查询那个教师（id查询）-->
<select id="getTeacher" resultMap="TeacherStudent">
    select s.id sid,s.name sname,t.name tname,t.id tid
    from student s,teacher t
    where s.tid=t.id and t.id =#{tid}
</select>
<!-- *************************************************************** -->
<resultMap id="TeacherStudent" type="Teacher">
    <!-- 单个属性 -->
    <result property="id" column="tid"/>
    <result property="name" column="tname"/>
    <!--
        实体类中复杂的属性，需要单独处理，对象：association，集合：collection
        javaType：属性的类型；
        集合中的泛型信息List<泛型>，使用：ofType

        实体类Teacher中，复杂属性：List<Student>
    -->
    <collection property="students" ofType="Student">
        <result property="id" column="sid"/>
        <result property="name" column="sname"/>
        <result property="tid" column="tid"/>
    </collection>
</resultMap>
```

③	测试

```java
    @Test
    public void getTeacher(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        TeacherMapper teacherMapper = sqlSession.getMapper(TeacherMapper.class);
		// 查询id为1的教室，并查询对应的：所有学生信息
        Teacher teacher = teacherMapper.getTeacher1(1);

        System.out.println(teacher);
        sqlSession.close();
    }
```

**结果：**
Teacher(id=0, name=秦老师, 

students=[Student(id=1, name=小明, tid=1), 
Student(id=2, name=小红, tid=1), 
Student(id=3, name=小张, tid=1), 
Student(id=4, name=小李, tid=1), 
Student(id=5, name=小王, tid=1)])



## 3、按照查询嵌套处理

①	抽象方法

```java
public interface TeacherMapper {
    // 获取指定老师下的所有学生及老师的信息（先查教师表，再根据教师表来查询学生表）
    Teacher getTeacher2(@Param("tid") int id);
}
```

②	xml文件编写

```xml
<!-- 第一步：先查询指定老师的信息 -->
<select id="getTeacher2" resultMap="TeacherStudent2">
    select * from teacher where id = #{tid};
</select>

<!-- 第二步：
	在结果集中，根据查询出来的老师：id来查询对应的学生信息（子查询）；
	column的值为传参值 -->
<resultMap id="TeacherStudent2" type="Teacher">
    <collection property="students" column="id" javaType="ArrayList" ofType="Student" select="getStudentByTeacherId"/>
</resultMap>

<select id="getStudentByTeacherId" resultType="Student">
    select * from student where tid = #{tid};
</select>
```

# 十二、动态SQL

​		[根据**不同的条件**生成不同的SQL语句]()

​		[所谓的动态SQL，**本质还是SQL语句**，只是我们可以在SQL层面里面去**执行一个逻辑代码**]()



**下列案例：通过java代码来实现插入信息，insert**

## 1、搭建环境（Insert）

<img src="/../images/动态插入SQL结构图.png" alt="动态插入SQL结构图" style="zoom:33%;" />

### ①	创建数据库表

```mysql
CREATE TABLE blog(
id VARCHAR(50) NOT NULL COMMENT '博客id',
title VARCHAR(100) NOT NULL COMMENT '博客标题',
author VARCHAR(30) NOT NULL COMMENT '博客作者',
create_time DATETIME NOT NULL COMMENT '创建时间',
views INT(30) NOT NULL COMMENT '浏览量'
)ENGINE=INNODB DEFAULT CHARSET=utf8
```

#### 驼峰命名自动映射

**注意：因为字段存在：create_time（不支持下划线）**

需要在对应项目的：**mybatis-confgi.xml**文件中，设置<settings>:mapUnderscoreToCamelCase

是否开启**驼峰命名自动映射**，即从经典数据库**列名** A_COLUMN **映射**到经典 Java 属性名 aColumn。

```xml
<settings>
    <!--  mapUnderscoreToCamelCase
        是否开启驼峰命名自动映射，即从经典数据库列名 A_COLUMN 映射到经典 Java 属性名 aColumn。
    -->
    <setting name="mapUnderscoreToCamelCase" value="true"/>
</settings>
```

图片：
<img src="/../images/数据库的字段名存在下换线问题.png" alt="数据库的字段名存在下换线问题" style="zoom:150%;" />

### ②	编写配置文件

​	db.properties与mybatis-config.xml  以及：**pom.xml**文件下的：依赖，例如：Lombok

### ③	编写与数据库对应的实体类

com.kuang.pojo目录下：

```java
@Data
public class Blog {
    private String id;
    private String title;
    private String author;
    private Date createTime;
    private int views;
}
```

### ④ 编写实体类对应的Mapper接口和Mapper.xml文件

java/com.kuang.Dao目录下：**BlogMapper**

```java
public interface BlogMapper {
    // 搜索
    Blog selectUser();
}
```

resources/com.kuang.Dao目录下：**BlogMapper.xml**

```xml
<!--  parameterType ：传入的参数变量-->
<select id="addBook" parameterType="blog">
    insert into mybatis.blog(id, title, author, create_time, views)
    values (#{id},#{title},#{author},#{createTime},#{views});
</select>
```

```xml
需要在mybatis-config.xml文件中注册：
<mapper resource="com/kuang/Dao/BlogMapper.xml"/>
```

### ⑤	测试

java/com/kuang/utils/IDutils.java

```java
@SuppressWarnings("all")
public class IDutils {
    //随机获得一个ID
    public static String getId(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }
    @Test
    public void test(){
        System.out.print(IDutils.getId());
    }
}
```

```java
@Test
public void addBlogTest() {
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    BlogMapper mapper = sqlSession.getMapper(BlogMapper.class);

    Blog blog = new Blog();
    blog.setId(IDutils.getId());
    blog.setTitle("Mybatis");
    blog.setAuthor("狂神说");
    blog.setCreateTime(new Date());
    blog.setViews(9999);

    mapper.addBook(blog);

    blog.setId(IDutils.getId());
    blog.setTitle("Java");
    mapper.addBook(blog);

    blog.setId(IDutils.getId());
    blog.setTitle("Spring");
    mapper.addBook(blog);

    blog.setId(IDutils.getId());
    blog.setTitle("微服务");
    mapper.addBook(blog);

    sqlSession.close();
}
```

<img src="/../images/动态插入数据库blog.png" alt="动态插入数据库blog" style="zoom:25%;" />

## 2、动态where语法

* where：元素只会在**子元素**返回任何内容的情况下**才**插入 “WHERE” 子句。
* 而且，若子句的开头为 “AND” 或 “OR”，*where* 元素也会将它们**去除**。

```xml
<select id="findActiveBlogLike"  resultType="Blog">
  SELECT * FROM BLOG
  <where>
    <if test="state != null">
         state = #{state}
    </if>
    <if test="title != null">
        AND title like #{title}
    </if>
    <if test="author != null and author.name != null">
        AND author_name like #{author.name}
    </if>
  </where>
</select>
```

## 3、与where等价的trim语法

```xml
<select id="findActiveBlogLike"  resultType="Blog">
  SELECT * FROM BLOG
  <trim prefix="WHERE" prefixOverrides="AND |OR ">
    <if test="state != null">
         state = #{state}
    </if>
      ......
 </trim>
</select>
```

<trim prefix="WHERE" prefixOverrides="AND |OR ">：prefixOverrides表示：若内部子元素有**一个或者以上**的条件成立，则加上：where，若存在多个，则使用**and或者or拼接**

## 4、动态if语法

①	编写抽象方法

```java
public interface BlogMapper {
    // 使用if语句动态插入
    List<Blog> queryBlogIF(Map<String,String> map);
}
```

②	xml文件中编写SQL语句

  <if test="title != null"> 中的：**test**为标签自带属性，用于获得传递过来的值

```xml
<!-- 使用到动态查询，if -->
<select id="queryBlogIF" parameterType="map" resultType="Blog">
  select * from mybatis.blog where 1=1
  <if test="title != null">
      and title=#{title}
  </if>
  <if test="author != null">
      and author=#{author}
  </if>
</select>
```

③	测试

```java
    @Test
    public void queryBlogIF(){
        SqlSession sqlSession = MybatisUtils.getSqlSession();
        BlogMapper blogMapper = sqlSession.getMapper(BlogMapper.class);
        HashMap map = new HashMap();
//        map.put("title","Spring");
        map.put("author","狂神说4");	// 查询author为：狂神说4
        List<Blog> blogs = blogMapper.queryBlogIF(map);
        for(Blog blog:blogs){
            System.out.println(blog);
        }
        sqlSession.close();
    }
```

最终的SQL语句为：

```mysql
select * from mybatis.blog where 1=1 and   and author="狂神说4";
```

## 5、动态choose语法

类似： Java 中的 switch 语句。<otherwise>：其他情况，类似java中的**default**

```xml
<select id="findActiveBlogLike"
        resultType="Blog">
    SELECT * FROM BLOG
    <where>
        <choose>
            <when test="title != null">
                AND title like #{title}
            </when>
            <when test="author != null and author.name != null">
                AND author_name like #{author.name}
            </when>
            <otherwise>
                AND featured = 1
            </otherwise>
        </choose>
    </where>
</select>
```

## 6、set语法

​	*set* 元素会动态地在**行首插入** SET 关键字，并会**删掉额外的逗号**（这些逗号是在使用条件语句给列赋值时引入的）

```xml
<update id="updateAuthorIfNecessary">
  update Author
    <set>
      <if test="username != null">username=#{username},</if>
      <if test="password != null">password=#{password},</if>
      <if test="email != null">email=#{email},</if>
      <if test="bio != null">bio=#{bio}</if>
    </set>
  where id=#{id}
</update>
```

## 7、set等价的trim语法

```xml
<update id="updateAuthorIfNecessary">
  update Author
	<trim prefix="SET" suffixOverrides=",">
 	 ...
	</trim>
```

## 8、SQL片段

​	可以将一部分功能抽取出来，方便复用。

①	使用SQL标签抽取公共的部分

```xml
<sql id="if-title-author">
    <if test="title!=null">
        title=#{title}
    </if>
    <if test="author!=null">
        and author=#{author}
    </if>
</sql>
```

②	在需要使用的地方使用**include**标签引用即可

```xml
<select id="queryBlogIF" parameterType="map" resultType="Blog">
    select * from mybatis.blog
    <where>
        <include refid="if-title-author"></include>
    </where>
</select>
```

## 9、ForEach

​		动态 SQL 的另一个常见使用场景是对集合进行遍历（尤其是在构建 **in 条件**语句的时候）。比如：

![foreach介绍](/../images/foreach介绍.png)

​		**#{item}**：为集合中的元素。

①	接口类实现对应的方法

```java
List<Blog> queryBlogChoose(Map<String,String> map);
```

②	xml文件实现抽象方法

​		传递一个万能的map时，使**map中存在**一个：集合（List）

```xml
<!--
    select * from mybatis.blog where 1=1 and (id=1 or id=2 or id3_
    传递一个万能的map时，使map中存在一个：集合（List）
-->
<select id="queryBlogIF" parameterType="map" resultType="Blog">
    select * from mybatis.blog
    <where>
        <foreach collection="ids" item="id"
        open="and (" close=")" separator="or">
            id=#{id}
        </foreach>
    </where>

</select>
```

③	测试

```java
@Test
public void queryBlogIF(){
    SqlSession sqlSession = MybatisUtils.getSqlSession();
    BlogMapper blogMapper = sqlSession.getMapper(BlogMapper.class);
    HashMap map = new HashMap();

    ArrayList<Integer> ids = new ArrayList<Integer>();
    ids.add(1);
    ids.add(2);
    ids.add(3);

    map.put("ids",ids);

    List<Blog> blogs = blogMapper.queryBlogIF(map);

    for(Blog blog:blogs){
        System.out.println(blog);
    }
    sqlSession.close();
}
```



# 十三、缓存

## 1、简介

### 1、什么是缓存 [ Cache ]？

​	存在内存中的**临时数据**。

​	将用户经常查询的数据放在缓存（内存）中，用户去查询数据就**不用从磁盘上**(关系型数据库数据文件)查询，从缓存中查询，从而提高查询效率，解决了高并发系统的性能问题。

### 2、为什么使用缓存？

​	减少和数据库的**交互次数**，减少系统开销，提高系统效率。

### 3、什么样的数据能使用缓存？

​	经常查询并且**不经常改变**的数据。

## 2、Mybatis缓存

​	MyBatis包含一个非常强大的查询缓存特性，它可以非常方便地定制和配置缓存。缓存可以极大的提升查询效率。

​	MyBatis系统中默认定义了两级缓存：**一级缓存和二级缓存**

​	默认情况下，只有一级缓存开启。（SqlSession级别的缓存，也称为**本地缓存**）

​	二级缓存需要手动开启和配置，他是基于**namespace级别**的缓存。

​	为了提高扩展性，MyBatis定义了缓存接口Cache。我们可以通过实现Cache接口来自定义二级缓存

### （1）一级缓存

​	一级缓存是SqlSession级别的缓存，是一直开启的，我们**关闭不了**它；

​	一级缓存**失效**情况：没有使用到当前的一级缓存，效果就是，还需要**再向**数据库中发起一次查询请求！

​	一级缓存就是一个**map**；

- 与数据库同一次会话期间查询到的数据会放在本地缓存中。
- 以后如果需要获取相同的数据，直接从缓存中拿，没必须再去查询数据库；

<img src="/../images/一级缓存1.png" alt="一级缓存1" style="zoom: 50%;" />

----

### （2）一级缓存失效的四种情况

​	①	sqlSession不同：**每个sqlSession中的缓存相互独立**

​	②	sqlSession相同，查询条件不同：（发送了两条SQL语句），原因**当前缓存中，不存在这个数据**

​	③	sqlSession相同，**两次查询**之间执行了增删改操作！
​			**因为增删改操作可能会对当前数据产生影响**

​	④	sqlSession相同，**手动清除**一级缓存

```java
@Test
public void testQueryUserById(){
    SqlSession session = MybatisUtils.getSession();
    UserMapper mapper = session.getMapper(UserMapper.class);
 
    User user = mapper.queryUserById(1);
    System.out.println(user);
 
    session.clearCache();//手动清除缓存
 
    User user2 = mapper.queryUserById(1);
    System.out.println(user2);
 
    System.out.println(user==user2);
 
    session.close();
```



### （3）二级缓存

​	二级缓存也叫**全局缓存**，一级缓存作用域太低了，所以诞生了二级缓存

​	基于namespace级别的缓存，一个名称空间，对应一个二级缓存；

​	**工作机制：**

​		①	一个会话查询一条数据，这个数据就会被**放在当前会话的一级缓存**中；

​		②	如果当前会话关闭了，这个会话对应的一级缓存就没了；但是我们想要的是，会话关闭了，一级缓存中的数据被**保存到二级缓存**中；

​		③	新的会话查询信息，就可以从二级缓存中获取内容；

​		④	**不同的mapper**查出的数据会**放在自己对应的缓存**（map）中；

### （4）二级缓存使用

​		①	开启全局缓存 【mybatis-config.xml】

```xml
<setting name="cacheEnabled" value="true"/>
```

​		②	去每个mapper.xml中配置使用二级缓存，这个配置非常简单；【xxxMapper.xml】

​	这个更高级的配置创建了一个 **FIFO 缓存**，每隔 60 秒刷新，最多可以存储结果对象或列表的 **512 个引用**，而且返回的对象被认为是**只读**的，因此对它们进行修改可能会在不同线程中的调用者产生冲突。

```
<cache
  eviction="FIFO"
  flushInterval="60000"
  size="512"
  readOnly="true"/>
```

### （5）缓存原理图

<img src="/../images/缓存原理图.png" alt="缓存原理图" style="zoom:50%;" />

## 3、EhCache

​	Ehcache是一种广泛使用的**java分布式缓存**，用于通用缓存；

### （1）使用Ehcache

```xml
<!-- https://mvnrepository.com/artifact/org.mybatis.caches/mybatis-ehcache -->
<dependency>
    <groupId>org.mybatis.caches</groupId>
    <artifactId>mybatis-ehcache</artifactId>
    <version>1.1.0</version>
</dependency>
```

（2）在mapper.xml中使用对应的缓存即可

```xml
<mapper namespace = “org.acme.FooMapper” > 
    <cache type = “org.mybatis.caches.ehcache.EhcacheCache” /> 
</mapper>
```

（3）编写ehcache.xml文件，如果在加载时**未找到/ehcache.xml资源或出现问题**，则将使用**默认配置**

```xml

<?xml version="1.0" encoding="UTF-8"?>
<ehcache xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:noNamespaceSchemaLocation="http://ehcache.org/ehcache.xsd"
         updateCheck="false">
    <!--
       diskStore：为缓存路径，ehcache分为内存和磁盘两级，此属性定义磁盘的缓存位置。参数解释如下：
       user.home – 用户主目录
       user.dir  – 用户当前工作目录
       java.io.tmpdir – 默认临时文件路径
     -->
    <diskStore path="./tmpdir/Tmp_EhCache"/>
    
    <defaultCache
            eternal="false"
            maxElementsInMemory="10000"
            overflowToDisk="false"
            diskPersistent="false"
            timeToIdleSeconds="1800"
            timeToLiveSeconds="259200"
            memoryStoreEvictionPolicy="LRU"/>
 
    <cache
            name="cloud_user"
            eternal="false"
            maxElementsInMemory="5000"
            overflowToDisk="false"
            diskPersistent="false"
            timeToIdleSeconds="1800"
            timeToLiveSeconds="1800"
            memoryStoreEvictionPolicy="LRU"/>
    <!--
       defaultCache：默认缓存策略，当ehcache找不到定义的缓存时，则使用这个缓存策略。只能定义一个。
     -->
    <!--
      name:缓存名称。
      maxElementsInMemory:缓存最大数目
      maxElementsOnDisk：硬盘最大缓存个数。
      eternal:对象是否永久有效，一但设置了，timeout将不起作用。
      overflowToDisk:是否保存到磁盘，当系统当机时
      timeToIdleSeconds:设置对象在失效前的允许闲置时间（单位：秒）。仅当eternal=false对象不是永久有效时使用，可选属性，默认值是0，也就是可闲置时间无穷大。
      timeToLiveSeconds:设置对象在失效前允许存活时间（单位：秒）。最大时间介于创建时间和失效时间之间。仅当eternal=false对象不是永久有效时使用，默认是0.，也就是对象存活时间无穷大。
      diskPersistent：是否缓存虚拟机重启期数据 Whether the disk store persists between restarts of the Virtual Machine. The default value is false.
      diskSpoolBufferSizeMB：这个参数设置DiskStore（磁盘缓存）的缓存区大小。默认是30MB。每个Cache都应该有自己的一个缓冲区。
      diskExpiryThreadIntervalSeconds：磁盘失效线程运行时间间隔，默认是120秒。
      memoryStoreEvictionPolicy：当达到maxElementsInMemory限制时，Ehcache将会根据指定的策略去清理内存。默认策略是LRU（最近最少使用）。你可以设置为FIFO（先进先出）或是LFU（较少使用）。
      clearOnFlush：内存数量最大时是否清除。
      memoryStoreEvictionPolicy:可选策略有：LRU（最近最少使用，默认策略）、FIFO（先进先出）、LFU（最少访问次数）。
      FIFO，first in first out，这个是大家最熟的，先进先出。
      LFU， Less Frequently Used，就是上面例子中使用的策略，直白一点就是讲一直以来最少被使用的。如上面所讲，缓存的元素有一个hit属性，hit值最小的将会被清出缓存。
      LRU，Least Recently Used，最近最少使用的，缓存的元素有一个时间戳，当缓存容量满了，而又需要腾出地方来缓存新的元素的时候，那么现有缓存元素中时间戳离当前时间最远的元素将被清出缓存。
   -->
</ehcache>
```



# 十四、常用操作

## 1、java文件分屏显示

<img src="/../images/分屏显示.png" alt="分屏显示" style="zoom: 50%;" />