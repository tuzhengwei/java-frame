---
typora-root-url: images
---

# Maven

​	网址：https://mp.weixin.qq.com/mp/homepage?__biz=MzA5MTkxMDQ4MQ==&hid=3&sn=eebd634372eda11880ba859ee011942d&scene=1&devicetype=android-29&version=2800023d&lang=zh_CN&nettype=3gnet&ascene=7&session_us=gh_6a32bacc9543&wx_header=1&uin=&key=&fontgear=2&pass_ticket=8C0S9Gr3gCRalJ3HUtX2xTxUHqJX12ApEHjuod22lxEs5C4hOg7uLKx8Q8FCHhfS

# 一、概述

## 1、不适用maven存在的问题

（1）jar包难以寻找

（2）jar包依赖的问题

（3）jar包版本冲突问题

（4）jar不方便管理

（5）项目结构五花八门

## 2、Maven是什么？

​	maven给每个jar**定义了唯一的标志**，这个在maven中叫做**项目的坐标**，通过这个**坐标定位到**你需要用到的任何版本的jar包；

​	maven可以很容易的**解决不同版本**之间的**jar冲突**的问题；

​	maven使开发者更加方便的**控制整个项目的生命周期**；

```
mvn clear 可以清理上次已编译好的代码
mvn compile 可以自动编译项目
mvn test 可以自动运行所有测试用例
mvn package 可以完成打包需要的所有操作(自动包含了清理、编译、测试的过程)
```

官方解释什么是maven：**maven是apache软件基金会组织维护的一款自动化构建工具，专注服务于java平台的项目构建和依赖管理**。

# 二、linux中安装安装maven

1. jdk1.8
2. maven3.6.2

---

## 1、JDK

### （1）安装jdk

​		可以到oracle官网上去下载jdk-8u181-linux-x64.tar.gz，将其放在/opt/jdk目录中，如下：

```cmd
[root@ady01 jdk]# cd /opt/jdk/
[root@ady01 jdk]# ll
total 181300
-rw-r--r-- 1 root root 185646832 Nov  1 13:30 jdk-8u181-linux-x64.tar.gz
```

### （2）解压jdk

```cmd
[root@ady01 jdk]# tar -zvxf jdk-8u181-linux-x64.tar.gz
[root@ady01 jdk]# ll
total 181304
drwxr-xr-x 7   10  143      4096 Jul  7  2018 jdk1.8.0_181
-rw-r--r-- 1 root root 185646832 Nov  1 13:30 jdk-8u181-linux-x64.tar.gz
```

### （3）配置环境变量

在/etc/profile文件末尾追加下面几行

```cmd
export JAVA_HOME=/opt/jdk/jdk1.8.0_181
export PATH=$JAVA_HOME/bin:$PATH
export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar
```

​	运行下面命令使环境变量生效

```cmd
[root@ady01 jdk1.8.0_181]# source /etc/profile
```

### （4）查看jdk版本

```cmd
[root@ady01 jdk]# java -version
java version "1.8.0_181"
Java(TM) SE Runtime Environment (build 1.8.0_181-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.181-b13, mixed mode)
```

---

​	新建/opt/jdk/HelloWorld.java，内容如下：

```java
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("hello maven！");
    }
}
```

```cmd
[root@ady01 jdk]# cd /opt/jdk/
[root@ady01 jdk]# javac HelloWorld.java 
[root@ady01 jdk]# java HelloWorld
hello maven
```

## 2、maven

咱们到maven官网中下载最新的maven，地址如下：

```
https://maven.apache.org/download.cgi
```

```cmd
[root@ady01 jdk]# mkdir /opt/maven
[root@ady01 jdk]# cd /opt/maven/
[root@ady01 maven]# wget http://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.2/binaries/apache-maven-3.6.2-bin.tar.gz
--2019-11-01 13:47:11--  http://mirrors.tuna.tsinghua.edu.cn/apache/maven/maven-3/3.6.2/binaries/apache-maven-3.6.2-bin.tar.gz
Resolving mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)... 101.6.8.193, 2402:f000:1:408:8100::1
Connecting to mirrors.tuna.tsinghua.edu.cn (mirrors.tuna.tsinghua.edu.cn)|101.6.8.193|:80... connected.
HTTP request sent, awaiting response... 200 OK
Length: 9142315 (8.7M) [application/octet-stream]
Saving to: ‘apache-maven-3.6.2-bin.tar.gz’

100%[==================================================================================================================================>] 9,142,315   10.2MB/s   in 0.9s   

2019-11-01 13:47:13 (10.2 MB/s) - ‘apache-maven-3.6.2-bin.tar.gz’ saved [9142315/9142315]
[root@ady01 maven]# ls
apache-maven-3.6.2-bin.tar.gz
```

> 上面我们创建了/opt/maven目录用来存放maven相关软件，然后使用到**wget命令**，这个是linux中的一个命令，可以访问一个http地址，将其下载到当前目录。

### （1）解压maven

```cmd
[root@ady01 maven]# tar -zvxf apache-maven-3.6.2-bin.tar.gz
[root@ady01 maven]# ls
apache-maven-3.6.2  apache-maven-3.6.2-bin.tar.gz
```

### （2）maven目录结构

```cmd
[root@ady01 maven]# ll /opt/maven/maven/
total 40
drwxr-xr-x 2 root  root   4096 Nov  1 13:49 bin
drwxr-xr-x 2 root  root   4096 Nov  1 13:49 boot
drwxrwxr-x 3 mysql mysql  4096 Aug 27 23:01 conf
drwxrwxr-x 4 mysql mysql  4096 Nov  1 13:49 lib
-rw-rw-r-- 1 mysql mysql 12846 Aug 27 23:09 LICENSE
-rw-rw-r-- 1 mysql mysql   182 Aug 27 23:09 NOTICE
-rw-rw-r-- 1 mysql mysql  2533 Aug 27 23:01 README.txt
```

> bin：存放可以**执行的文件**
>
> conf：存放maven的**配置文件**
>
> lib：maven是java编写的，里面会用到很多**第三方的jar包**，这些jar包位于lib中

### （3）创建一个软连接

​	创建一个软连接指向apache-maven-3.6.2目录

```cmd
[root@ady01 maven]# ln -s apache-maven-3.6.2 maven
[root@ady01 maven]# ll
total 8936
drwxr-xr-x 6 root root    4096 Nov  1 13:49 apache-maven-3.6.2
-rw-r--r-- 1 root root 9142315 Sep  3 05:43 apache-maven-3.6.2-bin.tar.gz
lrwxrwxrwx 1 root root      18 Nov  1 13:56 maven -> apache-maven-3.6.2
```

> `ln –s 源文件 目标文件`是linux中的一个命令，这个相当于给`源文件`创建了一个快捷方式，快捷方式的名称叫做`目标文件`。
>
> `ln -s apache-maven-3.6.2 maven`是指给`apache-maven-3.6.2`创建了一个快捷方式`maven`，访问`maven`就相当于访问`apache-maven-3.6.2`。
>
> 此处为什么需要用快捷方式？
>
> 以后升级maven更方便一些，以后如果我们需要将maven升级到最新的版本，比如3.7，那么只需下载3.7到当前目录，再运行一下`ln -s apache-maven-3.7 maven`命令修改一下快捷方式的指向就行了

### （4）配置maven环境变量

> 在/etc/profile**文件末尾追加**下面几行

```cmd
export M2_HOME=/opt/maven/maven
export PATH=$M2_HOME/bin:$PATH
```

​	运行下面的命令**让环境变量生效**

```cmd
[root@ady01 maven]# source /etc/profile
```

​	验证maven是否正常

```
[root@ady01 maven]# mvn -v
Apache Maven 3.6.2 (40f52333136460af0dc0d7232c0dc0bcf0d9e117; 2019-08-27T23:06:16+08:00)
Maven home: /opt/maven/maven
Java version: 1.8.0_181, vendor: Oracle Corporation, runtime: /opt/jdk/jdk1.8.0_181/jre
Default locale: en_US, platform encoding: UTF-8
OS name: "linux", version: "3.10.0-693.2.2.el7.x86_64", arch: "amd64", family: "unix"
```

> `mvn -v`输出maven的版本号信息，若输出和上面类似，那么恭喜你，maven安装成功!

## 3、Maven的运行原理详解

> 本文后面会用到`~`这个符号，先对这个符号做一下说明，这个符号**表示当前用户的目录**
>
> window中默认在`C:\Users\用户名`
>
> linux root用户默认在`/root`目录，其他用户的~对应`/home/用户名`
>
> 后面的文章中我们就用~表示用户目录，这个地方不再说明。

```cmd
C:\Users\Think>mvn help:system
[INFO] Scanning for projects...
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-antrun-plugin/1.3/maven-antrun-plugin-1.3.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-antrun-plugin/1.3/maven-antrun-plugin-1.3.pom (4.7 kB at 4.0 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-plugins/12/maven-plugins-12.pom
Downloaded from central: https://repo.maven.apache.org/maven2/org/apache/maven/plugins/maven-plugins/12/maven-plugins-12.pom (12 kB at 21 kB/s)
Downloading from central: https://repo.maven.apache.org/maven2/org/apache/maven/maven-parent/9/maven-parent-9.pom
......
```

​	上面运行`mvn help:system`命令之后，好像从`https://repo.maven.apache.org`站点中在下载很多东西，最后又输出了系统所有环境变量的信息。

> `mvn help:system`这个**命令的运行过程**：

> 1. 运行`mvn help:system`之后
> 2. 系统会去环境变量PATH对应的所有目录中寻找mvn命令，然后在`D:\installsoft\maven\apache-maven-3.6.2\bin`中找到了可执行的`mvn`文件
> 3. **运行mvn文件，也就是执行mvn命令**
> 4. 通常一些软件启动的时候，会有一个启动配置文件，maven也有，mvn命令启动的时候会去`~/.m2`目录寻找配置文件`settings.xml`，这个文件是mvn命令启动配置文件，可以对maven进行一些启动设置（如本地插件缓存放在什么位置等等），若`~/.m2`目录中找不到`settings.xml`文件，那么会去`M2_HOME/conf`目录找这个配置文件，然后运行maven程序
> 5. mvn命令后面跟了一个参数：`help:sytem`，这个是什么意思呢？这个表示运行`help`插件，然后给help插件发送`system`命令
> 6. maven查看本地缓存目录（默认为`~/.m2`目录）寻找是否有help插件，如果本地没有继续下面的步骤
> 7. maven会去默认的一个站点（apache为maven提供的一个网站[repo.maven.apache.org]，这个叫中央仓库）下载help插件到`~/.m2`目录
> 8. 运行help插件，然后给help插件发送`system`指令，help插件收到`system`指令之后，输出了本地环境变量的信息，如果系统找不到指定的插件或者给插件发送无法识别的命令，都会报错

---

## 4、Maven的一些配置

### （1）启动文件设置

​		上面提到了`mvn`运行的时候，会加载**启动的配置文件**`settings.xml`，这个文件默认在`M2_HOME/conf`目录，一般我们会**拷贝一个放在`~/.m2`目录中**，**前者是全局范围**的配置文件，整个机器上所有用户都会受到该配置的影响，而**后者是用户范围级别**的，只有当前用户才会受到该配置的影响。推荐使用用户级别的，将其放在`~/.m2`目录，而不去使用全局的配置，以免影响到其他用户的使用。还有这样使用方便日后maven版本升级，一般情况下maven整个安装目录我们都不要去动，升级的时候只需要替换一下安装文件就可以了，很方便。

### （2）配置本地缓存目录

​	settings.xml中有个`localRepository`标签，可以**设置本地缓存目录**，maven从远程仓库下载下来的插件以及以后所有我们用到的jar包都会放在这个目录中，如下：

```
<localRepository>C:/Users/Think/.m1/repository</localRepository>
```

## 5、总结

（1）掌握maven的安装过程

（2）`~`表示**当前**用户目录

（3）maven的配置文件settings.xml一般我们放在`~/.m2`目录中，方便maven的升级，避免影响其他用户的配置

（4）了解mvn命令的执行过程

---

# 三、详解maven解决依赖问题

## 1、约定目录

​	Maven 提倡使用一个**共同的标准目录结构**，Maven 使用**约定优于配置**的原则，大家尽可能的遵守这样的目录结构，如下所示：

|                目录                | 作用                                                         |
| :--------------------------------: | ------------------------------------------------------------ |
|             ${basedir}             | 存放：pom.xml和所有子目录                                    |
|      ${basedir}/src/main/java      | 项目的java源代码                                             |
|   ${basedir}/src/main/resources    | 项目的资源，比如：property文件，springmvc.xml                |
|      ${basedir}/src/test/java      | 项目的测试类，比如：junit代码                                |
|   ${basedir}/src/test/resources    | 测试用的资源                                                 |
| ${basedir}/src/main/webapp/WEB-INF | WEB应用文件，WEB项目的信息，例如：存放web.xml、本地图片、JSP视图页面 |
|         ${basedir}/target          | 打包输出目录                                                 |
|     ${basedir}/target/classess     | 编译输出目录                                                 |
|   ${basedir}/target/test-classes   | 测试编译输出目录                                             |
|             Test.java              | Maven只会自动符合该命令规则的测试类                          |
|          -/.m2/repository          | Maven默认的本地参库目录位置                                  |

​	这是maven项目标准的结构，大家都按照这个约定来，然后maven中打包、运行、部署时候就非常方便了，**maven它自己就知道你项目的源码、资源、测试代码、打包输出的位置**。

---

## 2、pom文件

​	POM( **Project Object Model，项目对象模型** ) 是 Maven 工程的基本工作单元，是一个**XML文件**，包含了项目的基本信息，用于描述项目**如何构件**，声明**项目依赖**，等等。

---

POM 中可以**指定以下配置**：

- 项目依赖
- 插件
- 执行目标
- 项目构件 profile
- 项目版本
- 项目开发者列表
- 相关邮件列表信息

---

​	在创建 POM 之前，我们**首先**需要**描述项目组 (groupId)**，项目的唯一ID。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <!-- 模型版本 -->
    <modelVersion>4.0.0</modelVersion>

    <!-- 定义当前构件所属的组，通常与域名反向一一对应 -->
    <groupId>com.javacode2018</groupId>
    <!--项目的唯一ID，一个groupId下面可能多个项目，就是靠artifactId来区分的-->
    <artifactId>maven-chat02</artifactId>

    <!-- 版本号 -->
    <version>1.0-SNAPSHOT</version>

</project>
```

## 3、maven坐标

​	maven中**引入了坐标**的概念，每个构件都有唯一的坐标，我们使用maven创建一个项目需要标注其坐标信息，而项目中用到其他的一些构件，也需要知道这些构件的坐标信息。

​	maven中构件坐标是通过一些元素定义的，他们是：`groupId、artifactId、version、packaging、classifier`，如我springboot项目，它的pom中坐标信息如下：

```xml
<groupId>com.javacode2018</groupId>
<artifactId>springboot-chat01</artifactId>
<version>0.0.1-SNAPSHOT</version>
<packaging>jar</packaging>
```

> goupId：定义当前构件**所属的组**，通常与域名反向一一对应。
>
> artifactId：项目组中**构件的编号**。
>
> version：当前构件的**版本号**，每个构件可能会发布多个版本，通过版本号来区分不同版本的构件。
>
> package：定义该构件的**打包方式**，比如我们需要把项目打成jar包，采用`java -jar`去**运行这个jar包**，那这个值为jar；若当前是一个web项目，需要打成war包部署到tomcat中，那这个值就是war，可选（**jar、war、ear、pom、maven-plugin**），比较常用的是jar、war、pom，这些后面会详解。

---

## 4、maven导入依赖的构件

​	项目中如果**需要使用第三方的jar**，我们需要**知道其坐标信息**，然后将这些信息放入pom.xml文件中的`dependencies`元素中：

```xml
<project>
    <dependencies>
        <!-- 在这里添加你的依赖 -->
        <dependency>
            <groupId></groupId>
            <artifactId></artifactId>
            <version></version>
            <type></type>
            <scope></scope>
            <optional></optional>
            <exclusions>
                <exclusion></exclusion>
                <exclusion></exclusion>
            </exclusions>
        </dependency>
    </dependencies>
</project
```

- dependencies元素中**可以包含多个**`dependency`，每个`dependency`就表示当前项目需要依赖的一个构件的信息
- dependency中groupId、artifactId、version是**定位一个构件必须要提供的信息**，所以这几个是必须的
- type：依赖的类型，表示所要依赖的构件的类型，对应于被依赖的构件的packaging。大部分情况下，该元素不被声明，**默认值为jar**，表示被依赖的构件是一个jar包。
- scope：依赖的范围，后面详解
- option：标记依赖是否可选，后面详解
- exclusions：用来**排除传递性**的依赖

---

## 5、maven依赖范围（scope）

> 我们的需求：
>
> 1. 我们在开发项目的过程中，可能需要用junit来写一些测试用例，此时需要引入junit的jar包，但是当我们项目部署在线上运行了，测试代码不会再执行了，此时junit.jar是不需要了，所以**junit.jar只是在编译测试代码**，运行测试用例的时候用到，而**上线之后用不到了**，所以部署环境中是不需要的
> 2. 我们开发了一个web项目，在项目中用到了servlet相关的jar包，但是部署的时候，我们将其部署在tomcat中，而**tomcat中自带了servlet的jar包**，那么我们的需求是**开发、编译、单元测试**的过程中**需要这些jar**，上线之后，servlet相关的jar由web容器提供，也就是说打包的时候，**不需要将servlet相关的jar打入war包了**
> 3. 像`jdbc的驱动`，只有**在运行的时候才需要，编译的时候是不需要的**
>
> 这些需求怎么实现呢？

---

​	**java中编译代码、运行代码都需要用到classpath变量，classpath用来列出当前项目需要依赖的jar包**，

* classpath和jar关系

​	网址：http://www.itsoku.com/article/234

---

​	**maven用到classpath的地方有：编译源码、编译测试代码、运行测试代码、运行项目，这几个步骤都需要用到classpath。**

​	如上面的需求，编译、测试、运行需要的**classpath对应的值可能是不一样的**，这个maven中的scope为我们提供了支持，`scope`是用来**控制被依赖的构件与classpath的关系**（编译、打包、运行所用到的classpath），**scope有以下几种值**：

---

#### -	compile

​	编译依赖范围，如果没有指定，默认使用该依赖范围，**对于编译源码、编译测试代码、测试、运行4种classpath都有效**，比如上面的spring-web。

#### -	test

​	测试依赖范围，使用此依赖范围的maven依赖，**只对编译测试、运行测试的classpath有效，在编译主代码、运行项目时无法使用此类依赖**。比如junit，它只有在编译测试代码及运行测试的时候才需要。

#### -	provide

​	已提供依赖范围。表示项目的运行环境中已经提供了所需要的构件，对于此依赖范围的maven依赖，**对于编译源码、编译测试、运行测试中classpath有效，但在运行时无效**。比如上面说到的servlet-api，这个在编译和测试的时候需要用到，但是在运行的时候，web容器已经提供了，就不需要maven帮忙引入了。

#### -	runtime

​	运行时依赖范围，使用此依赖范围的maven依赖，**对于编译测试、运行测试和运行项目的classpath有效，但在编译主代码时无效**，比如jdbc驱动实现，运行的时候才需要具体的jdbc驱动实现。

#### -	system

​	系统依赖范围，该依赖**与classpath有关系**，**和provided依赖范围**完全一致。但是，使用system范围的依赖时必须通过systemPath元素显示第指定依赖文件的路径。这种依赖直接依赖于本地路径中的构件，可能每个开发者机器中构件的路径不一致，所以如果使用这种写法，你的机器中可能没有问题，别人的机器中就会有问题，所以建议谨慎使用。

---

## 6、依赖范围与classpath关系

| 依赖范围 | 编译源码 | 编译测试代码 | 运行测试 | 运行项目 | 示例 |
| :------: | :------: | :----------: | :------: | :------: | :--: |
| compile  |    Y     |      Y       |    Y     |    Y     |      |
|   test   |    -     |      Y       |    Y     |    -     |      |
| provide  |    Y     |      Y       |    Y     |    -     |      |
| runtime  |    -     |      Y       |    Y     |    Y     |      |
|  system  |    Y     |      Y       |    Y     |    -     |      |

> scope如果对于运行范围有效，意思是指依赖的jar包会**被打包到项目的运行包中**，最后运行的时候会被添加到classpath中运行。如果scope对于运行项目无效，那么项目打包的时候，这些依赖**不会被打包到运行包中**。

---

## 7、依赖的传递

​	我们创建的`maven-chat02`中依赖了spring-web，而**我们只引入了**`spring-web`依赖；

​	**而spring-web又依赖了**`spring-beans、spring-core、spring-jcl`，这3个依赖也被自动加进来了，这种叫做依赖的传递。

---

​	假设A依赖于B，B依赖于C，我们说A对于B是**第一直接依赖**，B对于C是**第二直接依赖**，而A对于C是**传递性依赖**，而第一直接依赖的scope和第二直接依赖的scope决定了传递依赖的范围，即决定了A对于C的scope的值。

## 8、maven依赖调解功能

### （1）路径最近原则

> 上面`A->B->C->Y(1.0)，A->D->Y(2.0)`，Y的2.0版本**距离A更近一些**，所以maven会选择2.0。

### （2）最先声明原则

> 如果出现了**路径一样的**，此时会看A的pom.xml中所依赖的B、D在`dependencies`中的位置，**谁的声明在最前面，就以谁的为主**，比如`A->B`在前面，那么最后Y会选择1.0版本。

## 9、排除依赖

```xml
<dependency>
    <groupId>com.javacode2018</groupId>
    <artifactId>B</artifactId>
    <version>1.0</version>
    <exclusions>
        <exclusion>
            <groupId>com.javacode2018</groupId>
            <artifactId>C</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

​	上面使用使用`exclusions元素`**排除了B->C依赖的传递**，也就是B->C**不会被**传递到A中。

​	exclusions中可以**有多个**`exclusion`元素，可以**排除一个或者多个依赖**的传递，声明exclusion时只需要写上groupId、artifactId就可以了，version可以省略。

# 四、仓库

​	**maven采用引用的方式将依赖的jar引入进来，不对真实的jar进行拷贝，但是打包的时候，运行需要用到的jar都会被拷贝到安装包中。**

## 1、Maven寻找依赖的jar

> 当我们项目中需要使用某些jar时，只需要**将**这些jar的maven**坐标添加到**pom.xml中就可以了，这背后maven是如何找到这些jar的呢？

> maven官方为我们**提供了一个站点**，这个站点中**存放了很多第三方常用的构建**（jar、war、zip、pom等等），当我们需要使用这些构件时，只需**将其坐标加入到**pom.xml中，此时maven会**自动将这些构建下载到本地一个目录**，然后进行**自动引用**。

---

maven站点，我们叫做：**maven中央仓库**，本地目录叫做**本地仓库**。

---

> 默认情况下，当项目中引入依赖的jar包时，maven**先在本地仓库**检索jar，若本地仓库没有，maven**再去从中央仓库**寻找，然后从中央仓库中将依赖的构件下载到本地仓库，然后才可以使用，如果2个地方**都没有，maven会报错**。

![](/../images/仓库.png)

## 2、仓库

​	在 Maven 中，仓库是一个位置，这个位置是用来**存放各种第三方构件的**，所有maven项目可以**共享这个仓库中的构件。**

> 构件：任何一个**依赖、插件或者项目构建**的输出（主要是jar包）

​	Maven 仓库能帮助我们**管理构件**（主要是jar包），它就是放置所有jar文件（jar、war、zip、pom等等）的地方。

---

### （1）本地仓库

> 默认情况下，maven本地仓库**默认地址**是`~/.m2/respository`目录，这个默认我们也可以在`~/.m2/settings.xml`文件中进行修改：

```
<localRepository>本地仓库地址</localRepository>
```

​	Maven 的本地仓库，在**安装 Maven 后并不会创建**，当我们**执行第一条 maven 命令的时候本地仓库才会创建**，此时会从远程仓库下载构建到本地仓库给maven项目使用。

---

​	默认情况下，`~/.m2/settings.xml`这个文件是**不存在的**（`~`是指用户目录）；
​	我们需要从Maven安装目录中拷贝`conf/settings.xml`文件，将`M2_HOME/conf/settings.xml`**拷贝到**`~/.m2`目录中，然后对`~/.m2/settings.xml`**进行编辑**。

---

​	`M2_HOME/config/settings.xml`这个文件其**实也是可以使用**的，不过我们**不建议直接使用**，这个修改可能**会影响其他所有使用者**，还有修改了这个文件，也不利于以后maven的升级；

​	如果我们使用`~/.m2/settings.xml`，而maven安装目录中的配置不动，升级的时候只需要替换一下安装包就好了，所以我们建议将maven安装目录中的`settings.xml`拷贝到`~/.m2`中进行编辑，这个是用户级别的，**只会影响当前用户**。

---

### （2）远程仓库

​	远程仓库可以**有多个**，当本地仓库找不到构件时，可以去远程仓库找，然后放置到本地仓库中进行使用。

> 远程仓库又分为：**中央仓库、私服、其他公共远程仓库**

#### -	中央仓库

​	maven默认执行了一些下载操作，这个**下载地址**就是**中央仓库的地址**，这个地址是**maven社区**为我们提供的，是maven内置的一个默认的远程仓库地址，不需要用户去配置。

```xml
# maven的安装包地址
apache-maven-3.6.1\lib\maven-model-builder-3.6.1.jar\org\apache\maven\model\pom-4.0.0.xml
```

在pom-4.0.0.xml中，如下：

```xml
<repositories>
    <repository>
      <id>central</id>
      <name>Central Repository</name>
      <url>https://repo.maven.apache.org/maven2</url>
      <layout>default</layout>
      <snapshots>
        <enabled>false</enabled>
      </snapshots>
    </repository>
  </repositories>
```

> https://repo.maven.apache.org/maven2

---

中央仓库有几个特点：

* 中央仓库是由maven**官方社区提供给大家使用的**
* 不需要我们手动去配置，maven内部集成好了
* 使用中央仓库时，机器**必须是联网状态**，需要可以访问中央仓库的地址

---

> 检索构件的站点
>
> https://search.maven.org/

---

#### -	私服

> 我们为什么需要私服呢？
>
> ​		如果我们一个团队中有几百个人在开发一些项目，**都是采用maven的方式**来组织项目，那么我们**每个人都需要**从远程仓库中把需要依赖的构件下载到本地仓库，这**对公司的网络要求也比较高**，为了节省这个宽带和加快下载速度，我们在公司内部局域网内部可以架设一台服务器，这台服务器起到一个代理的作用，公司里面的所有开发者去访问这个服务器，这台服务器将需要的构建返回给我们；
>
> ​		如果这台服务器中也**没有我们需要的构建**，那么这个代理服务器会**去远程仓库**中查找，然后将其**先下载到代理服务器中**，然后**再返回给**开发者本地的仓库。

**总体上来说私服有以下好处：**

* 加速maven构件的下载速度
* 节省宽带
* 方便部署自己的构件以供他人使用
* 提高maven的稳定性，中央仓库需要本机能够访问外网，而如果采用私服的方式，只需要本机可以访问内网私服就可以了

## 3、构建文件的布局

​	远程仓库中组织构件的方式和本地仓库是一样的，以fastjson在本地仓库中的信息为例来做说明，如下：

```xml
<dependency>
    <groupId>com.alibaba</groupId>
    <artifactId>fastjson</artifactId>
    <version>1.2.62</version>
</dependency>
```

​	上面是fastjson 1.2.62这个jar，我们看一下这个jar在本地仓库中的位置，如下图

![](/../images/本地仓库.png)

> fastjson这个jar的地址是：
>
> ~\.m2\repository\com\alibaba\fastjson\1.2.62\fastjson-1.2.62.jar

---

`~\.m2\repository\`是仓库的目录，所有本地构件都位于该目录中，我们主要看一下后面的部分，是怎么构成的。

构件所在**目录的构成**如下：

```
groupId+"."+artifactId+"."+版本号
```

通过上面获取一个字符串，字符串由`groupId、artifactId、版本号`之间用`.`连接，然后将这个字符串中的`.`替换为**文件目录分隔符**然后**创建多级目录**。

而构件文件名称的组成如下：

```
[artifactId][-verion][-classifier].[type]
```

上面的fastjson-1.2.62.jar信息如下：

```
artifactId为fastjson
version为1.2.62
classifier为空
type没有指定，默认为jar
```

所以构件文件名称为`fastjson-1.2.62.jar`。

---

## 4、关于构件版本问题

​	在**发布稳定版本之前**，会有很**多个不稳定的测试版本**，我们版本我们称为：**快照版本**，用SNAPSHOT表示，回头去看看本文开头搭建的`maven-cha03`的pom.xml文件，默认是快照版本的，如下：

```
<version>1.0-SNAPSHOT</version>
```

​	version以`-SNAPSHOT`结尾的，表示这是一个不稳定的版本；

​	将`-SNAPSHOT`**去掉**，然后**发布一个稳定的版本**，表示这个版本是稳定的

---

## 5、Maven中远程仓库的配置

> 方式一：pom.xml中配置远程仓库，语法如下：

```xml
<project>
    <repositories>
        <repository>
            <id>aliyun-releases</id>
            <url>https://maven.aliyun.com/repository/public</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>
</project>
```

在repositories元素下，可以使用repository子元素**声明一个或者多个远程仓库**。

repository元素说明：

- id：远程仓库的一个标识，中央仓库的id是`central`，所以添加远程仓库的时候，id不要和中央仓库的id重复，会把中央仓库的覆盖掉
- url：远程仓库地址
- releases：主要用来配置是否需要从这个远程仓库下载稳定版本构建
- snapshots：主要用来配置是否需要从这个远程仓库下载快照版本构建

​	releases和snapshots中有个`enabled`属性，是个boolean值，默认为true，表示是否需要从这个远程仓库中下载稳定版本或者快照版本的构建，一般使用第三方的仓库，都是下载稳定版本的构建。

快照版本的构建以`-SNAPSHOT`结尾，稳定版没有这个标识。

---

示例

​	来感受一下pom方式配置远程仓库的效果。

​	**文本编辑器**打开`maven-chat03/pom.xml`，将下面内容贴进去：

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.javacode2018</groupId>
    <artifactId>maven-chat03</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.62</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>2.2.1.RELEASE</version>
        </dependency>
    </dependencies>

    <repositories>
        <repository>
            <id>aliyun-releases</id>
            <url>https://maven.aliyun.com/repository/public</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>

</project>
```

> 上面我们配置了一个远程仓库，地址是阿里云的maven仓库地址，`releases`的`enabled`为`true`，`snapshots`的`enabled`为`false`，表示这个远程仓库我们只允许下载稳定版本的构件，而不能从这个仓库中下载快照版本的构建。

**删除本地仓库**中以下几个目录：

```
~\.m2\repository\org\springframework
~\.m2\repository\com\alibaba
```

maven-chat03项目目录中打开cmd运行：

```
mvn compile
```

输出如下：

```
D:\code\IdeaProjects\maven-chat03>mvn compile
[INFO] Scanning for projects...
[INFO]
[INFO] -------------------< com.javacode2018:maven-chat03 >--------------------
[INFO] Building maven-chat03 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from aliyun-releases: https://maven.aliyun.com/repository/public/org/springframework/boot/spring-boot-starter-web/2.2.1.RELEASE/spring-boot-starter-web-2.2.1.RELEASE.pom
...
```

​	**输出中有很多**`Downloaded from aliyun-releases`，`Downloaded from`后面跟的**`aliyun-releases`就是上面我们在pom.xml中配置的远程仓库repository元素中的id**，后面还可以看到很多下载地址，这个地址就是我们上面在pom.xml中指定的远程仓库的地址，可以看到项目中依赖的构建从我们指定的远程仓库中下载了。

pom中配置远程仓库的方式**只对当前项目起效**，如果我们需要对所有项目起效，我们可以下面的方式2，向下看。

----

> 方式2：镜像的方式

​	如果仓库X可以提供仓库Y所有的内容，那么我们就可以认为X是Y的一个镜像，通俗点说，可以从Y获取的构件都可以从他的镜像中进行获取。

​	可以采用**镜像的方式配置远程仓库**，镜像在`settings.xml`中进行配置，对所有使用该配置的maven项目起效，配置方式如下：

```xml
<mirror>
  <id>mirrorId</id>
  <mirrorOf>repositoryId</mirrorOf>
  <name>Human Readable Name for this Mirror.</name>
  <url>http://my.repository.com/repo/path</url>
</mirror>
```

mirrors元素下面可以有多个mirror元素，每个mirror元素表示一个远程镜像，元素说明：

- id：镜像的id，是一个标识
- name：镜像的名称，这个相当于一个描述信息，方便大家查看
- url：镜像对应的远程仓库的地址
- mirrorOf：指定哪些远程仓库的id使用这个镜像，这个对应pom.xml文件中repository元素的id，就是表示这个镜像是给哪些pom.xml文章中的远程仓库使用的，这里面需要列出远程仓库的id，多个之间用逗号隔开，`*`表示给所有远程仓库做镜像

---

​	这里主要对mirrorOf再做一下说明，上面我们在项目中定义远程仓库的时候，pom.xml文件的repository元素中有个id，这个id就是远程仓库的id，而mirrorOf就是用来配置哪些远程仓库会走这个镜像去下载构件。

mirrorOf的配置有以下几种:

```xml
<mirrorOf>*</mirrorOf> 
```

> 上面**匹配所有远程仓库id**，这些远程仓库都会走这个镜像下载构件

```xml
<mirrorOf>远程仓库1的id,远程仓库2的id</mirrorOf> 
```

> 上面匹配指定的仓库，这些指定的仓库会走这个镜像下载构件

```xml
<mirrorOf>*,! repo1</mirrorOf> 
```

> 上面匹配所有远程仓库，repo1除外，使用感叹号将仓库从匹配中移除。

​	需要注意镜像仓库完全屏蔽了被镜像的仓库，所以当镜像仓库无法使用的时候，maven是无法自动切换到被镜像的仓库的，此时下载构件会失败，这个需要了解。

---

**示例**

将maven-chat03中的pom.xml修改为：

```xml
xml<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.javacode2018</groupId>
    <artifactId>maven-chat03</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
        <dependency>
            <groupId>com.alibaba</groupId>
            <artifactId>fastjson</artifactId>
            <version>1.2.62</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>2.2.1.RELEASE</version>
        </dependency>
    </dependencies>

</project>
```

修改~/.m2/settings.xml，**加入镜像配置**，如下：

```xml
<mirrors>
    <mirror>
        <id>mirror-aliyun-releases</id>
        <mirrorOf>*</mirrorOf>
        <name>阿里云maven镜像</name>
        <url>https://maven.aliyun.com/repository/public</url>
    </mirror>
</mirrors>
```

> 上面配置了一个阿里云的镜像，注意镜像的id是`mirror-aliyun-releases`
>
> 下面我们就来验证一下镜像的效果。

**删除本地仓库中**的以下几个目录：

```
~\.m2\repository\org\springframework
~\.m2\repository\com\alibaba
```

maven-chat03项目目录中打开cmd运行：

```cmd
mvn compile
```

输出如下：

```cmd
D:\code\IdeaProjects\maven-chat03>mvn compile
[INFO] Scanning for projects...
[INFO]
[INFO] -------------------< com.javacode2018:maven-chat03 >--------------------
[INFO] Building maven-chat03 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
Downloading from mirror-aliyun-releases: https://maven.aliyun.com/repository/public/com/alibaba/fastjson/1.2.62/fastjson-1.2.62.pom
Downloaded from mirror-aliyun-releases: https://maven.aliyun.com/repository/public/com/alibaba/fastjson/1.2.62/fastjson-1.2.62.pom (9.7 kB at 17 kB/s)
Downloading from mirror-aliyun-releases: https://maven.aliyun.com/repository/public/org/springframework/boot/spring-boot-starter-web/2.2.1.RELEASE/spring-boot-starter-web-2.2.1.RELEASE.pom
...
```

​	上面复制了部分内容，大家仔细看一下`Downloaded from`后面显示的是`mirror-aliyun-releases`，这个和settings.xml中镜像的id一致，表示我们配置的镜像起效了，所有依赖的构建都从镜像来获取了。

​	**关于镜像一个比较常用的用法是结合私服一起使用，由于私服可以代理所有远程仓库（包含中央仓库），因此对于maven用来来说，只需通过访问一个私服就可以间接访问所有外部远程仓库了，这块后面我们会在私服中做具体说明。**

# 五、私服

​	网址：https://mp.weixin.qq.com/s?__biz=MzA5MTkxMDQ4MQ==&mid=2648933584&idx=1&sn=cbe30eb4dd5799f780a44b604b114e45&scene=19#wechat_redirect

​	有3种专门的maven仓库管理软件可以用来帮助我们搭建私服：

- Apache基金会的archiva

```
http://archiva.apache.org/
```

- JFrog的Artifactory

```
https://jfrog.com/artifactory/
```

- Sonatype的**Nexus**  `[ˈnɛksəs]`

```
https://my.sonatype.com/
```

​	这些都是**开源的**私服软件，都可以自由使用。用的最多的是第三种Nexus

## 1、Windows10中安装Nexus[ˈnɛksəs]私服

### （1）准备条件

> **安装jdk1.8**
>
> **nexus下载地址：**https://help.sonatype.com/repomanager3/download

> ##### 百度网盘下载地址
>
> 链接：https://pan.baidu.com/s/1N8A4JH1Ykvetn3xs05LszA 
> 提取码：vig6

---

### （2）解压latest-win64.zip

> latest-win64.zip解压之后会**产生两个文件目录**：**nexus-3.19.1-01和sonatyp-work**

![](/../images/私服.webp.jpg)

---

### （3）启动nexus

**cmd**中直接运行`nexus-3.19.1-01/bin/nexus.exe /run` ，如下：

```cmd
D:\installsoft\maven\nexus\nexus-3.19.1-01\bin>nexus.exe /run
```

如果输出中出现了下面的**异常请忽略**

```cmd
java.io.UnsupportedEncodingException: Encoding GBK is not supported yet (feel free to submit a patch)
```

浏览器中打开

```
http://localhost:8081/
```

效果如下：

<img src="/../images/登录.webp.jpg" style="zoom:50%;" />

---

### （4）登录Nexus

​	点击上图右上角的`Sign in`，输入用户名和密码，nexus默认用户名是`admin`

​	nexus这个版本的密码是第一次启动的时候生成的，密码位于下面的文件中：

```
安装目录/sonatype-work/nexus3/admin.password
```

### （5）其他一些常见的操作

​	**停止Nexus的命令**

​		启动的cmd窗口中按：`ctrl+c`，可以停止Nexus。

​	**修改启动端口**

​		默认端口是8081，如果和本机有冲突，可以在下面的文件中修改：

```
nexus-3.19.1-01\etc\nexus-default.properties
```

> nexus使用java开发的web项目，内置了jetty web容器，所以可以直接运行。

---

## 2、Linux安装Nexus私服

### （1）准备条件

​	百度网盘中下载linux版本的nexus安装包，选择`latest-unix.tar.gz`文件，下载地址如下：

> 百度网盘
>
> 链接：https://pan.baidu.com/s/1N8A4JH1Ykvetn3xs05LszA 
> 提取码：vig6

将上面的安装包放在`/opt/nexus/`目录。

```cmd
# 解压
[root@test1117 nexus]# tar -zvxf latest-unix.tar.gz
[root@test1117 nexus]# ls
latest-unix.tar.gz  nexus-3.19.1-01  sonatype-work
```

启动：（为了安全性，最好自己创建个用户来操作。）

```cmd
[root@test1117 bin]# /opt/nexus/nexus-3.19.1-01/bin/nexus start
WARNING: ************************************************************
WARNING: Detected execution as "root" user.  This is NOT recommended!
WARNING: ************************************************************
Starting nexus
```

### （2）开放端口

**开放端口**

​	在`/etc/sysconfig/iptables`文件中加入下面内容：

```
-A INPUT -p tcp -m state --state NEW -m tcp --dport 8081 -j ACCEPT
```

​	执行下面命令，让上面**配置生效**：

```
[root@test1117 bin]# service iptables restart
Redirecting to /bin/systemctl restart  iptables.service
```

**验证效果**

​	**访问**

```
http://nexus私服所在的机器ip:8081/
```

​	出现下面效果表示一切ok。

<img src="/../images/登录.webp.jpg" style="zoom:50%;" />

---

**登录**

​	用户名为`admin`，密码在：

```
/opt/nexus/sonatype-work/nexus3/admin.password
```

​	登录之后请请**立即修改密码**：

----

### （3）Nexus中仓库分类

​	用户可以通过nexus去访问远程仓库，可以**将本地的构件发布到nexus中**，nexus是如何支撑这些操作的呢？

​	nexus中有个仓库列表，里面包含了各种各样的仓库，有我们说的被代理的第三方远程仓库，如下图：

<img src="/../images/代理.webp.jpg" style="zoom: 50%;" />

上图中是nexus安装好默认自带的仓库列表，主要有**3种类型**：

- 代理仓库
- 宿主仓库
- 仓库组

----

#### -	代理仓库

> ​	让使用者通过**代理仓库**来**间接访问外部的第三方远程仓库的**，如通过代理仓库访问maven中央仓库、阿里的maven仓库等等。代理仓库会从被代理的仓库中下载构件，缓存在代理仓库中以供maven用户使用。

​	我们在nexus中创建一个阿里云的maven代理仓库来看下过程如下。

​	Nexus仓库列表中点击`Create repository`按钮，如下图：

<img src="/../images/创建仓库.webp.jpg" style="zoom:50%;" />

进入**添加页面**，选择`maven2(proxy)`，这个表示`代理仓库`，如下图：

<img src="/../images/添加代理仓库.webp.jpg" style="zoom:50%;" />

**输入远程仓库的信息**，如下图：

<img src="/../images/进入远程仓库.webp.jpg" style="zoom:50%;" />

---

```
第一个红框中输入仓库名称：maven-aliyun

第二个红框选择：Release，表示从这个仓库中下载稳定版的构件

第三个红框输入阿里云仓库地址：https://maven.aliyun.com/repository/public
```

​	点击底部的`Create repository`按钮，**创建完成**，如下图：

<img src="/../images/创建仓库成功.webp.jpg" style="zoom: 33%;" />

---

#### -	宿主仓库

宿主仓库主要**是给我们自己用的**，主要有2点作用

- 将私有的一些构件通过nexus中网页的方式上传到宿主仓库中给其他同事使用
- 将自己开发好一些构件发布到nexus的宿主仓库中以供其他同事使用

---

#### -	仓库组

> ​	maven用户可以从代理仓库和宿主仓库中下载构件至本地仓库，为了方便从多个代理仓库和宿主仓库下载构件，maven提供了仓库组，仓库组中可以**有多个代理仓库和宿主仓库**，而maven用户只用访问一个仓库组就可以间接的访问这个组内所有的仓库，仓库组中多个仓库是有顺序的，当maven用户从仓库组下载构件时，仓库组会按顺序依次在组内的仓库中查找组件，查找到了立即返回给本地仓库，所以一般情况我们会将速度快的放在前面。
>
> 仓库组内部实际上是**没有构件内容的**，他只是起到一个**请求转发的作用**，将maven用户下载构件的请求转发给组内的其他仓库处理。

nexus默认有个仓库组`maven-public`，如下：

![](/../images/仓库组.png)

点击一下`maven-public`这行记录，进去看一下，如下图：

<img src="/../images/添加.png" style="zoom:50%;" />

> 上图中第一个红框是这个**仓库组对外的一个url**，我们本地的maven可以通过这个url来从仓库组中下载构件至本地仓库。
>
> 第二个红框中是这个**仓库组中的成员**，目前包含了3个仓库，第1个是宿主的releases版本仓库，第1个是宿主快照版本的仓库，第3个是代理仓库（maven社区中央仓库的代理）。
>
> 刚才我们新增的`maven-aliyun`在左边，我们将其也加到右边的仓库成员（`Members`）列表，然后将`maven-aliyun`这个仓库放在第3个位置，这个仓库的速度比`maven-central`要快一些，能加速我们下载maven构件的速度.

---

### （4）配置本地Maven从nexus下载构件

​	见对应的博客。
