---
typora-root-url: images
---

# Maven

​	网址：https://mp.weixin.qq.com/mp/homepage?__biz=MzA5MTkxMDQ4MQ==&hid=3&sn=eebd634372eda11880ba859ee011942d&scene=1&devicetype=android-29&version=2800023d&lang=zh_CN&nettype=3gnet&ascene=7&session_us=gh_6a32bacc9543&wx_header=1&uin=&key=&fontgear=2&pass_ticket=8C0S9Gr3gCRalJ3HUtX2xTxUHqJX12ApEHjuod22lxEs5C4hOg7uLKx8Q8FCHhfS

# 三、详解maven解决依赖问题

## 1、约定目录

​	Maven 提倡使用一个**共同的标准目录结构**，Maven 使用**约定优于配置**的原则，大家尽可能的遵守这样的目录结构，如下所示：

|                目录                | 作用                                                         |
| :--------------------------------: | ------------------------------------------------------------ |
|             ${basedir}             | 存放：pom.xml和所有子目录                                    |
|      ${basedir}/src/main/java      | 项目的java源代码                                             |
|   ${basedir}/src/main/resources    | 项目的资源，比如：property文件，springmvc.xml                |
|      ${basedir}/src/test/java      | 项目的测试类，比如：junit代码                                |
|   ${basedir}/src/test/resources    | 测试用的资源                                                 |
| ${basedir}/src/main/webapp/WEB-INF | WEB应用文件，WEB项目的信息，例如：存放web.xml、本地图片、JSP视图页面 |
|         ${basedir}/target          | 打包输出目录                                                 |
|     ${basedir}/target/classess     | 编译输出目录                                                 |
|   ${basedir}/target/test-classes   | 测试编译输出目录                                             |
|             Test.java              | Maven只会自动符合该命令规则的测试类                          |
|          -/.m2/repository          | Maven默认的本地参库目录位置                                  |

​	这是maven项目标准的结构，大家都按照这个约定来，然后maven中打包、运行、部署时候就非常方便了，**maven它自己就知道你项目的源码、资源、测试代码、打包输出的位置**。

---

## 2、pom文件

​	POM( **Project Object Model，项目对象模型** ) 是 Maven 工程的基本工作单元，是一个**XML文件**，包含了项目的基本信息，用于描述项目**如何构件**，声明**项目依赖**，等等。

---

POM 中可以**指定以下配置**：

- 项目依赖
- 插件
- 执行目标
- 项目构件 profile
- 项目版本
- 项目开发者列表
- 相关邮件列表信息

---

​	在创建 POM 之前，我们**首先**需要**描述项目组 (groupId)**，项目的唯一ID。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <!-- 模型版本 -->
    <modelVersion>4.0.0</modelVersion>

    <!-- 定义当前构件所属的组，通常与域名反向一一对应 -->
    <groupId>com.javacode2018</groupId>
    <!--项目的唯一ID，一个groupId下面可能多个项目，就是靠artifactId来区分的-->
    <artifactId>maven-chat02</artifactId>

    <!-- 版本号 -->
    <version>1.0-SNAPSHOT</version>

</project>
```

## 3、maven坐标

​	maven中**引入了坐标**的概念，每个构件都有唯一的坐标，我们使用maven创建一个项目需要标注其坐标信息，而项目中用到其他的一些构件，也需要知道这些构件的坐标信息。

​	maven中构件坐标是通过一些元素定义的，他们是：`groupId、artifactId、version、packaging、classifier`，如我springboot项目，它的pom中坐标信息如下：

```xml
<groupId>com.javacode2018</groupId>
<artifactId>springboot-chat01</artifactId>
<version>0.0.1-SNAPSHOT</version>
<packaging>jar</packaging>
```

> goupId：定义当前构件**所属的组**，通常与域名反向一一对应。
>
> artifactId：项目组中**构件的编号**。
>
> version：当前构件的**版本号**，每个构件可能会发布多个版本，通过版本号来区分不同版本的构件。
>
> package：定义该构件的**打包方式**，比如我们需要把项目打成jar包，采用`java -jar`去**运行这个jar包**，那这个值为jar；若当前是一个web项目，需要打成war包部署到tomcat中，那这个值就是war，可选（**jar、war、ear、pom、maven-plugin**），比较常用的是jar、war、pom，这些后面会详解。

---

## 4、maven导入依赖的构件

​	项目中如果**需要使用第三方的jar**，我们需要**知道其坐标信息**，然后将这些信息放入pom.xml文件中的`dependencies`元素中：

```xml
<project>
    <dependencies>
        <!-- 在这里添加你的依赖 -->
        <dependency>
            <groupId></groupId>
            <artifactId></artifactId>
            <version></version>
            <type></type>
            <scope></scope>
            <optional></optional>
            <exclusions>
                <exclusion></exclusion>
                <exclusion></exclusion>
            </exclusions>
        </dependency>
    </dependencies>
</project
```

- dependencies元素中**可以包含多个**`dependency`，每个`dependency`就表示当前项目需要依赖的一个构件的信息
- dependency中groupId、artifactId、version是**定位一个构件必须要提供的信息**，所以这几个是必须的
- type：依赖的类型，表示所要依赖的构件的类型，对应于被依赖的构件的packaging。大部分情况下，该元素不被声明，**默认值为jar**，表示被依赖的构件是一个jar包。
- scope：依赖的范围，后面详解
- option：标记依赖是否可选，后面详解
- exclusions：用来**排除传递性**的依赖

---

## 5、maven依赖范围（scope）

> 我们的需求：
>
> 1. 我们在开发项目的过程中，可能需要用junit来写一些测试用例，此时需要引入junit的jar包，但是当我们项目部署在线上运行了，测试代码不会再执行了，此时junit.jar是不需要了，所以**junit.jar只是在编译测试代码**，运行测试用例的时候用到，而**上线之后用不到了**，所以部署环境中是不需要的
> 2. 我们开发了一个web项目，在项目中用到了servlet相关的jar包，但是部署的时候，我们将其部署在tomcat中，而**tomcat中自带了servlet的jar包**，那么我们的需求是**开发、编译、单元测试**的过程中**需要这些jar**，上线之后，servlet相关的jar由web容器提供，也就是说打包的时候，**不需要将servlet相关的jar打入war包了**
> 3. 像`jdbc的驱动`，只有**在运行的时候才需要，编译的时候是不需要的**
>
> 这些需求怎么实现呢？

---

​	**java中编译代码、运行代码都需要用到classpath变量，classpath用来列出当前项目需要依赖的jar包**，

* classpath和jar关系

​	网址：http://www.itsoku.com/article/234

---

​	**maven用到classpath的地方有：编译源码、编译测试代码、运行测试代码、运行项目，这几个步骤都需要用到classpath。**

​	如上面的需求，编译、测试、运行需要的**classpath对应的值可能是不一样的**，这个maven中的scope为我们提供了支持，`scope`是用来**控制被依赖的构件与classpath的关系**（编译、打包、运行所用到的classpath），**scope有以下几种值**：

---

#### -	compile

​	编译依赖范围，如果没有指定，默认使用该依赖范围，**对于编译源码、编译测试代码、测试、运行4种classpath都有效**，比如上面的spring-web。

#### -	test

​	测试依赖范围，使用此依赖范围的maven依赖，**只对编译测试、运行测试的classpath有效，在编译主代码、运行项目时无法使用此类依赖**。比如junit，它只有在编译测试代码及运行测试的时候才需要。

#### -	provide

​	已提供依赖范围。表示项目的运行环境中已经提供了所需要的构件，对于此依赖范围的maven依赖，**对于编译源码、编译测试、运行测试中classpath有效，但在运行时无效**。比如上面说到的servlet-api，这个在编译和测试的时候需要用到，但是在运行的时候，web容器已经提供了，就不需要maven帮忙引入了。

#### -	runtime

​	运行时依赖范围，使用此依赖范围的maven依赖，**对于编译测试、运行测试和运行项目的classpath有效，但在编译主代码时无效**，比如jdbc驱动实现，运行的时候才需要具体的jdbc驱动实现。

#### -	system

​	系统依赖范围，该依赖**与classpath有关系**，**和provided依赖范围**完全一致。但是，使用system范围的依赖时必须通过systemPath元素显示第指定依赖文件的路径。这种依赖直接依赖于本地路径中的构件，可能每个开发者机器中构件的路径不一致，所以如果使用这种写法，你的机器中可能没有问题，别人的机器中就会有问题，所以建议谨慎使用。

---

## 6、依赖范围与classpath关系

| 依赖范围 | 编译源码 | 编译测试代码 | 运行测试 | 运行项目 | 示例 |
| :------: | :------: | :----------: | :------: | :------: | :--: |
| compile  |    Y     |      Y       |    Y     |    Y     |      |
|   test   |    -     |      Y       |    Y     |    -     |      |
| provide  |    Y     |      Y       |    Y     |    -     |      |
| runtime  |    -     |      Y       |    Y     |    Y     |      |
|  system  |    Y     |      Y       |    Y     |    -     |      |

> scope如果对于运行范围有效，意思是指依赖的jar包会**被打包到项目的运行包中**，最后运行的时候会被添加到classpath中运行。如果scope对于运行项目无效，那么项目打包的时候，这些依赖**不会被打包到运行包中**。

---

## 7、依赖的传递

​	我们创建的`maven-chat02`中依赖了spring-web，而**我们只引入了**`spring-web`依赖；

​	**而spring-web又依赖了**`spring-beans、spring-core、spring-jcl`，这3个依赖也被自动加进来了，这种叫做依赖的传递。

---

​	假设A依赖于B，B依赖于C，我们说A对于B是**第一直接依赖**，B对于C是**第二直接依赖**，而A对于C是**传递性依赖**，而第一直接依赖的scope和第二直接依赖的scope决定了传递依赖的范围，即决定了A对于C的scope的值。

## 8、maven依赖调解功能

### （1）路径最近原则

> 上面`A->B->C->Y(1.0)，A->D->Y(2.0)`，Y的2.0版本**距离A更近一些**，所以maven会选择2.0。

### （2）最先声明原则

> 如果出现了**路径一样的**，此时会看A的pom.xml中所依赖的B、D在`dependencies`中的位置，**谁的声明在最前面，就以谁的为主**，比如`A->B`在前面，那么最后Y会选择1.0版本。

## 9、排除依赖

```xml
<dependency>
    <groupId>com.javacode2018</groupId>
    <artifactId>B</artifactId>
    <version>1.0</version>
    <exclusions>
        <exclusion>
            <groupId>com.javacode2018</groupId>
            <artifactId>C</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

​	上面使用使用`exclusions元素`**排除了B->C依赖的传递**，也就是B->C**不会被**传递到A中。

​	exclusions中可以**有多个**`exclusion`元素，可以**排除一个或者多个依赖**的传递，声明exclusion时只需要写上groupId、artifactId就可以了，version可以省略。

