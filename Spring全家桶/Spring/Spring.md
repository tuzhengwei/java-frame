---
typora-root-url: images
---

Spring基础

# 一、简介

## 1、历史

​	Spring : **春天** —>给软件行业带来了春天

​	2002年，Rod Jahnson首次推出了Spring框架雏形interface21框架。

​	2004年3月24日，Spring框架以interface21框架为基础，经过重新设计，发布了1.0正式版。

​	很难想象Rod Johnson的学历 , 他是悉尼大学的博士，然而他的专业不是计算机，而是音乐学。

​	Spring理念 : 使现有技术更加实用， 本身就是一个大杂烩 , 整合现有的框架技术

## 2、导入依赖（Maven）

官网 : http://spring.io/

官方下载地址 : https://repo.spring.io/libs-release-local/org/springframework/spring/

GitHub : https://github.com/spring-projects

```xml
<!-- https://mvnrepository.com/artifact/org.springframework/spring-webmvc -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>5.2.0.RELEASE</version>
</dependency>
<!-- 
	https://mvnrepository.com/artifact/org.springframework/spring-jdbc 
	JDBC块
-->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-jdbc</artifactId>
    <version>5.2.0.RELEASE</version>
</dependency>
```

## 3、优缺点

1、Spring是一个开源免费的框架 , 容器 .

2、Spring是一个轻量级的框架 , 非侵入式的 .

**3、控制反转 IoC , 面向切面 Aop**

4、对事物的支持 , 对框架的支持

…

一句话概括：

**Spring是一个轻量级的控制反转(IoC)和面向切面(AOP)的容器（框架）。**

## 4、组成

<img src="/../images/spring组成.png" alt="spring组成" style="zoom:33%;" />

​	Spring 框架是一个分层架构，由 **7** 个定义良好的模块组成。Spring 模块构建在核心容器之上，核心容器定义了**创建、配置和管理** bean 的方式 .

<img src="/../images/Spring框架.png" alt="Spring框架" style="zoom:33%;" />

​	组成 Spring 框架的每个模块（或组件）都可以**单独存在**，或者与其他**一个或多个**模块联合实现。每个模块的功能如下：

- **核心容器**：
  		核心容器提供 Spring 框架的基本功能。核心容器的主要组件是 **BeanFactory**，它是工厂模式的实现。		BeanFactory 使用*控制反转*（IOC） 模式将应用程序的配置和依赖性规范与实际的应用程序代码分开。
- **Spring 上下文**：
          Spring 上下文是一个**配置文件**，向 Spring 框架提供上下文信息。Spring 上下文包括企业服务，例如JNDI、EJB、电子邮件、**国际化**、校验和调度功能。
- **Spring AOP**：
         通过配置**管理特性**，Spring AOP 模块直接将**面向切面**的编程功能 , 集成到了 Spring 框架中。所以，可以很容易地使 Spring 框架管理任何支持 AOP的对象。Spring AOP 模块为基于 Spring 的应用程序中的对象提供了**事务管理服务**。通过使用 Spring AOP，**不用依赖组件**，就可以将声明性事务管理集成到应用程序中。
- **Spring DAO**：
         JDBC DAO 抽象层提供了有意义的**异常层次结构**，可用该结构来管理异常处理和不同数据库供应商抛出的错误消息。异常层次结构简化了错误处理，并且极大地降低了需要编写的异常代码数量（例如打开和关闭连接）。Spring DAO 的面向 **JDBC 的异常**遵从通用的 DAO 异常层次结构。
- **Spring ORM**：
         Spring 框架插入了若干个 **ORM 框架**，从而提供了 ORM 的对象关系工具，其中包括 JDO、Hibernate 和 iBatis SQL Map。所有这些都遵从 Spring 的通用事务和 DAO 异常层次结构。
- **Spring Web 模块**：
         Web 上下文模块建立在应用程序上下文模块之上，为基于 Web 的应用程序提供了上下文。所以，Spring 框架支持与 Jakarta Struts 的集成。Web 模块还简化了处理多部分请求以及将请求参数绑定到域对象的工作。
- **Spring MVC 框架**：
          MVC 框架是一个全功能的构建 Web 应用程序的 MVC 实现。通过策略接口，MVC 框架变成为高度可配置的，MVC 容纳了大量视图技术，其中包括 JSP、Velocity、Tiles、iText 和 POI。

## 5、扩展

**Spring Boot与Spring Cloud**

- Spring Boot 是 Spring 的一套**快速配置脚手架**，可以基于Spring Boot 快速开发单个微服务;
- Spring Cloud是基于Spring Boot实现的；
- Spring Boot专注于快速、方便集成的单个微服务个体，Spring Cloud关注全局的服务治理框架；
- Spring Boot使用了约束优于配置的理念，很多集成方案已经帮你选择好了，能不配置就不配置 , Spring Cloud很大的一部分是基于Spring Boot来实现，Spring Boot可以离开Spring Cloud独立使用开发项目，但是Spring Cloud离不开Spring Boot，属于依赖的关系。
- SpringBoot在SpringClound中起到了承上启下的作用，如果你要学习SpringCloud必须要学习SpringBoot。



# 二、IOC理论推导

## 1、原来IOC执行方式

<img src="/../images/IOC原来的模式.png" alt="IOC原来的模式" style="zoom:33%;" />

在Service的**实现类**中：

```java
public class UserServiceImpl implements UserService {
   private UserDao userDao = new UserDaoImpl();		// 调用默认的功能
   @Override
   public void getUser() {
       userDao.getUser();
  }
}
```

若现在**新增**了：UserDaoMysqllmpl.java 功能

```java
public class UserServiceImpl implements UserService {
//   private UserDao userDao = new UserDaoImpl();		// 调用默认的功能
   /*
   		1、因为新增一个业务，所以需要程序员重新new一个与之对应的：对象
   		2、创建对象，使用向上造型
   */
   private UserDao userDao = new UserDaoMysqllmpl();	
    
   @Override
   public void getUser() {
       userDao.getUser();
  }
}
```

## 2、改进：由用户自己来选择

**思路**：利用**set**实现，用户需要什么功能，传入即可，参数为父类型：UserDao，实现类传入：向上造型

Server层：**UserServiceImpl.java**

```java
public class UserServiceImpl implements UserService {
   private UserDao userDao;
   /*
   	1、利用set实现，用户需要什么功能，传入即可；
   	2、参数为父类型：UserDao，实现类传入：向上造型（传入的功能都是UserDao的实现类中的方法）
   */
   public void setUserDao(UserDao userDao) {
       this.userDao = userDao;
   }
   @Override
   public void getUser() {
       userDao.getUser();
  }
}
```

主方法：

```java
@Test
public void test(){
  	// 用户实际调用的是：业务层(Service)，dao层他们不需要接触
    UserService userService = new UserServiceImpl();
    // new UserDaoImpl()：传入的是用户需要实现的功能对象（被动，由用户选择）
    ((UserServiceImpl)userService).setUserDao(new UserDaoImpl());
    
    userService.getUser();
}
```

## 3、IOC本质

<img src="/../images/以前的IOC.png" alt="以前的IOC" style="zoom:50%;" />

​		这种思想 , 从本质上解决了问题 , 我们程序员**不再去管理**对象的创建了 , 更多的去关注**业务的实现** . **耦合性**大大降低 . 这也就是IOC的原型。

​		**控制反转IoC(Inversion of Control)，是一种设计思想，DI(依赖注入)是实现IoC的一种方法**，也有人认为DI只是IoC的另一种说法。没有IoC的程序中 , 我们使用面向对象编程 , 对象的创建与对象间的依赖关系完全硬编码在程序中，**对象的创建**由**程序自己控制**，控制反转后将对象的创建**转移给第三方**。

​		个人认为所谓控制反转就是：**获得   依赖对象的方式  反转**了。



# 三、快速上手Spring

​		现在**不需要**在程序中去改动，要实现不同的操作，只需要在**xml配置文件**中进行**修改**。
​		IOC：**对象由Spring来创建、管理、装配。**

## 1、HelloSpring

### （1）创建一个对象

​	路径：java/com/kuang/pojo/Hello.java

```java
public class Hello {
    private String name;
    public Hello(){
    }
    public Hello(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void out(){
        System.out.println("name的值为："+name);
    }
}
```

### （2）编写对应的Bean.xml 文件

​	**初始化原型.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">

</beans>
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">
    <!--
        使用Spring来创建对象，在Spring这些都成为：Bean
        等同于：类型 变量名 = new 类型()
        id：变量名      class：new的对象类型
        property：相当于给对象中的属性设置一个值
        name：属性名，value：给定的值
    -->
    <bean id="hello" class="com.kuang.pojo.Hello">
        <property name="name" value="Spring"/>
    </bean>
</beans>
```

### （3）测试

```java
@Test
public void test(){
    //解析beans.xml文件 , 生成管理相应的Bean对象
    ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
    //getBean : 参数即为spring配置文件中bean的id .
    // 强制转换
    Hello hello = (Hello) context.getBean("hello");
    hello.out();
}
```

## 2、IOC与ref

**ref**：引用Spring容器中**已经创建好**的对象。

![SpringBean使用](/../SpringBean使用.png)

​		若需要**新增**或者需要**更换**其他功能，直接在：**ref**（**引用对象**）中修改即可，新增对象，也可以直接**在xml文件****中**使用<bean>标签来创建，然后在xml中调用。

# 四、IOC创建对象的方式

## 1、默认使用：**无参构造器**来创建对象！

## 2、改为有参构造器的方法

​	**resources/beans.xml文件**

### （1） 下标赋值

```xml
<!-- 使用有参构造器的方法：
    第一种：下标赋值！
 -->
<bean id="user" class="com.kuang.pojo.User">
    <!-- index 跟 实体类中的属性按从上到下的顺序对应 -->
    <constructor-arg index="0" value="豆奶"/>
</bean>
```

### （2）通过类型创建，不建议使用

```xml
<!--第二种：通过类型创建，不建议使用 -->
<bean id="user" class="com.kuang.pojo.User">
    <constructor-arg type="java.lang.String" value="豆奶"/>
</bean>
```

​	**缺点**：若实体类中有**多个一样**的类型，则会报错。

### （3）直接通过参数名来设置

```xml
<!--第三种：直接通过参数名来设置 -->
<bean id="user" class="com.kuang.pojo.User">
    <constructor-arg name="name" value="豆奶"/>
</bean>
```

# 五、Spring配置

## 1、别名

​	如果添加了别名，可以使用**别名来获得**创建的对象

```xml
<bean id="hello" class="com.kuang.pojo.Hello">
    <property name="name" value="Spring"/>
</bean>
```

```xml
<!-- 别名，如果添加了别名，可以使用别名来获得创建的对象 -->
<alias name="hello" alias="Myhello"/>
```

```java
    @Test
    public void test(){
        //解析beans.xml文件 , 生成管理相应的Bean对象
        ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
        Hello hello = (Hello) context.getBean("Myhello");//通过别名来访问
        hello.out();
    }
```

## 2、Bean的配置

 <bean>中的**name**，表示：别名,可以起**多个别名**

```xml
<!--
    使用Spring来创建对象，在Spring这些都成为：Bean
    等同于：类型 变量名 = new 类型()
    id：变量名      class：new的对象类型
    property：相当于给对象中的属性设置一个值
    name：属性名，value：给定的值
-->
<bean id="hello" class="com.kuang.pojo.Hello" name="h1,h2">
    <property name="name" value="Spring"/>
</bean>
```

测试：

```java
Hello hello = (Hello) context.getBean("h1");
hello.out();
```

## 3、import

​	团队的**合作**通过import来实现 。

​	一般都会在resource/**applicationContext**.**xml文件**中导入大部分beans.xml文件

```xml
<import resource="{path}/beans.xml"/>
```

​	使用的时候，直接引入：**applicationContext**.xml

```java
ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
```

# 六、依赖注入

​	注入方法：**构造器注入、Set注入**

## 1、Set注入

### （1）创建一个实体类

```java
public class Address {
    private String address;
}
```

```java
public class Student {
    private String name;
    private Address address;
    private String[] books;
    private List<String> hobbys;
    private Map<String,String> card;
    private Set<String> games;
    private String wife;
    private Properties info;
    public void Student(){
}
```

**创建一个resources/beans02.xml文件**

### （2）beans02.xml文件

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        https://www.springframework.org/schema/beans/spring-beans.xsd">
    <!-- 创建一个Address对象 -->
    <bean id="address" class="com.kuang.pojo.Address"/>
    
    <!-- name为class对应实体类的属性变量名  -->
    <bean id="student" class="com.kuang.pojo.Student">
        ...
        
</beans>
```

**name**：class**对应实体类**的**属性变量名**

### （3）普通值注入：value

```xml
 <!-- 第一种：普通值注入：value -->
<property name="name" value="普通值注入方法"/>
```

### （4）Bean注入

​	将**创建好的对象**，通过bean注入

```xml
<!-- 第二种，Bean注入；ref：引用bean.xml文件以及创建好的：对象变量名 -->
<property name="address" ref="address"/>
```

### （5）数组注入

```xml
<!-- 第三种：数组注入，ref -->
<property name="books">
    <array>
        <value>数组注入方法</value>
        <value>西游记</value>
        <value>水釜城</value>
    </array>
</property>
```

### （6）List注入

```xml
<!-- 第四种，List注入 -->
<property name="hobbys">
    <list>
        <value>列表注入方法</value>
        <value>西游记1</value>
        <value>水釜城2</value>
    </list>
</property>
```

### （7）Map注入

```xml
<!-- 第五种，Map注入 -->
<property name="card">
    <map>
        <entry key="身份证"  value="12345789"></entry>
        <entry key="银行卡"  value="45679789"></entry>
    </map>
</property>
```

### （8）Set注入

```xml
<!-- 第六种，Set注入 -->
<property name="games">
    <set>
        <value>Set注入</value>
        <value>BOB</value>
    </set>
</property>
```

### （9）Null注入

```xml
<!-- 第七种，Null注入 -->
<property name="wife">
    <null/>
</property>
```

### （10）特殊类型：Properties

```xml
<!-- 第八种，特殊类型：Properties -->
<property name="info">
    <props>
        <prop key="key">值写在两个尖括号之间</prop>
        <prop key="key的值">map需要将值写在value里面</prop>
    </props>
</property>
```

### （11）测试

Student student = (Student) context.getBean("student");可以改为：
Student student = context.getBean("student",Studen.class);	// **指定**实体类，则不用强转

```java
@Test
public void test(){
    //解析beans.xml文件 , 生成管理相应的Bean对象
    ApplicationContext context = new ClassPathXmlApplicationContext("beans02.xml");
    // 返回的值为：Object类型， 故需要强制转换
    Student student = (Student) context.getBean("student");
    System.out.println(student.toString());
}
```

## 2、其他注入方法

### （1）P标签的使用

​	p命令空间注入，**直接**可以**注入属性的值**。

​	再使用p命名空间时，需要**首先导入约束**：

```xml
 xmlns:p="http://www.springframework.org/schema/p"	<!-- p命名空间约束 -->
```

p:name 可以对**简单属性**直接赋值；对于**复杂的属性**，可以使用：p:score-ref 

```xml
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:p="http://www.springframework.org/schema/p"	<!-- p命名空间约束 -->
    xsi:schemaLocation="http://www.springframework.org/schema/beans
    https://www.springframework.org/schema/beans/spring-beans.xsd">

	<!-- 创建一个score对象 -->
    <bean id="score" class="com.leishida.spring.Score"></bean>
	<!--  p:name 可以对简单属性直接赋值；对于复杂的属性，可以使用：p:score-ref -->
    <bean id="user4" class="com.leishida.spring.User" p:name="P标签导入" p:age="18" p:score-ref="score"/>
</beans>
```

### （2）C命令空间

​	c命名空间：通过**构造器**注入（construct）

​	再使用c命名空间时，需要**首先导入约束**：

```xml
 xmlns:p="http://www.springframework.org/schema/c"	<!-- c命名空间约束 -->
```

```xml
<bean id="user4" class="com.leishida.spring.User" c:name="P标签导入" c:age="18" c:score-ref="score"/>
```

​		必须要**确保**实体类中含有：**无参和有参**构造器。

## 3、Bean的作用域

| Scope       | Description                                                  |
| ----------- | ------------------------------------------------------------ |
| singleton   | （**默认**）为每个Spring IoC容器创建单个实例。单例作用域     |
| prototype   | **多例**作用域，为每个请求创建一个新的bean实例               |
| request     | 单个HTTP请求的生命周期。也就是说，每个HTTP请求都有自己的bean实例，这些实例是在单个bean定义的基础上创建的。仅在可感知web的Spring应用程序上下文中有效。 |
| session     | HTTPSession的生命周期。仅在可感知web的Spring应用程序上下文中有效。 |
| application | ServletContext的生命周期。仅在可感知web的Spring应用程序上下文中有效。 |
| websocket   | websocket的生命周期。仅在可感知web的Spring应用程序上下文中有效。 |

### （1）单例作用域

​		**仅仅**创建一个实例。

<img src="/../images/单例模式.png" alt="单例模式" style="zoom:50%;" />

```xml
<bean id="accountService" class="com.something.DefaultAccountService" scope="singleton"/>
```

​		单例模式对**对象的作用域**进行硬编码，这样每个类装入器只能创建一个特定类的实例

### （2） 原型范围（多例）

<img src="/../images/原型范围（多例）.png" alt="原型范围（多例）" style="zoom:50%;" />

​		**每次调用**都：创建了一个全新的bean实例

```xml
<bean id="accountService" class="com.something.DefaultAccountService" scope="prototype"/>
```



# 七、Bean的自动装配

**beans.xml**

## 1、byName

​	注意：**必须保证全局**beans.xml文件中，bean的**id值要唯一**，否则会报错。

（1）实体类

​	**setDog**

```java
public void setDog(Dog dog) {
    this.dog = dog;
}
```

（2）xml文件

 ByName ：会自动在容器的**上下文**中查找，和自己对象：**set方法后面的值**对应的 bean **id相同的**对象

**例如**：方法为：setDog()与bean id=“dog"

```xml
<bean id="cat" class="com.kuang.pojo.Cat"/>
<bean id="dog" class="com.kuang.pojo.Dog"/>

<!--
    autowire：自动装配
    ByName ：会自动在容器的上下文中查找，和自己对象：set方法后面的值对应的 bean id相同的对象
-->
<bean id="dog" class="com.kuang.pojo.Dao" autowire="byName">
    <property name="dog" value="小狗"/>
</bean>
```

## 2、byType

​	会自动在容器上下文中查找，和**自己对象属性类型**相同的：bean；

​	注意：**必须保证全局**beans.xml文件中**只能有一个**与**属性类型**相同的bean，否则会报错。

```xml
<bean id="people" class="com.kuang.pojo.People" autowire="byType">
    <property name="name" value="我"/>
</bean>
```

## 3、注解实现自动装配

​	jdk1.5开始支持注解，spring2.5开始全面支持注解。

### （1）导入context依赖

​	在spring配置文件中引入**context文件头**

```xml
<!-- 开启注解的支持 -->
<context:annotation-config/>
```
​	xmlns:context="http://www.springframework.org/schema/context"

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">

    <!-- 开启注解的支持 -->
    <context:annotation-config/>

    <bean id="cat" class="com.kuang.pojo.Cat"/>
    <bean id="dog" class="com.kuang.pojo.Dog"/>
    <bean id="people" class="com.kuang.pojo.People"/>

</beans>
```

### （2）@Autowired

​		直接在**属性**/**set方法**上使用：

```java
@Autowired
private Cat cat;
```

```java
@Autowired
public void setCat(Cat cat) {
    this.cat = cat;
}
```

注意：**Autowired**默认值为：true，表示**不能为：Null**

```java
public @interface Autowired {
    boolean required() default true;
}
```

```java
@Autowired(required = false)
/**
 * 如果显示定义了Autowired的required属性为：False，说明这个对象可以为：Null，否则默认：不能为Null
 */
private Cat cat;
```

### （3）@Qualifier

```xml
<bean id="cat" class="com.kuang.pojo.Cat"/>
<bean id="dog" class="com.kuang.pojo.Dog"/>
<bean id="people" class="com.kuang.pojo.People"/>
```

​	使用@Autowired注解，必须要**保证**：bean的**id值**要跟实体类的**属性值**对应。

​	若自动装配的环境比较复杂，**无法通过**一个注解完成，可以使用**@Qualifier注解**，格式：@Qualifier(value="")去配置，**指定**一个bean对象注入。

```java
@Autowired
@Qualifier(value = "dog2")
private Dog dog;
```

```xml
<bean id="dog2" class="com.kuang.pojo.Dog"/>
```

​	**id值**跟@Qualifier的**value值**一致。

### （4）Resource

​		包含：@Autowried和@Qualifier

​		该注解会**先匹配**id值，如id值都不符合；**再去查找**类型。两者都不符合的条件下，则会**报错**。

```java
@Resource
private Dog dog;
```

```xml
<bean id="dog2" class="com.kuang.pojo.Dog"/>
```

### （5）@Nullable

​	概念：可以是参数为：**Null**

```java
//perople的构造器方法，默认name是不能为空的
public People(@Nullable String name){

}
```

## 4、注解装配总结

@Autowired与@Resource异同：

​	（1）@Autowired与@Resource**都可以**用来装配bean。都可以写在字段上，或写在setter方法上。

​	（2）@Autowired默认**按类型装配**（属于spring规范），默认情况下必须要求**依赖对象必须存在**，如果要允许null 值，可以设置它的required属性为false，如：@Autowired(required=false) ，如果我们想使用名称装配可以结合@Qualifier注解进行使用

​	（3）@Resource（属于J2EE复返），**默认**按照**名称**进行装配，名称可以通过name属性进行指定。如果没有指定name属性，当注解写在字段上时，默认取字段名进行按照名称查找，如果注解写在setter方法上默认取属性名进行装配。**当找不到与名称匹配的bean时**才按照**类型**进行装配。但是需要注意的是，如果name属性一旦指定，就只会按照名称进行装配。

​	它们的作用相同都是用注解方式注入对象，但**执行顺序不同**。@Autowired先byType，@Resource先byName。


# 八、Spring注解

​	在spring4之后，想要使用**注解形式**，必须得要引入**aop的包**。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">

    <!-- 指定要扫描的包，这个包下的注解都会：生效 -->
    <context:component-scan base-package="com.kuang"/>

    <!-- 开启注解的支持 -->
    <context:annotation-config/>

</beans>
```

## 1、Bean

​	使用： **@Component注解组件**

​	等价与： <bean id="cat" class="com.kuang.pojo.User"/>

```java
/**
 * @Component注解组件等价与： <bean id="cat" class="com.kuang.pojo.User"/>
 */
@Component
public class User {

}
```

## 2、属性如何注入

​	使用注解：  **@Value("值")**

​	等价于： <property name="name" value="小狗"/>

```java
@Component
public class User {
        @Value("小狗")
        /**
         *     <bean id="cat" class="com.kuang.pojo.User">
          		   @value注解等同于：
         *         <property name="name" value="小狗"/>
         *     </bean>
         */
        public String name;
}
```

## 3、衍生的注解

@Component有几个**衍生注解**，在Web开发中，会按照**MVC三层架构**来分层！

- dao

   数据层
  【使用注解：@Repository】

- service
  业务层
  【注解：@Service】

- controller
  控制层
  【注解：@Controller】

  以上四个注解功能都是一样，都是**将某个类**注册到**Spring容器中**。



## 4、自动装配置

​		**见第七部分**

## 5、作用域

​	注解：@scope

- singleton：默认的，Spring会采用单例模式创建这个对象。关闭工厂 ，**所有的对象都会销毁**。
- prototype：多例模式。关闭工厂 ，所有的**对象不会销毁**。内部的垃圾回收机制会回收

```java
@Controller("user")
@Scope("prototype")	// 指定作用域
public class User {

   @Value("秦疆")
   public String name;

}
```

## 6、小结

**XML与注解比较**

- XML可以**适用任何**场景 ，结构清晰，维护方便
- 注解**不是自己提供**的类使用不了，开发简单方便

**xml与注解整合开发** ：推荐最佳实践

- xml管理Bean
- 注解**完成属性**注入
- 使用过程中， 可以不用扫描，扫描是为了类上的注解

# 九、基于Java类进行配置

​	JavaConfig 原来是 **Spring 的一个子项目**，它通过 Java 类的方式提供 Bean 的定义信息，在 Spring4 的版本， JavaConfig 已正式成为 **Spring4 的核心功能** 。

## 1、创建一个MyConfig.java

​		java/com/kuang/config/MyConfig.java

```java
/**
 * @Configuration注解：
 *   由Spring容器托管，注册到容器中，因为该注解本来就是一个@Component，
 *   @Configuration：代表是一个配置类，类似于：beans.xml来获取对应的配置信息
 *   @Configuration 等价于：@ComponentScan("com.kuang")扫描包下的文件
 */
@Configuration
public class MyConfig {

    /**
     * 注解一个bean，等价与：xml文件中的：<bean>标签
     * 该方法的：方法名，等价与：xml文件中的：<bean>标签的：id属性
     * 该方法的返回值，等价与：xml文件中的：<bean>标签class属性
     */
    @Bean
    public User getUser(){
        // 返回就是要注入到：bean的对象
        return new User("123");
    }
}
```

## 2、创建一个实体类

​		java/com/kuang/pojo/User.java

```java
public class User {
    private String name;
    public User() {}
    public User(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
```

## 3、测试

​		如果使用了配置类方法，只能通过：**AnnotationConfigApplicationContext上下文**来获取容器，通过：配置类的class对象加载

```java
@Test
public void text(){
    /**
     * 如果使用了配置类方法，只能通过：AnnotationConfigApplicationContext上下文来获取容器，通过：配置类的class对象加载
     */
    ApplicationContext context = new AnnotationConfigApplicationContext(MyConfig.class);
    /**
     * 直接获取配置文件中的Bean方法，参数为需要访问方法的：方法名
     */
    User getUser = context.getBean("getUser",User.class);
    System.out.println(getUser.getName());
}
```



# 十、代理模式

<img src="..\images\代理.png" alt="代理" style="zoom:50%;" />

## 1、静态代理

| 静态代理：代理类是**已经写好**的；                         |
| ---------------------------------------------------------- |
| **动态代理**：使用的时候**动态创建**，利用：**反射机制**。 |

**静态代理角色分析**

、抽象角色 : 一般使用**接口**或者**抽象类**来实现

、真实角色 : **被代理**的角色

、代理角色 : 代理真实角色 ; 代理真实角色后 , 一般会做一些附属的操作 .

、客户  :  使用代理角色来进行一些操作 .

<img src="/../images/代理模式案例.png" alt="代理模式案例" style="zoom: 33%;" />

静态代理的好处：

​	可以使得我们的**真实角色更加纯粹**， 不再去关注一些公共的事情 .

​	**公共的业务**由**代理来完成** . 实现了业务的分工 ,

​	**公共业务**发生扩展时变得**更加集中**和方便 .

缺点 :

​	**类多**了 , 多了代理类 , 工作量变大了 ，开发效率降低 .

​	我们想要静态代理的好处，又不想要静态代理的缺点，所以 , 就有了**动态代理** !

### 1.1	AOP理解

<img src="/../images/AOP模式.png" alt="AOP模式" style="zoom:33%;" />

​	不能随意改变程序，故采用代理的方法，来进行**横向开发**。

## 2、动态代理

### 1、接口InvocationHandler

```java
public interface InvocationHandler{...}
```

- InvocationHandler
  	由”代理实例（对象）“的调用：处理程序实现的接口（**对象：调用实现的接口**）。
- 每个一个：代理实例，**都**有**一个关联**的**调用处理程序**。**当**代理实例**调用方法**的时候，方法调用将**被编码**，**并**分派到**其** 调用**处理程序**的 **invoke**方法。

### 2、invoke方法

```java
//自动生成代理类
public class ProxyInvocationHandler implements InvocationHandler {
    ...
```

​	继承了InvocationHandler接口，则需要重写：invoke方法。

```java
Object invoke(Object proxy,方法 method,Object[] args)
throws Throwable
```

| 参数   | 解释                                           |
| ------ | ---------------------------------------------- |
| proxy  | 调用该方法的代理实例（对象）                   |
| method | 所述方法对应与调用代理实例上的接口方法的实例   |
| args   | 包含的方法调用传递代理实例的参数值的对象的阵列 |

​	**该方法作用**：处理代理实例上的：方法调用，并返回的结果。
​	当在**与之关联的代理实例上**调用方法时，将在调用处理程序中调用此方法。

### 3、Proxy类

```java
public class Proxy extends Object implements Serializable{...}
```

​	Proxy类提供了：创建**动态代理类**和**实例**的**静态方法**，**也是**由这些方法创建的所有动态代理类的**超类**。

```java
// 动态创建代理类
public Object getProxey(){
    return Proxy.newProxyInstance(
        	this.getClass().getClassLoader(),
            target.getClass().getInterfaces(),
        	this
    	);
}
```

### 4、newProxyInstance

<img src="/../images/newProxyInstance代理方法.png" alt="image" style="zoom: 33%;" />

|    参数    | 解释                                 |
| :--------: | ------------------------------------ |
|   loader   | 系统的、用户的、应用的               |
| interfaces | 代理的类（传入代理类**实现的接口**） |
|     h      | 实现了 InvocationHandler 接口的类    |

```java
// 官方用法
Foo f = 
  (Foo)Proxy.newProxyInstance(Foo.class.getClassLoader(),
  new Class<?>[] {Foo.class},
  handler); //实现了 InvocationHandler 接口的类
```

​	返回**指定接口**的**代理类的实例**

```java
// 案例
public class ProxyInvocationHandler implements InvocationHandler {
    ...
// 动态创建代理类
public Object getProxey(){
    return Proxy.newProxyInstance(
        	// 获得代理类的类路径，当前方法就是在代理类中实现，所以使用：this
        	this.getClass().getClassLoader(),
            target.getClass().getInterfaces(),//获得target代理实例的所实现的接口
        	this	// 实现了 InvocationHandler 接口的类（当前类）
    	);
}
```

### 5、万能代理类

```java
/**
 * 自动生成代理类
 */
public class ProxyInvocationHandler implements InvocationHandler {

    // 被代理的接口
    private Object target;

    // 传入被代理的接口（真实角色）
    public void setTarget(Object target) {
        this.target = target;
    }

    // 生成代理类
    public Object getProxey(){
        return Proxy.newProxyInstance(this.getClass().getClassLoader(),
                target.getClass().getInterfaces(),this);
    }

    // 处理代理实例，并返回结果(调用处理程序，并返回一个结果)
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        log(method.getName());//    放射：获得方法名
        Object result  = method.invoke(target,args);
        return result;
    }

    // 打印调用了那个方法
    public void log(String methodName){
        System.out.println("执行了"+methodName+"方法");
    }

}
```

### 6、测试

（1）编写实体类

```java
// 业务接口类
public interface UserService {
    public void add();
    public void delete();
    public void update();
    public void query();
}
```

```java
public class UserServiceImpl implements UserService {
    @Override
    public void add() {
        System.out.println("新增一个");
    }
    @Override
    public void delete() {
        System.out.println("删除一个");
    }
    @Override
    public void update() {
        System.out.println("更新一个");
    }
    @Override
    public void query() {
        System.out.println("查询一个");
    }
}
```

（2）测试类

```java
public static void main(String[] args){
    // 创建真实角色
    UserServiceImpl userService = new UserServiceImpl();
    // 代理角色（不存在，需要使用：代理类 来 动态创建）
    ProxyInvocationHandler pih = new ProxyInvocationHandler();
    //设置要代理的对象（真实角色）
    pih.setTarget(userService); 
    // 动态生成代理类
    UserService proxy = (UserService)pih.getProxey();
    // 使用代理类去调用相关方法
    proxy.delete();
}
```

## 3、动态代理优缺点

动态代理的好处：
	（1）静态代理有的它都有，静态代理没有的，它也有！
	（2）可以使得我们的**真实角色更加纯粹** ，不再去关注一些公共的事情 .
	（3）**公共的业务**由代理来完成 . 实现了业务的分工 ，公共业务发生扩展时变得更加集中和方便 .
	（4）一个动态代理 , 一般代理**某一类业务**；
	（5）**一个**动态代理可以**代理多个类**，代理的是**接口**！

## 4、总结

（1）**生成代理类**Proxy.newProxyInvocation(参数1，参数2，参数3)
		参数1：表名你要**用那个类加载器**去加载生成的代理类。（这个是JVM类加载的知识）。
		参数2：说明你要**生成的代理类的接口**。
		参数3：实现了InvocationHandle的类，这个类只有一个方法（invoke）需要你要实现它。

（2）**invoke**(Object proxy, Method method, Object[] args) ｛
		参数1：是生成的代理类，**目前没发现用处，不管它**。
		参数2：是执行的方法（利用反射的原理，可以去看反射，也很简单)
		参数3：是执行某方法需要的参数。

​	参数（1,2）解释执行的方法意思是：代理类不是要代理某个对象么，然后增强里面的方法么，指得就是这个方法，代理类几乎为所有方法都增强，除非你在这里做判断。

​	返回值，是执行这个方法所返回的值。
​		然后你要去**执行方法**：
​			就是用第二参数的invoke(obj,args)；第一个参数是你要增强的对象；第二个是参数。		
​		object：**返回的类型**
​		Object object= method.invoke(obj,args);

# 十一、AOP

## 1、概念	

​	AOP (Aspect Oriented Programming)意为：**面向切面编程**，通过预编译方式和运行期动态代理实现
程序功能的统一维护的一种技术。AOP是**0OP的延续**，是软件开发中的一个热点，也是Spring框架中的
一个重要内容，是函数式编程的一种衍生范型。利用AOP可以**对业务逻辑**的各个部分进行**隔离**，从而使
得**业务逻辑各部分之间**的**耦合度降低**，提高程序的可重用性，同时提高了开发的效率。

<img src="/../images/AOP业务逻辑.png" alt="AOP业务逻辑" style="zoom: 50%;" />

## 2、Aop在Spring中的作用

提供**声明式事务**；允许**用户自定义**切面.
	●	横切关注点：跨越应用程序多个模块的方法或功能。即是，**与我们业务逻辑无关的，**但是我们需要
关注的部分，就是横**切关注点**。如**日志,安全,缓存,事务**等等....
	●	切面(ASPECT)：横切关注点被模块化的特殊对象。即，它是一个类。
	●	通知(Advice)：切面必须要完成的工作。即，它是类中的一个方法。
	●	目标(Target) ：被通知对象。
	●	代理(Proxy)：向目标对象应用通知之后创建的对象。
	●	切入点(PointCut)：切面通知执行的“地点”的定义。
	●	连接点UointPoint) ：与切入点匹配的执行点。

<img src="/../images/AOP切入点.png" alt="AOP切入点" style="zoom:50%;" />

​	切入点：就是**在那个地方执行**。

## 3、Advice

​	SpringAOP中，通过Advice定义横切逻辑，Spring中支持**5种类型的Advice**；

<img src="/../images/SpringAOPAdvice.png" alt="SpringAOPAdvice" style="zoom:50%;" />

## 4、导入依赖文件

```xml
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.9.4</version>
</dependency>
```

## 5、使用Spring的API接口实现

#### （1）创建两个日志文件

​	java/com/kuang/log/Log.java

```java
public class Log implements MethodBeforeAdvice {
    /**
     *  当代理实例调用方法的时候，方法调用将被编码，并分派到其 调用处理程序的方before法。
     *  method：要执行的目录对象的方法
     *  objects：参数
     *  target：目标对象
     *  before：返回之前
     */
    @Override
    public void before(Method method, Object[] objects, Object target) throws Throwable {
        System.out.println(target.getClass().getName()+"的"+method.getName()+"被执行了");
    }
}
```

​	java/com/kuang/log/AfterLog.java

```java
public class AfterLog implements AfterReturningAdvice {

    /**
     *  returnValue：返回值
     *  afterReturning：返回之后
     */
    @Override
    public void afterReturning(Object returnValue, Method method, Object[] objects, Object target) throws Throwable {
        System.out.println("执行了"+method.getName()+"的返回结果为"+returnValue);
    }
}
```

#### （2）创建对应的server类

​	java/com/kuang/Server/UserService.java

```java
public interface UserService {
    public void add();
    public void delete();
    public void update();
    public void query();
}
```

​	java/com/kuang/Server/UserServiceImpl.java

```java
public class UserServiceImpl implements UserService {
    @Override
    public void add() {
        System.out.println("新增一个");
    }
    @Override
    public void delete() {
        System.out.println("删除一个");
    }
    @Override
    public void update() {
        System.out.println("更新一个");
    }
    @Override
    public void query() {
        System.out.println("查询一个");
    }
}
```

#### （3）execution表达式

​	在applicationContext.xml文件使用：execution	表达式

  **execution表达式：**

```
execution(* com.comple.service.impl. * . * (..));
```

​	①	execution()：表示**主体**；
​	②	第一个 * 号：表示**返回类型**，*  号：表示所有类型；

​	③	包名：需要**拦截的包名**，后面**一个句点**表示**当前包**和**当前包的所有子包**；

​	④	第二个 * 号：表示**类名**，* 号表示：所有类；

​	⑤	* (..) ：表示方法名，* 号：表示所有的方法，括号表示**方法的参数**，.. 表示**任意参数**。

```xml
*（开头）com.kuang.Service.UserServiceImpl（执行的类）.（类下的所有方法）(..)（参数不限）
expression="execution(* com.kuang.Service.UserServiceImpl.*(..))"
```

```java
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">

    <!-- 注册bean -->
    <bean id="userService" class="com.kuang.Service.UserServiceImpl"/>
    <bean id="log" class="com.kuang.log.Log"/>
    <bean id="afterlog" class="com.kuang.log.AfterLog"/>

    <!-- 方式一：使用原生：Spring API接口 -->
    <!-- 配置aop，需要导入aop约束 -->
    <aop:config>
        <!-- 切入点：expression；
        	表达式：execution （要执行的位置！
            execution格式：
*（开头）com.kuang.Service.UserServiceImpl（执行的类）.*（类下的所有方法）(..)（参数不限）
        -->
       <aop:pointcut id="pointcut" expression="execution(* com.kuang.Service.UserServiceImpl.*(..))"/>

        <!--  执行环绕增加！-->
        <!-- 将创建好的：log对象，切入到：pointcut -->
        <aop:advisor advice-ref="log" pointcut-ref="pointcut"/>
        <aop:advisor advice-ref="afterlog" pointcut-ref="pointcut"/>
    </aop:config>
</beans>
```

#### （4）测试

​	动态代理代理的是：**接口**

```java
@Test
public void test(){
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    // 动态代理代理的是：接口
    // 这里的：userService为UserServiceImpl对象，使用：向上造型
    UserService userService =(UserService) context.getBean("userService");

    userService.add();
}
```

**结果：**

com.kuang.Service.UserServiceImpl的add被执行了
新增一个
执行了add的返回结果为null



## 6、自定义类实现AOP

### （1）自定义类

java/com/kuang/diy/DiyPointCut.java

```java
public class DiyPointCut {

    public void before(){
        System.out.println("-----方法执行前-------");
    }

    public void after(){
        System.out.println("-----方法执行后-------");
    }
}
```

### （2）编写配置文件

applicationContext.xml文件

```xml
<!-- 方法二：自定义类 -->
<bean id="diy" class="com.kuang.diy.DiyPointCut"/>

<aop:config>
    <!-- 自定义切面，ref要引用的类 -->
    <aop:aspect ref="diy">
        <!-- 
			1. 切入点，要执行的位置；
			2. 例如：userService.add(); add为UserServiceImpl类中的方法
		-->
        <aop:pointcut id="point" expression="execution(* com.kuang.Service.UserServiceImpl.*(..))"/>
        <!-- 2.通知：执行对应的方法（DiyPointCut中的方法）  -->
        <aop:before method="before" pointcut-ref="point"/>
        <aop:after method="after" pointcut-ref="point"/>
    </aop:aspect>
</aop:config>
```

### （3）测试

Service/**UserService和UserServiceImpl.java**文件跟：使用SpringAPI接口实现步骤一样。

```java
@Test
public void test(){
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    // 动态代理代理的是：接口
    UserService userService =(UserService) context.getBean("userService");

    userService.add();
}
```

-----方法执行前-------
新增一个
-----方法执行后-------

## 7、使用注解实现AOP

### （1）编写一个注解类

java/com/kuang/diy/AnnotationPointCut.java

```java
**
 *  使用注解的方法实现：AOP
 *  @Aspect（标注这个类是一个切面）
 */
@Aspect
public class AnnotationPointCut {
    @Before("execution(* com.kuang.Service.UserServiceImpl.*(..))")
    public void before(){
        System.out.println("-----方法执行前-------");
    }
    @After("execution(* com.kuang.Service.UserServiceImpl.*(..))")
    public void after(){
        System.out.println("-----方法执行后-------");
    }
    /**
     *  在环绕增强中，我们可以给定一个参数，代表我们要获取处理切入的点；
     */
    @Around("execution(* com.kuang.Service.UserServiceImpl.*(..))")
    public void around(ProceedingJoinPoint jp) throws Throwable {
        System.out.println("环绕前");
        Signature signature = jp.getSignature();// 获得签名（执行的方法）
        System.out.println("signature:"+signature);
        // 标签：signature:void com.kuang.Service.UserService.add()

        Object proceed = jp.proceed();// 执行方法
        System.out.println("环绕后");
    }
}
```

### （2）编写配置

applicationContext.xml

```xml
<!-- 注册bean -->
<bean id="userService" class="com.kuang.Service.UserServiceImpl"/>
<bean id="log" class="com.kuang.log.Log"/>
<bean id="afterlog" class="com.kuang.log.AfterLog"/>

<!-- 方法三： -->
<bean id="annotationPointCut" class="com.kuang.diy.AnnotationPointCut"/>

<!-- 开启注解支持  JKD（默认） proxy-target-class="false"：默认-->
<aop:aspectj-autoproxy />
```

### （3）测试

```java
@Test
public void test(){
    ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
    // 动态代理代理的是：接口
    UserService userService =(UserService) context.getBean("userService");
    userService.add();
}
```

环绕前
signature:void com.kuang.Service.UserService.add()
-----方法执行前-------
新增一个
环绕后
-----方法执行后-------

# 十二、整合Mybatis

<img src="/../images/spring-batis1.png" alt="spring-batis1" style="zoom:33%;" />

## 1、方法一

### （1）导入相关依赖

pom.xml文件中：

```xml
<!-- 导入相关依赖-->
<dependencies>
    <!-- 导入mysql依赖-->
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
        <version>5.1.47</version>
    </dependency>
    <!-- 导入mybatis-spring模块 依赖-->
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis-spring</artifactId>
        <version>2.0.2</version>
    </dependency>
    <!-- 导入mybatis依赖-->
    <dependency>
        <groupId>org.mybatis</groupId>
        <artifactId>mybatis</artifactId>
        <version>3.5.2</version>
    </dependency>
    <!-- 导入junit依赖-->
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
    </dependency>
    <!-- 导入mvc依赖-->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>5.1.9.RELEASE</version>
    </dependency>
    <!-- spring操作数据库，需要spring-jdbc -->
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>5.1.9.RELEASE</version>
    </dependency>
</dependencies>
<!-- 手动配置自动过滤 -->
<build>
    <resources>
        <resource>
            <directory>src/main/resources</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>true</filtering>
        </resource>
        <!-- 加载 java文件下的配置文件 -->
        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>true</filtering>
        </resource>
    </resources>
</build>
```

### （2）创建实体类

java/com/kuang/pojo/User.java

```java
public class User {
    private int id;
    private String name;
    private String password;
    ...get/set等方法
}
```

### （3）创建接口以及对应的mapper.xml文件

java/com/kuang/mapper/UserMapper.java

**注意**：若接口与对应的xml文件放在java目录下，需要**扩展搜索路径**

```java
public interface UserMapper {
    // 抽象方法，用于存储获取的数据库值
    List<User> getUserList();
}
```

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<!--
namespace : 绑定一个对应的mapper/Mapper接口
    这个功能类似：实现了Mapper接口（如：public class UserDao implements  UserMapper）
-->
<mapper namespace="com.kuang.mapper.UserMapper">
    <resultMap id="userMap" type="User">
        <result column="pwd" property="password"/>
    </resultMap>
    <!-- 查询指定ID的用户信息，因参数只有一个，故直接在Sql中写对应的参数名，来获取值 -->
    <select id="getUserList" resultMap="userMap" >
        select * from mybatis.user_table;
    </select>
</mapper>
```

### （4）创建UserMapperimpl.java文件

​	java/com/kuang/mapper/**UserMapperimpl.java**

​	**原来由**UserMapper.xml文件**作为**接口的**实现类**，**现在改**为：UserMapperimpl.java作为**实现类**

```java
/**
 * 用来存放接口的实现类，impl的全称为implement
 */
public class UserMapperImpl implements UserMapper{
    /**
     *  在原来：所有的操作，都使用sqlSession来执行；
     *  现在：使用SqlSessionTemplate
     */
    private SqlSessionTemplate sqlSession;

    // 获取sqlSession（这里的setSqlSession方法为：构造器
    public void setSqlSession(SqlSessionTemplate sqlSession) {
        this.sqlSession = sqlSession;
    }
    @Override
    public List<User> getUserList() {
        // 获得接口类，然后用接口类实例去访问：实现类中的方法
        UserMapper userMapper = sqlSession.getMapper(UserMapper.class);
        return userMapper.getUserList();
    }
}
```

### （5）编写spring-dao.xml文件

​	**resources/spring-dao.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">

    <!--
    第一步：
        DataSource ：使用Spring的数据源来替换：Mybatis配置
        这里使用Spring提供的JDBC：org.springframework.jdbc.datasource.DriverManagerDataSource
    -->
    <bean id="datasource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/mybatis?useSSL=true&amp;userUnicode=true&amp;characterEncoding=utf-8"/>
        <property name="username" value="root"/>
        <property name="password" value="123456"/>
    </bean>

    <!--
     第二步：
        SqlSessionFactoryBean 来创建 SqlSessionFactory-->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <!-- ref：引用Spring-JDBC对象（datasource），注入数据源 -->
        <property name="dataSource" ref="datasource"/>
        <!-- 绑定Mbatis配置文件 -->
        <property name="configLocation" value="classpath:mybatis-config.xml"/>
        <!-- 类似：mapper注册 -->
        <property name="mapperLocations" value="classpath:com/kuang/mapper/*.xml"/>
    </bean>
    <!--
    第三步：
        SqlSessionTemplate ：获取:sqlSession（使用构造器来传递参数） -->
    <bean id="sqlSession" class="org.mybatis.spring.SqlSessionTemplate">
        <!-- 因为无：set方法，只能使用构造器注入：sqlSessionFactory -->
        <constructor-arg index="0" ref="sqlSessionFactory"/>
    </bean>

    <!--
    第四步：
        创建实现类对象（UserMapperImpl），并使用ref引用：SqlSessionTemplate创建的SqlSession对象（第三步）
    -->
    <bean id="userMapper" class="com.kuang.mapper.UserMapperImpl">
        <property name="sqlSession" ref="sqlSession"/>
    </bean>
</beans>
```

### （6）编写mybaits-config.xml文件

​	**resources/mybaits-config.xml**

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
     <!--扫描指定包下的文件，这样在Mapper.xml文件中可以直接写：实体名-->
    <typeAliases>
        <package name="com.kuang.pojo"></package>
    </typeAliases>

</configuration>
```

### （7）测试

```java
@Test
public void test(){
    ApplicationContext context = new ClassPathXmlApplicationContext("spring-dao.xml");
    // 获得UserMapperImpl的接口类，然后用接口类去调用实现类的方法；
    // 在实现类（UserMapperImpl）中，可以使用sqlSession来调用访问数据库的方法
    UserMapper userMapper = context.getBean("userMapper",UserMapper.class);
    for(User user:userMapper.getUserList()){
        System.out.println(user);
    }
}
```

### （8）使用applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">
            
   <!-- 导入spring-dao配置  -->
    <import resource="spring-dao.xml"/>

    <!-- 导入spring-mvc 配置-->
    <!--<import resource="spring-mvc.xml"/>-->

    <!--
    第四步：
        创建实现类对象（UserMapperImpl），并使用ref引用：SqlSessionTemplate创建的SqlSession对象（第三步）
    -->
    <bean id="userMapper" class="com.kuang.mapper.UserMapperImpl">
        <property name="sqlSession" ref="sqlSession"/>
    </bean>

</beans>
```

​	创建**实现类对象**，也可以在**此**配置文件中写。

## 2、方法二

​		新增方法，可以**直接继承**：SqlSessionDaoSupport类，这个类是一个：**抽象支持类**，给我们**提供了**：SqlSession，调用**getSqlSession()**方法，我们可以得到：SqlSessionTemplate，之后执行SQL方法：

```java
public class UserMapperImpl extends SqlSessionDaoSupport implements UserMapper{
    @Override
    public List<User> getUserList() {
        // 获得接口类，然后用接口类实例去访问：实现类中的方法
        //UserMapper usermapper = getSqlSession().getMapper(UserMapper.class);
        // usermapper.getuserList();
        
        return getSqlSession().getMapper(UserMapper.class).getUserList();
    }
    
}
```

​	配置类中，只需要获得：**sqlSessionFactory**即可，然后SqlSessionDaoSupport会帮我们获得：SqlSession对象。

```xml
<!--
第一步：
    DataSource ：使用Spring的数据源来替换：Mybatis配置
    这里使用Spring提供的JDBC：org.springframework.jdbc.datasource.DriverManagerDataSource
-->
<bean id="datasource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
    <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
    <property name="url" value="jdbc:mysql://localhost:3306/mybatis?useSSL=true&amp;userUnicode=true&amp;characterEncoding=utf-8"/>
    <property name="username" value="root"/>
    <property name="password" value="123456"/>
</bean>

<!--
 第二步：
    SqlSessionFactoryBean 来创建 SqlSessionFactory-->
<bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
    <!-- ref：引用Spring-JDBC对象（datasource），注入数据源 -->
    <property name="dataSource" ref="datasource"/>
    <!-- 绑定Mbatis配置文件 -->
    <property name="configLocation" value="classpath:mybatis-config.xml"/>
    <!-- 类似：mapper注册 -->
    <property name="mapperLocations" value="classpath:com/kuang/mapper/*.xml"/>
</bean>
<!--
```
# 十三、事务

## 1、事务4原则

（1）原子性
			事务是原子性操作，由一系列动作组成，事物的原子性确保动作要么全部完成，要不完全不起作用；

（2）一致性
			一旦所有事务动作完成，事务就要被提交。数据和资源处于一种满足业务规则的一致性状态；

（3）隔离性
			可能多个事务会同时处理相同的数据，因此每个事务都应该与其他事务隔离开，防止数据损坏；

（4）持久性
			事务一旦完成， 无论系统发生什么错误，结果都不会受到影响。

## 2、在spring-dao.xml

```xml
<!--第一步：连接数据库（对象：datasource）-->   
<bean id="datasource" class="org.springframework.jdbc.datasource.DriverManagerDataSource">
        <property name="driverClassName" value="com.mysql.jdbc.Driver"/>
        <property name="url" value="jdbc:mysql://localhost:3306/mybatis?useSSL=true&amp;userUnicode=true&amp;characterEncoding=utf-8"/>
        <property name="username" value="root"/>
        <property name="password" value="123456"/>
    </bean>

<!-- 事务管理
  第二步：配置声明式事务（引入DataSourceTransactionManager 对象
-->
<bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
   <!-- 引入：获取的数据库对象 -->
    <constructor-arg ref="datasource" />
</bean>
<!--
 第三步：
    结合AOP实现事务的切入
    配置事务通知
-->
<tx:advice id="txAdvice" transaction-manager="transactionManager">
    <!-- 给对应的方法配置事务 -->
    <!-- 配置事务的传播特性： propagation：REQUIRED -->
    <tx:attributes>
        <!-- name="add"表示：add方法会配置事务，报错则会插入失败  -->
        <tx:method name="add" propagation="REQUIRED"/>
        <!--* 表示：所有的方法都配置事务  -->
        <tx:method name="*" propagation="REQUIRED"/>
    </tx:attributes>
</tx:advice>

<!--
 第四步：配置事务切入
 -->
<aop:config>
    <!-- UserServiceImpl.java中的方法都会被编辑：事务  或 com.kuang.Service.*（包中所有的方法）-->
    <aop:pointcut id="txPointCut" expression="execution(* com.kuang.Service.UserServiceImpl.*(..))"/>
    <aop:advisor advice-ref="txAdvice" pointcut-ref="txPointCut"/>
</aop:config>
```

<img src="/../images/Spring-batis-7种事务属性.png" alt="Spring-batis-7种事务属性" style="zoom: 33%;" />

​		**propagation事务属性**。



# 十四、Spring面试题

## 1、Spring Bean生命周期

![](/../images/spring 加载过程.png)

---

简单点说：

Spring Bean的生命周期**只有四个阶段**：**实例化** Instantiation --> **属性赋值** Populate --> **初始化** Initialization --> **销毁** Destruction

----

具体点说：

（1）实例化Bean

（2）设置对象属性（依赖注入）

（3）处理Aware接口

（4）BeanPostProcessor前置处理（对Bean进行一些**自定义**的前置处理）

（5）InitializingBean（为bean提供了初始化方法，只包括`afterPropertiesSet`方法：**初始化bean**）

（6）init-method（Bean在Spring配置文件中配置了 init-method 属性，则会**自动调用其配置的初始化方法**）

（7）BeanPostProcessor后置处理（在**Bean初始化结束时**调用的，所以可以被应用于**内存或缓存技术**）

（8）DisposableBean（当Bean不再需要时，会经过**清理阶段**）

（9）destroy-method（如果这个Bean的Spring配置中配置了destroy-method属性，会**自动调用其配置的销毁方法**）

bean详细加载流程的感兴趣的读者，可以阅读这篇文章：https://blog.csdn.net/a745233700/article/details/113840727

## 2、Spring 的优点？

（1）spring属于低侵入式设计，代码的污染极低；

（2）spring的DI机制将**对象之间的依赖关系交由框架处理**，**减低组件的耦合性**；

（3）Spring提供了AOP技术，支持将一些通用任务，如安全、事务、日志、权限等进行集中式管理，从而提供更好的复用。

（4）spring**对于主流的应用框架提供了集成支持**。



# 十五、其他

异常：若将xml文件放到java 目录下，超过了spring默认路径，需要重新设置：

创建在类路径资源[spring]中定义的名为“sqlSessionFactory”的bean时出错-dao.xml文件]：调用init方法失败；嵌套异常为org.springframework.core.NestedIOException:未能分析映射资源：“类路径资源[com/kuang/mapper”/用户映射器.xml]'；嵌套异常为java.io.FileNotFoundException异常：类路径资源[com/kuang/mapper]/用户映射器.xml]无法打开，因为它不存在

```xml
  <!-- 手动配置自动过滤 -->
    <build>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>true</filtering>
            </resource>
            <!-- 加载 java文件下的配置文件 -->
            <resource>
                <directory>src/main/java</directory>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
                <filtering>true</filtering>
            </resource>
        </resources>
    </build>
```

