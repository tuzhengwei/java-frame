package com.example.manager;

import com.example.manager.dao.DepartmentDao;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ManagerApplicationTests {

    @Test
    void contextLoads() {
        DepartmentDao departmentDao = new DepartmentDao();
        System.out.println(departmentDao.getDepartmentById(101));
    }

}
