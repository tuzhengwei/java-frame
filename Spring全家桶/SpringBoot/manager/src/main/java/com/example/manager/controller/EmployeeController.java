package com.example.manager.controller;

import com.example.manager.dao.DepartmentDao;
import com.example.manager.dao.EmployeeDao;
import com.example.manager.pojo.Department;
import com.example.manager.pojo.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.server.PathParam;
import java.util.Collection;

@Controller
public class EmployeeController {

    @Autowired
    EmployeeDao employeeDao;

    @Autowired
    DepartmentDao departmentDao;
    /**
     * 获取所有的员工信息
     */
    @RequestMapping("/emps")
    public String list(Model model){
        Collection<Employee> employees = employeeDao.getAll();
        model.addAttribute("emps",employees);
        return "emp/list";
    }

    /**
     * 跳转去：添加员工信息页面
     * （将所有员工信息传递过去）
     */
    @GetMapping("/toAddpage")
    public String toAddpage(Model model){
        // 查询所有的部门信息
        Collection<Department> departments = departmentDao.getDepartments();
        model.addAttribute("departments",departments);
        return "emp/add";
    }

    /**
     * 增加员工
     * （接收从前端传来的员工信息）
     */
    @PostMapping("/addemp")
    public String addEmp(Employee employee){

        System.out.println("员工的信息" + employee);

        employeeDao.add_employee(employee);
        // 若添加成功，则进行重定向控制器（emps）
        return "redirect:/emps";
    }

    /**
     *  跳转去：修改员工页面
     *  （根据传递ID，将选中的员工信息传递到修改页面）
     */
    @GetMapping("/toUpdatepage/{id}")
    public String toUpdateEmp(@PathVariable("id") Integer id,Model model){
        Employee employee = employeeDao.getEmployeeById(id);
        model.addAttribute("emp",employee);

        // 获得所有的部门
        Collection<Department> departments = departmentDao.getDepartments();
        model.addAttribute("departments",departments);
        return "emp/update";
    }
    /**
     * 修改员工信息
     */
    @PostMapping("/updateemp")
    public String updateEmp(Employee employee){
        // 根据map的性质，添加，同一个ID，则直接覆盖原有数据
        employeeDao.add_employee(employee);
        return "redirect:/emps";
    }


    /**
     * 删除员工
     */
    @GetMapping("/deledeEmp/{id}")
    public String deleteEmp(@PathVariable("id")Integer id){
        employeeDao.deleteEmployeeById(id);
        return "redirect:/emps";
    }

}
