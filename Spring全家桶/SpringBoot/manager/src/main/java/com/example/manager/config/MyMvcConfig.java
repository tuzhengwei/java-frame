package com.example.manager.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyMvcConfig implements WebMvcConfigurer {
    /**
     * 接管 视图解析器
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("index");
        registry.addViewController("/index.html").setViewName("index");
        // 地址为：http://localhost:8080/main.html
        registry.addViewController("/main.html").setViewName("dashboard");
    }
    /**
     * 将自定义的国家化组件交给spring
     */
    @Bean
    public LocaleResolver localeResolver(){
        return new MyLocaleResolver();
    }

    /**
     * 添加自定义拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(
                new LoginHandlerInterceptor()).
                addPathPatterns("/**").  // 拦截所有请求
                // 排除不需要拦截的请求
                excludePathPatterns(
                        "/index.html",
                        "/",
                        "/user/login",
                        "/css/*",
                        "/js/**",
                        "/img/**");
    }
}
