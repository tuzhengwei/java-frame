package com.example.manager.config;


import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * 创建一个区域解析器，可以在连接上携带区域信息
 */
public class MyLocaleResolver implements LocaleResolver {
    /**
     * 重写（通过点击按钮，来实现国际化）
     * resolveLocale()：解析请求
     */
    @Override
    public Locale resolveLocale(HttpServletRequest httpServletRequest) {
        // 若超链接信息带有：local信息
        String local = httpServletRequest.getParameter("l");

        // 若浏览器没带：local参数，则使用系统默认的配置
        Locale locale = Locale.getDefault();
        // 检查带来的参数是否为NULL
        if(!StringUtils.isEmpty(local)){
            //英文带来的参数是：zh_CN，使用split将下划线去掉
            String[] split = local.split("_");
            // 拼接，创建一个Locale区域对象
            locale = new Locale(split[0],split[1]);
        }
        return locale;
    }

    @Override
    public void setLocale(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Locale locale) {
    }
}
