package com.example.manager.dao;

import com.example.manager.pojo.Department;
import com.example.manager.pojo.Employee;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Repository
public class EmployeeDao {

    /**
     *  模拟 数据库中的数据
     */
    private static Map<Integer, Employee> employees = null;
    private DepartmentDao departmentDao = new DepartmentDao();
    static{
        employees = new HashMap<Integer, Employee>();

        employees.put(101,new Employee(101,"AA","123@qq.com",0,new Department(101,"教学部"),new Date()));
        employees.put(102,new Employee(102,"BB","123@qq.com",1,new Department(102,"市场部"),new Date()));
        employees.put(103,new Employee(103,"CC","123@qq.com",1,new Department(103,"教研部"),new Date()));
        employees.put(104,new Employee(104,"DD","123@qq.com",0,new Department(104,"运营部"),new Date()));
        employees.put(105,new Employee(105,"EE","123@qq.com",1,new Department(105,"后勤部"),new Date()));
    }
    /**
     * 增加员工
     */
    private static Integer initId = 1006;
    public void add_employee(Employee employee){
        if(employee.getId() == null){
            employee.setId(initId++);
        }
        // 根据id获得：部门名
        employee.setDepartment(departmentDao.getDepartmentById((employee.getDepartment().getId())));

        employees.put(employee.getId(),employee);
    }
    /**
     * 查询全部员工信息
     */
    public Collection<Employee> getAll(){
        return employees.values();
    }
    /**
     * 通过id查询员工
     */
    public Employee getEmployeeById(Integer id){
        return employees.get(id);
    }
    /**
     * 通过id删除员工
     */
    public void deleteEmployeeById(Integer id){
        employees.remove(id);
    }
}
