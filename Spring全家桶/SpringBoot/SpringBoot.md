---
typora-root-url: images
---

# SpringBoot

​		Spring Boot是Spring开源组织下的子项目，是Spring组件一站式解决方案，主要是**简化了**使用Spring的难度，简省了繁重的配置，提供了各种启动器，开发者能**快速上手**。

# 原理初探

​	SpringBoot 所在自动配置都是：启动的时候 扫描并加载：`spring.factories`所有的自动配置类，但是不一定全部生效，要判断条件是否成立，只要导入了对应的：`start`（启动器），才可以自动装配生效。

---

> **启动步骤：**

​	1、SpringBoot 在启动的时候，从类路径下：`/META-INF/spring.factories`获取指定的值；

​	2、将这些自动配置的类导入容器，自动配置就会生效，进行自动配置；

​	3、自动配置在：spring-boot-autoconfigure-2.2.0.release.jar Z这个包下；

​	4、将所有需要导入的组件，以类名的方式返回，这些组件就会被添加到容器中；

---

# 属性赋值

|                | @ConfigurationProperties |     @value     |
| :------------: | :----------------------: | :------------: |
|      功能      | 批量注入配置文件中的属性 | 一个一个的指定 |
|    松散绑定    |           支持           |     不支持     |
|      SqEL      |          不支持          |      支持      |
| JSR303数据校验 |           支持           |     不支持     |
|  复杂类型封装  |           支持           |     不支持     |
|                |                          |                |

## 1 Value

```java
@Value("${person.name}")
private String name;
```

## 2 ConfigurationProperties

![](/../images/属性赋值2.png)

## 3 JSR303数据校验

​		https://www.jianshu.com/p/554533f88370

​		使用：`@validated`来校验数据，若**数据异常则会统一抛出异常**，方便异常中心统一处理。

```java
@validated
public class Person{
	@Email
	private String email;  // 注入必须是邮箱格式，否则会失败
    // @Email(message = "邮箱格式错误")
	// private String email;  // 注入必须是邮箱格式，否则会失败
}
```

|        Constraint         | 作用                                                         |
| :-----------------------: | ------------------------------------------------------------ |
|           @Null           | 被注解的元素必须为：null                                     |
|         @NotNull          | 被注解的元素必须为：非空                                     |
|        @AssertTrue        | 被注解的元素必须为：true                                     |
|       @AssertFalse        | 被注解的元素必须为：false                                    |
|        @Min(value)        | 被注解的元素必须为：是一个数字，其值**大于等于**指定的最小值 |
|        @Max(value)        | 被注解的元素必须为：是一个数字，其值**小于等于**指定的最大值 |
|    @DecimalMin(Value)     | 被注解的元素必须为：是一个数字，其值**大于等于**指定的最小值 |
|    @DecimalMax(Value)     | 被注解的元素必须为：是一个数字，其值**小于等于**指定的最大值 |
|           @Size           | 被注解的元素大小必须：在指定的范围内                         |
| @Digits(integer,fraction) | 被注解的元素是一个数字，其值必须在可接受的范围               |
|           @Past           | 被注解的元素必须为：是一个过去的日期                         |
|          @Future          | 被注解的元素必须为：是一个将来的日期                         |
|      @Pattern(value)      | 被注解的元素必须为：符合指定的正则表达式                     |
|                           |                                                              |

| Constraint | 作用                                   |
| :--------: | -------------------------------------- |
|   @Email   | 被注解的元素是：电子邮箱地址           |
|  @Length   | 被注解的字符串的大小必须在指定的范围内 |
| @NotEmpty  | 被注解的字符串必须：非空               |
|   @Range   | 被注解的元素必须：在适合的范围内       |



## 4 结论

​	-	配置`yml` 和 配置 `properties` 都可以获取值，推荐使用：`yml` ；

​	-	若只需要获取配置文件中的**某个值**，使用：`@value`；

​	-	专门编写一个`JavaBean`来**和配置文件**进行映射，使用：`configurationProperties`。

# 日志

## 1 市面上的日志框架

> JUL、JCL、Jboss-logging、logback、log4j、log4j2、slf4j...

---

日志门面（**日志抽象层**）
（1）JCL（jakarta Commons Logging）（已淘汰）
（2）SLF4j（Simple Logging Facade for java）
（3）jboss-logging

----

**日志实现层**
（1）Log4 j （抽象层：SLF4j）
（2）Logback（抽象层：SLF4j）
（3）JUL（java.util.logging）自带的
（4）Log4j2（打着log4j的名气，很强大）

---

## 2 SpringBoot所选日志

抽象层：SLF4j
实现层：Logback（**Log4j 的升级版**）

> Spring默认使用jul日志框架

## 3 SLF4j

> SLF4j 官方文档：http://www.slf4j.org/manual.html

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloWorld {
  public static void main(String[] args) {
    /*
     使用：LoggerFactory，来记录HelloWorld.class的执行信息
*/
    Logger logger = LoggerFactory.getLogger(HelloWorld.class);
    logger.info("Hello World");   /* 打印日志信息 */
  }
}
```

---

> 框架之间的转换关系
>
> 解析：
> -	只有使用抽象层slf4j，没有任何实现，所以输出：NULL
> -	面向抽象层：slf4j，也调用了logback来实现具体的日志；
> -	若slf4j 需要绑定 log4j：则需要一个适配层，上实现：slf4j，下调用log4j的API；
> -	类似3
> .....
> **浅蓝色：实现抽象层**
> **深蓝色：具体的实现**
> **深绿色：适配器（适配其他层）**

<img src="/../images/日志1.png" style="zoom: 50%;" />

## 4 其他日志框架统一转换为SLF4J

![](/../images/日志2.png)

## 5 日志依赖关系

```xml
# SpringBoot的日志框架
<dependency>
	<groupId>org.springframework.boot</groupId>
    <artifactid>spring-boot-starter-logging</artifactid>
</dependency>
```

SpringBoot**底层使用**：`slf4j+logback`  进行日志记录；

----

> **查看当前项目的所有依赖关系**：
>
> 右击项目某个文件里的空白处——Diagrams——Show Dependencies...

![](/../images/日志3.png)

---

> 引入其他日志框架
>
> （注意：一定要把这个框架的**默认日志依赖移除**）

---

eg：
		Spring框架用的是：commons-logging日志框架

```xml
<dependency>
	<groupid>org.springframework</groupid>
    <artifactId>spring-core</artifactId>
    <exclusions>
    	<exclusion>
        	<groupId>commons-logging</groupId>
            <artifactId>commons-logging</artifactId>
        </exclusion>
    </exclusions>
</dependency>
```

​    SpringBoot能自动配置所有的日志，而且底层使用的：slf4+logback的方式记录日志，引入其他框架的时候，只需要**把这个框架依赖的日志框架 “ 排除掉 ”**；

# Web开发

## 1 导入静态资源

> SpringBoot对**静态资源的映射规则**
>
> ​	所有的**/webjar/**** ，都去classpath:/META-INF/resources/webjars/找资源
>

---

> webjar 介绍
>
> 官方文档：https://www.webjars.org/

以**jar包的形式**来**使用前端的各种框架、组件**；
（资源（JavaScript，Css等）打成jar包文件，以对资源进行统一依赖管理。WebJars的jar包部署在Maven中央仓库上。）

---

> 为什么使用webjar

​		开发Java web项目的时候会使用像Maven，Gradle等构建工具以实现对jar包版本依赖管理，以及项目的自动化管理，但是对于JavaScript，Css等前端资源包，我们只能采用**拷贝到webapp下的方式**，这样做就无法对这些资源进行依赖管理。

​		那么WebJars就提供给我们这些前端资源的jar包形势，我们就可以进行依赖管理

----

> eg：引入jQuery框架

第一步：在webjars官方找到对应的框架依赖

第二步：在spring boot中自动导入依赖，并启动主程序

```xml
 <!-- 
        引入前端框架：Jquery 
        在访问的时候，只需要写webjars下面资源的名称即可
         eg：
          <script src="/webjars/jquery/3.1.1/jquery.min.js"></script>  
          <script src="/webjars/bootstrap/3.3.7-1/js/bootstrap.min.js"></script>  
-->
<dependency>
    <groupId>org.webjars</groupId>
    <artifactId>jquery</artifactId>
    <version>3.6.0</version>
</dependency>
```

> 可以打开浏览器输入：http://localhost:8080/webjars/jquery/3.2.1/jquery.js

## 2 Thymeleaf

### 2.1 引入

> 在pom.xml 的`<dependencies>`中添加配置

```xml
<dependencies>
    <!-- 引入thymeleaf 版本 ，2.1.6 （若需要切换版本，则需要在<properties>中编写对应的配置 -->
    <dependency>
        <groupId>org.thymeleaf</groupId>
        <artifactId>thymeleaf-spring5</artifactId>
    </dependency>
    <dependency>
        <groupId>org.thymeleaf.extras</groupId>
        <artifactId>thymeleaf-extras-java8time</artifactId>
    </dependency>
</dependencies>
```

> **切换版本**
>
> 若需要更改thymeleaf版本，则需要在pom.xml
> 的：`<properties>`标签中添加：

```xml
 <properties>
    .......
    <!-- 用于切换 thymeleaf 版本 -->
    <thymeleaf.version>3.0.9.RELEASE</thymeleaf.version>
    <thymeleaf-layout-dialect.version>2.2.2</thymeleaf-layout-dialect.version>
</properties>
```

---

**若报异常：**
`javax.servlet.ServletException: Circular view path [success]: would dispatch back to the current handler URL [/success] again. Check your V`

若运行不成功，则改为：

还有第二种方法要比这个适配度高：

```xml
<thymeleaf-spring5.version>3.0.9.RELEASE</thymeleaf-spring5.version>
<thymeleaf-layout-dialect.version>2.2.2</thymeleaf-layout-dialect.version>
```

把pom文件中的注入thymeleaf改为**---spring5.version**这种高版本方式即可。

---

### 2.2 介绍

> *Search* *Everywhere* 1,在窗口点击`Ctrl+shift+A`复合键——ThymeleafProperties.java

```java
private static final Charset DEFAULT_ENCODING;
public static final String DEFAULT_PREFIX = "classpath:/templates/";
public static final String DEFAULT_SUFFIX = ".html";
private boolean checkTemplate = true;
private boolean checkTemplateLocation = true;
private String prefix = "classpath:/templates/";
private String suffix = ".html";
private String mode = "HTML";
private Charset encoding;
...
```

​      只要我们把HTML页面放在`classpath:/templates/`，thymeleaf就能**自动渲染**； 

![](/../images/leaf.png)

### 2.3 语法

> 官方文档：https://www.thymeleaf.org/documentation.html

```html
<!-- 导入thymeleaf的名称空间 -->
<html lang="en" xmlns:th="http://www.thymeleaf.org">
```

```html
<div th:text="${msg}"></div>
<div th:utext="${msg}"></div>
```

```html
<h3 th:each="user:${users}" >[[${user}]]</h3>
<!--<h3 th:each="user:${users}" th:text="${user}"></h3>-->
```

...

## 3 装配扩展MVC

...

## 4 增删改查

....

## 5 拦截器

...

## 6 国际化

