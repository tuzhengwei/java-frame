---
typora-root-url: images
---

# Spring MVC

shift+shift+双击（可进入搜索框）

http://localhost:8080/Springmvchelloword/hello

访问网址：http://localhost:8080/项目名/控制器

# 一、概念

## 1、MVC

- MVC是模型(Model)、视图(View)、控制器(Controller)的简写，是一种软件设计规范。
- 是将业务逻辑、数据、显示分离的方法来组织代码。
- MVC主要作用是**降低了视图与业务逻辑间的双向偶合**。
- MVC不是一种设计模式，**MVC是一种架构模式**。当然不同的MVC存在差异。

**Model（模型）**： 数据模型，提供要展示的数据，因此包含**数据和行为**，可以认为是领域模型或JavaBean组件（包含数据和行为），不过现在一般都分离开来：Value Object（数据Dao） 和 服务层（行为Service）。也就是模型提供了模型数据查询和模型数据的状态更新等功能，包括数据和业务。

**View（视图）**：负责进行**模型的展示**，一般就是我们见到的**用户界面**，客户想看到的东西。

**Controller（servlet ）（控制器）**：**接收用户请求**，委托给模型进行处理（状态改变），处理完毕后**把返回的模型数据**返回**给视图**，由视图负责展示。也就是说控制器做了个调度员的工作。

**最典型的MVC就是JSP + servlet + javabean的模式。**

<img src="最典型的MVC就是JSP + servlet + javabean的模式.png" style="zoom:33%;" />

## 2、回顾Servlet

​	JSP ：就是一个servlet，本身就可以做一些控制。

​	创建一个：父工程，再创建一个子工程（**springmvc-serlvet**）。

### （1）导入依赖（pom.xml）

```xml
<dependencies>
   <dependency>
       <groupId>junit</groupId>
       <artifactId>junit</artifactId>
       <version>4.12</version>
   </dependency>
   <dependency>
       <groupId>org.springframework</groupId>
       <artifactId>spring-webmvc</artifactId>
       <version>5.1.9.RELEASE</version>
   </dependency>
   <dependency>
       <groupId>javax.servlet</groupId>
       <artifactId>servlet-api</artifactId>
       <version>2.5</version>
   </dependency>
   <dependency>
       <groupId>javax.servlet.jsp</groupId>
       <artifactId>jsp-api</artifactId>
       <version>2.2</version>
   </dependency>
   <dependency>
       <groupId>javax.servlet</groupId>
       <artifactId>jstl</artifactId>
       <version>1.2</version>
   </dependency>
</dependencies>
```

### （2） 添加Web app的支持

​	右击：子工程，选择——>AddFramework Support——>Web Application（选中）

<img src="/../images/创建web项目.png" style="zoom:33%;" />

### （3）子工程导入servlet 和 jsp 的 jar 依赖

```xml
<dependency>
   <groupId>javax.servlet</groupId>
   <artifactId>servlet-api</artifactId>
   <version>2.5</version>
</dependency>
<dependency>
   <groupId>javax.servlet.jsp</groupId>
   <artifactId>jsp-api</artifactId>
   <version>2.2</version>
</dependency>
```

### （4）编写一个Servlet类，用来处理用户的请求

​		路径：java.com.kuang.servlet.HelloServlet

​		继承HttpServlet后，**重写**：**get和post方法**

<img src="/../images/重写HttpServlet方法.png" style="zoom: 33%;" />

```java
public class HelloServlet  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       // 1、获得前端参数
        String method = req.getParameter("method");
        if (method.equals("add")){
            req.getSession().setAttribute("msg","执行了add方法");
        }
        if (method.equals("delete")){
            req.getSession().setAttribute("msg","执行了delete方法");
        }
        //2、调用业务层
        //3、视图转发或者重定向
        req.getRequestDispatcher("/WEB-INF/jsp/test.jsp").forward(req,resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp); //调用get方法
    }
```

### （5）编写一个jsp文件

web/WEB-INF/jsp/test.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    ${msg}
</body>
</html>
```

### （6）在web.xml文件中注册

```xml
<!-- 注册 -->
<servlet>
    <servlet-name>test</servlet-name>
    <servlet-class>com.kuang.servlet.HelloServlet</servlet-class>
</servlet>
<!-- 请求test.jsp页面，
        然后会跳转到：com.kuang.servlet.HelloServlet 中执行对应的代码  -->
<servlet-mapping>
    <servlet-name>test</servlet-name>
    <url-pattern>/test</url-pattern>
</servlet-mapping>
```

### （7）配置Tomcat

- 点击Run->EDit Configurations；
- 点击Templates；
- 点击右边：句子中的 "+"号，选择tomcat-server——>local
- 配置Configurations，并选择需要运行的：web 项目

<img src="/../images/tomcat配置.png" style="zoom:33%;" />

	- 运行（ok）网址：http://localhost:8080/

```java
http://localhost:8080/test?method=add  // 传递参数，method，否则会报：空异常
```

<img src="/../images/serlvet运行结果.png" style="zoom: 50%;" />

## 3、MVC框架要做哪些事情

1. 将url**映射**到java类或java类的方法 .
2. **封装**用户提交的数据 .
3. 处理请求——调用相关的业务处理——封装响应数据 .
4. 将响应的数据**进行渲染** . jsp / html 等表示层数据 .

## 4、什么是SpringMVC

​	spring MVC是：**Spring Framework**的一部分，是**基于Java实现**MVC的**轻量级Web框架**。

---

### （1）特点

1. 轻量级，简单易学
2. 高效 , **基于请求响应的MVC框架**
3. 与Spring兼容性好，无缝结合
4. 约定优于配置
5. 功能强大：RESTful、数据验证、格式化、本地化、主题等
6. 简洁灵活

---

​	Spring的web框架围绕：**DispatcherServlet** [ 调度**Servlet** ] 设计。

​	DispatcherServlet的作用是：将请求**分发到不同**的处理器。

### （2）中心控制器

- Spring的web框架**围绕  ** **DispatcherServlet**   设计。
- DispatcherServlet的作用是将：“**请求**”  分发到  **不同的处理器**。从Spring 2.5开始，使用Java 5或者以上版本的用户可以采用：**基于注解**的**controller**声明方式。

---

​	Spring MVC框架像许多其他MVC框架一样, **以  “ 请求” 为驱动** , **围绕一个中心  Servlet分派请求   及提供其他功能**，**DispatcherServlet是一个实际的Servlet (它   继承   自HttpServlet 基类)**。

### （3）SpringMVC的原理

​	①	当发起请求时  **被前置的控制器**   **拦截**到请求；

​	②	根据**请求参数**生成**代理请求**，**找到**请求对应的**实际控制器**；

​	③	控制器**处理请求**，创建数据模型，访问数据库，将模型响应**给**中心控制器；

​	④	控制器使用**模型与视图**渲染视图结果，将**结果返回**给中心控制器，**再将**结果返回**给请求者**。

![/../images/](MVC原理.png)

### （4）SpringMVC执行原理

![](/../images/MVC的执行原理.png)

​	①	DispatcherServlet表示**前置控制器**，是整个SpringMVC的**控制中心**。用户发出请求，DispatcherServlet**接收请求**并**拦截请求**。

- 我们假设请求的url为 : http://localhost:8080/SpringMVC/hello
- **如上url拆分成三部分：**
- http://localhost:8080 ------> 服务器域名
- SpringMVC ------> 部署在服务器上的web站点
- hello ------> 表示**控制器**
- 通过分析，如上url表示为：**请求位于**服务器localhost:8080上的SpringMVC站点的**hello控制器**。

---

​	②	HandlerMapping为**处理器映射**。DispatcherServlet调用HandlerMapping，HandlerMapping根据**请求url**查找**Handler**。

----

​	③	HandlerExecution表示**具体的Handler**，其主要作用是**根据url**查找**控制器**，如上url被查找控制器为：**hello**。

----

​	④	HandlerExecution将**解析后的信息**传递**给**DispatcherServlet，如解析控制器映射等。

----

​	⑤	HandlerAdapter表示**处理器适配器**，其按照**特定的规则**去**执行Handler**。

----

​	⑥	Handler让**具体的Controller**执行。

---

​	⑦	Controller将**具体的执行信息**返回给**HandlerAdapter**，如ModelAndView。

---

​	⑧	HandlerAdapter将**视图逻辑名**或**模型**传递给**DispatcherServlet**。

---

​	⑨	DispatcherServlet调用**视图解析器**(ViewResolver)来**解析**HandlerAdapter**传递的**  “**逻辑视图名**”。

---

​	⑩	视图解析器将**解析的逻辑视图名**传给DispatcherServlet。

---

​	最终，DispatcherServlet**根据**视图解析器解析的**视图结果**，**调用具体的**视图。最终视图**呈现给用户**。

# 二、Helloworld（配置版）

**流程：**第一步：通过**模型来存储：数据**

![](/../images/MVC流程.png)

## （1）配置版

- 新建一个Moudle ， springmvc-02-hello ， 添加web的支持！

## （2）导入pom依赖

- 确定导入了SpringMVC 的依赖！

```xml
<!--导入相关依赖  -->
<dependencies>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>5.1.9.RELEASE</version>
    </dependency>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>servlet-api</artifactId>
        <version>2.5</version>
    </dependency>
    <dependency>
        <groupId>javax.servlet.jsp</groupId>
        <artifactId>jsp-api</artifactId>
        <version>2.2</version>
    </dependency>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>jstl</artifactId>
        <version>1.2</version>
    </dependency>
</dependencies>
```

## （3）配置web.xml（核心）

- 配置**web.xml** ， 注册DispatcherServlet
- springmvc-servlet.xml 文件为：SpringMVC 的 **配置文件**
- 注册DispatcherServlet，为：SpringMVC核心：请求分发器：**前端控制器**

```xml
 <!--1.注册DispatcherServlet，为：SpringMVC核心：请求分发器：前端控制器-->
    <servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--关联一个springmvc的配置文件:【servlet-name】-servlet.xml-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:springmvc-servlet.xml</param-value>
        </init-param>
        <!--启动级别-1-->
        <load-on-startup>1</load-on-startup>
    </servlet>

    <!-- / ： 匹配所有的请求；（不包括.jsp）-->
    <!--/* ：匹配所有的请求；（包括.jsp）-->
    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
```

## （4）编写配置文件springmvc-servlet.xml

​	**springmvc-servlet.xml : [servletname]-servlet.xml**

​	处理映射器/处理器适配器/视图解析器 均在**此文件中**配置。

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd">

</beans>
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd">
<!-- *********************************************************************  -->
    <!-- 
		SpringMVC三要素：
			处理映射器；处理器适配器；视图解析器
	-->
    <bean class="org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping"/>
    <bean class="org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter"/>
<!-- *********************************************************************  -->
    <!--视图解析器:DispatcherServlet给他的ModelAndView-->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="InternalResourceViewResolver">
        <!--前缀-->
        <property name="prefix" value="/WEB-INF/jsp/"/>
        <!--后缀-->
        <property name="suffix" value=".jsp"/>
    </bean>
<!-- *********************************************************************  -->
    <!--
		Handler，接收到hello请求后，会寻找对应的Controller，
		本案例也就是：HelloController	，此步：类似于：servlet-->
    <bean id="/hello" class="com.kuang.controller.HelloController"/>
<!-- *********************************************************************  -->
</beans>
```

## （5）添加 处理映射器

​	**resources/springmvc-servlet.xml**

​	此处理器**一般不用**。

```xml
<bean class="org.springframework.web.servlet.handler.BeanNameUrlHandlerMapping"/>
```

​	BeanNameUrlHandlerMapping这个处理器，**需要根据：bean的id来寻找**，例如：

```xml
 <bean id="/hello" class="com.kuang.controller.HelloController"/>
```

## （6）加 处理器适配器

​	**resources/springmvc-servlet.xml**

```xml
<bean class="org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter"/>
```

​	 处理映射器与处理器适配器：**寻找Controller**。

## （7）添加 视图解析器

​	**resources/springmvc-servlet.xml**

​	这里主要是用来解析：项目中的视图文件（jsp等）

```xml
<!--视图解析器:DispatcherServlet给他的ModelAndView-->
<bean class="org.springframework.web.servlet.view.InternalResourceViewResolver" id="InternalResourceViewResolver">
    <!--前缀-->
    <property name="prefix" value="/WEB-INF/jsp/"/>
    <!--后缀-->
    <property name="suffix" value=".jsp"/>
</bean>
```

​	配置视图解析器后，下列代码可简化：

```java
 req.getRequestDispatcher("/WEB-INF/jsp/test.jsp").forward(req,resp);
```

```java
// 直接写需访问的：jsp，不用写前缀
req.getRequestDispatcher("test.jsp").forward(req,resp); 
```

## （8）编写操作业务

java/com/kuang/controller/**HelloController**.java

​	模型：存数据；  ModelAndView mv = new ModelAndView();

​	视图：将数据返回到某个页面。

```java
/**
 * 记得导入：import org.springframework.web.servlet.mvc.Controller;包
 *
 */
public class HelloController implements Controller {

    /**
     *  1、要么 实现Controller接口；要么 增加注解；
     *  2、需要返回一个ModelAndView，装数据，封视图；
     */
    @Override
    public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
        //ModelAndView 模型和视图
        ModelAndView mv = new ModelAndView();

        //封装对象，放在ModelAndView中。Model
        //msg为：jsp中访问的变量名； HelloSpringMVC：为msg的值
        mv.addObject("msg","HelloSpringMVC!");

        // 封装要跳转的视图，放在ModelAndView中
        // 这里会跳转到：视图解析器（springmvc-servlet.xml），然后有视图解析器，去寻找最终的：目标文件（jsp）
        mv.setViewName("hello"); //: /WEB-INF/jsp/hello.jsp
        return mv;
    }
}
```

## （9）将自己的类交给SpringIOC容器，注册bean

​	**SpringIOC容器**：springmvc-servlet.xml

```xml
<!--Handler-->
<bean id="/hello" class="nuc.ss.controller.HelloController"/>
```

​	这里的**操作等价**与：servlet在**web.xml中注册**。

## （10）jsp页面

​	Web/WEB-INF/jsp/hello.jsp

```xml
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
   <title>Kuangshen</title>
</head>
<body>
		${msg}
</body>
</html>
```

## （11）测试

​	启动Tomcat，选择需要运行的：web项目。

<img src="/../images/helloMVC结果.png" style="zoom:33%;" />

## （12）若出现404异常

1. 查看控制台输出，看一下**是不是缺少了什么jar包**。
2. **如果jar包存在，显示无法输出，就在IDEA的项目发布中，添加lib依赖！**
3. 重启Tomcat 即可解决！
4. 运行项目，必须要保证：Tomcat只能有一个项目存在，默认路径为：/，一般为：/项目名，访问的网站为：http://localhost:8080/Springmvchelloword/hello

<img src="/../images/web.png" style="zoom:25%;" />

上述三种方法中，**第二种**最为注意：

​	**解决方法：**
​		①	File——>Project Structure

​		②	Artifacts——>选中对应的web项目（此为：Springmvchelloword）

​		③	手动添加lib文件夹

<img src="/../images/手动添加文件夹：lib.png" style="zoom:33%;" />

​		④	创建后，将lib文件夹，**拖到：WEB-INF下**，作为它的：子目录，**而不是**classes的子目录

<img src="/../images/目录平级.png" style="zoom:33%;" />

​		⑤	添加jar包

<img src="/../images/为lib中添加jar包.png" style="zoom: 33%;" />

# 三、Helloworld（注解版）

![](/../images/注解开发流程.png)

## （1）新建项目

​	**新建一个Moudle，springmvc-03-hello-annotation 。添加web支持！**

## （2）资源过滤的问题

​	在项目的：pom.xml文件中：

```xml
<build>
   <resources>
       <resource>
           <directory>src/main/java</directory>
           <includes>
               <include>**/*.properties</include>
               <include>**/*.xml</include>
           </includes>
           <filtering>false</filtering>
       </resource>
       <resource>
           <directory>src/main/resources</directory>
           <includes>
               <include>**/*.properties</include>
               <include>**/*.xml</include>
           </includes>
           <filtering>false</filtering>
       </resource>
   </resources>
</build>
```

## （3）引入相关的依赖

​		见第二章

## （4）配置web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
        version="4.0">

   <!--1.注册servlet-->
   <servlet>
       <servlet-name>SpringMVC</servlet-name>
       <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
       <!--通过初始化参数指定SpringMVC配置文件的位置，进行关联-->
       <init-param>
           <param-name>contextConfigLocation</param-name>
           <param-value>classpath:springmvc-servlet.xml</param-value>
       </init-param>
       <!-- 启动顺序，数字越小，启动越早 -->
       <load-on-startup>1</load-on-startup>
   </servlet>

   <!--所有请求都会被springmvc拦截 -->
   <servlet-mapping>
       <servlet-name>SpringMVC</servlet-name>
       <url-pattern>/</url-pattern>
   </servlet-mapping>

</web-app>
```

## （5）添加Spring MVC配置文件

​	resources/springmvc-servlet.xml 

​		在**resource目录下**添加springmvc-servlet.xml配置文件，配置的形式与Spring容器配置基本类似，为了**支持基于注解的IOC**，设置了**自动扫描包的功能**，具体配置信息如下：

​		 支持mvc注解驱动（处理映射器与处理器适配器自动配置，这里**自动配置，不用手动**）

   <!-- 自动扫描包，让指定包下的注解生效,由IOC容器统一管理 -->
    <context:component-scan base-package="com.kuang.controller"/>

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd
       http://www.springframework.org/schema/mvc
       https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- 自动扫描包，让指定包下的注解生效,由IOC容器统一管理 -->
    <context:component-scan base-package="com.kuang.controller"/>
    <!-- 让Spring MVC不处理静态资源
        类似：.css .js等文件
    -->
    <mvc:default-servlet-handler />

    <!--
    支持mvc注解驱动（处理映射器与处理器适配器自动配置）

        在spring中一般采用@RequestMapping注解来完成映射关系
        要想使@RequestMapping注解生效
        必须向上下文中注册DefaultAnnotationHandlerMapping
        和一个AnnotationMethodHandlerAdapter实例
        这两个实例分别在“类级别”和“方法级别”处理。
        而annotation-driven配置帮助我们“自动完成”上述两个实例的注入。
     -->
    <mvc:annotation-driven />

    <!-- 视图解析器 -->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"
          id="internalResourceViewResolver">
        <!-- 前缀 -->
        <property name="prefix" value="/WEB-INF/jsp/" />
        <!-- 后缀 -->
        <property name="suffix" value=".jsp" />
    </bean>

</beans>
```

## （7）编写：HelloController

​	在**类上标注**又在**方法上**标注：
​		@RequestMapping("Main")，则访问路径为：http://localhost:8080/项目/Main/hello

​	若**只在  方法上标注**：
​		 @RequestMapping("hello")，则访问路径为：http://localhost:8080/项目名/hello

```java
/**
 *  @Controller 等价与：<bean id="/hello" class="nuc.ss.controller.HelloController"/>
 *  自动装配
 * @RestController：不会被解析
 */
@Controller
/**
 *  在类上标注：@RequestMapping("Main")，则访问路径为：http://localhost:8080/项目/Main/hello名/Main/hello
 *  在方法上标注： @RequestMapping("hello")，则访问路径为：http://localhost:8080/项目名/hello
 */
//@RequestMapping("Main")
public class HelloController {
//------------------------------------------------------------------------------
    // 真实访问地址 : http://localhost:8080/项目名/hello
    @RequestMapping("hello")
    // model 用来：封装数据
    public String hello(Model model){
        // 封装数据
        model.addAttribute("msg","hello,SpringMVCannotation!");
        return "hello";//返回给视图解析器处理，值为：jsp对应的文件名，例如：hello.jsp
    }
}
```

​		返回给视图解析器处理，值为：**jsp对应的文件名**，例如：hello.jsp

## （8）创建jsp文件

​	根据**视图解析器的路径**来创建对应的jsp文件

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
   <title>SpringMVC</title>
</head>
<body>
		${msg}
</body>
</html>
```

## （9）测试

<img src="/../images/注解结果.png" style="zoom: 50%;" />

# 四、注解开发总结

## 1、注解实现步骤

**注解实现**步骤其实非常的简单：

1. 新建一个**Maven项目**，并添加**web项目** 支持
2. 导入相关**jar包**（同时也要创建一个**lib文件**）
3. 编写web.xml , **注册**DispatcherServlet
4. 编写springmvc配置文件（**springmvc-servlet.xml**）
5. 接下来就是去创建**对应的控制类** , controller
6. 最后完善**前端视图**和controller之间的对应
7. 测试运行调试.

----

使用springMVC必须配置的三大件

​		**处理器映射器、处理器适配器、视图解析器**

​		通常，我们只需要**手动配置视图解析器**，而**处理器映射器**和**处理器适配器**只需要**开启**    **注解驱动**即可，而省去了大段的xml配置。

## 2、@Controller

​	用于声明Spring类的实例是一个**控制器**；

​	Spring可以使用**扫描机制**来找到**应用程序中**所有**基于注解的控制器类**，为了保证Spring能找到你的控制器，需要**在配置文件中声明**组件扫描。*（springmvc-serlvet.xml）*

```xml
<!-- 自动扫描包，让指定包下的注解生效,由IOC容器统一管理 -->
<context:component-scan base-package="com.kuang.controller"/>
```

## 3、@RequestMapping

​	用于**映射url** 到  **控制器类**或**一个特定的处理程序方法**。可用于类或方法上。用于类上，表示类中的所有响应请求的方法都是以该地址作为**父路径**。

```java
@Controller
public class TestController {
    
   @RequestMapping("/h1")
   public String test(){
       return "test";
  }
}
```

# 五、RestFul 风格

​	Restful就是一个**资源定位**及**资源操作**的风格。不是标准也不是协议，只是**一种风格**。基于这个风格设计的软件可以更简洁，更有层次，更易于**实现缓存**等机制。

## 1、功能

​	资源：互联网所有的事物都可以*被抽象为资源*

​	资源操作：使用POST、DELETE、PUT、GET，使用**不同方法**对**资源进行操作**。

​	分别对应：添加、 删除、修改、查询。

## 2、传统方式操作资源

​	通过：？...
​	通过**不同的参数**来**实现不同的效果**！方法单一，post 和 get

​	 http://127.0.0.1/item/queryItem.action?id=1 查询,GET

​	 http://127.0.0.1/item/saveItem.action 新增,POST

​	 http://127.0.0.1/item/updateItem.action 更新,POST

 	http://127.0.0.1/item/deleteItem.action?id=1 删除,GET或POST

```java
	@RequestMapping("/add")
    public String test1(int a,int b,Model model){
        int res = a+b;
        model.addAttribute("msg","结果为"+res);
        return "test";
    }
```

​	浏览器通过：http://localhost:8080/add?a=1&b=2， 给a和b两个参数  **传参**。

## 3、使用RESTful操作资源

​	通过：**斜杆** 的方式
​	可以通过**不同的请求方式**来**实现不同的效果**！如下：请求地址一样，但是功能可以不同！

​	http://127.0.0.1/item/1/	查询,GET

​	 http://127.0.0.1/item/	新增,POST

​	 http://127.0.0.1/item/	更新,PUT

​	 http://127.0.0.1/item/1/	删除,DELETE

​	在Spring MVC中可以使用 **@PathVariable 注解**，让方法参数的值对应**绑定到**一个URI模板变量上：

```java
// 让方法参数的值对应：绑定到一个URI模板变量
@RequestMapping("/add/{a}/{b}")
public String test1(@PathVariable int a, @PathVariable int b, Model model){
    int res = a+b;
    model.addAttribute("msg","结果为"+res);
    return "test";
}
```

​	浏览器通过：http://localhost:8080/add/1/2， 给a和b两个参数  **传参**。

## 4、method属性指定请求类型

​	用于**约束请求**的类型，可以收窄请求范围。

​	浏览器地址栏进行访问**默认是Get请求**，会报错405。

```java
//映射访问路径,必须是POST请求
@RequestMapping(path ="/add/{a}/{b}",method = {RequestMethod.POST})
public String hello(int a,int b,Model model){
   model.addAttribute("msg", "hello!");
   return "test";
}
```

## 5、方法级别的注解变体

@GetMapping 

@PostMapping 

@PutMapping 

@DeleteMapping 

@PatchMapping

----

```java
@GetMapping("/add/{a}/{b}")	// 使用get请求
public String hello(int a,int b,Model model){
    ...
```


扮演的是 **@RequestMapping(method =RequestMethod.GET)** 的一个**快捷方式**.

---

```java
@PostMapping("/add/{a}/{b}")	// 使用post请求
public String hello(int a,int b,Model model){
    ...
```

```html
<form action="add/1/2" method="post">
    <input type="submit"> 
</form>
```



# 六、转发与重定向

通过**SpringMVC**来实现转发和重定向 - 

## 1、无需视图解析器

- 默认为forward转发（也可以加上）
- redirect转发需特别加

```java
@Controller
public class ResultSpringMVC {
    
   @RequestMapping("/rsm/t1")
   public String test1(){
       //转发
       return "/index.jsp";
  }
  // 浏览器：http://localhost:8080/rsm/t1
    
   @RequestMapping("/rsm/t2")
   public String test2(){
       //转发二
       return "forward:/index.jsp";
  }
	// 浏览器：http://localhost:8080/rsm/t2
    
   @RequestMapping("/rsm/t3")
   public String test3(){
       //重定向
       return "redirect:/index.jsp";
  }
}
```

## 2、需视图解析器

重定向 , **不需要视图解析器** , 本质就是重新请求一个新地方嘛 , 所以注意路径问题.

可以重定向到另外一个请求实现 .

- 默认为forward转发（不可以加上）
- redirect转发需特别加

```java
@Controller
public class ResultSpringMVC2 {
    
   @RequestMapping("/rsm2/t1")
   public String test1(){
       //转发
       return "test";
  }
   @RequestMapping("/rsm2/t2")
   public String test2(){
       //重定向
       return "redirect:/index.jsp";
       //return "redirect:hello.do"; //hello.do为另一个请求/
  }
}
```

​	重定向：
​		http://localhost:8080/Springmvcannotation_war_exploded/rsm2/t2 会**转变为**下方地址：
​		http://localhost:8080/Springmvcannotation_war_exploded/index.jsp

# 七、数据处理

## 1、处理提交数据

### （1）提交的**域名称**和处理方法的**参数名 **  **一致**

​	提交数据 : http://localhost:8080/hello?name=kuangshen

```java
@RequestMapping("/hello")
public String hello(String name){
   System.out.println(name);
   return "hello";
}
```

​	后台输出 : kuangshen

### （2）提交的**域名称**和处理方法的**参数名 **  **不一致**

​	提交数据 : http://localhost:8080/hello?username=kuangshen

```java
//@RequestParam("username") : username提交的域的名称 .
@RequestMapping("/hello")
public String hello(@RequestParam("username") String name){
   System.out.println(name);
   return "hello";
}
```

​	后台输出 : kuangshen

### （3）提交的是一个**对象**

```java
public class User {
    private int id;
    private String name;
    private int age;
}
```

​	提交的**表单域**和**对象的属性名**一致 , 参数使用对象即可

​	浏览器：http://localhost:8080/Springmvcannotation_war_exploded/t2?id=1&name=tzw&age=15

```java
/**
 * 1、接收前端用户传递的参数，判断参数的名字，假设名字直接在方法上，可以直接使用；
 * 2、假设传递的是一个对象（User），则会匹配对象中的成员变量，如果完全跟对象的成员变量一致，则匹配成功，否则，失败
 */
@GetMapping("/t2")
public String test2(User user){
    System.out.println(user);
    return "hello";
}
```

后台输出 : User { id=1, name=‘tzw’, age=15 }

说明：如果使用对象的话，前端传递的参数名和对象名必须一致，否则就是null。

## 2、数据显示到前端

​	可以写**多个addAttribute方法**

### （1）ModelAndView

```java
public class ControllerTest1 implements Controller {

   public ModelAndView handleRequest(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {
       //返回一个模型视图对象
       ModelAndView mv = new ModelAndView();
       mv.addObject("msg","ControllerTest1");
       mv.setViewName("test");
       return mv;
  }
}
```

### （2）ModelMap

```java
@RequestMapping("/hello")
public String hello(@RequestParam("username") String name, ModelMap modelMap){
   //封装要显示到视图中的数据
   //相当于req.setAttribute("name",name);
   modelMap.addAttribute("name",name);
   System.out.println(name);
   return "hello";
}
```

### （3）Model

```java
@RequestMapping("/ct2/hello")
public String hello(@RequestParam("username") String name, Model model){
   //封装要显示到视图中的数据
   //相当于req.setAttribute("name",name);
   model.addAttribute("msg",name);
   model.addAttribute("as",name);	// 可以写多个addAttribute方法
   System.out.println(name);
   return "test";
}
```

### （4）对比三种

​	Model 只有寥寥几个方法只适合用于储存数据，简化了新手对于Model对象的操作和理解；
​	ModelMap 继承了 LinkedMap ，除了实现了自身的一些方法，**同样的继承** LinkedMap 的方法和特性；
​	ModelAndView 可以在储存数据的同时，可以进行设置返回的逻辑视图，进行控制展示层的跳转。

# 八、乱码问题

<img src="/../images/乱码问题.png" style="zoom:50%;" />

## 0、准备

（1）创建一个：form.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <!-- action：注意要加：项目名 -->
    <form action="/Springmvcannotation_war_exploded/hello" method="post">
        <input type="text" name="name">
        <input type="submit">
    </form>
</body>
</html>
```

（2）后台编写对应的处理类

```java
    // 真实访问地址 : http://localhost:8080/项目名/hello
    @RequestMapping("/hello")
    // model 用来：封装数据
    public String hello(Model model,String name){
        // 封装数据
        model.addAttribute("msg",name);
        return "hello";//返回给视图解析器处理，值为：jsp对应的文件名，例如：hello.jsp
    }
```

（3）测试

## 1、使用SpringMVC提供的配置解决

​	以前乱码问题通过**过滤器**解决 , 而SpringMVC给我们提供了一个过滤器 ；
​    可以在**web.xml**中配置 。

```xml
<filter>
   <filter-name>encoding</filter-name>
   <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
   <init-param>
       <param-name>encoding</param-name>
       <param-value>utf-8</param-value>
   </init-param>
</filter>

<filter-mapping>
   <filter-name>encoding</filter-name>
   <url-pattern>/*</url-pattern>
</filter-mapping>
```

​	修改了xml文件需要**重启服务器**；

​	注意：这里写**/***，写  “/”  的话**过滤不了**jsp页面，不能解决乱码；

​	但是我们发现 , 有些**极端情况下**.这个过滤器**对get的支持**不好

## 2、若SpringMVC配置还无法解决，可以参考下面两种方法解决

## 3、修改tomcat配置文件 ：设置编码！

<img src="/../images/修改Tomcat的server编码.png" style="zoom: 50%;" />

```xml
<Connector URIEncoding="utf-8" port="8080" protocol="HTTP/1.1"
          connectionTimeout="20000"
          redirectPort="8443" />
```

## 4、自定义过滤器（万能解决）

### （1）创建一个过滤器类

（**java/com/kuang/filter/GenericEncodingFilter.java**）

```java
package com.kuang.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
* 解决get和post请求 全部乱码的过滤器
*/
public class GenericEncodingFilter implements Filter {

   @Override
   public void destroy() {
  }

   @Override
   public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
       //处理response的字符编码
       HttpServletResponse myResponse=(HttpServletResponse) response;
       myResponse.setContentType("text/html;charset=UTF-8");

       // 转型为与协议相关对象
       HttpServletRequest httpServletRequest = (HttpServletRequest) request;
       // 对request包装增强
       HttpServletRequest myrequest = new MyRequest(httpServletRequest);
       chain.doFilter(myrequest, response);
  }

   @Override
   public void init(FilterConfig filterConfig) throws ServletException {
  }

}

//自定义request对象，HttpServletRequest的包装类
class MyRequest extends HttpServletRequestWrapper {

   private HttpServletRequest request;
   //是否编码的标记
   private boolean hasEncode;
   //定义一个可以传入HttpServletRequest对象的构造函数，以便对其进行装饰
   public MyRequest(HttpServletRequest request) {
       super(request);// super必须写
       this.request = request;
  }

   // 对需要增强方法 进行覆盖
   @Override
   public Map getParameterMap() {
       // 先获得请求方式
       String method = request.getMethod();
       if (method.equalsIgnoreCase("post")) {
           // post请求
           try {
               // 处理post乱码
               request.setCharacterEncoding("utf-8");
               return request.getParameterMap();
          } catch (UnsupportedEncodingException e) {
               e.printStackTrace();
          }
      } else if (method.equalsIgnoreCase("get")) {
           // get请求
           Map<String, String[]> parameterMap = request.getParameterMap();
           if (!hasEncode) { // 确保get手动编码逻辑只运行一次
               for (String parameterName : parameterMap.keySet()) {
                   String[] values = parameterMap.get(parameterName);
                   if (values != null) {
                       for (int i = 0; i < values.length; i++) {
                           try {
                               // 处理get乱码
                               values[i] = new String(values[i]
                                      .getBytes("ISO-8859-1"), "utf-8");
                          } catch (UnsupportedEncodingException e) {
                               e.printStackTrace();
                          }
                      }
                  }
              }
               hasEncode = true;
          }
           return parameterMap;
      }
       return super.getParameterMap();
  }

   //取一个值
   @Override
   public String getParameter(String name) {
       Map<String, String[]> parameterMap = getParameterMap();
       String[] values = parameterMap.get(name);
       if (values == null) {
           return null;
      }
       return values[0]; // 取回参数的第一个值
  }

   //取所有值
   @Override
   public String[] getParameterValues(String name) {
       Map<String, String[]> parameterMap = getParameterMap();
       String[] values = parameterMap.get(name);
       return values;
  }
}
```

### （2）将自定义的过滤器添加到web.xml中

​	注意：**去掉**SpringMVC的自带的过滤器

```xml
<!-- 配置自定义：解决乱码的过滤器 -->
<filter>
    <filter-name>encoding</filter-name>
    <!-- 配置那个类 进行过滤 -->
    <filter-class>com.kuang.filter.GenericEncodingFilter</filter-class>
</filter>
<filter-mapping>
    <filter-name>encoding</filter-name>
     <!-- 选择过滤的请求，/* 当前目录下，/** 所有请求 -->
    <url-pattern>/*</url-pattern>
</filter-mapping>
```

# 九、Json交互处理

## 1、什么是JSON？

- JSON(JavaScript Object Notation, JS 对象标记) 是一种**轻量级的数据交换格式**，目前使用特别广泛。
- 采用完全独立于编程语言的**文本格式**来存储和表示数据。
- 简洁和清晰的层次结构使得 JSON 成为理想的数据交换语言。
- 易于人阅读和编写，同时也易于机器解析和生成，并有效地提升网络传输效率。

```json
var obj = {a: 'Hello', b: 'World'}; //这是一个对象，注意键名也是可以使用引号包裹的
var json = '{"a": "Hello", "b": "World"}'; //这是一个 JSON 字符串，本质是一个字符串
```

## 2、JSON 和 JavaScript 对象互转

（1）从JSON字符串**转换为**JavaScript 对象，使用 JSON.**parse() 方法**：

```json
var obj = JSON.parse('{"a": "Hello", "b": "World"}');
//结果是 {a: 'Hello', b: 'World'}
```

（2）从JavaScript 对象**转换为**JSON字符串，使用 JSON.**stringify()** 方法：

```json
var json = JSON.stringify({a: 'Hello', b: 'World'});
//结果是 '{"a": "Hello", "b": "World"}'
```

## 3、演示

```html
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>JSON_秦疆</title>
</head>
<body>

<script type="text/javascript">
   //编写一个js的对象
   var user = {
       name:"秦疆",
       age:3,
       sex:"男"
  };
   //将js对象转换成json字符串
   var str = JSON.stringify(user);
   console.log(str);
   
   //将json字符串转换为js对象
   var user2 = JSON.parse(str);
   console.log(user2.age,user2.name,user2.sex);

</script>

</body>
</html>
```

## 4、Controller返回JSON数据

### （1）引入依赖

​	pom.xml文件

```xml
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
<dependency>
   <groupId>com.fasterxml.jackson.core</groupId>
   <artifactId>jackson-databind</artifactId>
   <version>2.9.8</version>
</dependency>
```

### （2）配置SpringMVC，web.xml（过滤器）

​	文件：**web.xml**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
        version="4.0">

   <!--1.注册servlet-->
   <servlet>
       <servlet-name>SpringMVC</servlet-name>
       <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
       <!--通过初始化参数指定SpringMVC配置文件的位置，进行关联-->
       <init-param>
           <param-name>contextConfigLocation</param-name>
           <param-value>classpath:springmvc-servlet.xml</param-value>
       </init-param>
       <!-- 启动顺序，数字越小，启动越早 -->
       <load-on-startup>1</load-on-startup>
   </servlet>

   <!--所有请求都会被springmvc拦截 -->
   <servlet-mapping>
       <servlet-name>SpringMVC</servlet-name>
       <url-pattern>/</url-pattern>
   </servlet-mapping>

     <!-- Springmvc 自带的乱码解决过滤器 -->
   <filter>
       <filter-name>encoding</filter-name>
       
       <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
       <init-param>
           <param-name>encoding</param-name>
           <param-value>utf-8</param-value>
       </init-param>
   </filter>
   <filter-mapping>
       <filter-name>encoding</filter-name>
       <url-pattern>/*</url-pattern>
   </filter-mapping>

</web-app>
```

### （3）springmvc-servlet.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:context="http://www.springframework.org/schema/context"
      xmlns:mvc="http://www.springframework.org/schema/mvc"
      xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd
       http://www.springframework.org/schema/mvc
       https://www.springframework.org/schema/mvc/spring-mvc.xsd">

   <!-- 自动扫描指定的包，下面所有注解类交给IOC容器管理 -->
   <context:component-scan base-package="com.kuang.controller"/>

   <!-- 视图解析器 -->
   <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"
         id="internalResourceViewResolver">
       <!-- 前缀 -->
       <property name="prefix" value="/WEB-INF/jsp/" />
       <!-- 后缀 -->
       <property name="suffix" value=".jsp" />
   </bean>

</beans>
```

### （4）User的实体类

​	java/com/kuang/pojo/**User.java**

```java
public class User {
    private int id;
    private String name;
    private int age;
}
```

### （5）编写Controller

​	java/com/kuang/controller/**UserController.java**

​	**@RestController** ：当前类中所有的方法**都为转变为**：字符串
​		（包含：**@Controller 和  @ResponseBody**）

```java
@Controller // 走：视图解析器，配合： @ResponseBody 使用
public class UserController {
    @RequestMapping("j1")
    @ResponseBody   // 配置该注解，则不会走：视图解析器，会直接返回一个：字符串
    public String json1() throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        User user = new User(3,"1号",15);
		// 将一个对象转变为：字符串
        String user_str = mapper.writeValueAsString(user);

        return user.toString();
    }
}
```

### （6）运行Tomcat

​	**注意**：记得创建lib文件夹，并导入相关包

​	运行结果：英文正常显示，**中文乱码**

## 5、解决json乱码问题

### （1）produces:指定响应体返回类型和编码

```java
//produces:指定响应体返回类型和编码
@RequestMapping(value = "/j1",produces = "application/json;charset=utf-8")
```

### （2）乱码统一解决mvc:annotation-driven

​	在**springmvc的配置文件上**添加一段消息**StringHttpMessageConverter**转换配置！

​	文件：**springmvc-servlet.xml**

```xml
<mvc:annotation-driven>
    <mvc:message-converters register-defaults="true">
        <bean class="org.springframework.http.converter.StringHttpMessageConverter">
            <constructor-arg value="UTF-8"/>
        </bean>
        <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            <property name="objectMapper">
                <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                    <property name="failOnEmptyBeans" value="false"/>
                </bean>
            </property>
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```

## 6、测试集合输出

```java
@RequestMapping("/j2")
public String json2() throws JsonProcessingException {

    //创建一个jackson的对象映射器，用来解析数据
    ObjectMapper mapper = new ObjectMapper();
    //创建一个对象
   	User user1 = new User(1, "秦疆1号", 12);
    User user2 = new User(2, "秦疆2号", 12);
    User user3 = new User(3, "秦疆3号", 12);
    User user4 = new User(4, "秦疆4号", 12);
    User user5 = new User(5, "秦疆5号", 12);
    List<User> list = new ArrayList<User>();
    list.add(user1);
    list.add(user2);
    list.add(user3);
    list.add(user4);
    list.add(user5);
    //将我们的对象解析成为json格式
    String str = mapper.writeValueAsString(list);
    return str;
}
```

### （1）输出时间对象

```java
@RequestMapping("/j3")
public String json3() throws JsonProcessingException {

   ObjectMapper mapper = new ObjectMapper();

   //创建时间一个对象，java.util.Date
   Date date = new Date();
   //将我们的对象解析成为json格式
   String str = mapper.writeValueAsString(date);
   return str;
}
```

![](/../images/时间1.png)

- 默认日期格式会**变成一个数字**，是1970年1月1日到当前**日期的毫秒数**！
- Jackson 默认是会把时间转成 **timestamps形式**

### （2）取消timestamps形式 ， 自定义时间格式

```java
@RequestMapping("/j3")
public String json4() throws JsonProcessingException {
   ObjectMapper mapper = new ObjectMapper();

   //不使用时间戳的方式
   mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
   //自定义日期格式对象
   SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
   //指定日期格式
   mapper.setDateFormat(sdf);

   Date date = new Date();
   String str = mapper.writeValueAsString(date);

   return str;
}
```

![时间2](/../images/时间2.png)

### （3）写一个日期工具类

​	java/com/kuang/utils/**JsonUtils .java**

```java
public class JsonUtils {
    
    // 若用户只传入：日期（一个参数），则走这个方法
    public static String getJson(Object object) {
        // 这里调用 重载方法（getJson），并给定 默认的 输出格式
        return getJson(object,"yyyy-MM-dd HH:mm:ss");
    }
//---------------------------------------------------------------------------
     // 若用户传入：日期和输出格式（两个参数），则走这个方法
    public static String getJson(Object object, String dateFormat) {
        ObjectMapper mapper = new ObjectMapper();

        //java自定义日期格式
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        //sdf.format(date)

        // 使用ObjectMapper 来格式化输出
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,false);
        mapper.setDateFormat(sdf);

        try {
            //ObjectMapper,时间解析后的默认格式为：TImestamp.时间戳
            return mapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}
```

​	使用工具类：

```java
@RequestMapping("/j3")
    public String json3(){
        Date date = new Date();
		// 传入 日期 和 输出格式
        return JsonUtils.getJson(date,"yyyy-MM-dd HH:mm:ss");
    }
}
```

# 十、FastJson

- fastjson.jar是**阿里开发**的一款专门**用于Java开发的包**，
- 实现json对象与JavaBean对象的转换，
- 实现JavaBean对象与json字符串的转换，
- 实现json对象与json字符串的转换。
- 实现json的转换方法很多，最后的实现结果都是一样的。

## （1）引入依赖

```xml
<dependency>
   <groupId>com.alibaba</groupId>
   <artifactId>fastjson</artifactId>
   <version>1.2.60</version>
</dependency>
```

## （2）三个主要类介绍

1. JSONObject 代表 json 对象
   - JSONObject实现了Map接口, 猜想 JSONObject**底层操作**是**由Map实现**的。
   - JSONObject对应json对象，通过各种形式的**get()方法**可以获取json对象中的数据，也可利用诸如size()，isEmpty()等方法获取"键：值"对的个数和判断是否为空。其本质是通过实现Map接口并调用接口中的方法完成的。
2. JSONArray 代表 json 对象数组
   - **内部**是有List接口中的方法来完成操作的。
3. JSON代表 JSONObject和JSONArray的转化
   - JSON类源码分析与使用
   - 仔细观察这些方法，主要是实现json对象，json对象数组，javabean对象，json字符串之间的相互转化。

## （3）演示

```java
@RequestMapping("/j4")
    //@ResponseBody//他就不会走视图解析器，会直接返回一个 字符串
    public String json4(){

        User user1 = new User(1, "秦疆1号", 12);
        User user2 = new User(2, "秦疆2号", 12);
        User user3 = new User(3, "秦疆3号", 12);
        User user4 = new User(4, "秦疆4号", 12);
        User user5 = new User(5, "秦疆5号", 12);

        List<User> list = new ArrayList<User>();
        list.add(user1);
        list.add(user2);
        list.add(user3);
        list.add(user4);
        list.add(user5);

        System.out.println("*******Java对象 转 JSON字符串*******");
        String str1 = JSON.toJSONString(list);
        System.out.println("JSON.toJSONString(list)==>"+str1);
        String str2 = JSON.toJSONString(user1);
        System.out.println("JSON.toJSONString(user1)==>"+str2);

        System.out.println("\n****** JSON字符串 转 Java对象*******");
        User jp_user1=JSON.parseObject(str2,User.class);
        System.out.println("JSON.parseObject(str2,User.class)==>"+jp_user1);

        System.out.println("\n****** Java对象 转 JSON对象 ******");
        JSONObject jsonObject1 = (JSONObject) JSON.toJSON(user2);
        System.out.println("(JSONObject) JSON.toJSON(user2)==>"+jsonObject1.getString("name"));

        System.out.println("\n****** JSON对象 转 Java对象 ******");
        User to_java_user = JSON.toJavaObject(jsonObject1, User.class);
        System.out.println("JSON.toJavaObject(jsonObject1, User.class)==>"+to_java_user);

        return JSON.toJSONString(list);
    }
```



# 十一、Ajax

## 1、简介

- **AJAX = Asynchronous JavaScript and XML（异步的 JavaScript 和 XML）。**
- AJAX 是一种在  **无需重新**  加载整个网页的情况下，能够**更新部分网页**的技术。
- **Ajax 不是一种新的编程语言，而是一种用于创建更好更快以及交互性更强的Web应用程序的技术。**

## 2、伪造Ajax

​	使用<iframe></iframe>标签

```js
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>iframe测试体验页面无刷新</title>
    <script>
       function go(){
         //所有的值变量，提前获取
         var url =  document.getElementById("url").value;
         document.getElementById("iframe1").src="https://www.bilibili.com/video/BV1KJ411v7Gp?from=search&seid=4642343396568867975";
       }
    </script>
  </head>
  <body>
    <div>
      <p>请输入地址：</p>
      <<p>
        <input type="text" id="url">
        <input type="button" value="提交" onclick="go()">
      </p>
    </div>

    <div>
      <iframe  id="iframe1" style="width:500px;height:500px"></iframe>
    </div>
  </body>
</html>
```

## 3、jQuery.ajax

- Ajax的核心是**XMLHttpRequest对象**(XHR)。XHR为向服务器发送请求和解析服务器响应提供了接口。能够以异步方式从服务器获取新数据。
- jQuery 提供多个**与 AJAX 有关**的方法。
- 通过 jQuery AJAX 方法，您能够使用 HTTP **Get** 和 HTTP Post 从远程服务器上请求文本、HTML、XML 或 JSON – 同时您能够把这些**外部数据直接载入网页**的被选元素中。
- jQuery 不是生产者，而是大自然搬运工。
- jQuery Ajax**本质就是** XMLHttpRequest，对他进行了封装，方便调用！

```json
jQuery.ajax(...)
      部分参数：
            url：请求地址
            type：请求方式，GET、POST（1.9.0之后用method）
        headers：请求头
            data：要发送的数据
    contentType：即将发送信息至服务器的内容编码类型(默认: "application/x-www-form-urlencoded; charset=UTF-8")
          async：是否异步
        timeout：设置请求超时时间（毫秒）
      beforeSend：发送请求前执行的函数(全局)
        complete：完成之后执行的回调函数(全局)
        success：成功之后执行的回调函数(全局)
          error：失败之后执行的回调函数(全局)
        accepts：通过请求头发送给服务器，告诉服务器当前客户端可接受的数据类型
        dataType：将服务器端返回的数据转换成指定类型
          "xml": 将服务器端返回的内容转换成xml格式
          "text": 将服务器端返回的内容转换成普通文本格式
          "html": 将服务器端返回的内容转换成普通文本格式，在插入DOM中时，如果包含JavaScript标签，则会尝试去执行。
        "script": 尝试将返回值当作JavaScript去执行，然后再将服务器端返回的内容转换成普通文本格式
          "json": 将服务器端返回的内容转换成相应的JavaScript对象
        "jsonp": JSONP 格式使用 JSONP 形式调用函数时，如 "myurl?callback=?" jQuery 将自动替换 ? 为正确的函数名，以执行回调函数
```

## 4、配置静态资源（jQuery）

在**applicationContext.xml**中导入：**静态资源过滤**

```xml
<!-- 让Spring MVC不处理静态资源
    类似：.css .js等文件 -->
<!--静态资源过滤-->
<mvc:default-servlet-handler />
```

<img src="/../images/静态资源过滤.png" style="zoom: 50%;" />

​	创建一个：**static**文件夹，里面可以**放置：静态资源**。

## 5、Ajax演示

<img src="/../images/Ajax简单流程.png" style="zoom:50%;" />

​	Ajax为前后端分离的重要要素，原来**由后端处理用户请求**，**现在改为**由：前端处理。

​	Ajax**要素重点**：url（请求路径）、data（要发送的数据）、callback（成功之后执行的回调函数(全局)）

### （1）配置**web.xml** 和 **springmvc的配置文件**

​		【记得**静态资源过滤**和**注解驱动**配置上】

​	案例文件目录：resources/**applicationContext.xml**

```xml
<!-- 让Spring MVC不处理静态资源
    类似：.css .js等文件 -->
<!--静态资源过滤-->
<mvc:default-servlet-handler />

<!--
支持mvc注解驱动（处理映射器与处理器适配器自动配置）

    在spring中一般采用@RequestMapping注解来完成映射关系
    要想使@RequestMapping注解生效
    必须向上下文中注册DefaultAnnotationHandlerMapping
    和一个AnnotationMethodHandlerAdapter实例
    这两个实例分别在“类级别”和“方法级别”处理。
    而annotation-driven配置帮助我们“自动完成”上述两个实例的注入。
 -->
<mvc:annotation-driven />
```

### （2）编写一个AjaxController

```java
//@RestController：不会通过视图解析器
@RequestMapping("/a1")
public void a1(String name, HttpServletResponse response) throws IOException {
    System.out.println("名字为"+name);
    if("tzw".equals(name)){
        response.getWriter().print("true");	// 返回给浏览器
    }else{
        response.getWriter().print("false");
    }
}
```

### （3）导入jquery ， 可以使用在线的CDN ， 也可以下载导入

```html
<%--导入jquery ， 可以使用在线的CDN ， 也可以下载导入--%>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="${pageContext.request.contextPath}/statics/js/jquery-3.1.1.min.js"></script>
```

### （4）编写index.jsp测试

```js
 function a() {
          //post请求数据
          $.post({   
            url:"${pageContext.request.contextPath}/a1",
            data:{"name":$("#username").val()}, //获得id为username的值
            //获得执行成功后返回的结果
            success:function (data) {
              alert(data);
            }
          })
      }
```

​	发出：**post请求**，**请求**${pageContext.request.contextPath}/a1**地址**，并将对应的数据**传递到后台**，由后台执行对应的代码，然后**将结果返回到前端**（success接收），并**做出一定的行为**。

```html
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>iframe测试体验页面无刷新</title>

    <%--导入jquery ， 可以使用在线的CDN ， 也可以下载导入--%>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/statics/js/jquery-3.1.1.min.js"></script>
    <script>
        function a() {
          //post请求数据
          $.post({   
            url:"${pageContext.request.contextPath}/a1",
            data:{"name":$("#username").val()}, //获得id为username的值
            //获得执行成功后返回的结果
            success:function (data) {
              alert(data);
            }
          })
      }
    </script>
  </head>
  <body>
    <%--失去焦点的时候，发起一个请求到后台 --%>
    <input type="text" id="username" onblur="a()">
  </body>
</html>
```

![](/../images/Ajax请求.png)

​	但**未在**输入框输入信息的时候，会发起一个请求到后台，然后由**后台反馈到前端**显示。

## 6、Ajax异步加载数据

### （1）创建一个实体类

​	com/kuang/pojo/**User.java**

```java
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    private String name;
    private int age;
    private String sex;
}
```

### （2）编写一个AjaxController

```java
@RequestMapping("/a2")
public List<User> ajax2(){
    List<User> userlist = new ArrayList<User>();
    userlist.add(new User("秦疆1号",3,"男"));
    userlist.add(new User("秦疆2号",3,"男"));
    userlist.add(new User("秦疆3号",3,"男"));
    return userlist; //由于@RestController注解，将list转成json格式返回
}
```

### （3）编写JSP页面

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/statics/js/jquery-3.1.1.min.js"></script>
    <script>
        $(function () {
            $("#btn").click(function () {
                // console.log('点击事件成功')
                // $.post(url,param,[可以省略],success)
                $.post("${pageContext.request.contextPath}/a2",function (data) {
                    console.log(data[0].name);
                    let html =  "";
                    for (let i = 0; i < data.length; i++) {
                        html += `<tr>
                                    <td>${"${data[i].name}"}</td>
                                    <td>${"${data[i].age}"}</td>
                                    <td>${"${data[i].sex}"}</td>
                                </tr>`
                    }
                    $("#content").html(html)	// 将html添加到id为：content的标签中
                    console.log(html)
                })
            })
        })
    </script>
</head>
<body>
    <input type="button" value="加载数据" id="btn">
    <table>
        <thead>
        <tr>
            <td>姓名</td>
            <td>年龄</td>
            <td>性别</td>
        </tr>
        </thead>
        <tbody id="content">
        <%--数据：后台--%>
        </tbody>
    </table>
</body>
</html>
```

![](/../images/异步加载数据.png)

​	**点击**：加载数据，会**将从后台获取的数据**显示在前端（不用更换页面）

## 7、Ajax验证用户名体验

### （1）编写一个AjaxController

由于**@RestController注解**，将msg转成json格式返回

```java
@RequestMapping("/a3")
public String a3(String name,String pwd){
    String msg="";//用于向浏览器传递信息
    if (name!=null){
        // 判断用户名是否为：admin
        if("admin".equals(name)){
            msg = "ok";
        }else{
            msg = "用户名有误";
        }
    }else{
        // 判断密码是否为：123456
        if("123456".equals(pwd)){
            msg = "ok";
        }else{
            msg = "密码有误";
        }
    }
    //由于@RestController注解，将msg转成json格式返回
    return msg;
}
```

### （2）编写JSP

​	注意：jQuery导入一定要下载好，否则就**在线导入**

```html
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ajax</title>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <%--<script src="${pageContext.request.contextPath}/statics/js/jquery-3.1.1.min.js"></script>--%>
    <script>

        function a1(){
            $.post({
                url:"${pageContext.request.contextPath}/a3",
                data:{'name':$("#name").val()},
                success:function (data) {
                    if (data.toString()=='OK'){
                        $("#userInfo").css("color","green");
                    }else {
                        $("#userInfo").css("color","red");
                    }
                    $("#userInfo").html(data);
                }
            });
        }
        function a2(){
            $.post({
                url:"${pageContext.request.contextPath}/a3",
                data:{'pwd':$("#pwd").val()},
                success:function (data) {
                    if (data.toString()=='OK'){
                        $("#pwdInfo").css("color","green");
                    }else {
                        $("#pwdInfo").css("color","red");
                    }
                    $("#pwdInfo").html(data);
                }
            });
        }
    </script>
</head>
<body>
    <p>
        用户名:<input type="text" id="name" onblur="a1()"/>
        <span id="userInfo"></span>
    </p>
    <p>
        密码:<input type="text" id="pwd" onblur="a2()"/>
        <span id="pwdInfo"></span>
    </p>
</body>
</html>
```

### （3）乱码

**applicationContext.xml**

```xml
<mvc:annotation-driven>
    <mvc:message-converters register-defaults="true">
        <bean class="org.springframework.http.converter.StringHttpMessageConverter">
            <constructor-arg value="UTF-8"/>
        </bean>
        <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            <property name="objectMapper">
                <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                    <property name="failOnEmptyBeans" value="false"/>
                </bean>
            </property>
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```

## 8、拦截器

​	**过滤器**在：**web.xml 配置文件中**，由**Tomcat**来操作

​	**拦截器**在：（**springmvc.xml** 或 **applicationContext.xml**），由**spring** 来做

### （0）环境配置

​	（注意：记得**配置lib架包文件夹**）

---

​	①	配置**applicationContext**.xml 和 **web**.xml文件

​	②	applicationContext.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
       http://www.springframework.org/schema/beans/spring-beans.xsd
       http://www.springframework.org/schema/context
       https://www.springframework.org/schema/context/spring-context.xsd
       http://www.springframework.org/schema/mvc
       https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- 自动扫描包，让指定包下的注解生效,由IOC容器统一管理 -->
    <context:component-scan base-package="com.kuang.controller"/>

    <!-- 让Spring MVC不处理静态资源
        类似：.css .js等文件 -->
    <!--静态资源过滤-->
    <mvc:default-servlet-handler />

    <!--
    支持mvc注解驱动（处理映射器与处理器适配器自动配置）
     -->
    <mvc:annotation-driven />

    <!-- 视图解析器 -->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"
          id="internalResourceViewResolver">
        <!-- 前缀 -->
        <property name="prefix" value="/WEB-INF/jsp/" />
        <!-- 后缀 -->
        <property name="suffix" value=".jsp" />
    </bean>

    <!--xml中配置 表示返回的数据的字符集：乱码统一解决-->
    <mvc:annotation-driven>
        <mvc:message-converters register-defaults="true">
            <bean class="org.springframework.http.converter.StringHttpMessageConverter">
                <constructor-arg value="UTF-8"/>
            </bean>
            <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
                <property name="objectMapper">
                    <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                        <property name="failOnEmptyBeans" value="false"/>
                    </bean>
                </property>
            </bean>
        </mvc:message-converters>
    </mvc:annotation-driven>

    <!--拦截器配置-->
    <mvc:interceptors>
        <!--<mvc:interceptor>-->
            <!--&lt;!&ndash;包括这个请求：下面的所有请求&ndash;&gt;-->
            <!--<mvc:mapping path="/**"/>-->
            <!--&lt;!&ndash;使用那个类进行拦截&ndash;&gt;-->
            <!--<bean class="com.kuang.config.MyInterceptor"/>-->
        <!--</mvc:interceptor>-->

        <mvc:interceptor>
            <!--拦截：user下的所有请求-->
            <mvc:mapping path="/user/**"/>
            <!--使用那个类进行拦截-->
            <bean class="com.kuang.config.LoginInterceptor"/>
        </mvc:interceptor>
    </mvc:interceptors>

</beans>
```

​	③	web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <!--1.注册servlet-->
    <servlet>
        <servlet-name>SpringMVC</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--通过初始化参数指定SpringMVC配置文件的位置，进行关联-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:applicationContext.xml</param-value>
        </init-param>
        <!-- 启动顺序，数字越小，启动越早 -->
        <load-on-startup>1</load-on-startup>
    </servlet>

    <!--所有请求都会被springmvc拦截 -->
    <servlet-mapping>
        <servlet-name>SpringMVC</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <!--乱码问题-->
    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>

    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

</web-app>
```

### （1）导入MVC拦截器依赖

```xml
    <!--拦截器配置-->
    <mvc:interceptors>
        <mvc:interceptor>
            <!--拦截这个请求下面的：所有请求-->
            <mvc:mapping path="/**"/>
            <!--使用那个类进行拦截-->
            <bean class="com.kuang.config.MyInterceptor"/>
        </mvc:interceptor>

    </mvc:interceptors>
```

### （2）自定义拦截器类

​	文件目录：com.kuang.config.**MyInterceptor.java**

```java
public class MyInterceptor implements HandlerInterceptor {
    // 处理前
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        System.out.println("===========处理前===========");
        /**
         *  return false  ：表示 不执行下一个拦截器（不放行）
         *  return true   ：表示 执行：下一个拦截器（放行）
         */
        return false;
    }

    // 处理后
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        System.out.println("===========处理后===========");
    }

    // 清理
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        System.out.println("===========清理===========");
    }
}
```

### （3）编写一个Controller类

```java
@RestController
public class HelloController {

    @RequestMapping("/hello")
    public String test(){
        System.out.println("我进来了");
        return "hello";
    }
}
```

### （4）测试

​	输入：http://localhost:8080/Springintercepter_war_exploded/hello

![](/../images/拦截器.png)

​	因为，preHandle方法返回的是：**false**，所有**不执行**下一个拦截器（不放行）。

## 9、拦截器登录判断验证

### （1）创建一个LoginInterceptor.java拦截器

​	文件目录：com/kuang/config/**LoginInterceptor.java**

​	注意：**拦截器在于**，满足什么条件**放行**，什么条件**不放行**。

```java
/**
 * 登录拦截器
 */
public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        HttpSession httpSession = request.getSession();//获得session中的用户信息

        // 放行，判断什么情况下登录
        // 发起goLogin请求，进入登录页面也会放行（如果请求的路径是：登录url）
        if(request.getRequestURI().contains("goLogin")){
            return true;
        }

        // 若进行提交信息到login控制器，会：放行
        if(request.getRequestURI().contains("login")){
            return true;
        }

        // 若session获取的值：userLoginInfo（用户信息）不为空，则：不拦截
        if(httpSession.getAttribute("userLoginInfo")!=null){
            return true;
        }
        // 不放行（未登录），判断什么情况下没有登录，这里直接调转到：登录页面
        request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request,response);
        return false;
    }
}
```

### （2）拦截器配置

​	**拦截器**在：（**springmvc.xml** 或 **applicationContext.xml**），由**spring** 来做

​	拦截器会**按顺序执行**，可以将**先执行的拦截器**放置前

```xml
</mvc:interceptors>
    <!--拦截器配置-->
    <mvc:interceptors>
        <!--拦截器会按顺序执行，可以将先执行的拦截器放置前-->
        <mvc:interceptor>
            <!--拦截：user下的所有请求-->
            <mvc:mapping path="/user/**"/>
            <!--使用那个类进行拦截-->
            <bean class="com.kuang.config.LoginInterceptor"/>
        </mvc:interceptor>

        <mvc:interceptor>
            <!--包括这个请求：下面的所有请求-->
            <mvc:mapping path="/**"/>
            <!--使用那个类进行拦截-->
            <bean class="com.kuang.config.MyInterceptor"/>
        </mvc:interceptor>

    </mvc:interceptors>
```

### （3）编写一个Controller类

​	com.kuang.controller.**LoginController.java**

```java
@Controller
/**
 *  设置：父路径；
 *  并在拦截器中添加： <mvc:mapping path="user/**"/>，表示：只拦截user下的所有请求
 */
@RequestMapping("/user")	
public class LoginController {
```

​	进入首页（会被拦截器拦截，因为**未有登录的用户信息**）

```java
/**
 * 直接进入首页
 * 未登录，不能直接进入首页，将会被：拦截，然后调转到登录页面
 */
@RequestMapping("/main")
public String Main1(){
    return "main";
}
```

​	进入登录页面（不会被拦截器拦截）

```java
// 直接进入登录页面（不会被拦截），地址为：
// http://localhost:8080/Springintercepter_war_exploded/goLogin
@RequestMapping("/goLogin")
public String login() {
    return "login";  // 返回到指定的jsp页面
}
```

​	处理登录提交的信息（不会被拦截器拦截）

```java
// 控制器：login，用于处理从登录页面提交的信息，然后通过密码判断 进入 首页
@RequestMapping("/login")
public String login(HttpSession httpSession, Model model, String username, String password){
    //把用户的信息存在session中
    httpSession.setAttribute("userLoginInfo",username);  // 添加：用户登录信息
    model.addAttribute("password",password);  // 密码
    model.addAttribute("username",username);  // 账户
    return "main";
}
```

​	注销（不会被拦截器拦截）

```java
// 进入首页后，可以点击：“注销”,注销后，会销毁：内存的用户值，然后返回到：登录页面
@RequestMapping("/goOut")
public String goOut(HttpSession sessionl) {
    // 删除：用户信息
    sessionl.removeAttribute("userLoginInfo");
    return "login";
}
```

### （4）jsp页面

#### 	①	默认页面（/index.jsp）

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
    <h1>
      <a href="${pageContext.request.contextPath}/user/goLogin">登录页面</a>

      <a href="${pageContext.request.contextPath}/user/main">首页</a>
    </h1>
  </body>
</html>
```

#### 	②	登录页面（/jsp/login.jsp）

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>登录页面</title>
</head>
<body>
    <%--在Web-INF下的所有页面或者资源，只能通过：controller或者servlet进行访问 --%>
    <form action="${pageContext.request.contextPath}/user/login" method="post">
        用户名：<input type="text" name="username"/>
        密  码：<input type="password" name="password"/>
        <input type="submit" value="提交"/>
    </form>

</body>
</html>
```

#### 	③	首页（/jsp/main.jsp）

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/30
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>主页</title>
</head>
<body>
    <h1>首页</h1>

    <span>${username}</span>

    <p>
        <a href="${pageContext.request.contextPath}/user/goOut">注销</a>
    </p>
</body>
</html>
```

![](/../images/拦截器拦截成功.png)

​	若**未**配置拦截器，则会**直接进入到：首页**（main.jsp）；配置拦截器后，若未登录，则会被拦截，进入到登录页面。

## 10、文件上传和下载



# 十二、总结

## 1、常用依赖包

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.kuang</groupId>
    <artifactId>springmvc</artifactId>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>
    <modules>
        <module>Springmvc-serlvet</module>
        <module>Springmvc-helloword</module>
        <module>Springmvcannotation</module>
        <module>Springmvcjson</module>
        <module>Springajax</module>
    </modules>

    <!--导入相关依赖  -->
    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-webmvc</artifactId>
            <version>5.1.9.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
            <version>2.5</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet.jsp</groupId>
            <artifactId>jsp-api</artifactId>
            <version>2.2</version>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jstl</artifactId>
            <version>1.2</version>
        </dependency>
    </dependencies>

</project>
```