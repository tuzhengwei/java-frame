---
typora-root-url: SSMimages
---

# SSM框架整合

**项目访问流程：**

- 页面发送请求给控制器;
- 控制器调用业务层处理逻辑；
- 逻辑层向持久层发送请求；
- 持久层与数据库交互，后将结果返回给业务层；
- 业务层将处理逻辑发送给控制器；
- 控制器再调用视图展现数据 

**环境：**

- IDEA
- MySQL 5.7.19
- Tomcat 9
- Maven 3.6

![](/../SSMimages/流程图.png)

# 一、数据库设计

## 1、创建数据库

```mysql
CREATE DATABASE ssmbuild;
 
USE ssmbuild;
 
DROP TABLE IF EXISTS books;
 
CREATE TABLE books (
  bookID INT(10) NOT NULL AUTO_INCREMENT COMMENT '书id',
  bookName VARCHAR(100) NOT NULL COMMENT '书名',
  bookCounts INT(11) NOT NULL COMMENT '数量',
  detail VARCHAR(200) NOT NULL COMMENT '描述',
  KEY bookID (bookID)
) ENGINE=INNODB DEFAULT CHARSET=utf8
 
INSERT INTO books(bookID,bookName,bookCounts,detail) VALUES 
(1,'Java',1,'从入门到放弃'),
(2,'MySQL',10,'从删库到跑路'),
(3,'Linux',5,'从进门到进牢');
```

## 2、IDEA连接数据库

<img src="/../SSMimages/数据库连接.png" style="zoom:50%;" />

# 二、基本环境搭建

<img src="/../SSMimages/SSM项目结构.png" style="zoom:50%;" />

## 1、新建一Maven项目！ssmbuild ， 添加web的支持

## 2、父工程导入相关的pom依赖！

若复制报错，则**一个一个**的导入。**（pom.xml)**

**依赖需要导入**：junit、数据库驱动、连接池、servlet、jsp、mybatis、mybatis-spring、spring

### （1）junit依赖

```xml
<!-- 导入junit依赖-->
<dependency>
    <groupId>junit</groupId>
    <artifactId>junit</artifactId>
    <version>4.12</version>
</dependency>
```

### （2）mysql依赖与常量池依赖

```xml
<!-- 导入mysql依赖-->
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <version>5.1.47</version>
</dependency>
<!-- 导入数据库连接池 -->
<dependency>
    <groupId>com.mchange</groupId>
    <artifactId>c3p0</artifactId>
    <version>0.9.5.2</version>
</dependency>
```

### （3）导入 servlet jsp

```xml
<!-- 导入 servlet jsp -->
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>servlet-api</artifactId>
    <version>2.5</version>
</dependency>
<dependency>
    <groupId>javax.servlet.jsp</groupId>
    <artifactId>jsp-api</artifactId>
    <version>2.2</version>
</dependency>
<dependency>
    <groupId>javax.servlet</groupId>
    <artifactId>jstl</artifactId>
    <version>1.2</version>
</dependency>
```

### （4）导入mybatis-spring模块 依赖

```xml
<!-- 导入mybatis-spring模块 依赖-->
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis-spring</artifactId>
    <version>2.0.2</version>
</dependency>
<!-- 导入mybatis依赖-->
<dependency>
    <groupId>org.mybatis</groupId>
    <artifactId>mybatis</artifactId>
    <version>3.5.2</version>
</dependency>
```

### （5）导入spring模块

```xml
<!--导入spring模块 -->
<!-- 导入mvc依赖-->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-webmvc</artifactId>
    <version>5.1.9.RELEASE</version>
</dependency>
<!-- spring操作数据库，需要spring-jdbc -->
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-jdbc</artifactId>
    <version>5.1.9.RELEASE</version>
</dependency>
```

### （6）Lombok的使用

```xml
<!-- Lombok的使用 -->
<dependency>
    <groupId>org.projectlombok</groupId>
    <artifactId>lombok</artifactId>
    <version>1.16.18</version>
    <scope>provided</scope>
</dependency>
```

### （7）导入事务aop

```xml
<!--导入事务aop-->
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.8.13</version>
</dependency>
```

## 3、Maven资源过滤设置

```xml
<!-- 手动配置自动过滤 -->
<build>
    <resources>
        <resource>
            <directory>src/main/resources</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>true</filtering>
        </resource>
        <!-- 加载 java文件下的配置文件 -->
        <resource>
            <directory>src/main/java</directory>
            <includes>
                <include>**/*.properties</include>
                <include>**/*.xml</include>
            </includes>
            <filtering>true</filtering>
        </resource>
    </resources>
</build>
```

## 4、建立基本结构

**java目录下：**

- com.kuang.pojo（实体类，例如：User.java）

- com.kuang.dao（数据访问层）

  （来**封装对数据库**的新增，删除，查询，修改。例如：UserMapper.java（**接口类**））

- com.kuang.service（业务服务层，一般**对应：数据访问层** 的功能）

- com.kuang.controller（控制层）

**resources目录下：**

- com.kuang.dao（与接口对应的**XXXmapper文件**，例如：UserMapper.xml）
- mybatis-config.xml（mybatis配置文件，例如：**绑定对应**的UserMapper.xml文件）
- db.properties（数据库配置文件）
- applicationContext.xml（主配置文件）

## 5、配置框架

### （0）配置文件关联问题

注意：**配置文件**必须**被整合到   主配置文件   中**，否则会出问题。

<img src="/../SSMimages/配置文件整合.png" style="zoom:33%;" />

```xml
<!--  或者：通过：import 导入对应的配置文件 -->
<import resource="classpath:spring-dao.xml"/>
```

### （1）mybatis-config.xml

```xml
<!-- 初始化 -->
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
     <settings>
        <!--配置：默认日志-->
        <setting name="logImpl" value="STDOUT_LOGGING"/>
    </settings>
    
    <!--扫描指定包下的文件，这样在Mapper.xml文件中可以直接写：实体名-->
    <typeAliases>
        <package name="com.kuang.pojo"></package>
    </typeAliases>
    
   <!-- 绑定：BookMapper.xml 到配置文件中
        每一个XXXMapper.xml都需要在Mybatis核心配置文件（mybatis-config）中注册
    -->
    <mappers>
        <mapper resource="com/kuang/dao/BookMapper.xml"/>
    </mappers>
    
</configuration>
```

### （2）db.properties

**注意**：记得**修改数据库名**  (变量名**一定要是**：**jdbc.** 开头)

```properties
jdbc.driver=com.mysql.jdbc.Driver
jdbc.url=jdbc:mysql://localhost:3306/ssmbuild?useSSL=true&userUnicode=true&characterEncoding=utf-8
jdbc.username=root
jdbc.password=123456
```

### （3）applicationContext.xml

​	**核心配置类**（包含：mvc、dao、service.xml）

```xml
<!-- 初始化 -->
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">

</beans>
```

## 5、创建实体类

```java
/**
 *  尽量保证跟：实体类的属性 与 数据库的字段一致
 */
@Data
@AllArgsConstructor  // 有参构造器
@NoArgsConstructor  // 无参构造器
public class Books {
    private int boodID;
    private String bookName;
    private int bookCounts;
    private String detaill;
}
```

## 6、创建 数据访问层 接口

```java
/**
 *  数据访问层
 */
public interface BookMapper {

    // 增加一本书
    int addBook(Books books);

    // 删除一本书
    int deleteBookById(@Param("bookID") int id);

    // 更新一本书
    int updateBook(Books books);

    // 查询一本书
    Books queryBookById(@Param("bookID") int id);

    // 查询全部的书
    List<Books> queryAllBook();
}
```

## 7、创建于接口对应的XXXmapper.xml文件

文件位于：**resources/java/kuang/dao/BookMapper.xml**

namespace : **绑定**一个对应的mapper/Mapper接口（作为这个**接口的：实现类**）
      这个功能类似：**实现了BookMapper接口**（如：public class UserDao implements  UserMapper）

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<!--
namespace : 绑定一个对应的mapper/Mapper接口（作为这个接口的：实现类）
      这个功能类似：实现了Mapper接口（如：public class UserDao implements  UserMapper）
-->
<mapper namespace="com.kuang.dao.BookMapper">
    <!--增加-->
    <insert id="addBook" parameterType="Books">
        insert into ssmbuild.books (bookName, bookCounts, detail)
        values (#{bookName},#{bookCounts},#{detail});
    </insert>

    <!--删除-->
    <delete id="deleteBookById" parameterType="int">
        delete from ssmbuild.books where bookID=#{bookID};
    </delete>

    <!--更新-->
    <update id="updateBook" parameterType="Books">
        update ssmbuild.books
        set bookName=#{bookName},bookCounts=#{bookCounts},detail={#detail}
        where bookID=#{bookID};
    </update>

    <!--根据ID查询-->
    <select id="queryBookById" resultType="Books">
        select * from ssmbuild.books where bookID=#{bookID};
    </select>

    <!--查询所有的信息-->
    <select id="queryAllBook" resultType="Books">
        select * from ssmbuild.books;
    </select>
</mapper>
```

## 8、在mybatis-config核心配置文件中绑定Mapper.xml

**文件所在：resources/mybatis-config.xml**

可以使用：class**类绑定**，不过要注意：**接口与xml文件名要一样**，并且**同一个目录下**。

```xml
<mappers>
    <mapper class="com/kuang/dao/BookMapper.xml"/>
</mappers>
```
资源绑定路径：resources/com/kuang/dao/**BookMapper**.xml

```xml
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
        PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
        "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
    <!--扫描指定包下的文件，这样在Mapper.xml文件中可以直接写：实体名-->
    <typeAliases>
        <package name="com.kuang.pojo"></package>
    </typeAliases>

    <!-- 绑定：BookMapper.xml 到配置文件中
        每一个XXXMapper.xml都需要在Mybatis核心配置文件（mybatis-config）中注册
    -->
    <mappers>
        <mapper resource="com/kuang/dao/BookMapper.xml"/>
    </mappers>
</configuration>
```

## 9、创建 业务层 接口（功能跟：数据访问层 一致）

​	业务层（**service目录下**）：**对应 数据访问层的功能**

​	**BookService**.java

```java
**
 * 业务层：对应 数据访问层的功能
 */
public interface BookService {
    // 增加一本书
    int addBook(Books books);

    // 删除一本书
    int deleteBookById(int id);

    // 更新一本书
    int updateBook(Books books);

    // 查询一本书
    Books queryBookById(int id);

    // 查询全部的书
    List<Books> queryAllBook();
}
```

## 10、创建 业务层 的实现类

​	**BookServiceImpl**.java

```java
public class BookServiceImpl implements BookService {
    /**
     * service 层 调 dao 层，组合dao
     */
    private BookMapper bookMapper; // 获得对应的接口类，然后用接口类，去调用 它的实现类（xml文件）
    public void setBookMapper(BookMapper bookMapper){
        this.bookMapper = bookMapper;
    }

    @Override
    public int addBook(Books books) {
        return bookMapper.addBook(books);
    }

    @Override
    public int deleteBookById(int id) {
        return  bookMapper.deleteBookById(id);
    }

    @Override
    public int updateBook(Books books) {
        return bookMapper.updateBook(books);
    }

    @Override
    public Books queryBookById(int id) {
        return bookMapper.queryBookById(id);
    }

    @Override
    public List<Books> queryAllBook() {
        return bookMapper.queryAllBook();
    }
}
```

## 11、整合spring-dao.xml

​	整合**数据访问层**

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">

    <!-- 1、关联数据库配置文件：db.properties -->
    <context:property-placeholder location="classpath:db.properties"/>

    <!-- 2、数据库连接池
        dbcp：半自动化操作，不能自动连接
        3cp0：自动化操作（自动化的加载配置文件，并且可以自动设置到对象中
    -->
    <bean id="dataSource" class="com.mchange.v2.c3p0.ComboPooledDataSource">
        <!-- 配置连接池属性 -->
        <property name="driverClass" value="${jdbc.driver}"/>
        <property name="jdbcUrl" value="${jdbc.url}"/>
        <property name="user" value="${jdbc.username}"/>
        <property name="password" value="${jdbc.password}"/>

        <!-- c3p0连接池的私有属性 -->
        <property name="maxPoolSize" value="30"/>
        <property name="minPoolSize" value="10"/>
        <!-- 关闭连接后不自动commit -->
        <property name="autoCommitOnClose" value="false"/>
        <!-- 获取连接超时时间 -->
        <property name="checkoutTimeout" value="10000"/>
        <!-- 当获取连接失败重试次数 -->
        <property name="acquireRetryAttempts" value="2"/>

    </bean>

    <!-- 3.配置SqlSessionFactory对象 -->
    <bean id="sqlSessionFactory" class="org.mybatis.spring.SqlSessionFactoryBean">
        <!-- 注入数据库连接池 -->
        <property name="dataSource" ref="dataSource"/>
        <!-- 配置MyBaties全局配置文件:mybatis-config.xml -->
        <property name="configLocation" value="classpath:mybatis-config.xml"/>
    </bean>

    <!-- 4.配置扫描Dao接口包，动态实现Dao接口注入到spring容器中（原来采用：impl实现接口的方式注入） -->
    <!--解释 ：https://www.cnblogs.com/jpfss/p/7799806.html-->
    <bean class="org.mybatis.spring.mapper.MapperScannerConfigurer">
        <!-- 注入sqlSessionFactory -->
        <property name="sqlSessionFactoryBeanName" value="sqlSessionFactory"/>
        <!-- 给出需要扫描Dao接口包 -->
        <property name="basePackage" value="com.kuang.dao"/>
    </bean>

</beans>
```

## 12、整合spring-service.xml

​	整合业务层

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd
            http://www.springframework.org/schema/tx
            http://www.springframework.org/schema/tx/spring-tx.xsd">

    <!--1、扫描service目录下的：包 -->
    <context:component-scan base-package="com.kuang.service"/>

    <!--2、将我们所有的业务类，注入到Spring，可以通过配置 或者 注解实现-->
    <!-- BookServiceImpl注入到IOC容器中-->
    <bean id="BookServiceImpl" class="com.kuang.service.BookServiceImpl">
        <!-- ref="bookMapper"  引用的 dao层的  BookMapper 接口名（小写）  -->
        <property name="bookMapper" ref="bookMapper"/>
    </bean>

    <!--3、配置事务管理器 -->
    <bean id="transactionManager" class="org.springframework.jdbc.datasource.DataSourceTransactionManager">
        <!-- 注入数据库连接池，引入的dataSource来自于：spring-dao.xml文件 -->
        <property name="dataSource" ref="dataSource" />
    </bean>

    <!--4、aop事务支持-->
    <!--结合AOP实现事务的切入-->
    <!--配置事务通知-->
    <tx:advice id="txAdvice" transaction-manager="transactionManager">
        <!-- 给对应的方法配置事务 -->
        <!-- 配置事务的传播特性： propagation：REQUIRED -->
        <tx:attributes>
            <!--* 表示：所有的方法都配置事务  -->
            <tx:method name="*" propagation="REQUIRED"/>
        </tx:attributes>
    </tx:advice>

    <!--5、配置事务切入，除了select，修改和删除需要提交事务，才能添加到数据库中-->
    <aop:config>
        <!--对应类中的方法都会被编辑：事务 或 com.kuang.dao.*（包中所有的方法）-->
        <aop:pointcut id="txPointCut" expression="execution(* com.kuang.dao.*.*(..))"/>
        <aop:advisor advice-ref="txAdvice" pointcut-ref="txPointCut"/>
    </aop:config>

</beans>
```

## 13、整合spring-mvc.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd
            http://www.springframework.org/schema/mvc
            https://www.springframework.org/schema/mvc/spring-mvc.xsd">

    <!-- 配置SpringMVC -->
    <!-- 1.开启SpringMVC注解驱动 -->
    <mvc:annotation-driven />
    <!-- 2.静态资源默认servlet配置-->
    <mvc:default-servlet-handler/>

    <!-- 3.配置jsp 显示ViewResolver视图解析器 -->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver">
        <property name="viewClass" value="org.springframework.web.servlet.view.JstlView" />
        <property name="prefix" value="/WEB-INF/jsp/" />
        <property name="suffix" value=".jsp" />
    </bean>

     <!-- 过滤器 拦截器 都在mvc配置文件中 -->
    
    <!-- 4.扫描web相关的 bean -->
    <context:component-scan base-package="com.kuang.controller" />

</beans>
```

## 14、绑定到主配置文件（applicationContext.xml）

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
            https://www.springframework.org/schema/beans/spring-beans.xsd
            http://www.springframework.org/schema/context
            https://www.springframework.org/schema/context/spring-context.xsd
            http://www.springframework.org/schema/aop
            https://www.springframework.org/schema/aop/spring-aop.xsd">
    <!-- 导入spring-dao配置  -->
    <import resource="spring-dao.xml"/>
    <!-- 导入spring-service配置  -->
    <import resource="spring-service.xml"/>
    <!-- 导入spring-mvc 配置-->
    <import resource="spring-mvc.xml"/>

</beans>
```

## 15、创建一个web项目

<img src="/../SSMimages/web.png" style="zoom: 50%;" />

## 16、设置web.xml文件

​	（**注册DispatcherServlet**和**乱码**等）

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
    <!--
		1、注册DispatcherServlet，为：SpringMVC核心：请求分发器：前端控制器-->
    <servlet>
        <servlet-name>springmvc</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--关联一个springmvc的配置文件:【servlet-name】-servlet.xml-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:spring-mvc.xml</param-value>
        </init-param>
        <!--启动级别-1-->
        <load-on-startup>1</load-on-startup>
    </servlet>

    <!-- / ： 匹配所有的请求；（不包括.jsp）-->
    <!--/* ：匹配所有的请求；（包括.jsp）-->
    <servlet-mapping>
        <servlet-name>springmvc</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>

    <!--2、乱码问题-->
    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>

    <!--3、Session过期时间 -->
    <session-config>
        <session-timeout>15</session-timeout>
    </session-config>
</web-app>
```

# 三、Controller 

​	java/com/kuang/controller/**BookController.java**

​	**编写**：业务类中的方法，用于**接收前端传来的控制器**，然后根据  控制器  来 **进行对应的操作**。

​	 controller **调 service 层**<u>（ service 调 dao层）</u>

## 1、获得业务实现类

​	**获得**业务实现类（BookServiceImpl）后，可以**用实现类的对象**，去**调用对应**的方法。

```java
@Controller
@RequestMapping("/book") //父路径
public class BookController {
    /**
     *  controller 调 service 层
     */
    @Autowired
    //@Qualifier(value="")去配置，指定一个bean对象注入
    @Qualifier("BookServiceImpl")
    private BookService bookService;
```

## 1、查询书籍信息

```java
    // 查询书籍信息
    @RequestMapping("/allBook")
    public String list(Model model){
        List<Books> list = bookService.queryAllBook();
        model.addAttribute("list",list);
        return "allBook";
    }
```

## 2、增加书籍

（先跳转到：增加页面，再提交）

```java
// 跳转到：增加书籍页面
@RequestMapping("/toAddBook")  //接收：toAddBook控制器
public String toAddPaper(){
    return "addBook";   // 进入视图解析器，寻找：addBook.jsp
}

// 进行增加书籍的操作
@RequestMapping("/addBook")
public String addBook(Books books){
    System.out.println("书的名字"+books);
    bookService.addBook(books);
    return "redirect:/book/allBook"; //重定向到：@RequestMapping("/allBook")
}
```

## 3、修改书籍信息

（先跳转到：修改页面，再提交）

```java
// 调转到修改页面
@RequestMapping("/toUpdate")
public String toUpdatePaper(int id,Model model){
    Books books = bookService.queryBookById(id);// 根据 id 查找对应的信息，然后传递到修改页面
    model.addAttribute("QBook",books);  // 将books对象，赋值给 QBook，使用QBook在页面访问对应的字段属性
    System.out.println("修改的书籍信息为："+books);
    return "updateBook";
}

// 进行修改操作
@RequestMapping("/updateBook")
public String updateBook(Books books){
    System.out.println("修改的书籍为："+books);
    int i = bookService.updateBook(books);
    if(i>0){
        System.out.println("修改成功");
    }
    return "redirect:/book/allBook";
}
```

## 4、删除操作

```java
// 使用result风格
// 删除操作
@RequestMapping("/deleteBook/{bookID}")
public String deleteBook(@PathVariable("bookID") int id){
    bookService.deleteBookById(id);
    return "redirect:/book/allBook";
}
```

# 四、视图层编写

## 1、首页（index.jsp）

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>首页</title>
    <style type="text/css">
      a{
        text-decoration:none;
      }
    </style>
  </head>
  <body>
    <h3>
      <a href="${pageContext.request.contextPath}/book/allBook">进入书籍页面</a>
    </h3>
  </body>
</html>
```

## 2、显示全部书籍（**allBook**.jsp）

**文件位置**：web/WEB-INF/jsp/**allBook**.jsp

<img src="/../SSMimages/前端页面.png" style="zoom: 50%;" />

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>书籍展示</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-12 column">
               <div class="page-header">
                    <h1>
                      <small>书籍列表 —— 显示所有书籍</small>
                    </h1>
                </div>
            </div>
        </div>

        <%-- 添加书籍 与 查询书籍 --%>
        <div class="row">
            <div class="col-md-4 column">
                <a class="btn btn-primary" href="${pageContext.request.contextPath}/book/toAddBook">新增书籍</a>
                <a class="btn btn-primary" href="${pageContext.request.contextPath}/book/allBook">显示全部书籍</a>
            </div>
            <div class="col-md-4 column"></div>
            <div class="col-md-4 column" style="float:right">
                <%--form-inline：表单自动对齐  --%>
                <form class="form-inline" action="${pageContext.request.contextPath}/book/queryBook" method="post">
                    <span style="color:red;font-weight: bold">${error}</span>
                    <input type="text" name="queryBookName" class="form-control" placeholder="请输入要查询的书籍名称">
                    <input type="submit" value="查询" class="btn btn-primary">
                </form>
            </div>
        </div>

        <div class="row clearfix">
            <div class="col-md-12 column">
                <table class="table table-hover table-striped">
                    <head>
                        <tr>
                            <th>书籍编号</th>
                            <th>书籍名字</th>
                            <th>书籍数量</th>
                            <th>书籍详情</th>
                         </tr>
                    </head>
                    <tbody>
                        <c:forEach var="book" items="${list}">
                            <tr>
                                <td>${book.bookID}</td>
                                <td>${book.bookName}</td>
                                <td>${book.bookCounts}</td>
                                <td>${book.detail}</td>
                                <td>
                                    <a href="${pageContext.request.contextPath}/book/toUpdate?id=${book.bookID}">修改</a>
                                    &nbsp;&nbsp;
                                    <%--删除使用：result风格--%>
                                    <a href="${pageContext.request.contextPath}/book/deleteBook/${book.bookID}">删除</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
```

## 3、新增书籍（**addBook**.jsp）

**文件位置**：web/WEB-INF/jsp/**addBook**.jsp

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/27
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>增加书籍</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container">
        <div class="col-md-12 column">
            <div class="page-hreader">
                <h1>
                    <small>新增书籍</small>
                </h1>
            </div>
        </div>

        <form action="${pageContext.request.contextPath}/book/addBook" method="post">
            <div class="form-group">
                <label for="bkname">书籍名称</label>
                <%-- required ：表示必须要填写 --%>
                <input type="text" name="bookName" class="form-control" id="bkname" required>
            </div>
            <div class="form-group">
                <label for="bcount">书籍数量</label>
                <input type="text" name="bookCounts"  class="form-control" id="bcount" required>
            </div>
            <div class="form-group">
                <label for="bshow">书籍描述</label>
                <input type="text" name="detail" class="form-control" id="bshow" required>
            </div>
            <div class="form-group">
                <input type="submit"  class="form-control" value="提交">
            </div>
        </form>
    </div>
</body>
</html>
```

## 4、修改书籍（**updateBook**.jsp）

**文件位置**：web/WEB-INF/jsp/**updateBook**.jsp

```jsp
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2021/3/27
  Time: 21:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改书籍</title>
    <link href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="col-md-12 column">
        <div class="page-hreader">
            <h1>
                <small>修改书籍</small>
            </h1>
        </div>
    </div>

    <form action="${pageContext.request.contextPath}/book/updateBook" method="post">
        <%--前端传递：隐藏域 --%>
        <input type="hidden" name="bookID" value="${QBook.bookID}">

        <div class="form-group">
            <label for="bkname">书籍名称</label>
            <%-- required ：表示必须要填写 --%>
            <input type="text" name="bookName" class="form-control" value="${QBook.bookName}" id="bkname" required>
        </div>
        <div class="form-group">
            <label for="bcount">书籍数量</label>
            <input type="text" name="bookCounts"  class="form-control" value="${QBook.bookCounts}" id="bcount" required>
        </div>
        <div class="form-group">
            <label for="bshow">书籍描述</label>
            <input type="text" name="detail" class="form-control" value="${QBook.detail}" id="bshow" required>
        </div>
        <div class="form-group">
            <input type="submit"  class="form-control" value="修改">
        </div>
    </form>
</div>
</body>
</html>
```

# 五、新增一个业务流程（查询书籍）

<img src="/../SSMimages/新增业务.png" style="zoom: 33%;" />

​	**思路：从下往上修改**
​		第一步：在dao层添加相关接口以及实现（...Mapper.xml）

​		第二步：在业务层中添加对应的接口以及实现（**结合dao层**）

​		第三步：在controller层添加对应的控制器，用于接收来自用户的请求（**结合service层**）

## 1、dao层添加方法

​	文件：java/com/kuang/dao/**BookMapper.java（接口）**

```java
// 查询指定的书本
Books queryBookByName(@Param("bookName")String bookName);
```

## 2、dao层对应的实现类

​	文件：resources/com/kuang/dao/**BookMapper.xml（类似接口实现类）**

```xml
<!--根据书名查询书籍-->
<select id="queryBookByName" resultType="Books">
    select * from ssmbuild.books where bookName = #{bookName};
</select>
```

## 3、service层添加对应的方法

​	文件：java/com/kuang/service/**Bookservice.java（接口）**

```java
// 查询指定的书本
Books queryBookByName(String bookName);
```

## 4、对业务层的接口进行实现

​	文件：java/com/kuang/service/**BookserviceImpl.java（接口实现类）**

```java
public Books queryBookByName(String bookName) {
    return  bookMapper.queryBookByName(bookName);
}
```

## 5、修改前端页面

```jsp
<form class="form-inline" action="${pageContext.request.contextPath}/book/queryBook" method="post">
    <span style="color:red;font-weight: bold">${error}</span>
    <input type="text" name="queryBookName" class="form-control" placeholder="请输入要查询的书籍名称">
    <input type="submit" value="查询" class="btn btn-primary">
</form>
```

6、控制器

​	文件：java/com/kuang/controller/**BookController**.java

```java
// 查询一本书
@RequestMapping("/queryBook")
public String queryBook(String queryBookName,Model model){
    Books books = bookService.queryBookByName(queryBookName);
    /**
     * ==> Parameters: java(String)
     * <==    Columns: bookID, bookName, bookCounts, detail
     * <==        Row: 1, Java, 11, 从入门到放弃
     * <==        Row: 4, java, 11, 入门到放弃
     * <==      Total: 2
     *
     * 若是数据库中存在：两条一样的数据，则会出错，解决方法：让返回的值为：list即可
     * List<Books> queryBookByName(@Param("bookName")String bookName);
     */
    System.out.println("书籍为："+books);
    List<Books> list = new ArrayList<Books>();
    if(books==null){ // 如果书籍不存在，则将全部的书籍放入：list中
        list = bookService.queryAllBook();
        model.addAttribute("error","未查询到");
    }else{
        list.add(books);
    }
    System.out.println("查询的书籍为："+list);
    model.addAttribute("list",list);
    return "allBook";
}
```



# 六、异常

## 1、web项目是否添加：lib文件

<img src="/../SSMimages/添加lib文件.png" style="zoom:50%;" />

## 2、排错

**检查问题：**
     1、查看这个Bean注入 **是否成功**！
     2、Junit **单元测试**，看代码是否能查询出结果！
     3、检查Spring是否除了问题
     4、SpringMVC，整合的时候**有没有**调用到我们的service层
	1、applicationContext.xml 没有注入bean
	2、web.xml中，也需要绑定最终的xml文件（**applicationContext.xml** ）

## 3、后续若重新加了依赖包，必须重新给Web工程的lib重新导入

异常：Cannot resolve reference to bean **'txPointCut**' while setting bean property 'pointcut'; nested exception is org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'txPointCut':

```xml
<!--导入事务aop-->
<dependency>
    <groupId>org.aspectj</groupId>
    <artifactId>aspectjweaver</artifactId>
    <version>1.9.4</version>
</dependency>
```

<img src="/../SSMimages/添加lib文件.png" style="zoom: 25%;" />