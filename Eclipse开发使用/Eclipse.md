---
typora-root-url: images
---

# Eclipse

# 一 配置Maven

**第一步：**下载好对应版本的`maven`

**第二步：**打开Eclipse，选择：

​				`Window - Maven - Installations - add - （下载好的maven路径）`

**第三步：**配置user settings：	

​				`Window - Maven - User Settings- Global Settings- （下载好的maven路径 - conf - settings`

# 二 Eclipse的Spring Boot插件