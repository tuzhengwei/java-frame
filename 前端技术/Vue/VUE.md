---
typora-root-url: images
---

# VUE

# 一 MVVM

## 1 概念

​	VVM（Model-View-ViewModel）是一种**软件设计模式**，由微软WPF（用于替代WinForm，以前就是用这个技术开发桌面应用程序的）和Silverlight（类似于Java Applet，简单点说就是在浏览器上运行WPF）的架构师Ken Cooper和Ted Peters开发，**是一种简化用户界面的事件驱动编程方式**。
​	MVVM源自于经典的MVC（Model-View-Controller）模式。MVVM的**核心是ViewModel层**，负责转换Model中的数据对象来让数据变得更容易管理和使用。其作用如下：

- 该层向上与视图层进行**双向数据绑定**
- 向下与Model层通过**接口请求**进行数据交互

![](/../images/mvvm.png)

>  View层展现的：**不是Model层的数据， 而是ViewModel的数据**， 由ViewModel负责**与Model层交互**

## 2 优点

MVVM模式和MVC模式一样，主要目的是**分离视图（View）和模型（Model）**，有几大好处

​	**低耦合：**视图（View）可以独立于Model变化和修改，一个ViewModel可以**绑定到不同的View上**，当View变化的时候Model可以不变，当Model变化的时候View也可以不变。
​	**可复用：**你可以把一些视图逻辑放在一个ViewModel里面，让很多View重用这段视图逻辑。
​	**独立开发：**开发人员可以专注于业务逻辑和数据的开发（ViewMode），设计人员可以专注于页面设计。
​	**可测试：**界面素来是比较难以测试的，而现在测试可以针对ViewModel来写。
<img src="/../images/V.png" style="zoom: 80%;" />

## 3 三层介绍

### 3.1 View

​	View是视图层， 也就是用户界面。**前端主要由HTH L和csS来构建**， 为了更方便地展现vi eu to del或者Hodel层的数据， 已经产生了各种各样的前后端模板语言， 比如FreeMarker，Thyme leaf等等， 各大MV VM框架如Vue.js.Angular JS， EJS等也都有自己用来构建用户界面的内置模板语言。

### 3.2 Model

​	Model是指数据模型， 泛指后端进行的各种业务逻辑处理和数据操控， **主要围绕数据库系统展开**。这里的难点主要在于需要和前端约定统一的接口规则。

### 3.3 ViewModel

​	ViewModel是由前端开发人员组织生成和维护的**视图数据层**。

​	在这一层， 前端开发者对从后端获取的Model数据进行转换处理， 做二次封装， 以生成符合View层使用预期的视图数据模型。

---

> 需要注意的是View Model所封装出来的数据模型包括**视图的状态和行为**两部分， 而Model层的数据模型是只包含状态的
>
> 比如页面的这一块展示什么，那一块展示什么这些都属于视图状态(展示)
> 页面加载进来时发生什么，点击这一块发生什么，这一块滚动时发生什么这些都属于视图行为(交互)
> 视图状态和行为都封装在了View Model里。这样的封装使得View Model可以完整地去描述View层。

​	由于实现了双向绑定， View Model的内容会实时展现在View层， 这是激动人心的， 因为前端开发者**再也不必低效又麻烦地通过操纵DOM去更新视图**。
  MVVM框架已经把最脏最累的一块做好了， 我们开发者只需要处理和维护View Model， 更新数据视图就会自动得到相应更新，真正实现事件驱动编程。

---

  View层展现的：**不是Model层的数据， 而是ViewModel的数据**， 由ViewModel负责**与Model层交互**， 这就**完全解耦了View层和Model层**， 这个解耦是至关重要的， 它是前后端分离方案实施的重要一环。

# 二 初步认识Vue

​	是一套用于**构建用户界面的渐进式框架**， 发布于2014年2月。与其它大型框架不同的是， Vue被设计为可以**自底向上逐层应用**。Vue的核心库**只关注视图层**， 不仅易于上手， 还便于与第三方库(如：vue-router，vue-resource，vue x) 或既有项目整合。

## 1 MVVM模式的实现者

- **Model：模型层**， 在这里表示JavaScript对象

- **View：视图层**， 在这里表示DOM(HTML操作的元素)

- **ViewModel：**连接**视图和数据**的中间件， Vue.js就是MVVM中的View Model层的实现者

  ​		在MVVM架构中， 是不允许数据和视图直接通信的， 只能通过ViewModel来通信， 而View Model就是定义了一个**Observer观察者：**

​		（1）ViewModel能够**观察**到**数据**的变化， 并对视图对应的**内容进行更新**
​		（2）ViewModel能够**监听**到**视图**的变化， 并能够通知**数据发生改变**

----

Vue.js就是一个MV VM的实现者， 核心：**实现了DOM监听与数据绑定**

----

## 2 优点

	-	**轻量级**， 体积小是一个重要指标。Vue.js压缩后有只有20多kb(Angular压缩后56kb+，React压缩后44kb+)
	-	**移动优先**。更适合移动端， 比如移动端的Touch事件
	-	**易上手**，学习曲线平稳，文档齐全
 -	吸取了Angular(模块化) 和React(虚拟DOＭ) 的长处， 并**拥有自己独特的功能**，如：计算属性
   开源，社区活跃度高
	-	...

## 3 第一个Vue程序

### 3.1 安装Vue插件

<img src="/../images/first.png" style="zoom:50%;" />

### 3.2 添加vue文件模板

<img src="/../images/vue文件模型.png" style="zoom: 50%;" />

```html
<template>
  <div>
  #[[$END$]]#
  </div>
</template>

<script scoped>
  export default {
    name: "${COMPONENT_NAME}",
    props: {
    },
    components: {},
    computed: {},
    data() {
      return {
      }
    },
    methods: {
    },
    mounted() {
    },
    created() {
    }
  }
</script>

<style scoped>
 
</style>
```

添加文件模板后，后续创建后缀为：.vue的文件为：

<img src="/../images/模板结果.png" style="zoom: 50%;" />

### 3.3 vue下载地址

开发版本
	包含完整的警告和调试模式：https：//yuejs.org/js/vue.js
	删除了警告， 30.96KBmin+gzip：https：//vuejs.org/js/vue.min.js

---

CDN

```html
<script src=“https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.js”></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.min.js"></script>
```

### 3.4 创建一个HTML文件

> 使用：ES6**（settings ——> Language & Frameworks ——>JavaScript）**

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--导入vue-->
    <script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js"></script>
</head>
<body>
    <!--view层 模板-->
    <div id="app">
        {{message}}
    </div>

    <script>
        'use strict'
        /*Model 数据*/
        let vue = new Vue({
            el: "#app",
            data: {
                message: 'hello vue!'
            }
        })
    </script>
</body>
</html>
```

**浏览器输出：hello vue!**

### 3.5 vue实例

```html
<script type="text/javascript">
    var vm = new Vue({
        el:"#app",
        /*Model：数据*/
        data:{
            message:"hello,vue!"
        }
    });
</script>
```

- `el: '#vue'`：**绑定元素的ID**
- `data:{message:'Hello Vue!'}`：数据对象中**有一个名为message的属性**，并设置了初始值 Hello Vue！

### 3.6 数据绑定

```html
<!--view层，模板-->
<div id="app">
    {{message}}
</div>
```

​	说明：只需要在绑定的元素中使用**双花括号**将Vue创建的名为message属性包裹起来， 即可实现数据绑定功能， 也就实现了View Model层所需的效果

## 4 vue7大属性

- `el`：**标记**，用来指示vue编译器从什么地方开始解析vue的语法，可以说是一个占位符。
- `data`：用来阻止view中抽象出来的属性，可以说**将视图的数据抽象出来存放在data中**。
- `methods`：放置页面中的**业务逻辑**，js方法一般都放置在methods中。
- `render`：创建真正的Virtual Dom
- `computed`： 用来**计算**
- `watch`：**监听**data中数据的变化 ；function(new , old ){} ; 两个参数，一个返回新值，一个返回旧值 。

# 三 vue基础知识

## 1 基本语法

### 1.1 if

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--导入vue-->
    <script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js"></script>
</head>
<body>
<!--view层 模板-->
<div id="app">
    <h1 v-if="type==='A'">A</h1>
    <h1 v-else-if="type==='B'">B</h1>
    <h1 v-else="type==='C'">C</h1>
</div>

<script>
    'use strict'
    /*Model 数据*/
    let vue = new Vue({
        el: "#app",
        data: {
            message: 'hello vue!',
            type: 'A'
        }
    })
</script>
</body>
</html>
```

> **浏览器输出：A**

### 1.2 for

```html
...
<body>
<!--view层 模板-->
<div id="app">
    <p style='color:blue' v-for="item in items">{{item.message}}</p>
</div>

<script>
    'use strict'
    /*Model 数据*/
    let vue = new Vue({
        el: "#app",
        data: {
            message: 'hello vue!',
            type: 'A',
            items: [
                {message: '小范'},
                {message: '小梁'},
                {message: '小红'}
            ]
        }
    })
</script>
</body>
...
```

> 浏览器输出：
>
> 小范
>
> 小梁
>
> 小红

### 1.3 v-bind

```html
<!--完整语法-->
<a v-bind:href="url">...</a>
<!--缩写-->
<a :href="url">...</a>
```

```html
<!DOCTYPE html>
<html lang="en" xmlns:v-bind="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--导入vue-->
    <script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js"></script>
</head>
<body>
<!--view层 模板-->
<div id="app">
    <span v-bind:title="message">
        message的值
    </span>
</div>

<script>
    'use strict'
    /*Model 数据*/
    let vue = new Vue({
        el: "#app",
        data: {
            message: 'hello vue!'
        }
    })
</script>
</body>
</html>
```

> 效果：

<img src="/../images/v-bind.png" style="zoom:50%;" />

## 2 事件v-on

```html
<!--完整语法-->
<a v-on:click="...">...</a>
<!--缩写-->
<a @click="...">...</a>
```

> 学习网址：https://www.jquery123.com/category/events/

```html
<!DOCTYPE html>
<html lang="en" xmlns:v-on="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--导入vue-->
    <script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js"></script>
</head>
<body>
<!--view层 模板-->
<div id="app">
    <button v-on:click="sayHi">点我</button>
</div>

<script>
    let vue = new Vue({
        el: "#app",
        data: {
            message: 'hello vue!',
            type: 'A',
            items: [{message: '小范'},{message: '小梁'},{message: '小红'}]
        },
        methods: {
            sayHi: function () {
                alert(this.message)
            }
        }
    });
</script>
</body>
</html>
```

## 3 双向绑定v-model

<img src="/../images/组件与实例.png" style="zoom: 67%;" />

> **注意：**
>
> ​	`v-model` ：会**忽略所有表单元素的**：value、checked、selected特性的**初始值**，而总是将`vue`实例的数据作为：数据来源。（可以通过：JavaScript在组件的`data`选项中：**声明初始值！**）

### 3.1 概念

​	**数据双向绑定：**即当数据发生变化时，视图也发生变化；反之，即当视图发生变化时，数据也同步变化；

### 3.2 为什么要实现？

​	在`VUE.JS`中，如果使用：`vuex`，实际上数据还是：单向的，只所以说是：双向绑定，即：**全局性数据流：单项；局部性数据流：双向**。

### 3.3 input

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <!--导入vue-->
    <script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js"></script>
</head>
<body>
<!--view层 模板-->
<div id="app">
   输入： <input type="text" v-model="message"/> {{message}}
</div>

<script>
    'use strict'
    /*Model 数据*/
    let vue = new Vue({
        el: "#app",
        data: {
            message: ""
        }
    })
</script>
</body>
</html>
```

![](/../images/1.png)

### 3.4 textarea

```html
<textarea name="" id="" cols="30" rows="10" v-model="message"></textarea>
```

### 3.5 radio

```html
<body>
<!--view层 模板-->
<div id="app">
   性别：
    男<input type="radio" name="sex" value="男" v-model="sex"/>
    女<input type="radio" name="sex" value="女" v-model="sex"/>

    <p>选中了：{{sex}}</p>
</div>

<script>
    'use strict'
    /*Model 数据*/
    let vue = new Vue({
        el: "#app",
        data: {
            sex:""
        }
    })
</script>
</body>
```

![](/../images/2.png)

### 3.6 select

```html
<body>
    <!--view层 模板-->
    <div id="app">
        选中：
        <select v-model="selected">
            <option value="" disabled>---请选择---</option>
            <option>A</option>
            <option>B</option>
            <option>C</option>
        </select>

        {{selected}}
    </div>

    <script>
        'use strict'
        /*Model 数据*/
        let vue = new Vue({
            el: "#app",
            data: {
                selected: ""
            }
        })
    </script>
</body>
```

![](/../images/3.png)

## 4 组件

> ​	组件是**可复用**的 Vue 实例，且**带有一个名字**

<img src="/../images/组件.png" style="zoom:50%;" />

---

```vue
Vue.component():  注册组件
my-component-li:  自定义组件的名字
template: 组件的模板
props: 传递参数到组件中（默认规则：不能有大写）
```

### 4.1 创建组件

```js
// 创建一个组件
Vue.component("tzw",{
    // 给组件传递参数，需：props,例：'xf'：参数变量
    props: ['xf'],
    template: '<h1>{{xf}}</h1>'
})
```

### 4.2 运用

```html
<body>
    <!--view层 模板-->
    <div id="app">
      <tzw v-for="item in items" v-bind:xf="item"></tzw>
    </div>

    <script>
        Vue.component("tzw",{
            // 给组件传递参数，需：props
            props: ['xf'],
            template: '<h1>{{xf}}</h1>'
        })
        /*Model 数据*/
        let vue = new Vue({
            el: "#app",
            data: {
               items: ["java", "python", "前端"]
            }
        });
    </script>
</body>
```

<img src="/../images/组件显示.png" style="zoom: 67%;" />

---

> 说明：
>
> ​	`v-for="item in items"`：遍历`Vue`实例中的`items`数组，并**创建同等数量**的组件；
>
> ​	`v-bind:xf="item"`：将遍历的`item` 绑定到：给定组件`props`名上（**也就是将item传递到对应的组件中**）；

## 5 Axios

> ​	Axios：一个开源的可以用在浏览器端 和 `node.js`的异步通信框架，主要作用：实现`ajax`异步通信。

### 5.1 特点

- 从浏览器中创建：`XMLHttpRequests`
- 从`node.js`创建`http`请求
- 支持`Promise API`[JS中链式编程]
- 拦截请求和响应
- 转换请求数据和响应数据
- 取消请求
- 自动转换`json`数据
- 客户端支持防御`XSRF`（跨站请求伪造）

> GitHub：https://github.com/axios/axios
>
> 中文文档：http://www.axios-js.com/

### 5.2 为什么使用？

​		由于`Vue.js` 是一个 视图层框架，并且作者严格遵守：`SoC`（关注度分离原则），所以`Vue.js`不包含：`AJAX`的通信功能。

​		为了解决通信问题，作者单独开发了一个名为：`vue-resource`的插件，不过进入2.0版本以后就没有维护了，故推荐：`Axios`。

> 注意：少使用`jQuery`，因为DOM操作太频繁

### 5.3 运用

> 第一步：创建一个data.sjon文件

```json
{
  "name":"百度",
  "url":"http://www.baidu.com",
  "page":66,
  "isNonProfit":true,
  "address":{
    "street":"海定区",
    "city":"北京市",
    "country":"中国"
  },
  "links":[
     {
       "name":"baidu",
       "url":"http://www.baidu.com"
     },
    {
      "name":"Google",
      "url":"http://www.google.com"
    },
    {
      "name":"Sougou",
      "url":"http://www.sougou.com"
    }
  ]
}
```

> 第二步：创建一个HTML文件，引用`json`中的数据
>
> **注意：使用cdn，必须是可以下载的地址**

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <!--引入vue和Axios-->
    <script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <titile>Axios应用程序</titile>
</head>
<body>
    <div id = "app">
        <div>
            名称：{{info.name}}
            <!--
             或者使用：
                 // 让href的值与下方的：info.name进行了一个绑定
                 名称：<a v-bind:href="info.name" target="_blank">{{info.name}}</a>
             -->
        </div>
        <div>url:{{info.url}}</div>
        <div>地址：{{info.address.street}}</div>
        <ul>
            <li v-for="link in info.links">
                {{link.name}}--->{{link.url}} <!--原样输出-->
            </li>
        </ul>
    </div>
    <script>
        let app = new Vue({
            //element的缩写，挂载元素
            el:"#app",
            //data数组 和 data方法
            data(){
                return{
                    info:{ // 接受从：data.json中捕获的数据
                        name:'',
                        url:'',
                        links:[]
                    }
                }
            },
            <!--在'methods'对象中定义方法-->
            mounted(){
                axios
                    .get('../data.json')
                    .then(response => this.info=response.data);
                    // .then(response=>(console.log(response.data)));
                /*
                    对拿到的数据进行数据转换：
                    response：请求响应；
                    this：指向当前Vue实例（就是data里面）；
                    response.data：请求的数据
                 */
            }
        })
    </script>
</body>
</html>
```

<img src="/../images/json.png" style="zoom: 50%;" />

## 6 计算属性

### 6.1 概念

​	**计算属性：**是一个能够将：计算结果缓存起来的属性（**将行为转化成：静态属性**），可以理解为：缓存。

> **注意：**methods 和 computed 里的东西**不能重名**
>
> **若重名，根据优先级，则会调用：methods里的方法**

---

​	**说明：**

	-	`methods`：定义方法，例：show()，**需要带括号**
	-	`computed`：定义计算属性，例：show，**不需要带括号**，`this.message`：为让show观察到数据变化而变化

----

​	结论：

​	调用方法时，每次都需要计算，既然有计算的过程，则必定产生系统开销。（若**结果不经常变化，则可以考虑将这个结果缓存起来**）

### 6.2 特点

​	为了将不经常变化的计算结果进行缓存，以节约系统的开销。

### 6.3 运用

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>hello</title>
    <!--引入vue和Axios-->
    <script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
</head>
<body>
    <div id = "app">
        <p>执行show方法：{{show1()}}</p>
        <p>执行show方法：{{show2}}</p>
    </div>
    <script>
        let app = new Vue({
            //element的缩写，挂载元素
            el:"#app",
            data:{ 
                message: "hello world"
            },
            methods:{
                // 若方法名与computed中存在同名，默认调用：方法
                show1: function(){
                    return Date.now(); // 获取当前的时间
                }
            },
            computed:{
                show2:function () {
                    return Date.now();
                }
            }
        })
    </script>
</body>
</html>
```

## 7 内容分发（插槽）

​	在`vue.js`中我们使用：`<slot>`元素**作为承载分发内容的出口**，称其为：插槽，可以应用在**组合组件的场景中**。

### 7.1 案例

利用插槽实现以下效果：

```html
<div>
   <p>列表书籍</p>
    <ul>
        <li>Java</li>
        <li>Linux</li>
        <li>Python</li>
    </ul>
</div>
```

插槽实现：

```html
<body>
    <div id ="app">
        <!--
            例：app是一个主组件，内部含有：空缺部分（插槽）
               空缺部分则可以自定义组件（插槽），查漏补缺。
        -->
        <todo>
            <todo-title slot="todo-title" v-bind:title="title"></todo-title>
            <todo-items slot="todo-items" v-for="item in data_items" v-bind:item="item"></todo-items>
        </todo>
    </div>
    <script>
        Vue.component("todo",{
            // slot ：定义插槽
            template: '<div>\
                            <slot name="todo-title"></slot>\
                             <ul>\
                                <slot name="todo-items"></slot>\
                             </ul>\
                           </div>'
        });
        Vue.component("todo-title",{
            props: ['title'],
            template: '<div>{{title}}</div>'
        });
        Vue.component("todo-items",{
            props:['item'],
            template: '<li>{{item}}</li>'
        });
        var vue = new Vue({
            //element的缩写，挂载元素
            el:"#app",
            data:{
                title:"英语",
                data_items:["Java","Python","Linux"]
            }
        })
    </script>
</body>
```

### 7.2 自定义事件内容分发

​	通过以上代码可发现，**数据项在`vue`的实例中，但删除操作要在组件中完成**

<img src="/../images/组件与实例.png" style="zoom:67%;" />

**思考：那么组件如何才能删除`vue`实例中的数据呢？**

此时涉及到：**参数传递与事件分发**，`vue`为我们提供了自定义事件的功能，使用：`this.$emit('自定义事件名',参数)`

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset = "utf-8">
    <title>hello</title>
    <!--引入vue和Axios-->
    <script src="https://cdn.bootcdn.net/ajax/libs/vue/2.6.9/vue.min.js"></script>
    <script src="https://cdn.bootcdn.net/ajax/libs/axios/0.1.0/axios.min.js"></script>
</head>
<body>
    <div id ="app">
        <!--
            例：app是一个主组件，内部含有：空缺部分（插槽）
               空缺部分则可以自定义组件（插槽），查漏补缺。
        -->
        <todo>
            <todo-title slot="todo-title" v-bind:title="title"></todo-title>
            <todo-items slot="todo-items" v-for="(item,index) in data_items"
                        v-bind:item="item" v-bind:index="index"
                        v-on:remove1="removeItem(index)"></todo-items>
        </todo>

    </div>
    <script>
        Vue.component("todo",{
            // slot ：定义插槽
            template: '<div>\
                            <slot name="todo-title"></slot>\
                             <ul>\
                                <slot name="todo-items"></slot>\
                             </ul>\
                           </div>'
        });

        Vue.component("todo-title",{
            props: ['title'],
            template: '<div>{{title}}</div>'
        });
        Vue.component("todo-items",{
            // 获取数据某项数据元素和对应的下标
            props:['item','index'],
            template: '<li>{{item}}<button v-on:click="remove">删除</button></li>',
            methods:{
                remove: function(index){
                 /*
                    这里的自定义事件名 不能跟 实例中的方法名一样
                 */
                   this.$emit('remove1',index);
                }
            }
        });
        var vue = new Vue({
            //element的缩写，挂载元素
            el:"#app",
            data:{
                title:"英语",
                data_items:["Java","Python","Linux"]
            },
            methods:{
                // 删除数组元素
                removeItem: function(index){
                    this.data_items.splice(index,1);
                }
            }
        })
    </script>
</body>
</html>
```

# 四 Vue-cli

​	`vue-cli`官方提供的一个**脚手架**，用于**快速生成**一个：`vue`的项目模板；

​	预先定义好的：目录结构及基础代码，类似：`maven`项目可选骨架项目。

---

## 1 主要功能

-	统一的目录结果
-	本地调试
-	热部署
-	单元测试
-	集成打包上线

## 2 安装环境

**第一步：**安装`Node.js`和 `Git`

`Node.js` ：http://nodejs.cn/download/

`Git`：https://git-scm.com/downloads

镜像：https://npm.taobao.org/mirrors/git-for-windows/

----

**第二步：**确认`Node.js`安装成功

`cmd`下输入：`node -v`，查看是否可以正确打印版本号

`cmd`下输入：`nmp -v`，是否可以正确打印版本号

> npm ：软件包管理工具

----

**第三步：**安装`Node.js` **淘宝镜像加速器**（`cnpm`）

`cmd`下输入：

```cmd
# -g ：全局安装
# 淘宝镜像加速器
C:\Users\Lenovo>npm install cnpm -g

# 安装vue-cli 
C:\Users\Lenovo>cnpm install vue-cli -g

 # 安装webpack （建议全局安装(npm)
C:\Users\Lenovo>cnpm install -g webpack
C:\Users\Lenovo>cnpm install -g webpack-cli
```

**安装目录：**

` C:\Users\Lenovo\AppData\Roaming\npm\node_modules\cnpm`

`C:\Users\Lenovo\AppData\Roaming\npm\node_modules\vue-cli`

` C:\Users\Lenovo\AppData\Roaming\npm\webpack`

---

**第四步：**检测第三步是否安装成功

```cmd
C:\Users\Lenovo>vue list
```

```cmd
 Available official templates:

  ★  browserify - A full-featured Browserify + vueify setup with hot-reload, linting & unit testing.
  ★  browserify-simple - A simple Browserify + vueify setup for quick prototyping.
  ★  pwa - PWA template for vue-cli based on the webpack template
  ★  simple - The simplest possible Vue setup in a single HTML file
  ★  webpack - A full-featured Webpack + vue-loader setup with hot reload, linting, testing & css extraction.
  ★  webpack-simple - A simple Webpack + vue-loader setup for quick prototyping.
```

## 3 创建Vue项目

**第一步：**创建一个文件夹

`F:\Vue`

---

**第二步：**以**管理员身份**运行：`cmd`， `cmd`进入到`F:\Vue`

```cmd
C:\Users\Lenovo>f:

F:\>cd vue
# myvue：项目名
F:\Vue>vue init webpack vue_project_demo
```

> **注意：**
>
> ​	若报：`vue-cli · Failed to download repo vuejs-templates/webpack: read ECONNRESET`异常
>
> ​	原因：因为webpack项目过大，下载时网络问题很可能出现问题，使用vue init webpack myProject的时候就会出现报错
>
> ​	解决办法：更换网络或自己开热点

---

**第三步：**选择需要安装的插件

> 注意：因为是初次安装，**全部选择：NO**

<img src="/../images/cmd.png" style="zoom: 67%;" />

> 作者：tuzhengwei
>
>  Vue独立生成
>
> 安装vue路由器？不
>
> 使用ESLint来lint代码？不
>
> 设置单元测试编号
>
> 用Nightwatch设置e2e测试？不
>
> 在项目创建之后，我们是否应该为您运行“npm install”(（推荐）否

----

**第四步：** **手动安装**第三步自动安装的文件

> 根据：`package.json` 文件 **所配置的信息** 进行安装

```cmd
# 若修改了package中的版本，则需要重新输入：cnpm install
F:\Vue\vue_project_demo>npm install		# 安装依赖
```

```cmd
# 若安装依赖 出现错误，根据提示信息，进行修复
...
found 51 vulnerabilities (3 low, 42 moderate, 6 high)
  run `npm audit fix` to fix them, or `npm audit` for details
```

```cmd
F:\Vue\vue_project_demo>npm audit fix	# 修复
```

----

**第五步：** 启动项目

```cmd
# cmd界面运行
F:\Vue\vue_project_demo>npm run dev

> vue_project_demo@1.0.0 dev F:\Vue\vue_project_demo	# 打包
> webpack-dev-server --inline --progress --config build/webpack.dev.conf.js # 回车 运行
```

> http://localhost:8080

----

**第六步：**退出项目

`ctrl + c` 退出

---

# 五 WebPack

​	`WebPack`：一个现代的`JavaScript`应用程序的**静态模块打包器**。

​	当`WebPack`处理应用程序时，会**递归**地创建一个依赖关系图，其中包含应用程序需要的每个模块，然后将这些模块打包成：一个或多个`bundle`。

​	`WebPack`是当下最热门的前端资源模块化**管理和打包工具**，它可以**将许多松散耦合的模块按照依赖和规则打包**成符合生成环境部署的前端资源。

​	可以将**按需加载**的模块进行代码分离，**等实际需要时，再异步加载**。通过`loader`转换，任何形式的资源都可以当做模块，例如：CommonsJS、AMD、ES6、CSS...

----

## 1 模块化的演进

### 1.1 Script标签

```html
<script src="module.js"></script>
```

​	原始加载`JavaScript`文件加载方式，不同模块的调用都是一个作用域。

​	缺点：

	-	全局作用域下容易造成：**变量冲突**
	-	文件只能按照`<script>`的书写**顺序加载**
	-	开发人员必须主观解决模块和代码块的依赖关系
	-	在大型项目中各种资源难以管理，日积月累，代码块混乱

### 1.2 CommonsJS

​	服务器端的`Node.js`遵循`CommonsJS`规范。

​	**核心思想：**

​		允许模块通过：`require` 方法来：**同步加载**所需依赖的其他模块，然后通过：`exports` 或 `module.exports`来导出：**需要暴露的接口**。

```
require("module");
require("../module.js");
export.doStuff = function(){};
module.exports = someValue;
```

​	**优点：**

	-	服务器端模块便于重用
	-	NPM中已有超过45万个可以使用的模块包

---

​	**缺点：**

	-	同步的模块加载方式**不适合在浏览器环境中**（同步意味：阻塞加载，浏览器资源：异步加载）
	-	不能非阻塞的并行加载多个模块

----

### 	1.3 AMD

​		`Asynchronous Moudle Definition`规范其实主要是：一个接口`define(id?,dependencies?,factory);` 它要在声明模块的时候指定所有依赖，并且还要当做：薪酬 传递到：`factory`中，对于依赖的默默看提前执行。

```js
define("module",["dep1","dep2"],function(d1,d2){
	return someExprotedValue;
})
require(["module","../file.js"],function(module,file){})
```

​	**优点：**

	-	适合在浏览器环境中异步加载模块
	-	可以并行加载多个模块

---

​	**缺点：**

	-	提高了开发成本，代码的阅读和书写比较困难，模块定义方式的语义不畅
	-	不符合通用的模块化思维方式

----

### 	1.4 CMD

### 	1.5 ES6模块

​		增加了`JavaScript`语言层面的：**模块体系定义**。

​		`ES6`模块：使编译时就能确认模块的依关系，以及输入和输出的变量。

​	**优点：**

	-	容易进行静态分析
	-	面向未来的`EcmaScript`标准

----

​	**缺点：**

	-	原生浏览器端还没有实现该标准
	-	全新的命令，新版的：`Node.js`才支持

---

## 2 安装

```cmd
C:\Users\Lenovo>npm install webpack -g
C:\Users\Lenovo>npm install webpack-cli -g
```

**测试安装成功：**

```cmd
C:\Users\Lenovo>webpack -v

C:\Users\Lenovo>webpack-cli -v
```

## 3 打包配置

创建`webpack.config.js` 配置文件

- `entry`：入口文件，指定`WebPack`用哪个文件作为项目的入口
- `output`：输出，指定`WebPack`把处理完成的文件放置到指定路径
- `module`：模块，用于处理各种类型的文件
- `plugins`：差价，若：热更新、代码重用等
- `resolve`：设置路径指向
- `watch`：监听，用于设置文件改动后直接打包

----

```js
module.exports={
    entry: "",
    output:{
        path: "",
        filename: ""
    },
    module:{
        loaders:[
            {test: /\.js$/, loader: ""}
        ]
    },
    plugins: {},
    resolve: {},
    watch: true
}
```

## 4 使用

**第一步：**在任意目录下，创建一个：文件夹

例如：`F:\Vue\webpeak-study`

---

**第二步：**使用`IDEA`打开`webpeak-study`文件

右击——创建一个：Directory（名：module），放置：JS模块等资源文件

---

**第三步：**创建一个：`hello.js` 、`main.js`

```js
// hello.js
// 暴露一个方法
exports.sayHi = function(){
    document.write("<h1>Vue程序</h1>")
};
```

```js
// main.js
// 引入js文件(hello.js)，并访问内部方法
var hello = require("./hello");
hello.sayHi();
```

---

**第四步：**创建一个`webpack.config.js`文件

```js
module.exports = {
  entry: './module/main.js',
  output:{
      filename: "./js/bundle.js"
  }
};
```

---

**第五步：**打包

```cmd
# 控制台（terminal）输入
F:\Vue\webpeak-study>webpack
```

---

**第六步：**创建一个Index.html文件

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>测试</title>
</head>
<body>
    <script src="dist/js/bundle.js"></script>
</body>
</html>
```

效果：

<img src="/../images/webpack.png" style="zoom:67%;" />

---

**第七步：**更新

```cmd
# 监听，若有变化，则实时更新
F:\Vue\webpeak-study>webpack --watch
```

# 六 vue-router理由

​	`vue router`：是`vue.js`官方的**路由管理器**。

## 1 功能

	-	嵌套的路由/视图表
	-	模块化的、基于组件的路由配置
	-	路由参数、查询、通配符
	-	基于`vue.js`过度系统的视图过渡效果
	-	细粒度的导航控制
	-	带有自动激活的`CSS class`的链接
	-	HTML5 历史模式  或 hash 模式，在IE9 中能自动降级
	-	自定义的滚动条行为

## 2 安装

​	基于第一个`vue-cli`进行测试学习，先查看：`node_modules`中是否存在：`vue-router`。

​	`vue-router`是一个插件包，故使用：`npm/cnpm`安装。

```cmd
# 在当前项目下安装
F:\Vue\vue_project_demo>npm install vue-router --save-dev
# 若安装出错，根据提示进行修复
```

## 3 路由结构

`router/.. `：存放路由

```js
const routes = [
  {
      path:'',
      name:"", // 路由名
      redirect:'/home'  //若需要页面重定向
  },
  {
      path:'/home',
      component: () => import("../views/mill/Index"), //建立标准路由对应组件
      childrens:[      //若是使用嵌套路由
          {
              path:'new',
              name: "Index",
        	  component: () => import("../views/index/Index")
          }
      ]
  },
  ....
]
```

## 4 运用

![](/../images/项目目录.png)

----

**第一步：**在main.js中**配置路由**

```js
// 导入组件
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import router from './router'  // 自动扫描内部路由配置

Vue.config.productionTip = false

// 显示声明，使用：VueRouter
Vue.use(VueRouter);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  // 配置路由
  router,
  components: { App },
  template: '<App/>'
})
```

**第二步：**创建两个组件

`Content.vue`

```vue
<template>
    <h1>内容页</h1>
</template>

<script>
    export default {
        name: "Content"
    }
</script>

<style scoped>

</style>
```

`Main.vue`

```vue
<template>
    <h1>首页</h1>
</template>

<script>
    export default {
        name: "main"
    }
</script>

<style scoped>

</style>
```

**第三步：**路由处理，在router文件下，创建一个：index.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'

import Content from '../components/Content'
import Main from '../components/Main'

// 安装路由
Vue.use(VueRouter);

// 配置导出路由
export  default  new VueRouter({
  routes:[
    {
      //路由路径
      path: '/content',
      name: 'content',
      //跳转的组件
      component: Content
    },
    {
      //路由路径
      path: '/main',
      name: 'main',
      //跳转的组件
      component: Main
    }
  ]
});
```

**第四步：**App首页

```vue
<template>
  <div id="app">
    <h1>首页</h1>
    <!--点击，根据路由设置，调转到对应的组件，并显示组件的内容-->
    <router-link to="/main">首页</router-link>
    <router-link to="/content">内容</router-link>
    <router-view></router-view>
  </div>
</template>

<script>

export default {
  name: 'App'
}
</script>

<style>
#app {
  font-family: 'Avenir', Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>
```

<img src="/../images/首页.png" style="zoom: 50%;" />

---

# 七 ElementUI实战

​	学习网站：https://element.eleme.cn/#/zh-CN/component/installation

​	**组件库**

---

## 1 创建工程

```cmd
F:\Vue\vue_project_demo>vue init webpack hello-vue
```

## 2 安装依赖

```cmd
# 安装 vue—router
F:\Vue>cd hello-vue>npm install vue-router --save-dev
# 安装 element-ui -S
F:\Vue>cd hello-vue>npm i element-ui -S
# 安装依赖
# 若修改了package中的版本，则需要重新输入：cnpm install
F:\Vue>cd hello-vue>npm install
# 安装SASS加载器（npm安装失败，使用cnpm安装）
F:\Vue>cd hello-vue>cnpm install sass-loader --save-dev
F:\Vue>cd hello-vue>cnpm install node-sass  --save-dev

# 多个配置安装
F:\Vue>cd hello-vue>cnpm install sass-loader node-sass --save-dev
```

```cmd
# 测试
F:\Vue>cd hello-vue>npm run dev
```

## 3 NPM命令解释

- `npm install moduleName`：安装模块到项目目录下
- `npm install -g moduleName：` -g：将模块安装到全局，具体安装的位置——根据`npm config prefix`的位置
- `npm install --save moduleName`：--save：将模块安装到项目目录下，并在`package`文件的`dependencies`结点写入依赖，-S：该命名的缩写
- `npm install --save-dev moduleName`：--save-dev：将模块安装到项目目录下，并在`package`文件的`dependencies`结点写入依赖，-D：该命名的缩写

## 4 写一个登录实例

网址：https://blog.csdn.net/crazy__qu/article/details/81298623?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162082574416780269858666%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fblog.%2522%257D&request_id=162082574416780269858666&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~blog~first_rank_v2~rank_v29-1-81298623.nonecase&utm_term=vue&spm=1018.2226.3001.4450