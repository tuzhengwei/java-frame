---
typora-root-url: images
---

# TypeScript

# 一 VSCode安装和基本配置

## 1 安装

**第一步：**官网：https://code.visualstudio.com/   下载VS Code

**第二步：**汉化

​	安装后，配置**中文版**界面，安装个插件`Chinese`就可以

（打开VSCode之后**在编辑器左侧**找到这个拓展按钮，点击，然后在搜索框内搜索关键字"Chinese"，这里图中第一个插件就是。直接点击install安装，安装完成后重启VSCode即可。）

<img src="/../images/chinese中文插件.png" style="zoom: 33%;" />

---

**第三步：**编辑器配置

​	有一些编辑器相关配置，需要**在项目根目录下创建一个`.vscode`文件夹**，然后在这个文件夹创建一个`settings.json`文件，编辑器的配置都放在这里。且你还需要安装一个插件[EditorConfig for VS Code](https://marketplace.visualstudio.com/items?itemName=EditorConfig.EditorConfig)这样配置才会生效。配置文件里我们来看几个简单而且使用的配置：

> 打开VSCode之后**在编辑器左侧**找到这个拓展按钮，点击，然后在搜索框内搜索关键字"EditorConfig for VS Code"

```json
{
    "tslint.configFile": "./tslint.json",
    "tslint.autoFixOnSave": true,
    "editor.formatOnSave": true
}
```

`tslint.configFile`用来指定tslint.json文件的路径，注意这里是相对根目录的；

`tslint.autoFixOnSave`设置为true则每次保存的时候编辑器会自动根据我们的tslint配置对不符合规范的代码进行自动修改；

`tslint.formatOnSave`设为true则编辑器会对格式在保存的时候进行整理。

## 2 TypeScript相关插件

> 打开VSCode之后<button>**在编辑器左侧**</button>找到这个拓展按钮，点击，然后在搜索框内**搜索插件关键字**

​	`TSLint(deprecated)`是一个通过tslint.json配置在你写TypeScript代码时，**对代码风格进行检查和提示的插件**。

​	`TSLint Vue`加强了**对Vue中的TypeScript语法语句进行检查的能力**。

## 3 框架相关

​	如果使用`Vue`进行项目开发，那**Vue相关的插件**也是需要的，比如：`Vue 2 Snippets`。`Vetur`插件是`Vue`的**开发辅助工具**，安装它之后会得到代码高亮、输入辅助等功能。

## 4 提升开发体验

​	`Auto Close Tag`插件会**自动补充HTML闭合标签**，比如你输完<button>的后面的尖括号后，插件会自动帮你补充</button>；

​	`Auto Rename Tag`插件会在修改HTML标签名的时候，**自动把它对应的闭标签同时修改掉**；

​	`Bracket Pair Colorizer`插件会将你的括号一对一对地用颜色进行区分。

​	`Guides`插件能够**在代码缩进的地方用竖线展示**出索引对应的位置，而且点击代码，它还会将统一代码块范围的代码用统一颜色竖线标出

## 5 终端

​	在VSCode中有终端窗口，点击菜单栏的【查看】-【终端】，也可以使用快捷键 <button>control+`</button>打开。这样可以直接在编辑器运行启动命令，启动项目，边写代码边看报错。

## 6 用户代码片段

​	经常用到的**重复的代码片段**，可以使用`用户代码片段`配置，这样每次要输入这段代码就不用一行一行敲了，直接输入几个标示性字符即可。

​	**在VSCode左下角**有个设置按钮，点击之后选择【用户代码片段】：

​	（1）在弹出的下拉列表中可以选择【**新建全局代码片段文件**】，这样创建的代码片段是**任何项目都可用的**；

​	（2）可以选择【**新建"项目名"文件夹的代码片段文件**】，这样创建的代码片段**只在当前项目可用**。创建代码片段文件后它是一个类似于json的文件，文件有这样一个示例。

我们来看一下其中的几个关键参数：

- `Print to console`是要显示的提示文字
- `scope`是代码片段作用的语言类型
- `prefix`是你输入这个名字之后，就会出现这个代码片段的选项回车即可选中插入
- `body`就是你的代码片段实体
- `$1`是输入这个片段后光标放置的位置，这个$1不是内容，而是一个占位
- `description`是描述。如果你要输入的就是字符串`lt;$`

# 二 搭建开发环境

## 1 node.js

​	确定电脑有node的环境，如果你没有安装过node，先去[Node.js下载地址](https://nodejs.org/en/download/)下载对应你系统的node.js安装包，下载下来进行安装。

```cmd
npm install -g tslint
```

​	如果这个模块要作为项目依赖安装，**去掉-g参数即可**。更多关于node的知识，你可以参考[node官方文档](https://nodejs.org/dist/latest-v10.x/docs/api/)或[node中文文档](http://nodejs.cn/api/)，更多关于npm的使用方法，可以参考[npm官方文档](https://docs.npmjs.com/)或[npm中文文档](https://www.npmjs.com.cn/)。

## 2 初始化项目

​	**新建一个文件夹**“client-side”，作为**项目根目录**，进入这个文件夹：

### 2.1  npm 初始化

```cmd
# 使用npm默认package.json配置
C:\Users\Lenovo\Desktop\VSCode项目\client-side>npm init -y
```

​	或者**使用交互式自行配置**，遇到选项如果直接敲回车即使用括号内的值

```cmd
C:\Users\Lenovo\Desktop\VSCode项目\client-side>npm init
package name: (client-side) # 可敲回车即使用client-side这个名字，也可输入其他项目名
version: (1.0.0) # 版本号，默认1.0.0
description: # 项目描述，默认为空
entry point: (index.js) # 入口文件，我们这里改为./src/index.ts
test command: # 测试指令，默认为空
git repository: # git仓库地址，默认为空
keywords: # 项目关键词，多个关键词用逗号隔开，我们这里写typescript,client,lison
author: # 项目作者，这里写lison<lison16new@163.com>
license: (ISC) # 项目使用的协议，默认是ISC，我这里使用MIT协议
# 最后会列出所有配置的项以及值，如果没问题，敲回车即可。
```

这时我们看到了在根目录下已经创建了一个<button> **package.json** </button>文件，接下来我们**创建几个文件夹**：

- src：用来存放项目的开发资源，在 src 下创建如下文件夹：
  - utils：和业务相关的可复用方法
  - tools：和业务无关的纯工具函数
  - assets：图片字体等静态资源
  - api：可复用的接口请求方法
  - config：配置文件
- typings：模块声明文件
- build：webpack 构建配置

### 2.2 全局安装`typescript`

​	全局安装后，你就可以在任意文件夹使用tsc命令：

```cmd
C:\Users\Lenovo\Desktop\VSCode项目\client-side>npm install typescript -g
```

> 如果全局安装失败，多数都是权限问题，要以管理员权限运行。

### 2.3 typescript进行初始化

安装成功后我们进入项目根目录，使用typescript进行初始化：

```cmd
C:\Users\Lenovo\Desktop\VSCode项目\client-side>tsc --init
```

> 注意：运行的指令是tsc，不是typescript。

----

​	初始化之后，在项目根目录**多了一个 <button>tsconfig.json </button>文件**，里面有很多内容。 **TS 在 1.8 版本支持**的可以使用`//`和`/**/`注释。

​	tsconfig.json 里默认有 **4 项没有注释的配置**，有一个需要提前讲下，就是"**lib**"这个配置项，他是一个数组，他用来**配置需要引入的声明库文件**。若用到ES6语法，和DOM相关内容，需要引入两个声明库文件，需要在这个数组中添加"es6"和"dom"，也就是修改数组为`[“dom”, “es6”]`，其他暂时不用修改，接着往下进行。

![](/../images/es.png)

----

​	要搭配使用webpack进行编译和本地开发，不是使用tsc指令，所以**要在项目安装一下**：

```cmd
npm install typescript
```

## 3 配置TSLint

​	对**代码的风格统一有要求**，就需要用到TSLint了，另外TSLint会给你在很多地方起到提示作用。

<button>第一步：</button>在**全局安装TSLint**，记着要用管理员身份运行：

```cmd
C:\Users\Lenovo>npm install tslint -g
```

<button>第二步：</button>在**项目根目录下**，使用TSLint**初始化配置文件**：

```cmd
C:\Users\Lenovo\Desktop\VSCode项目\client-side>tslint -i
```

​	运行结束之后，项目根目录下多了一个<button>`tslint.json`</button>文件，这个就是**TSLint的配置文件**了，它会根据这个文件对我们的代码进行检查，生成的tslint.json文件有下面几个字段：

```json
{
  "defaultSeverity": "error",
  "extends": [
    "tslint:recommended"
  ],
  "jsRules": {},
  "rules": {},
  "rulesDirectory": []
}
```

- *defaultSeverity*是：**提醒级别，**如果为error则会报错，如果为warning则会警告，如果设为off则关闭，那TSLint就关闭了；
- *extends*可指定：继承指定的预设配置规则；
- *jsRules*用来配置对`.js`和`.jsx`文件的校验，配置规则的方法和下面的rules一样；
- *rules*是重点了，我们要让TSLint根据怎样的规则来检查代码，都是在这个里面配置，比如：当我们不允许代码中使用`eval`方法时，就要在这里配置`"no-eval": true`；
- *rulesDirectory*可以指定：规则配置文件，这里指定相对路径。

-----

## 4 配置webpack

​	（1）搭配使用 webpack 进行项目的开发和打包，先来安装 ：`webpack、webpack-cli 和 webpack-dev-server`：

```cmd
C:\Users\Lenovo\Desktop\VSCode项目\client-side>npm install webpack  -D
C:\Users\Lenovo\Desktop\VSCode项目\client-side>npm install webpack-cli -D
C:\Users\Lenovo\Desktop\VSCode项目\client-side>npm install webpack-dev-server -D
```

​	将**它们安装在项目中**，并且作为：**开发依赖(-D)安装**，`-g`：全局安装

​	（2）接下来**添加一个** webpack 配置文件，放在 build 文件夹下，我们给这个文件起名 <button>webpack.config.js</button>，然后在： <button>package.json </button>里**指定启动命令**：

```json
{
  "scripts": {
    "start": "cross-env NODE_ENV=development webpack-dev-server --mode=development --config build/webpack.config.js"
  }
}
```

----

​	（3）**安装插件**：<button>`cross-env`</button>

​	后面跟着一个参数 NODE*ENV=development，这个用来在 webpack.config.js 里通过 process.env.NODE*ENV 来获取当前是：**开发还是生产环境**，这个插件要安装：

```cmd
# C:\Users\Lenovo>npm install cross-env
C:\Users\Lenovo\Desktop\VSCode项目\client-side>npm install cross-env
```

----

​	（4）在 webpack.config.js 中书写配置：

```js
const HtmlWebpackPlugin = require("html-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
 
module.exports = {
  // 指定入口文件
  // 这里我们在src文件夹下创建一个index.ts
  entry: "./src/index.ts",
  // 指定输出文件名
  output: {
    filename: "main.js"
  },
  resolve: {
    // 自动解析一下拓展，当我们要引入src/index.ts的时候，只需要写src/index即可
    // 后面我们讲TS模块解析的时候，写src也可以
    extensions: [".tsx", ".ts", ".js"]
  },
  module: {
    // 配置以.ts/.tsx结尾的文件都用ts-loader解析
    // 这里我们用到ts-loader，所以要安装一下
    // npm install ts-loader -D
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /nodemodules/
      }
    ]
  },
  // 指定编译后是否生成source-map，这里判断如果是生产打包环境则不生产source-map
  devtool: process.env.NODEENV === "production" ? false : "inline-source-map",
  // 这里使用webpack-dev-server，进行本地开发调试
  devServer: {
    contentBase: "./dist",
    stats: "errors-only",
    compress: false,
    host: "localhost",
    port: 8089
  },
  // 这里用到两个插件，所以首先我们要记着安装
  // npm install html-webpack-plugin clean-webpack-plugin -D
  plugins: [
    // 这里在编译之前先删除dist文件夹
    new CleanWebpackPlugin({
      cleanOnceBeforeBuildPatterns: ["./dist"]
    }),
    // 这里我们指定编译需要用模板，模板文件是./src/template/index.html，所以接下来我们要创建一个index.html文件
    new HtmlWebpackPlugin({
      template: "./src/template/index.html"
    })
  ]
};
```

​	用到了两个webpack插件：

​	第一个`clean-webpack-plugin`插件用于：**删除某个文件夹**，我们编译项目的时候需要**重新清掉上次打包生成的dist文件夹**，然后进行重新编译，所以需要用到这个插件将上次打包的dist文件夹清掉。
​	第二个`html-webpack-plugin`插件用于：**指定编译的模板**，这里我们指定模板为`"./src/template/index.html"`文件，打包时会根据此**html文件生成页面入口文件**。

## 5 创建index.html 

位置：`"./src/template/index.html"`

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>TS-Learning</title>
  </head>
 
  <body></body>
</html>
```

## 6 启动本地服务