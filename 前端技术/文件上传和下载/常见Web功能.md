---
typora-root-url: images
---

# 常见Web功能

# 一 文件上传与下载

​	本案例是基于：`SpringBooot`，仍然使用：`multipartFile`完成上传，`SpringBoot`是使用`Servlet3`中的`Part`对象完成上传，不是使用：`fileupload`;

## 1 步骤

**第一步：**创建一个`<form>`标签，`method=post`请求

**第二步：**`form`标签的`encType`属性值必须为：`multipart/form-data`值

**第三步：**在`form`标签中使用`input type=file`，添加上传的文件

**第四步：**编写服务器代码接收，处理上传的数据

## 2 上传相关配置

```properties
spring.http.multipart.enabled=true; //是否允许处理上传
spring.http.multipart.maxFileSize=1MB; // 允许最大的单文件上传大小
spring.http.multipart.maxRequestSize=10MB; //允许的最大请求大小
```

---

或 通过创建一个：`MultipartConfigElement`类型的`bean`对上传进行配置

```java
@Bean
public MultipartConfigElement multipartConfigElement(){
	MultipartConfigFactory mcf = new MultipartConfigFactory();
    mcf.setMaxFileSize("1MB");
    mcf.setMaxRequestSize("10MB");
    return mcf.createMultipartConfig();
}
```

## 3 上传文件的处理

​	因为应用是打成：jar包，所以：一般会把上传的文件放到其他位置，通过设置：`spring.resources.static-locations` 完成资源位置映射。

```properties
spring.resources.static-locations=classpath://META-INF/resources/,classpath:/resources/,classpath:/static/,classpath:/public,/file:/Users/stef/devs/workspace/springboot-demo/upload/
```

## 4 编写前端文件

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <title>文件上传</title>
    <!--<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>-->
</head>
<body>
    <form th:action="@{/upload}" method="post" enctype="multipart/form-data">
        <!--用户名：<input type="text" name="username"/><br>-->
        文件：<input type="file" name="file"/><br>
        <input type="submit" value="上传"/>
    </form>
</body>
</html>
```

## 5 编写后端文件

```properties
# 设置上传文件所放置的路径
file.path = C:/Users/Lenovo/Desktop/Android/file/
```

```java
// 设置上传文件所放置的路径
@Value("${file.path}")
private String filePath;

@PostMapping("/upload")
@ResponseBody
public String upload(MultipartFile file) throws Exception {
    // 将文件复制一份到指定的文件目录下，先获取：文件名和后缀名
    String extName = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
    String fileName = UUID.randomUUID().toString() + extName;
    FileCopyUtils.copy(file.getInputStream(),new FileOutputStream(new File(filePath + fileName)));
    return fileName;
}
```

> 测试：http://localhost:8080/upload