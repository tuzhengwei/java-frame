---
typora-root-url: images
---

# HTML

网址：https://blog.csdn.net/weixin_43342105/article/details/106271856?utm_source=app&app_version=4.7.0&code=app_1562916241&uLinkId=usr1mkqgl919blen

# CSS

网址：https://blog.csdn.net/weixin_43331963/article/details/106784229?utm_source=app&app_version=4.7.0&code=app_1562916241&uLinkId=usr1mkqgl919blen

# JavaScript

> 使用：ES6**（settings ——> Language & Frameworks ——>JavaScript）**

​	 JavaScript（缩写为JS）是一种高级的、多范式、解释型的编程语言，是一门基于原型、函数先行的语言，它支持[面向对象编程](https://baike.sogou.com/lemma/ShowInnerLink.htm?lemmaId=458310&ss_c=ssc.citiao.link)、命令式编程以及[函数式编程](https://baike.sogou.com/lemma/ShowInnerLink.htm?lemmaId=623383&ss_c=ssc.citiao.link)。

​	在JS中，一切皆对象。

> 官方文档：www.w3school.com.cn/js/js_arrays.asp
>
> 推荐文档：https://www.liaoxuefeng.com/wiki/1022910821149312

# 一 快速入门

## 1 内部标签

```html
<script>
   alert('hello world');
</script>
```

## 2 外部引入

```html
# abs.js
...
```

```html
# test.html
<!-- 注意：script必须成对出现 -->
<script src="js/qj.js"></script>
```

## 3 基本语法

```html
<script>
  // 定义变量
  var num = 1;
  
  // 条件控制
  if (1 > 2){
      if(2 > 2){
          ...
      }else{
          ...
      }
  }
     
  // 打印输出（在浏览器的控制台输出）
  console.log("打印变量");
</script>
```

## 4 数据类型

### 4.1 Number

> `JS` **不区分小数和整数**

```js
123 // 整数
123.1  // 浮点数
1.123e3  // 科学计数法
-99 // 复数
NaN // not a number
Infinity  // 无穷大
```

---

### 4.2 字符串

```js
let name = “tzw";
let age = 3;
let msg = '你好啊，${name}';   // 模板字符串
----------------------
// 常用方法，跟JAVA类似
字符串.length
字符串.toUpperCase()
字符串.indexOf("查找元素")
字符串.substring(start,end)
...
```

---

### 4.3 比较运算符

> 不建议使用：==

```js
=		
==  // 等于（类型不一样，值一样，结果：true
===  // 绝对等于（类型一样，值一样）
```

---

### 4.4 浮点数

```js
(1/3) === (1-2/3)  // false ，原因：精度问题

Math.abs(1/3 - (1-2/3)) < 0.0000001		// true
// 1/3 - (1-2/3) 结果趋近：0.0000001	
```

---

### 4.5 数组

> `JAVA`中数组**必须是：相同的类型**；`JS`不需要这样。

```js
// 保证代码的可读性，尽量使用：[]
var arr = [1, 2, 3, 4 ,5, 'hello', null, true ];

new array(1, 12 ,13 ,5, 4, 'hello');
// 若越界，则结果：undefined
```

### 4.6 对象

```js
var person = {
	name : "tzw",
	age : 3,
	tags : ['js', 'java', 'web']
}
```

```html
// 浏览器console控制台
person.name		"tzw"
```

## 5 user strict

 严格模式 	： 	**`user strict`**

---

严格模式"有**两种调用方法**，适用于不同的场合：

​		**第一种：**
​			**针对整个脚本文件**： 将"use strict"放在脚本文件的**第一行**，则整个脚本都将以"严格模式"运行。如果这行语句**不在第一行，则无效**，整个脚本以"正常模式"运行。如果不同模式的代码文件**合并成一个文件**，这一点需要特别注意。

```html
<!--
	前提：
		IDEA 需要 支持：ES6语法
		use strict：严禁模式
		局部变量推荐使用：let
-->
<script>
	'use strict'
    // 变量必须被定义
</script>
```

​		**第二种：**

​			**针对单个函数**：将"use strict"放在**函数体的第一行**，则整个函数以"严格模式"运行。

```js
function f(){
　　　"use strict";
　　	...
};
```

# 二 数据类型

## 1 数组

### 1.1 length

```js
arr.length
```

注意：arr.length 赋值，数组大小会发生变化，若过小，元素会丢失。

---

### 1.2 indexOf

​	**通过元素获得下标索引**

```js
var index = arr.indexOf(2);
```

注意：字符串“1”与数字1是不同的。

----

### 1.3 slice

**截取字符串**

```js
slice();
// 类似substring
```

---

### 1.4 push与pop

push()：**压入**到**尾部**

pop()：**弹出尾部**的一个元素

```js
var arr = [1, 2, 3, 4 ];
arr.push("a");  // 结果：var arr = [1, 2, 3, 4 ,"a"];

arr.pop();  // 结果：var arr = [1, 2, 3, 4 ];
```

### 1.5 unshift与shift

unshift()：**压入到头部**

shift()：**弹出头部**的一个元素

```js
var arr = [1, 2, 3, 4 ];
arr.unshift("a");  // 结果：var arr = ["a", 1, 2, 3, 4];

arr.shift();  // 结果：var arr = [1, 2, 3, 4 ];
```

---

### 1.6 sort

```js
var arr = [1, 2, 3, 4 ];
arr.sort(); // 排序
```

---

### 1.7 reverse

```js
var arr = [1, 2, 3, 4 ];
arr.reverse(); //元素反转
```

---

### 1.8 concat

```js
var arr = [1, 2, 3, 4 ];
arr.concat([1, 2, 3]); // 拼接
// arr  [1, 2, 3, 4 ,1 , 2, 3];
```

注意：concat() 并**没有修改数组**，只是会返回一个：新的数组

---

### 1.9 join

```js
 // var arr = [1, 2, 3, 4 ];
arr.join("-"); // 连接符
// 输出：1-2-3-4
```

### 1.10 forEach

​	forEach() 方法：为每个数组元素**调用一次函数**（回调函数）。

```js
var txt = "";
var numbers = [45, 4, 9, 16, 25];
numbers.forEach(myFunction);

function myFunction(value, index, array) {
  txt = txt + value + "<br>"; 
}
```

**注释：**该函数接受 3 个参数：

- 项目值
- 项目索引
- 数组本身

上面的例子只用了 value 参数。这个例子可以重新写为：

---

```js
var txt = "";
var numbers = [45, 4, 9, 16, 25];
numbers.forEach(myFunction);

function myFunction(value) {
  txt = txt + value + "<br>"; 
}
```

### 1.11 splice

​	修改数组的”万能方法“，可以**从指定的索引**开始删除若干元素，然后再从该位置添加若干元素：

```js
var numbers = ['a', 'b', 'c', 'd', 'e'];
//从下标为2开始，删除3个元素，并从下标2开始，添加：'f','g'，这里返回：'c', 'd', 'e'
arr.splice(2,3,'f','g'); 

arr; ['a', 'b', 'f', 'g'];
```

## 2 对象

### 2.1 创建对象

```js
var person = {
  firstName: "Bill",
  lastName : "Gates",
  id       : 678,
  fullName : function() {
    return this.firstName + " " + this.lastName;
  }
};
```

### 2.2 访问不存在的成员

```js
person.date
结果：undefined  不报错
```

### 2.3 动态删除属性

```js
delete person.firstName
返回结果：true
```

### 2.4 动态添加成员

```
person.date = "2021"
```

### 2.5 是否包含成员

```js
'date' in person
返回结果：true
```

### 2.6 hasOwnProperty

​	判断：一个属性**是否是**这个对象自身拥有的。

```js
person.hasOwnProperty('toString');
结果：false
person.hasOwnProperty('age');
结果：true
```

## 3 流程控制

在 `JavaScript` 中，我们可使用如下**条件语句**：

- 使用` if `来规定要执行的代码块，如果指定条件为 true
- 使用` else` 来规定要执行的代码块，如果相同的条件为 false
- 使用 `else if `来规定要测试的新条件，如果第一个条件为 false
- 使用 `switch` 来规定多个被执行的备选代码块

---

除条件语句，循环语句如下：

```js
while(age < 20){
	...
}
```

```js
for(let i = 0 ;i < 10 ;i++){
	...
}
```

```js
do{
	age = age +1;
	...
}while(age < 20);
```

```js
for(var num in age){
	...
}
```

```js
var age = [1, 2, 3];
age.forEach(function(value)){
	...
}
```

## 4 Map和Set

```JS
// Map
var map = new Map(
	[
        ['tom', 100],
        ['jack', 200],
        ['haha', 300]
    ]
);
var name = map.get('tom');
map.set('admin',123456);
map.delete('tom');

// 遍历
for (let x of map){
    console.log(x);
}
```

```js
//Set
var set = new Set([3,1]);
set.add(2);
set.delete(1);

//遍历
for(let x of set){
    console.log(x);
}
```

# 三 函数及面向对象

## 1 函数定义变量作用域

### 1.1 定义

```js
// 方法一
function abs(x){
	if(x >= 0){
		return x;
	}else{
		return -x;
	}
}
```

```js
// 方法二，匿名函数
var abs = function (x){
	if(x >= 0){
		return x;
	}else{
		return -x;
	}
}
```

```js
// 方法三，结合（1和2）
var abs = function abs(x){
	if(x >= 0){
		return x;
	}else{
		return -x;
	}
}
```

```js
// 方法四：对象构造
var f = new Function('x', 'y', 'return x+y')
```

```js
// 方法五：=>简写
var f = (x,y) => { }
```

```js
// 调用函数
abs(10)
```

> JavaScript ：可以传**任意个**参数，也可以不传参数

**思考：**参数进来是否有问题？

```js
var abs = function abs(x){
    // 检查x是否为：number
	if(typeof x!= 'number'){
		throw 'Not a number';
	}
	if(x >= 0){
		return x;
	}else{
		return -x;
	}
}
```

### 1.2 typeod

> `typeof `：是用来**判断数据的类型**
>
> ----------
>
> typeod后面**加括号与不加括号**的区别：
>
>  typeof a ：第一个是a字面量，所以会返回String；
>
>  typeof (a)：第二个是变量a，返回值就要看a是什么类型

----

### 1.3 arguments

​	是一种**类数组对象（类似数组，但不是数组）**，只在函数的内部起作用，并且永远指向当前函数的调用者传入的参数

---

> 用法

- 使用arguments来**得到**函数的实际参数

```jsx
 function argumentsFn(a,b,c,d){
    console.log(argumentsFn.length)
    console.log(arguments.length)
    //打印出第1个实参 '我是第一个实参'
    console.log(arguments[0]) 
    console.log(arguments[1])
}
argumentsFn('我是第一个实参',3）
```

- 实现**函数重载**：当函数的**参数个数不明确时**，函数体根据参数的不同进行相应的处理
- 实现递归：在函数内部反复的调用函数本身

```jsx
//需求：实现数字的叠加
function calc(num){
    if(num <= 0){
        return 0;
    }else{
    // return num += calc(num-1);//方式一（原始方法）
        return num += arguments.callee(num-1)//方式二
    }
}
var result = calc;
calc = null;
console.log(result(3));//6
```

> arguments.callee ：返回**当前函数本身**。⚠️arguments.callee的用法在严格模式下是不允许的
>
> 以上两种方式的**区别**：
>
> ​		当外界定义了calc=null后，方式一会报错，因为此时calc已不是函数。
>
> ​		但是写成 return num += arguments.callee(num - 1) 不会报错；
>
> ​		因为arguments.callee指的是“当前函数”，并不是“calc”

---

**类数组对象arguments和数组的区别**：

典型的类数组对象：{0:12, 1:23}
 **相同点：**

- 都可以用下标访问每个元素
-  都有length属性

**不同点：**

- 数组对象的类型是**Array**，类数组对象的类型是**Object**；
-  类数组对象**不能直接**调用数组的API；
-  数组遍历可以用for in和for循环，类数组**只能用**for循环遍历；

---

**类数组对象转为数组对象方法：**

```jsx
// 方式一：
function func1() {
     console.log(Array.prototype.slice.call(arguments)); // [1, 2, 3]
}
func1(1, 2, 3);
// 方式二：使用ES6的...运算符 
function func(...arguments) {
    console.log(arguments); // [1, 2, 3]
}
func(1, 2, 3);
```

---

### 1.4 rest

​	ES6 引入 rest 参数（形式为…变量名），用于**获取函数的多余参数**，这样就**不需要使用arguments对象**了。rest 参数搭配的变量是一个数组，该变量将多余的参数放入数组中。

```js
function abs(a,b,...rest){
	console.log(rest);
}
```

​	rest 写在最后面，用：`...`标识

---

### 1.5 变量的作用域

> **变量作用域**

​	在申明变量是**凡是没有var关键字**，而直接赋值的变量**均为全局变量**。

​	JavaScript 中 函数**查找变量**是：从自身函数开始，**由内向外查找**，若内部已存在，则会自动屏蔽外部的变量。

---

> JavaScript实际上**只有一个：全局作用域**。任何变量（函数也可视为：变量），假设没有在函数的作用范围内找到，就会向外查找；若在全局作用域也没有找到，则会报：`RefrenceError`

---

> **变量命名规范**

​	为了**避免**全局变量名的冲突，可以使用：**唯一空间名字**，使用：JQuery。

---

> **let**

​	ES6的`let`关键字，**解决局部作用域**冲突问题！

```js
function abs(){
    for (let i = 0 ;i < 100; i++){
        console.log(i);
    }
}
```

---

> **const常量**

在ES6之前，变量为：大写字母，则为：常量

```js
var PI = '3.14'

P1 = '123'; // 可以二次更改常量的值
```

在ES6，使用：`const`定义常量

```js
const var PI = '3.14'

P1 = '123'; // 异常，不能二次赋值
```

## 2 方法

### 2.1 定义

```js
var a = {
	name: 'tzw',
	birth: 2000,
	// 方法
	age: function(){
		var now_age = new Date().getage();
		return not - this.birth;
	}
}
```

```js
// 属性
a.name
// 方法
a.age()
```

### 2.2 apply

​	`this` ： 是无法指向的，默认指向：调用它的对象。

​	`apply`：控制`this`的指向。

```js
function getAge(){
    var now_age = new Date().getage();
	return not - this.birth;
}
var a = {
	name: 'a',
	birth: 2000,
	age: getAge
};
var b = {
	name: 'b',
	birth: 2000,
	age: getAge
};
// 对象调用
a.age();

// apply调用,方法指定某个对象调用
getAge.apply(a,[])
getAge.apply(b,[])
```

## 3 闭包

​	如果一个函数，**使用了它范围外的变量**，那么（这个函数 + 这个变量）就是闭包。

## 4 class继承

```js
function Student(name){
    this.name = name;
}
// 给类：Student增加一个方法
Student.prototype.hello = function(){
    alert("hello");
}

// ES6 之后
class Student{
    constructor(name){
        this.name = name;
    }
    // 增加一个方法
    hello(){
        alert("hello");
    }
}

// 继承
class xiao extends Student{
    
}
```

## 5 原型链继承

![](../images/链.png)



# 四 常用对象

## 1 Date

```js
var now_date = new Date();
now_date.getFullYear(); // 年
now_date.getMonth();  // 月 0~11
now_date.getDate(); // 日
now_date.getDay();
now_date.getHours();
now_date.getMinutes(); // 分
now_date.getSeconds();// 秒
```

## 2 JSON

```js
var user = {
    name: "tzw",
    age: 3,
    sex: '男'
}
// 对象转换为：json
var json_user = JSON.stringify(user);
// json转换为：对象
var obj = JSON.parse('{"name":"tzw","age":3,"sex":"男"}')
```

## 3 Ajax

​	网页异步更新技术

# 五 操作DOM元素

> 文档对象模型

浏览器页面就是一个：**DOM树形结构。**

	-	更新：更新DOM节点
	-	遍历DOM节点：得到DOM节点
	-	删除：删除一个DOM节点
	-	添加：添加一个新的节点

要操作某个：DOM节点，必须先获取这个节点

## 1 获取DOM节点

```html
<div id="father">
    <h1>标题</h1>
    <p id="p1">p1</p>
    <p class="p2">p2</p>
</div>
```

```js
// 对应CSS选择器
var h1 = document.getElementsByTagName('h1');
var h1 = document.getElementById('p1');
var h1 = document.getElementByClassName('p2');
var father = document.getElementById('father');

var childrens = father.children; //获取父节点下的所有孩子节点
// father.firstChild
// father.lastChild
// father.children[0]
```

## 2 插入DOM节点

```html
<p id="js">JavaScript</p>

<div>
    <p id="se">javaA</p>
    <p id="ee">javaB</p>
    <p id="Ee">javaC</p>
</div>
```

> **插入**一个新节点

```js
var newP = document.createElement('p');  // 创建P标签
newP.id = 'newP'; 
newP.innerText = 'Hello, Kuangshen'; // 设置文本内容
```

> **追加**一个节点

```js
var js = document.getElementById('js');
var list = document.getElementById('list');

// 将js追加到：list后面
list.appentChild(js);
/*
<div>
    <p id="se">javaA</p>
    <p id="ee">javaB</p>
    <p id="Ee">javaC</p>
    <p id="js">JavaScript</p>  // 追加的一行
</div>
*/
```

> 创建一个**标签节点**

```js
var myScript = document.createElement('script');
myScript.setAttribute('type','text/javascript');
```

![](../images/1.png)

---

> 创建一个**Style标签**

```js
var myStyle = document.createElement('style');//创建空style标签
myStyle.setAttribute('type','text/css');
myStyle.innerHTML = 'body{background-color: chartreuse;}';
```

```js
// 给标签：head 添加自定义样式（myStyle）
doucment.getElementByTagName('head')[0].appendChild(myStyle);
```

---

> **插入权限**

```js
insertBefore(newNode,targetNode); // 将newNode插入到targetNode前

obj.replaceChild(newNode,oldNode); // 替换
```

```js
var js = document.getElementById('js');
var ee = document.getElementById('ee');
var list = document.getElementById('list');

list.insertBefore(js,ee); // 将js插入到ee的前面
```

## 3 更新DOM节点

```html
<div id="id1">
	
</div>
<script>
	var id1 = document.getElementById('id1');
</script>
```

> 操作文本

```js
id1.innerText = '456'	// 修改文本的值
id1.innerHtml = '<strong>123</strong>'  //  解析HTML标签
```

> 操作JS

```js
id1.style.color = 'yellow';
id1.style.fontSize = '20px';
```

## 4 删除DOM节点

```html
<div id="father">
    <h1>标题</h1>
    <p id="p1">p1</p>
    <p class="p2">p2</p>
</div>
```

```html
<script>
	var id1 = document.getElementById('p1');
    var father =  p1.parentElement;
    
    father.removeChild(self);
    
    //删除是一个动态的过程，以下这样删除可能会报：溢出异常
    father.removeChild(father.children[0]);
    father.removeChild(father.children[1]);
    father.removeChild(father.children[2]);
</script>
```

# 六 操作BOM元素

> BOM：浏览器对象模型

## 1 window

> 代表了**浏览器的窗口**

```js
window.alert(1);

window.innerHeight
258
window.innerWidth
919
window.outerHeight
994
window.outerWidth
919
```

## 2 navigator

> 封装了**浏览器的信息**（不建议使用）

```js
navigator.appName
"Netscape"
navigator.appVersion
"5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36 QIHU 360SE"
navigator.userActivation
UserActivation {hasBeenActive: true, isActive: true}
```

​	注意：**不建议使用**，因为会被人恶意修改。

## 3 screen

> 代表**屏幕尺寸**

```js
screen.width
1920 px
screen.height
1080 px
```

## 4 location

> 代表当前页面的**URL信息**

```js
location
Location {
    ancestorOrigins: DOMStringList,
    href: "https://hao.360.com/?a1004", 
    origin: "https://hao.360.com", 
    protocol: "https:", 
    host: "hao.360.com", …
}
ancestorOrigins: DOMStringList {
    length: 0
}
assign: ƒ assign()  // 设置新的地址
hash: ""
host: "hao.360.com"
hostname: "hao.360.com"
href: "https://hao.360.com/?a1004"
origin: "https://hao.360.com"
pathname: "/"
port: ""
protocol: "https:"
reload: ƒ reload()   // 刷新
replace: ƒ replace()
search: "?a1004"
toString: ƒ toString()
...
```

## 5 Document

> 代表当前的页面（HTML DOM文档树）

```js
document.title
"百度一下，你就知道"

document.title = '通知我'
"通知我"
```

----

​	eg：获得具体的文档树节点

```html
<d1>
	<dt>java</dt>
    <dd>javaSE</dd>
    <dd>javaEE</dd>
</d1>
<script>
	var d1 = document.getElementById('app');
</script>
------------
结果：
java
	javaSE
	javaEE
```

```js
// 获取cookie
document.cookie
```

---

> **劫持cookie原理**

​	eg：同时打开：淘宝和天猫网页端，登录其中一个，另外一个会自动获取对应的cookie，会造成：恶意人员获取cookie上传到其他服务器上。

- **可以窃取 Cookie 信息**。恶意 JavaScript 可以通过“document.cookie”获取 Cookie 信息，然后通过 XMLHttpRequest 或者 Fetch 加上 CORS 功能将数据发送给恶意服务器；恶意服务器拿到用户的 Cookie 信息之后，就可以在其他电脑上模拟用户的登录，然后进行转账等操作。
- **可以监听用户行为**。恶意 JavaScript 可以使用“addEventListener”接口来监听键盘事件，比如可以获取用户输入的信用卡等信息，将其发送到恶意服务器。黑客掌握了这些信息之后，又可以做很多违法的事情。
- 可以通过修改 DOM伪造假的登录窗口，用来**欺骗用户输入用户名和密码等信息**。
- 还可以**在页面内生成浮窗广告**，这些广告会严重地影响用户体验。

```js
// 服务器端可设置：
cookie.httpOnly
// 使用 HttpOnly 标记的 Cookie 只能使用在 HTTP 请求过程中,所以无法通过 JavaScript 来读
```

## 6 History

> 代表浏览器的**历史记录**

```js
history.back()  // 后退
history.forward() // 前进
```

# 七 操作表单

> form，DOM树

- 文本框 `<text>`
- 下拉框`<select`
- 单选框`radio`
- 多选框`checkbox`
- 隐藏域`hidden`
- 密码框`password`
- ...

## 1.1 常见表单

```html
<form action="post">
	<p>
        <span>用户名：</span>
        <input type="text" id="username">
    </p>
    
    <p>
        <span>性别：</span>
        <input type="radio" name="sex" value="man" id="boy">男
        <input type="radio" name="sex" value="woman" id="girl">女
    </p>
</form>
```

```js
<sript>
	var input_text = document.getElementById('username');
    var boy_radio = document.getElementById('boy');
    var girl_radio = document.getElementById('girl');
    
    // 得到输入框的值
    input_text.value
    
    // 修改输入框的值
    input_text.value = '123'

	// 对于单选框，多选框等固定的值，boy_radio 只能取到当前的值
	boy_radio.checked; // 查看返回的结果（boolean类型）
	boy_radio.checked = true; // 直接赋值
</sript>
```

## 1.2 提交表单

```html
<1--  MD5 工具类  -->
<script scr="https://cdn.bootcss.com/blueimp-md5/2.10.0/js/md5.min.js"></script>
```

> `onsubmit `事件会在表单中的**确认按钮被点击时**发生。
>
> 绑定一个函数，**得到函数返回的结果**

```html
<form action="https://123.sogou.com/" method="post" onsubmit="return aaa()">
	<p>
        <span>用户名：</span>
        <input type="text" id="username" name="username">
    </p>
    <p>
        <span>密码：</span>
        <input type="password" id="password" name="password">
    </p>
    <button type="submit">提交</button>
</form>
```

```js
<script>
	function aaa(){
		alert("进入");
		var name = document.getElementById('username');
		var pwd = document.getElementById('password');
		
		pwd.value = md5(pwd.value); // 加密
    	return true // 返回为：true，才可以进行跳转
	}
</script>
```

# 八 操作文件



# 九 JQuery

> **存有大量的JS函数**
>
> 学习网站：
>
> https://jquery.cuishifeng.cn/

```
公式：$(selector).action()
```

```html
<a href="" id="h"></a>
<script>
	document.getElementById('h');
    $('#h').click(function (){
        alert('hello');
    })
</script>
```

## 1.1 引入JQuery

> 官方
>
> 百度：www.jq22.com/cdn

```html
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
```

## 1.2 回忆选择器

```js
// 标签
document.getElementsByTagName();
// id
document.getElementById();
// 类
document.getElementsByClassName();
```

```js
$('p').click(); // 标签选择器
$('#id1').click(); // id选择器
$('.class1').click(); // 类选择器
```

## 1.3 事件

> 鼠标事件、键盘事件、其他事件

---

鼠标事件

```html
<style>
    #divMove{
     	width: 500px;
        height: 500px;
        border: 1px solid red;
    }
</style>
...
<body>
    <!--要求：动态获取鼠标的坐标-->
    mouse：<span id="mouseMove"></span>
    <div id="divMove">
        鼠标移动位置
    </div>
</body>
<script>
    // 当页面元素加载完后，响应事件
	$(function(){
       $('#divMove').mousemove(function (e){
           $('#movieMove').text('x:'+e.pageX + 'y:'+e.pageY)
       }) 
    });
</script>
```

## 1.4 操作DOM元素

```html
<ul id="test-ul">
	<li class="js">java</li>
    <li name="python">java</li>
</ul>
```

```js
// 获取指定的标签
$('#test-ul li[name=python]').text(); // 获得值
$('#test-ul li[name=python]').text('设置值'); // 设置值

$('#test-ul').html(); // 获得值
$('#test-ul').html('<>');  // 设置值
```

> **CSS操作**

```js
$('#test-ul li[name=python]').css({"color","red"});
```

> **元素的显示与隐藏**，本质：`display:none`

```js
$('#test-ul li[name=python]').show(); // 显示
$('#test-ul li[name=python]').hide(); // 隐藏
```

> 测试

```
$(window).width();
```

# 十 前端开发工具

Element：https://element.eleme.cn/#/zh-CN

源码之家：https://www.mycodes.net/

Layer弹窗组件：https://layer.layui.com/

# 十一 Ajax

```html
<script scr-"${pageContext.request.contextPath}/statics/js/jquery-3.4.1.js"/>
```

## 1.1 Ajax本质

  jQuery Ajax本质：`XMLHttpRequest`。

```js
1
jQuery.get(...)
       所有参数：
           url：待载入页面的URL地址
          data：待发送Key/value参数
       success：载入成功时，回调函数
      dataType：返回内容格式，XML 、json、 script、 text、 html
2
jQuery.post(...)
       所有参数：
           url：待载入页面的URL地址
          data：待发送Key/value参数
       success：载入成功时，回调函数
      dataType：返回内容格式，XML 、json、 script、 text、 html 3
jQuery.getJSON(...)
       所有参数：
           url：待载入页面的URL地址
          data：待发送Key/value参数
       success：载入成功时，回调函数
4
jQuery.getScript(...)
       所有参数：
           url：待载入页面的URL地址
          data：待发送Key/value参数
       success：载入成功时，回调函数
5
jQuery.ajax(...)
       部分参数：
           url：待载入页面的URL地址
          type：请求方式，get、post（1.9之后使用：method）
       headers：请求头
          data：要发送的数据
   contentType：发送信息至服务器的内容编码（默认：utf-8）
         async：是否异步
       timeout：设置请求超时时间（毫秒）
    beforeSend：发送请求前指向的函数（全局）
      complete：完成之后执行的回调函数
       success：载入成功时，回调函数
         error：失败之后执行的回调函数
      acceptes：通过请求头发送给服务器，告诉服务器当前客户端接受的数据类型
      dataType：将服务器端返回的数据转换成：指定类型
         "XML"：将返回的数据转换为：xml
        "text"：...
        "html"：...
      "script"：尝试将返回的值当做JS执行，再将返回的内容转换为：普通文本
        "json"：...
       "jsonp"：...
```

## 1.2 演示1

使用JQuery需要先导入jQuery的js文件；

```Java

@Controller
@RequestMapping("/ajax")
public class AjaxController{
    //第一种方式，服务器要返回一个字符串，直接使用response
    @RequestMapping("/a1")
    public void ajax(String name,HttpServletResponse response){
        if("admin".equals(name)){
            response.getWriter().print("true");
        }else{
            response.getWriter().print("false");
        }
    }
}
```

ajax 写法

```html
<script type="text/javascript">
    function a1() {
        //所有参数：
        //url:待载入页面的URL地址，Json
        //data:待发送Key/value参数
        //success:载入成功时回调函数
            //data：封装了服务器返回的数据!!!
            //status：状态
        $.ajax({
            url:"${pageContext.request.contextPath}/ajax/a1",
            data:{"name":$("txtName").val()},
            success:function (data,status) {
                console.log(data)
                console.log(status)
            }
        });
        //将文本输入的值，
        $("txtName").val();
        // //发送给服务器，
        // //接受服务器返回的数据
    }
```

## 1.3 演示2

```Java
@RequestMapping("/a2")
@ResponseBody
public List<User> ajax2(){
    List<User> list =new ArrayList<>();
    User user1 =new User("豪",1,"男");
    User user2=new User("豪",1,"男");
    list.add(user1);
    list.add(user2);
    return list; //由于加了@ResponseBody注解，他会返回一个字符串
}
```

​	`标签tbody 元素`应该与 thead 和 tfoot 元素结合起来使用。thead 元素**用于对 HTML 表格中的表头内容**进行分组。

```html
<input type="button" id="btn" value="获取数据"/>
<table width="80%" align="center">
    <tr>
    	<td>姓名</td>
        <td>年龄</td>
        <td>性别</td>
    </tr>
    <tbody id="content">
    </tbody>
</table>
...
<script>
    $(function(){
        // 捕获点击事件
    	$("#btn").click(function(){
            /* 提交请求，下方写法对应：演示1中的写法
       			url：${pageContest.request.contextPath}/ajax/a2
       			data：function(data)
       			success：中间部分（请求成功后，执行）
            */
            $.post("${pageContest.request.contextPath}/ajax/a2",function(data){
                console.log(data);
                var html="";
                for(var i=0;i<data.length;i++){
                    html+="<tr>"+
                        "<td>"+data[i].name+"</td>"+
                        "<td>"+data[i].age+"</td>"+
                        "<td>"+data[i].sex+"</td>"+
                        "</tr>"
                }
                $("#content").html(html);  
            })
        })
})
</script> 
```

测试：直接访问html

## 1.4 演示3

```Java
@RequestMapping("/a3")
@ResponseBody
public String ajax3(String name,String pwd){
    String msg="";
    if(name!=null){
        if("admin".equals(name)){
            msg="ok";
        }else{
            msg="用户名有误"；
        }
    }
    if(pwd != null){
        if("123456".equals(pwd)){
            msg="ok";
        }else{
            msg="密码输入有误";
        }
            
    }
    return msg;
}
```

```html
<html>
    <head>
        <title>Title</title>
        <script>
            function a1(){
                $.post({
                    url:"${pageContext.request.contextPath}/ajax/a3",
                    data:{"name":$("#name").val()},
                    success:function(data){
                        if(data.toString()=='ok'){
                            //信息核对成功
                            $('#userInfo').css("color","green");
                        }else{
                            $("#userInfo").css("color","red");
                        }
                        $("#userInfo").html(data);
                    }
                })
            }
            function a2(){
                $.post("${pageContext.request.contextPath}/ajax/a3",{"pwd":$("pwd").val()},function(data){
                    if(data.toString()='ok'){//信息核对成功
                        $('#pwdInfo').css("color","green");                     
                    }else{
                        $('#pwdInfo').css("color","red");
                    }
                    $("#pwdInfo").html(data);
                })
            }
        </script>
    </head>
    <body>
        <p>
            用户名：
            <input type="text" id="name" onblur="a1()"/>
            <span id="userInfo"></span>
        </p>
        <p>
            	密码：
        	<input type="text" id="pwd" onblur="a2()"/>
        	<span id="pwdInfo"></span>
            
        </p>
        
        
        
    </body>
</html>
```

![](../images/3.png)

# 十二 JSON

​	*JSON* 是一种轻量级的**数据交换格式**。

​	前后端分离，数据交互很重要。

​	学习网址：https://blog.csdn.net/qq_33369905/article/details/106647323?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522162045039916780271519589%2522%252C%2522scm%2522%253A%252220140713.130102334..%2522%257D&request_id=162045039916780271519589&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~sobaiduend~default-1-106647323.pc_search_result_before_js&utm_term=%E7%8B%82%E7%A5%9E%E8%AF%B4JSON&spm=1018.2226.3001.4187

## 1 JSON与JS的关系

> ​	JSON 是 JS 对象 的 字符串表示法，使用文本表示一个：JS对象的信息，**本质：一个字符串。**

```js
var obj = {a:'hello', b:"world"}; // JS
var json = '{"a":"hello","b":"world"}'; // JSON
```

## 2 JSON与JS对象互转

> JSON 转 JS，使用：JSON.parse()方法

```js
var Obj = JSON.parse('{"a":"hello","b":"world"}')
```

> JS 转 JSON，使用：JSON。stringify()方法

```js
var json = JSON.stringify({{a:'hello', b:"world"}})
```

## 3 Controller返回JSON数据

### 3.1 导入jackson

```XML
<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.9.8</version>
</dependency>
```

### 3.2 SpringMVC配置

#### web.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0">
 
    <!--1.注册servlet-->
    <servlet>
        <servlet-name>SpringMVC</servlet-name>
        <servlet-class>org.springframework.web.servlet.DispatcherServlet</servlet-class>
        <!--通过初始化参数指定SpringMVC配置文件的位置，进行关联-->
        <init-param>
            <param-name>contextConfigLocation</param-name>
            <param-value>classpath:springmvc-servlet.xml</param-value>
        </init-param>
        <!-- 启动顺序，数字越小，启动越早 -->
        <load-on-startup>1</load-on-startup>
    </servlet>
    <!--所有请求都会被springmvc拦截 -->
    <servlet-mapping>
        <servlet-name>SpringMVC</servlet-name>
        <url-pattern>/</url-pattern>
    </servlet-mapping>
    <filter>
        <filter-name>encoding</filter-name>
        <filter-class>org.springframework.web.filter.CharacterEncodingFilter</filter-class>
        <init-param>
            <param-name>encoding</param-name>
            <param-value>utf-8</param-value>
        </init-param>
    </filter>
    <filter-mapping>
        <filter-name>encoding</filter-name>
        <url-pattern>/</url-pattern>
    </filter-mapping>
</web-app>
```

#### springmvc-servlet.xml

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:mvc="http://www.springframework.org/schema/mvc"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd
        http://www.springframework.org/schema/context
        https://www.springframework.org/schema/context/spring-context.xsd
        http://www.springframework.org/schema/mvc
        https://www.springframework.org/schema/mvc/spring-mvc.xsd">
 
    <!-- 自动扫描指定的包，下面所有注解类交给IOC容器管理 -->
    <context:component-scan base-package="com.kuang.controller"/>
 
    <!-- 视图解析器 -->
    <bean class="org.springframework.web.servlet.view.InternalResourceViewResolver"
          id="internalResourceViewResolver">
        <!-- 前缀 -->
        <property name="prefix" value="/WEB-INF/jsp/" />
        <!-- 后缀 -->
        <property name="suffix" value=".jsp" />
    </bean>
</beans>
```

### 3.3 核心代码

```Java
@Controller
public class demo{
 /*
 	使用第三方包：jackson，只需要一个注解就可以实现：JSON与JS的转换，@ResponseBody
 */
    @RequestMapping("/json1")
    @ResponseBody
    public String json1() throws JsonProcessingException{
        // 使用jackson对象映射器（转对象转换为：JSON字符串
        ObjectMapper mapper = new ObjectMapper();
        
        // 创建一个对象
        User user = new User("t",17,"男")
        
        // 转换为：JSON
        String str = mapper.writeValueAsString(user);
        
        // @ResponseBody注解，返回：json字符串
        return str;
    }
}
```

> **注意：访问localhost，结果中文乱码**
>
> 解决方法： **@RequestMapping增加一个属性**，改为：utf-8格式
>
> product：指定响应体返回：类型与编码

### 3.4 乱码解决方法一

```java
@Controller
public class demo{
 /*
 	使用第三方包：jackson，只需要一个注解就可以实现：JSON与JS的转换，@ResponseBody
 */
 @RequestMapping(value="/json1",product="application/json;charset=utf-8")
    @ResponseBody
    public String json1() throws JsonProcessingException{
        User user = new User("t",17,"男")
        // @ResponseBody注解，返回：json字符串
        return new ObjectMapper().writeValueAsString(user);;
    }
}
```

### 3.5 乱码解决方法二

> ​	**方法一处理比较麻烦，若请求很多，则每一个都需要添加，可以使用：Spring配置统一指定编码。**

```xml
<mvc:annotation-driven>
    <mvc:message-converters register-defaults="true">
        <bean class="org.springframework.http.converter.StringHttpMessageConverter">
            <constructor-arg value="UTF-8"/>
        </bean>
        <bean class="org.springframework.http.converter.json.MappingJackson2HttpMessageConverter">
            <property name="objectMapper">
                <bean class="org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean">
                    <property name="failOnEmptyBeans" value="false"/>
                </bean>
            </property>
        </bean>
    </mvc:message-converters>
</mvc:annotation-driven>
```

**返回json字符串统一解决**

​	在**类上直接使用** **@RestController** ，这样子，里面所有的方法都只会返回 json 字符串了，不用再每一个都添加@ResponseBody ！我们在前后端分离开发中，一般都使用 @RestController ，十分便捷！