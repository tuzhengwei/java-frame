---
typora-root-url: images
---

# JAVASCRIPT硅谷

# - this对象

解析器在调用函数每次都会向函数内部传递进一个隐含的参数（this：指向一个对象：<button>上下文对象</button>）。

```html
<div onclick="start()">123</div>
```

```js
function fun(){
	console.log(this);//[object Window]
}
```

```js
var obj = {
	name: "tuzhengwei",
	age: 23,
	sayInfo: fun
};
// [name:"tuzhengwei", age:23, sayInfo:f]
obj.sayInfo();
// [object Window]
fun();
```

从上述结果可得：根据调用的方式不同，`this`指向不同的对象。

- 以函数的形式调用时，`this`永远都是：`window`
- 以方法的形式调用时，`this`就是调用方法的那个对象
- 以构造函数调用时，`this`就是：新创建的对象

----

# - 创建对象

## 1 普通创建对象

```js
var obj = {
    name: "孙悟空",
    age: 18,
    gender: "男",
    sayName: function(){
        aler(name);
    }
};
```

## 2 工厂方法创建对象

```js
function createPerson(name, age, gender){
    // 创建一个新对象
    var obj = new Object();
    // 添加属性
    obj.name = name;
    obj.age = age;
    obj.gender = gender;
    obj.sayName = function(){
        alert(this.name);
    }
    // 返回对象
    return obj;
}
```

​	构造方法都是：object，导致**无法区分**对象是：人还是动物

## 3 构造函数创建对象

```js
/*
* 构造函数创建对象
* 与普通函数的区别：构造函数需要用：new关键字
* 1. 将新建的对象设置为函数中的：this(就是当前对象)
* 例如：this.name = "孙悟空"  等价  obj.name = "孙悟空"
* 2. 逐行执行函数中的代码
* 3. 将创建的对象返回
*/
function Person(name){
	this.name = name;
}
var person1 = new Person();
console.log(person1);// Person{}
```

​	使用同一个构造函数创建的对象，称为：一类对象（类）。

​	通过一个构造函数创建的对象，称为是该：<button>类的实例</button>。

----

## 4 instanceof

作用：检查一个对象是否是一个类的实例。

```js
console.log(person1 instanceof Person);// true
```

## 5 Class

```Js
class Student{
	Student(name, age, gender){

	}
}
```

# 原型对象