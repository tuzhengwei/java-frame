---
typora-root-url: images
---

# GIT

必须要**先配置好**：**用户名和密码**

# 一、版本控制

## 1、什么是版本控制

​	版本控制（Revision control）是一种**在开发的过程中**用于**管理**我们对**文件、目录或工程**等内容的**修改历史**，方便**查看更改历史记录**，备份**以便恢复以前的版本**的软件工程技术。

​	简单说就是**用于管理多人协同开发项目的技术。**

## 2、版本控制的作用

- 实现跨区域**多人协同开发**
- **追踪和记载**一个或者多个**文件的历史记录**
- **组织和保护** 程序设计的**源代码和文档**
- 统计工作量
- 并行开发、提高开发效率
- **跟踪**记录整个软件的开发过程
- **减轻**开发人员的负担，节省时间，同时**降低人为错误**

## 3、图解

<img src="/../images/版本控制.png" style="zoom: 33%;" />

​	初始版本：项目解决方案，后续都是再此基础上演变的版本，若这时候**需要用到之前某个版本**，若**未使用版本控制器，则无法寻找**。

## 4、常见的版本控制工具

- **Git**
- **SVN**（Subversion）
- **CVS**（Concurrent Versions System）
- **VSS**（Micorosoft Visual SourceSafe）
- **TFS**（Team Foundation Server）
- Visual Studio Online

## 5、版本控制分类

### （1）本地版本控制

​	记录文件每次的更新，可以对每个版本做一个快照，或是记录补丁文件，适合个人用，如**RCS**。

<img src="/../images/本地版本控制.png" style="zoom:50%;" />

### （2）集中版本控制

​	**所有的**版本数据都保存在服务器上，协同开发者从服务器上**同步更新**或上传自己的修改

<img src="/../images/集中版本控制.png" style="zoom:50%;" />

​	所有的版本数据**都存在服务器上**，用户的**本地只有**自己以前所同步的版本；

​	如果**不连网的话**，用户就看不到历史版本，也无法切换版本验证问题，或在不同分支工作。

​	而且，所有数据**都保存在单一的服务器上**，有很大的风险这个服务器会损坏，这样就会丢失所有的数据，当然可以定期备份。代表产品：SVN、CVS、VSS

### （3）分布式版本控制

​	**每个人都拥有全部的代码！安全隐患！**

​	所有版本信息仓库**全部同步到本地的每个用户**，这样就可以**在本地查看**所有版本历史，可以**离线在本地提交**，只需在连网时，push到相应的服务器或其他用户那里。

​	由于每个用户那里保存的都是所有的版本数据，只要有一个用户的设备没有问题就可以恢复所有的数据，但这增加了本地存储空间的占用。

​	**不会**因为**服务器损坏或者网络问题**，**造成不能工作**的情况！

<img src="/../images/分布式版本控制.png" style="zoom:33%;" />



## 6、Git与SVN的主要区别

​	SVN是**集中式版本控制系统**，版本库是集中放在中央服务器的，而工作的时候，用的都是自己的电脑。

​			首先要**从中央服务器**得到**最新的版本**，然后工作，完成工作后，需要**把自己做完的活**推送到中央服务器。

​			集中式版本控制系统是**必须联网**才能工作，对网络带宽要求较高。

----

​	Git是**分布式版本控制系统**，没有中央服务器，每个人的电脑就是一个完整的版本库，工作的时候不需要联网了，因为版本都在自己电脑上。

​	协同的方法是这样的：比如说自己在电脑上改了文件A，其他人也在电脑上改了文件A，这时，你们两之间只需把各自的修改推送给对方，就可以互相看到对方的修改了。Git可以直接看到更新了哪些代码和文件！



# 二、Git历史

​	同生活中的许多伟大事物一样，Git 诞生于一个极富纷争大举创新的年代。

​	Linux 内核开源项目有着为数众广的参与者。绝大多数的 Linux 内核维护工作都花在了**提交补丁和保存归档**的繁琐事务上(1991－2002年间)。到 2002 年，整个项目组开始启用一个专有的分布式版本控制系统 BitKeeper 来管理和维护代码。

​	Linux社区中存在很多的大佬！破解研究 BitKeeper ！

​	到了 2005 年，开发 BitKeeper 的商业公司同 Linux 内核开源社区的合作关系结束，他们收回了 Linux 内核社区免费使用 BitKeeper 的权力。这就迫使 Linux 开源社区(特别是 Linux 的**缔造者** **Linus Torvalds)**基于使用 BitKeeper 时的经验教训，开发出自己的版本系统。（**2周左右**！） 也就是后来的 Git！

​	**Git是目前世界上最先进的分布式版本控制系统。**

​	Git是**免费、开源的**，最初Git是为**辅助 Linux 内核**开发的，来替代 BitKeeper！

# 三、环境配置

## 1、软件下载

​	（1）打开 [git官网] https://git-scm.com/，下载git对应操作系统的版本。

​	（2）官网下载太慢，我们可以使用**淘宝镜像下载**：http://npm.taobao.org/mirrors/git-for-windows/

## 2、镜像下载安装

​	进入镜像下载地址，选择对应的**版本**

### （1）选择安装包

<img src="/../images/git下载.png" style="zoom: 50%;" />

### （2）安装

​	直接：下一步

<img src="/../Images/选择文本编辑器.png" style="zoom:50%;" />

### （3）卸载

​	①	清理环境变量

​	②	控制面板卸载

## 3、启动Git

​	安装成功后在**开始菜单中**会有Git项，菜单下有3个程序：任意文件夹下右键也可以看到对应的程序！

<img src="/../images/启动Git.png" style="zoom: 50%;" />

​	**Git Bash：**Unix与**Linux风格**的命令行，使用最多，推荐最多

​	**Git CMD**：**Windows风格**的命令行

​	**Git GUI**：**图形界面的Git**，不建议初学者使用，尽量先熟悉常用命令

## 4、常用的Linux命令

​	创建一个**练习文件夹**：gitcode

#### 1）$ cd : 改变目录

<img src="/../images/进入gitcode目录.png" style="zoom: 67%;" />

<img src="/../images/进入多重目录.png" style="zoom:50%;" />

#### 2）$ cd . .  ：回退

​		作用：**回退到**上一个目录，直接cd进入默认目录

<img src="/../images/cd...png" style="zoom:67%;" />

#### 3）$ pwd

​		作用：显示**当前所在的目录路径**

<img src="/../images/显示目录.png" style="zoom:67%;" />

#### 4）$ ls(ll)

​		作用：都是**列出当前目录中**的**所有文件**，只不过ll(两个ll)列出的内容**更为详细**

<img src="/images/../显示文件.png" style="zoom:67%;" />

#### 5）$ touch ：新建

​		作用：**新建一个文件** 如 touch index.js 就会在当前目录下新建一个**index.js文件**

![](/../images/新建一个文件.png)

#### 6）$ rm

​		作用： **删除**一个**文件**, rm index.js 就会把index.js文件删除

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ rm index.js   // 删除gitcode文件中的index.js文件
```

#### 7）$ mkdir

​		作用：**新建**一个**目录**,就是新建一个文件夹

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ mkdir test   // 在gitcode文件中，创建一个test文件夹
```

#### 8）$ rm -r 

​		作用： 删除一个文件夹, rm -r src 删除src目录

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ rm -r test   // 在gitcode文件中，删除一个test文件夹
```

​		注意：

```cmd
//递归清理电脑中的全部文件，请勿在Linux中操作（Linux一切皆文件）
$ rm -rf /  
```

#### 9）$ mv

​		作用： 移动文件, mv index.html src index.html 是我们要移动的文件, src 是**目标文件夹**，当然, 这样写，必须**保证文件**和**目标文件夹在同一目录下**。

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ mv index.html test 
// 在gitcode文件中，将index.html移到test文件中
```

#### 10）$ reset 

​		作用：重新初始化终端/清屏

#### 11）$ clear 

​		作用：清屏

#### 12）$ history 

​		作用：查看命令历史

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ history
```

#### 13）$ help 

​		作用：帮助文件

#### 14）$ exit 

​		作用：退出

#### 15）\# 表示注释

## 5、Git配置

### （1）查看所有的配置

​	所有的**配置文件**，其实都**保存在本地**

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ git config -l
```

<img src="/../images/查找本地配置（所有的）.png" style="zoom:50%;" />

### （2）查找系统配置

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ git config --system --list
```

<img src="/../images/查找系统配置.png" style="zoom:50%;" />

### （3）查找本地配置

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ git config --global --list
```

<img src="/../images/本地配置.png" style="zoom:50%;" />

### （4）Git相关的配置文件

​		①	Git\etc\gitconfig  ：Git 安装目录下的 gitconfig   --**system 系统级**

<img src="/../images/系统配置文件：gitconfig.png" style="zoom:50%;" />

​		②	C:\Users\Administrator\ .gitconfig   只适用于**当前登录用户**的配置  --**global 全局**

<img src="/../images/配置用户名和密码.png" style="zoom: 67%;" />

​		这里是可以配置：**用户名和密码**。

### （5）配置用户名和密码

​		此步**一定要配置**，否则**后续无法操作**。

![](/../images/配置用户信息.png)

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ git config --global user.name "kuangshen"  #名称
$ git config --global user.email 24736743@qq.com   #邮箱
```

​		**只需要**做一次这个设置，如果你传递了--global 选项，因为Git将**总是会**使用该信息来**处理你在系统中所做的一切操作**。

---

​		如果你希望在一个特定的项目中**使用不同的**名称或e-mail地址，你可以在该项目中运行该命令而**不要--global选项**。总之--global为全局配置，**不加为**某个项目的**特定配置**。

# 四、基本理论

## 1、工作区域介绍

​	Git本地有三个工作区域：工作目录（Working Directory）、暂存区(Stage/Index)、资源库(Repository或Git Directory)。

​	如果在**加上远程的git仓库**(Remote Directory)就可以分为四个工作区域。文件在这四个区域之间的转换关系如下：

<img src="/../images/Git原理转换.png" style="zoom:50%;" />

​	流程：提交到远程仓库：**由低先高**；提交到本地仓库：**由高到底**。

---

​	提交**到远程仓库**：
​			本地工作目录（Working directory），使用：**git add  files** 添加到：暂存区（Stage/Index），**文件未保存**，需要提交；然后，使用：**git commit** 提交到本地仓库（History）；再通过：**git push** 添加到：远程仓库。

---

​	提交**到本地仓库**：git pull 、git reset、git checkout

---

- Workspace：工作区，就是你**平时存放项目代码**的地方
- Index / Stage：暂存区，用于**临时存放你的改动**，事实上它只是一个文件，保存即将提交到文件列表信息
- Repository：仓库区（或本地仓库），就是**安全存放数据**的位置，这里面有你提交到所有版本的数据。其中HEAD指向最新放入仓库的版本
- Remote：远程仓库，**托管代码的服务器**，可以简单的认为是你项目组中的一台电脑用于远程数据交换

## 2、内部原理介绍

<img src="/../images/git内部原理.png" style="zoom: 67%;" />

- Directory：使用Git管理的一个目录，也就是一个仓库，**包含**我们的**工作空间**和**Git的管理空间**。
- WorkSpace：需要通过Git进行**版本控制的目录和文件**，这些目录和文件组成了工作空间。
- .git：存放Git管理信息的目录，初始化仓库的时候自动创建。
- Index/Stage：暂存区，或者叫待提交更新区，在提交进入repo之前，我们可以把所有的更新放在暂存区。
- Local Repo：本地仓库，一个存放在本地的版本库；HEAD会只是当前的开发分支（branch）。
- Stash：隐藏，是一个工作状态保存栈，用于保存/恢复WorkSpace中的临时状态。

## 3、git流程介绍

git的**工作流程**一般是这样的：

​	①	在工作目录中**添加、修改**文件；

​	②	将需要进行版本管理的文件**放入暂存区域**；

​	③	将暂存区域的文件**提交到git仓库**。

因此，git管理的文件有**三种状态**：已修改（modified）,已暂存（staged）,已提交(committed)

<img src="/../images/git工作流程.png" style="zoom: 67%;" />

# 五、项目搭建

## 1、创建工作目录与常用指令

​	工作目录（WorkSpace)一般就是你希望Git帮助你管理的文件夹，可以是你项目的目录，也可以是一个空目录，建议不要有中文。

​	日常使用只要记住下图**6个命令**：

![](/../images/6个命令.png)

## 2、本地仓库搭建

​	创建本地仓库的方法有两种：

### 	（1）一种是创建全新的仓库

​		创建全新的仓库，需要用GIT管理的项目的**根目录执行**：

<img src="/../images/本地搭建项目.png" style="zoom:50%;" />

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
# 在当前目录新建一个Git代码库
$ git init
```

### 	（2）另一种是克隆远程仓库

<img src="/../images/git远程克隆下载.png" style="zoom: 33%;" />

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode  # 本地目录下
# 克隆一个项目和它的整个代码历史(版本信息)
$ git clone [url]  # https://gitee.com/kuangstudy/openclass.git
```

​		去 gitee 或者 github 上克隆一个测试！（注意：一定要在**本地目录下执行命名**）

# 六、文件操作

## 1、文件的四种状态

版本控制就是对文件的版本控制，要对文件进行修改、提交等操作，首先要知道文件当前在什么状态，不然可能会提交了现在还不想提交的文件，或者要提交的文件没提交上。

- **Untracked**未跟踪, 此文件在文件夹中, 并**没有加入到git库**, 不参与版本控制. 通过**git add** 命令，将**状态变为Staged**.
- **Unmodify**: 文件**已经入库**, **未修改**, 即版本库中的文件快照内容与文件夹中完全一致. 这种类型的文件有两种去处, 如果**它被修改**，而**变为Modified**. 如果使用**git rm命令**，则**移出版本库**, 则**成为Untracked文件**
- **Modified**: 文件**已修改, 仅仅是修改**, 并没有进行其他的操作. 这个文件也有两个去处, 通过**git add命令可进入暂存staged状态**, 使用**git checkout 命令**，则丢弃修改过, 返回到unmodify状态, 这个git checkout即从库中取出文件, 覆盖当前修改 !
- **Staged**: **暂存状态**. 执行**git commit命令**，则将**修改同步到库中**, 这时库中的文件和本地文件又变为一致, 文件为Unmodify状态， 执行**git reset** HEAD filename**取消暂存**, 文件状态为Modified

## 2、查看文件状态

**初始化**一个git目录

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
# 在当前目录新建一个Git代码库
$ git init
```

### （1）**查看所有文件状态**

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
# 在当前目录新建一个Git代码库
$ git status
```

<img src="/../images/没有文件跟踪.png" style="zoom: 67%;" />

​	①	在gitcode文件中**创建一个**：**hello.txt再**使用：**git status命令**

<img src="/../images/hell.png" style="zoom: 67%;" />

​	②	将hello.txt**添加到暂存区**中

<img src="/../images/添加到暂存区.png" style="zoom:67%;" />

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ git add .  	# 添加所有文件到暂存区
```

​	③	再次**查看文件状态**（hello.txt处于**跟踪状态**）

<img src="/../images/跟踪状态.png" style="zoom:67%;" />

### （2）查找指定文件

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
#查看指定文件状态
$ git status [filename]
```

### （3）将暂存区的文件提交到本地仓库

通过：**git add . 命令**  将hello.txt文件  **添加到暂存区后**，可提交到本地仓库。

```cmd
Administrator#kuangshen MINGW64 ~/Desktop/狂神说Git/gitcode
$ git commit -m "消息内容"    提交暂存区中的内容到本地仓库 -m 提交信息
```

<img src="/../images/提交到本地仓库.png" style="zoom:67%;" />

## 3、忽略文件

<img src="/../images/IDEA的git文件配置.png" style="zoom:50%;" />

​		有些时候我们**不想把某些文件纳入版本控制中**，比如数据库文件，临时文件，设计文件等

在**主目录下**建立"**.gitignore**"文件，此文件有如下规则：

 - 忽略文件中的空行或以井号（#）开始的行将会被忽略。

 - 可以使用**Linux通配符**。

   ​		例如：星号（*）代表任意多个字符，问号（？）代表一个字符，方括号（[abc]）代表可选字符范围，大括号（{string1,string2,...}）代表可选的字符串等。

 - 如果名称的最前面有一个感叹号（!），表示例外规则，将不被忽略。

 - 如果名称的最前面是一个路径分隔符（/），表示要忽略的文件在此目录下，而子目录中的文件不忽略。

	-	如果名称的最后面是一个路径分隔符（/），表示要忽略的是此目录下该名称的子目录，而非文件（默认文件或目录都忽略）。

 **.gitignore文件：**

```properties
#为注释
*.txt        #忽略所有 .txt结尾的文件,这样的话上传就不会被选中！
!lib.txt     #但lib.txt除外
/temp        #仅忽略项目根目录下的TODO文件,不包括其它目录temp（“/”在前，往上忽略）
build/       #忽略build/目录下的所有文件（“/”在后，往下忽略）
doc/*.txt    #会忽略 doc/notes.txt 但不包括 doc/server/arch.txt
```

## 4、 .gitignore文件配置

```properties
*.class  # 忽略class文件  #
*.log	 # 忽略日志文件  #
*.lock

# Package Files 忽略 #
*.jar
*.war
*.ear
target/		# target下的文件也忽略  #

# idea 配置忽略
.idea/
*.iml/

*velocity.log*

### STS ###
.apt_generated
.factorypath
.springBeans

### IntelliJ IDEA ###
*.iml
*.ipr
*.iws
.idea
.classpath
.project
.settings/
bin/

*.log
tem/  # 忽略临时文件 #

#rebel
*rebel.xml*
```



# 七、使用码云Gitee（远程仓库）

​	github 是有墙的，比较慢，在国内的话，我们一般使用 gitee ，公司中有时候会搭建自己的gitlab服务器。

## 1、注册

​	个人主页：https://gitee.com/profile/account_information

## 2、设置密码

## 3、设置本机绑定SSH公钥，实现免密码登录！

​		（免密码登录，这一步挺重要的，码云是远程仓库，我们是平时工作在本地仓库！)

<img src="/../images/SSH公钥.png" style="zoom:50%;" />

### （1）找到C盘的.SSH目录

​		进入 C:\Users\Administrator\.ssh 目录

<img src="/../images/SSM目录.png" style="zoom:50%;" />

​	**默认：没有文件**，若需要重新创建，则删除内部的所有文件，再创建SSM。

### （2） 生成公钥

```cmd
Administrator#kuangshen MINGW64 ~/.ssh
$ ssh-keygen -t rsa # -t rsa 加密算法
```

<img src="/../images/生成SSM.png" style="zoom:50%;" />

<img src="/../images/公共与私有钥匙.png" style="zoom:50%;" />

---

**注意：**若出现：`ssh-keygen`不是内部命令：

解决方法一：

- 找到`Git`目录下的：`/user/bin`目录下的：`ssh-keygen.exe`（如果找不到，则全文搜索）
- 设置环境变量，找到`Path`变量，将`ssh-keygen`的文件路径放入保存
- 重新执行：`ssh-keygen`命令

---

解决方式二：

- 若不想要创建环境变量，则进入`Git`目录下的：`/user/bin`目录下，右击选择：`Bash`，执行`ssh-keygen`命令，系统会优先在当前路径下：寻找：`ssh-keygen.exe`

---

### （3）将公钥添加到Gitee上

​	公钥：打开**id_rsa.pub文件**，将内容添加到下方：

<img src="/../images/设置公钥.png" style="zoom:50%;" />

### （4）将公钥信息public key添加到码云账户

<img src="/../images/SSM添加到云账户.png" style="zoom:50%;" />

### （5）使用码云创建一个自己的仓库，并下载到本地仓库

​	①	新建仓库

<img src="/../images/新建仓库.png" style="zoom:50%;" />

​	②	输入仓库信息

<img src="/../images/创建仓库2.png" style="zoom:50%;" />

​	③	远程仓库克隆到本地

<img src="/../images/远程仓库克隆到本地.png" style="zoom: 50%;" />

​	④	命令

<img src="/../images/命令.png" style="zoom:67%;" />

## 4 测试是否连接成功

```cmd
$ ssh -T git@gitee.com
```

![](/../images/连接测试.png)

# 八、IDEA中集成Git

## 1、新建项目，绑定Git

​	默认创建的项目，是**没有绑定Git**。

### （1）Git根目录就是项目的根目录

<img src="/../images/项目的目录就是Git的目录.png" style="zoom:67%;" />

### （2）万能方法，将远程的Git文件目录拷贝到项目中

#### 	①	未绑定Git状态

<img src="/../images/未绑定Git.png" style="zoom: 50%;" />

#### 	②	绑定Git

<img src="/../images/拷贝.png" style="zoom:67%;" />

#### 	③	绑定Git后的状态

<img src="/../images/绑定后的状态.png" style="zoom:50%;" />

## 2、修改文件，使用IDEA操作Git

​	界面介绍（也可以**直接使用命令**）

​	右击——Git——add

![](/../images/IDEA的Git操作.png)

#### ①	将hello.java添加到暂存区

<img src="/../images/将hello.java添加到：暂存区.png" style="zoom: 50%;" />

```cmd
$ git add.  # 所有文件都提交到暂存区
```

#### ②	git push提交到本地仓库

<img src="/../images/git push.png" style="zoom: 67%;" />

#### ③	提交到远程仓库

<img src="/../images/提交到远程仓库.png" style="zoom: 67%;" />



#### ④	查看提交记录

<img src="/../images/提交记录.png" style="zoom:67%;" />

若不希望某些文件提交到远程查看，可以使用：**过滤文件（ .gitignore）**

## 3、提交测试

# 九、Git分支

<img src="/../images/分支.png" style="zoom:67%;" />

## 1、git分支中常用指令

```
# 列出所有本地分支
git branch

# 列出所有远程分支
git branch -r

# 新建一个分支，但依然停留在当前分支
git branch [branch-name]

# 新建一个分支，并切换到该分支
git checkout -b [branch]

# 合并指定分支到当前分支
$ git merge [branch]

# 删除分支
$ git branch -d [branch-name]

# 删除远程分支
$ git push origin --delete [branch-name]
$ git branch -dr [remote/branch]
```

## 2、IDEA中操作

<img src="/../images/git分支IDEA.png" style="zoom:67%;" />

## 3、冲突解决

​		解决的办法是我们可以修改冲突文件后重新提交！
​		选择要**保留**他的代码还是你的代码！