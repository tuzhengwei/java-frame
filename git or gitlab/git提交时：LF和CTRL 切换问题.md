# GIT

> # 关于 LF will be replaced by CRLF 问题出现的原因以及解决方式

问题

> 执行：add .

```cmd
warning: LF will be replaced by CRLF in SpringBoot/manager/.idea/workspace.xml.
The file will have its original line endings in your working directory
```

> windows中的换行符为 CRLF，而在Linux下的换行符为LF，所以在执行add . 时出现提示

> 解决办法：https://blog.csdn.net/huihuikuaipao_/article/details/100183521

```cmd
#提交时转换为LF，检出时转换为CRLF
$ git config --global core.autocrlf true
```

