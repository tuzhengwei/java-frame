---
typora-root-url: images
---

# ShardingSphereJDBC

官网：http://shardingsphere.apache.org/document/legacy/4.x/document/cn/overview/#sharding-jdbc

`Apache ShardingSphere` 是一套开源的**分布式数据库解决方案组成的生态圈**，由`JDBC`、`Proxy`和`Sidecar（规划中）`这3款即能独立部署，又支持混合部署配合使用的产品组成。君提供标准化的数据库水平扩展、分布式事务和分布式治理等功能。如：java同构、异构语言、云原生等各种多样化的应用场景。

---

# 一、ShardingJDBC概述



# 二、ShardingJDBC-Linux安装MySQL-5.7

## 1、下载MySQL的rpm地址

地址：http://repo.mysql.com/yum/mysql-5.7-community/el/7/x86_64/

<img src="/../images/rpm.png" style="zoom: 50%;" />

```cmd
rpm -qa | grep -i mysql # 查询是否安装MYSQL
find /-name mysql

# 删除存在的MySQL文件
rm -rf /etc/selinux/targeted/active/modules/100/mysql  /user/lib64/mysql
```

## 2、配置MySQL扩展源

```cmd
rpm -ivh  http://repo.mysql.com/yum/mysql-5.7-community/el/7/x86_64/mysql57-community-release-el7-10.noarch.rpm
```

<img src="/../images/地址源.png" style="zoom:50%;" />

## 3、yum安装MySQL

```cmd
# 进入虚拟机安装
yum install mysql-community-server -y
```

## 4、启动MySQL，并加入开机自启

```cmd
systemctl start mysqld  # 启动
systemctl status mysqld  # 查看状态

systemctl stop mysqld   # 停止
systemctl enable mysqld # 加入开机自启
```

## 5、使用MySQL登录数据库

```cmd
# 前提：MySQL启动
#获取初始密码
[root@mysql-server ~]#  awk '/temporary password/ {print $NF}' /var/log/mysqld.log
----
N7)T-uM.sNS+
#（括号要用转义 ：\( xxx \)）
# 例：file(20170605) —— cd file\(20170605\) 
---
# 登录MySQL
mysql -uroot -pN7\)\T-uM.sNS+
```

![](/../images/查看初始密码.png)

---

第二种方法：

```cmd
# 一步到位，直接登进MySQL中
mysql -uroot -p$\(\awk '/temporary password/{print $nf}' /var/log/mysqld.log)
```

## 6、修改密码

​	数据库**默认密码规则**：必须携带大小写字母、特殊符号，字符长度大于8否则会报错；

​	因此设定较为简单的密码，需要**首先修改**：`set global validate_password_policy 和_length`参数值。

|   Policy    | Tests Performed                                              |
| :---------: | ------------------------------------------------------------ |
|  0 or LOW   | Length                                                       |
| 1 or medium | Length;numeric,lowercase/uppercase,and special charaters     |
| 2 or strong | Length;numeric,lowercase/uppercase,and special charaters;dictionary file |

---

```mysql
# 登入MySQL后
# 设置密码规则，保证只能是：数字和字母
mysql> set global validate_password_policy=0;
Query OK, 0 rows affected (0.00 sec)
mysql>set global validate_password_length=1;
```

```cmd
# 第一种
mysql> set password for root@localhost=password('123456');
Query OK, 0 rows affected, 1 warning (0.00 sec)
# 第二种
mysql>ALTER USER 'root'@'localhost' identified by '123456';
```

## 7、测试

```java
mysql>exit  # 退出
```

```cmd
[root@iZ2ze5ovuegpc8xpnw3jsfZ ~]# mysql -uroot -p123456
```

## 8、可视化工具—连接数据库

<img src="/../images/连接.png" style="zoom:50%;" />

---

​	若**无法连接成功**（授权失败，查看防火墙），则：

```mysql
mysql>grant all on *.* to root@'%' identified by '123456'; # 授权（*.*：所有权限，阿里云：3306打开）

mysql>flush privileges; # 刷新
```

# 三、ShardingJDBC-主从复制-数据层面

## 1、概述

​	**主从复制（AB复制）：**允许来自一个MySQL数据库服务器（**主服务器**）的数据复制到**一个或多个**MySQL数据库服务器（**从服务器**）

```
复制：异步，不需要永久连接 以 接收来自主站的更新
```

​	**根据配置：**可以复制数据库中**所有数据库** 或 **所选数据库** 或 **所选定的表**。

---

## 2、复制的优点

​	**横向扩展解决方法：**

​		在多个**从站之间分配负载**以提高性能。在此环境中，所有**写入和更新**都必须**在主服务器**上进行。但是，读取可以在一个或多个  **从设备**  上进行，该模型可以**提高写入性能**（因为主设备专用于更新），同时显著提高了：从设备的读取速度。

---

​	**数据安全性：**

​		因为数据被复制到从站，并且**从站**可以**暂停复制过程**，所以可以在  **从站上   运行备份服务**而不会破坏相应的主数据。

---

​	**分析：**

​		可以在主**服务区上创建实时数据**，而信息分析可以在：从服务器上进行，而不会影响主服务器的性能。

---

​	**远程数据分发：**

​		可以使用复制为远程站点**创建数据的本地副本**，而无需永久访问主服务器。

---

## 3、读写分离-主从复制

**之前的写法：**

![](/../images/读写不分离.png)

---

**主从复制与读写分离写法：**

**注意：**使用了读写分离，就一定要使用：主从负责，**保证数据的一致性和完整性。**

![](/../images/读写分离.png)

## 4、Replication原理

<img src="/../images/Replication原理.png" style="zoom:80%;" />

​	Master：将数据库存为：Binary log（二进制文件）——>同步到从服务器，由SQL thread 去**重新刷新：SQL语句**。

## 5、应用

​	**准备两台服务器**，一台：主服务器，一台：从服务器。

```
数据库必须一致
服务器：
主IP：	从IP:
```





# 四、ShardingJDBC配置及读写分离-业务层面



# 五、ShardingJDBC分库和分表



# 六、ShardingJDBC事务管理



# 七、ShardingJDBC总结



# 八、SpringBoot整合ShardingJDBC