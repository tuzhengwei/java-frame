---
typora-root-url: images
---

# 一、MySQL 5.7 安装

## 1、准备条件

​	下载压缩包：https://dev.mysql.com/downloads/mysql/5.7.html

​	将压缩包解压到自己准备的：目录下

![](/../images/解压.png)

## 2、创建一个my.ini 和 data文件

```mysql
[client] 
port=3309 
default-character-set=utf8
[mysql] 
default-character-set=utf8 
[mysqld] 
port=3309 
init_connect='SET collation_connection = utf8_unicode_ci' 
init_connect='SET NAMES utf8' 
collation-server=utf8_unicode_ci 
skip-character-set-client-handshake 
basedir="E:/MySostware/mysql-5.7.33-winx64"
datadir="E:/MySostware/mysql-5.7.33-winx64/Data/" 
character-set-server=utf8 
default-storage-engine=INNODB
```

![](/../images/data.png)

## 3、设置环境变量

```cmd
# 笨方法
# 打开环境变量配置——path：
E:\MySostware\mysql-5.7.33-winx64\bin
```

## 4、初始化数据库

> 以**管理员**的身份进入cmd界面 

```cmd
C:\WINDOWS\system32>E:

E:\>cd MySostware\mysql-5.7.33-winx64\bin

E:\MySostware\mysql-5.7.33-winx64\bin>mysqld -install
```

> 或
>
> 直接进入到注册表，**修改ImagePath值**
>
> 计算机\HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\mysql2

```
# imagePath值：
"E:\MySostware\mysql-5.7.33-winx64\bin\mysqld" --defaults-file="E:\MySostware\mysql-5.7.33-winx64\my.ini" MySQL
```

----

## 5、启动数据库

```cmd
# 查看初始化密码
E:\MySostware\mysql-5.7.33-winx64\bin>mysqld --initialize --console

2021-04-26T12:17:12.256835Z 0 [Warning] TIMESTAMP with implicit DEFAULT value is deprecated. Please use --explicit_defaults_for_timestamp server ......
2021-04-26T12:17:20.278460Z 1 [Note] A temporary password is generated for root@localhost: SgUDE:k>+5re

# 初始密码（注意大小写）： SgUDE:k>+5re
```

```cmd
# 启动数据库
E:\MySostware\mysql-5.7.33-winx64\bin>net start mysql
```

## 6、修改密码

```cmd
# 进入数据库
E:\MySostware\mysql-5.7.33-winx64\bin>mysql -u root -p
Enter password: ************
```

```cmd
# 修改密码
mysql> set password = password('123456');
Query OK, 0 rows affected, 1 warning (0.00 sec)
```

## 7、关闭数据库

```cmd
E:\MySostware\mysql-5.7.33-winx64\bin>net stop mysql
```

## 8、更改使用其他版本方法

> 进入注册表（**regedit**）
>
> 更改为：5.5版本

![](/../images/注册表.png)

> 以**管理员身份**运行：cmd

```cmd
C:\WINDOWS\system32>cd ..

C:\Windows>cd ..

C:\>cd Program Files\MySQL\MySQL Server 5.5\bin

C:\Program Files\MySQL\MySQL Server 5.5\bin>mysqld -install
```

# 二、常见错误

## 1、找不到指定文件

![](/../images/找不到指定文件.png)

>  原因：服务的路径与安装的路径不一致造成的，需要修改注册表；
>
> cmd ——> regedit即可

![](/../images/regit.png)

## 2、多个MySQL



# 三、多个MySQL安装

> https://www.cnblogs.com/xyabk/p/8971444.html

## 1、解压MySQL文件

> d:\mysql1
>
> d:\mysql2

## 2、创建和修改配置文件

```mysql
[mysqld]
basedir=d:\mysql2
datadir=d:\mysql2\data 
port = 3307
character-set-server=utf8
server_id = 2
#忘记密码时使用的
#skip-grant-tables
#修改认证方式
#default_authentication_plugin=mysql_native_password
[mysql]
default-character-set=utf8
```

> ​	重要的修改：
>
> ​				就是port和server_id ，默认如果不设置port，则是3306，这里分别设置：3306,3307，对应的server_id 分别为1,2

## 3、初始化MySQL

> 以管理员身份运行命令提示符；
>
> 进入到mysql2的bin目录,输入下面的命令

```cmd
mysqld --initialize --console
```

​	这里会**产生随机密码**，请牢记，如果不小心忘记了，**删除data**下的所有数据，从新再初始化。

## 4、安装MySQL

```cmd
mysqld --install mysql2
```

​	mysql2就是再服务里面**用作区分多个mysql的服务名**，如果不写，默认是MySQL,安装多个数据库，**如果不写，会导致无法启动服务**。

## 5、启动MySQL

```cmd
net start mysql2
```

​	正常应该启动成功。如果启动失败，检查你**是否配置了环境变量。

​	如果你给3个mysql都配置了环境变量，系统会**默认第一个有效**。

## 6、修改注册表

​	ctrl+r ：在出现的框里面输入regedit，回车就进入到注册表界面了。

> ​	**找打以下目录：**HKEY_LOCAL_MACHINE–>SYSTEM–>CurrentControlSet–>Services 

```
# 在imagePath的后面加上
--defaults-file="d:\mysql2\my.ini" mysql2
```

## 7、注意

> 安装多个mysql，不建议配置环境变量，虽然在命令行操作会比较方便，不需要去到bin目录，但是**只针对一个有效，其他的还是需要去相应的bin目录**，最重要的是，除了配置了环境变量这个mysql之后，剩下的mysql都需要去修改注册表才能正常启动。

## 8、启动

```cmd
# 请注明端口,第一个端口的P大写，后面密码的p小写
mysql -uroot -P3307 -p
```

## 9、删除

> 删除mysql服务sc delete mysql //这里的mysql是你要删除的服务名