---
typora-root-url: MySQLimages
---

学习网址：https://blog.csdn.net/ThinkWon/article/details/104778621?ops_request_misc=%257B%2522request%255Fid%2522%253A%2522161897357116780274131657%2522%252C%2522scm%2522%253A%252220140713.130102334.pc%255Fall.%2522%257D&request_id=161897357116780274131657&biz_id=0&utm_medium=distribute.pc_search_result.none-task-blog-2~all~first_rank_v2~rank_v29-1-104778621.first_rank_v2_pc_rank_v29&utm_term=%E6%9C%80%E5%B7%A6%E6%89%A7%E8%A1%8C

# SELECT语法

```mysql
SELECT [ALL | DISTINCT]
{* | 表名.* |[表名.字段[as 别名][,表名.字段[as 别名]][,...]]}
from 表名1 [as 表的别名]
	[left | right | inner join 表名2] # 联合查询
	[where ...] # 指定结果需满足的条件
	[group by ...] # 指定结果，按哪几个字段来分组
	[having] # 过滤分组的记录必须满足的：次要条件
	[order by ...] # 指定查询记录按：一个或多个条件排序
	[Limit {[offset,]row_count}  | row_countOFFSET offset}]; # 指定查询的记录从哪条到哪条
```

---

```sql
insert into instructor2 values(00001,'ai','math',2500.37);

UPDATE example  SET field1 = 'updated value' WHERE field2 = 'N';

delete from instructor2  where  field2 = 'N';
```

# MySQL

## 1、数据库四个事务和四个隔离级别

数据库**默认**事务隔离级别：

|   数据库   |        默认隔离级别         |
| :--------: | :-------------------------: |
|   mysql    | REPEATABLE-READ（可重复读） |
|   Oracle   | READ COMMITTED（读已提交）  |
| SQL Server | read committed（读已提交）  |

​	oracle数据库支持：READ COMMITTED 和 SERIALIZABLE这两种事务隔离级别。

|  事务  | 作用                                                         |
| :----: | ------------------------------------------------------------ |
| 原子性 | 对数据修改操作**要么全部执行，要么完全不执行**               |
| 一致性 | 在一个事务**执行之前和执行之后**数据库都必须**处于一致性状态** |
| 隔离性 | 并发的事务是相互隔离的                                       |
| 持久性 | 持久性指当系统或介质发生故障时，确保已提交的更新不能丢失     |

| 隔离级别                   | 脏读(Dirty Read) | 不可重复读(NoneRepeatable Read) | 幻读（Phantom Read） |
| -------------------------- | ---------------- | ------------------------------- | -------------------- |
| 读未提交(Read uncommitted) | 可能             | 可能                            | 可能                 |
| 读已提交(Read committed)   | 不可能           | 可能                            | 可能                 |
| 可重复(Repeatable read读)  | 不可能           | 不可能                          | 可能                 |
| 可串行化(Serializable)     | 不可能           | 不可能                          | 不可能               |

**脏读：**一个事务访问数据，对数据进行**修改未提交**，而另一个事务访问，使用了这个数据，读取的这个数据就称为脏数据。

**不可重复读：**一个事务A多次读取数据，假设在第一次读取数据的过程中，另一个事务B修改了数据，导致A**两次读取的数据不一致**。

**幻读：**事务A对数据进行了**修改操作**，同时事务B对数据进行了插入操作，事务A查询时会发现**还有一条未修改**的数据，就跟幻象一样。

## 2、操作隔离级别（mysql）

（1）查看当前会话隔离级别

```mysql
select @@tx_isolation;
```

（2）查看系统当前隔离级别

```mysql
select @@global.tx_isolation;
```

（3）设置当前会话隔离级别

```mysql
set session transaction isolatin level repeatable read;
```

（4）设置系统当前隔离级别

```mysql
set global transaction isolation level repeatable read;
```

## 3、Join用法

### （1）join on

​		作用：返回两张表中相等的数据，`on`：用于指定连接条件

```mysql
select * from testa join testb on aid=bid  # testa与testb为数据库表
```

​		等价：

```mysql
select * from testa,testb where aid=bid
```

```mysql
select * from testa where aid in(select bid from testb)
```

### （2）left join ... on

​		作用：以**左表为主表**，左表查询的是**所有数据**，右表查询**符合左表的数据**。

```mysql
# 以testa为主表，查询testb中和testa相同的数据，若数据只存在于testa中，testb不存在，则查询出来的testb字段为空
select * from testa left join testb on aid=bid
```

### （3）right join ... on

​		作用：以**右表为主表**，右表查询的是所有数据，左表查询符合左表的数据

```mysql
# 以testb为主表，查询testa中和testb相同的数据，若数据只存在于testb中，testa不存在，则查询出来的testa字段为空
select * from testa right join testb on aid=bid;
```

## 4、Group by

​	作用：Group By是先排序后分组，按列名分组，搭配聚合函数十分实用。

​	（查询不重复的**记录**， `distinct` ：用来查询不重复记录的**条数**）

### （1）概念

![](/../MySQLimages/Group By.png)

---

​	依次从上扫描，**第一次出现则纳入：分组的临时表中**，后续出现同样的值时，则**不记录**。

----

![](/../MySQLimages/group1.png)

```mysql
select * from student stu group by stu.s_sex # 按性别分组
```

![](/../MySQLimages/group2.png)

​	“男" 首次出现是第一行；”女“首次出现是第四行；不计重复出现的值。

### （2）单列group by

```mysql
select name from test group by name
```

### （3）多列group by

```mysql
select name,sum(id) from test group by name,number
```

### （4）分组的作用

​	获取不重复的记录，分组使用聚合函数。

---

```mysql
# 查询每个同学不及格的科目数量
select count(1) from score where s_score<=60
```

![](/../MySQLimages/分组2.png)

```mysql
# 查询每个同学不及格的科目数量（按学号分组）
select count(1) from score where s_score<=60 GROUP BY s_id;
```

![](/../MySQLimages/分组1.png)

## 5、Having

​	having是group by的一个关键字，**只能和group by同时出现**。

​	主要作用是进行**分组后的数据筛选**，允许使用多行函数，**补充where**中**不能使用多行函数**的不足。

```mysql
select column, group_function(column)
from table
[where condition]
[group by  group_by_expression]
[having group_condition]
[order by column];
```

---

​	SQL执行过程：

​		from—>WHERE---->group by---->select------>having------>order by

## 6、聚合函数

​		**聚合函数，就是用来输入多个数据，输出一个数据的**，如：

| 函数名称 |  描述  |
| :------: | :----: |
| COUNT()  |  计数  |
|  SUM()   |  求和  |
|  AVG()   | 平均值 |
|  MAX()   | 最大值 |
|  MIN()   | 最小值 |
|   ...    |        |

​	每个聚合函数的**输入**就是每一个**多数据的单元格**。

----

```mysql
count(*)	# 不会忽略：null值
count(1)	# 忽略：null值
```

## 7、Round()

语法：

```mysql
round(number,digits)
```

参数：
	number：要四舍五入的数；digits是：小数点后保留的位数
		如果 digits 大于 0，则四舍五入到指定的小数位。
		如果 digits 等于 0，则四舍五入到**最接近的整数**。
		如果 digits 小于 0，则在小数点左侧进行四舍五入。
		如果round函数只有参数number,等同于digits 等于 0。

## 8、UNION & UNION ALL 

​	**作用：**用于**合并两个或多个** SELECT 语句的结果集，并在一起显示出来。

​	请注意：UNION 内部的 SELECT 语句必须**拥有相同数量的列**。列也必须拥有**相似的数据类型**。同时，每条 SELECT 语句中的列的**顺序必须相同**。

​	**Union因为要进行重复值扫描，所以效率低。如果合并没有刻意要删除重复行，那么就使用Union All**

----

​	两者的**区别**：

​		`union`：会自动压缩多个结果集合中的重复结果；
​		`union all`：将所有的**结果全部显示出来**，不管是不是重复。

---

​	Union：对两个结果集进行**并集操作**，不包括重复行，同时进行默认规则的排序；

​	Union All：对两个结果集进行**并集操作**，包括重复行，**不进行排序**；

​	Intersect：对两个结果集进行**交集操作**，不包括重复行，同时进行默认规则的排序；

​	Minus：对两个结果集进行**差操作**，不包括重复行，同时进行默认规则的排序。

可以**在最后一个结果集中指定**Order by子句改变排序方式。

```mysql
# 联合两个表，没有重复
SELECT E_Name FROM Employees_China
UNION
SELECT E_Name FROM Employees_USA
```

![](/../MySQLimages/union.png)

## 9、Order by

​	**作用：**用于对**结果集**进行排序；用于根据指定的列对结果集进行排序；**默认按照升序**对记录进行排序。

​	（**降序**：对记录进行排序，可以使用 `DESC 关键字`）

```mysql
select * from student t order by t.sclass desc;
```

## 10、distinct

​	作用：**过滤掉**多余的重复记录，**只保留**一条，但往往只用它来**返回不重复记录的条数**，**而不是**用它来返回**不重记录的所有值**。

​	`distinct`  **只能返回**它的**目标字段**，而**无法返回其它字段**

​	distinct语句中select显示的字段：**只能是**distinct**指定的字段**，其他字段是不可能出现的。

---

​	例如，假如表A有“备注”列，如果想获取：`distinc name`，以及对应的“备注”字段，想**直接通过**distinct**是不可能实现的**。

## 11、count(1)、count(*)与count(列名)的执行区别

从执行效果上看：

```mysql
count(*) # 统计表中所有的记录数，包含：null字段
count(1) # 忽略所有列，用1代表代码行，不会忽略null
count(列名) # 仅统计列名一列，忽略：null字段
```

---

从执行效率上看：

```
列表为主键：count(列名)比count(1)快；
列表不为主键：count(1)比count(列名)快；
表多个列并且没有主键，则count(1)优于count(*)
如果有主键，则count(主键)执行效率是最优的
表只有一个字段，则count(*)最优
```

## 12、SQL注入的问题

​	**问题：**SQL存在漏洞，会被攻击**导致数据泄露**。（SQL**会被拼接**，使用：or）

```mysql
select * from users where `Name`=`kuangshen` and `password` = `123456`;
```

---

​	1=1 ：必定成功

```mysql
# 使用：or注入，盗取数据
select * from users where `Name`=`` or `1=1` and `password` = `` or `1=1`;
```

## 13、case when then else end

```
case 
	when <条件判断表达式一般与列名的判断有关> then<表达式的结果> 
else<不符合表达式的结果> end 结束
```

### （1）简单case函数

```mysql
case sex
	when '1' then '男'    # sex值为：1，则结果为：男
	when '2' then '女'	 # sex值为：2，则结果为：女
else '其他' end
```

### （2）case搜索函数

```mysql
case 
	when sex = '1' then '男'	 # sex值为：1，则结果为：男
    when sex = '2' then '女'	 # sex值为：2，则结果为：女
else '其他' end
```

## 14、数据库视图

​		视图就是一条`SELECT语句`执行后**返回的结果集**。本质上是一种**虚拟表**，其内容和真实表相似，包含了一系列带有名称的列和行的数据。但是视图**并不是在**数据库中，**以存储的数据值形式**存在。

​		为了**提高**复杂sql语句的**复用性**和表操作的**安全性**

---

### （1）视图的特点

- 视图的列可以**来自不同的表**，是表的抽象和逻辑意义上建立的关系；
- 视图是由基本表（实表）产生的表（**虚表**）；
- 视图的建立和删除**不影响基本表**；
- 对**视图的内容更新**（添加、删除、修改）**直接影响**基本表；
- 当视图**来自多个基本表**时，**不允许**添加和删除。

---

### （2）创建视图的语法形式

```mysql
 # 视图名不能和表名、也不能和其他视图重名
 create view view_name AS 查询语句
```

```mysql
# 创建视图
CREATE VIEW view_product AS SELECT id,name FROM t_product;
```

```mysql
# 查询指定的视图
SELECT *FROM view_product;
```

```mysql
# 查询所有的视图
SHOW TABLES;
```

```mysql
# 删除视图
DROP VIEW view_name;
```



# 数据库三大范式

​	 第一范式（1NF）：强调的是**列的原子性**，即列不能够再分成其他几列。 

​	 第二范式（2NF）：1NF的基础上，一是表必须**有一个主键**；二是**没有包含在主键中的列 **  **必须完全依赖于主键**，**而不能只**依赖于**主键的一部分**。 

​	第三范式（3NF）：首先是 2NF，另外**非主键列必须直接依赖于**主键，**不能存在传递依赖**。即不能存在：非主键列 A 依赖于非主键列 B，非主键列 B 依赖于主键的情况。

---

​	2NF，非主键列**是否完全**依赖于主键，还是依赖于**主键的一部分**；

​    3NF，非主键列**是直接**依赖于主键，还是直接依赖于**非主键列**。

# MySQL有关权限的表

`user权限表`：记录允许连接到服务器的**用户帐号信息**，里面**的权限是全局级**的。
`db权限表`：记录各个帐号在各个数据库上的**操作权限**。
`table_priv权限表`：记录数据**表级**的操作权限。
`columns_priv权限表`：记录数据**列级**的操作权限。
`host权限表`：配合db权限表对给定主机上数据库级操作权限作更细致的控制。这个权限表不受GRANT和REVOKE语句的影响。

# 索引

​	学习网址：http://blog.codinglabs.org/articles/theory-of-mysql-index.html

​	定义：索引（index）是帮助`MySQL` 高效获取数据的数据结构。

----

## 1、索引的分类

​	在一个表中，主键索引仅**只有一个**，唯一索引可以**有多个**。

---

- **主键索引（primary key）**

  唯一的标识，主键**不可重复**，只能有一个列作为：主键。

```mysql
# primary key (`列名（索引名也默认为：列名）`)  
primary key (`studentNo`)  
```

- **唯一索引（unique key）**

  **避免**重复的列出现。

```mysql
# unique key `索引名`(`列名`)
unique key `IdentityCard`(`IdentityCard`)
```

- **常规索引（key/index）**

  **默认的**，index。key 关键字来设置

```mysql
# key `索引名` (`列名`)
key `subjectno` (`subjectno`)
```

- **全文索引（fulltest）**

  在**特定的数据库引擎下**才有，以前仅：MylSAM

## 2、索引的使用

### （1）在**创建表的时候**，给字段增加索引

```mysql
# 创建成绩表
drop table if exists `result`;
create table `result`(
	`studentno` int(4) not null comment '学号',
    `subjectno` int(4) not null comment '课程编号',
    `examdate` datetime not null comment '考试日期',
    `studentresult` int (4) not null comment '考试成绩',
    key `subjectno` (`subjectno`)
)engine = innodb default charset = utf8;
```

---

```mysql
# key `索引名`(`列名`) ，给指定列名创建索引
key `subjectno` (`subjectno`)  
```

---

```mysql
# 显示所以的索引信息
show index from student;
```

---

### （2）创建完毕后，增加索引

```mysql
# 给数据库school.student表的 studentName列添加一个全文索引
alter table school.student add fulltest index `studentName`(`studentName`);
```

## 3、explain关键字

```mysql
# explain 分析sql执行的状况
explain select * from student; # 非全文索引
```

结果：

![](/../MySQLimages/查询sql状态.png)

## 4、测试索引

### （1）插入100W条数据

```mysql
-- 插入100万数据.
DELIMITER $$
-- 写函数之前必须要写，标志
CREATE FUNCTION mock_data ()
RETURNS INT
BEGIN
	DECLARE num INT DEFAULT 1000000;
	DECLARE i INT DEFAULT 0;
	WHILE i<num DO
		INSERT INTO `app_user`(`name`,`eamil`,`phone`,`gender`)VALUES(CONCAT('用户',i),'19224305@qq.com','123456789',FLOOR(RAND()*2));
		SET i=i+1;
	END WHILE;
	RETURN i;
END;

SELECT mock_data() -- 执行此函数 生成一百万条数据
```

### （2）查询name为：9999的用户（无索引）

```mysql
# 耗时：1.098  sec
select * from app_user where `name`=`用户9999`;
```

```mysql
# 分析sql语句
explain select * from app_user where `name`=`用户9999`;
```

结果：

![](/../MySQLimages/分析索引2.png)

​	查询的 `row` 有：991749行

​	没有创建索引，需要**循环遍历**，寻找对应的name值

----

### （3）给对应字段创建索引

```mysql
# create index 索引名 on 表(字段名)
create index id_app_user_name on app_user(`name`);
```

![](/../MySQLimages/索引原理1.png)

### （4）查询name为：9999的用户（有索引）

```mysql
# 耗时：0.001  sec
select * from app_user where `name`=`用户9999`;
```

```mysql
# 分析sql语句
explain select * from app_user where `name`=`用户9999`;
```

![](/../MySQLimages/分析索引.png)

​	创建索引后，**不需要遍历**，直接定位到：对应的行

---

## 5、索引原则

- 索引**不是越多越好**
- 不要对：**经常变动数据** 加索引
- 小数量的表不需要加索引
- 索引一般加载**常用来查询的字段上**

---

## 6、删除索引

​	根据**索引名删除**普通索引、唯一索引、全文索引：`alter table 表名 drop KEY 索引名`

​	删除**主键索引**：`alter table 表名 drop primary key`（因为主键只有一个）。这里值得注意的是，如果**主键自增长**，那么不能直接执行此操作（自增长依赖于主键索引）：

​	需要**取消**自增长再行删除：

```mysql
alter table user_index
-- 重新定义字段
modify id int,drop PRIMARY KEY
```

## 7、什么是最左前缀/匹配原则

​	顾名思义，就是**最左优先**，在创建**多列索引**时，要根据业务需求，where子句中使用**最频繁的一列放在最左边**。
​	最左前缀匹配原则，非常重要的原则，mysql会**一直向右匹配**直到**遇到范围查询**(>、<、between、like)就停止匹配，比如a = 1 and b = 2 and c > 3 and d = 4 如果建立(a,b,c,d)顺序的索引，d是用不到索引的，如果建立(a,b,d,c)的索引则都可以用到，a,b,d的顺序可以任意调整。
​	= 和  in**可以乱序**，比如a = 1 and b = 2 and c = 3 建立(a,b,c)索引可以任意顺序，mysql的查询优化器会帮你优化成索引可以识别的形式


# MySQL练习题

## 1、组合两个表

​	无论 person 是否有地址信息，都需要基于上述两表提供 person 的信息。

Person表**（左表）**

| 列表             | 类型    |
| ---------------- | ------- |
| PersonId（主键） | int     |
| FirstName        | varchar |
| LastName         | varchar |

Address表**（右表）**

| 列表      | 类型    |
| --------- | ------- |
| AddressId | int     |
| PersonId  | int     |
| City      | varchar |
| State     | varchar |

```mysql
select p.FirstName,p.LastName,a.City,a.State 
from Person p left outer join Address a
on p.PersonId=a.PersonId
```

​	思路：使用**左连接**，通过Address中的PersonId来判断。

​	**left outer join ...on**：左外连接；
​			结果表中**除了匹配行**外，还包括**左表有**而**右表中不匹配**的行，对于这样的行，**右表**选择列置为：**null** 

* left  *join* 是 *left* *outer* *join的***简写**，*left* *join*默认是*outer*属性的。

----

​	**right outer join ...on**：右外连接；
​			结果表中除了匹配行外，还包括**右表有**而**左表中不匹配**的行，对于这样的行，**左表**选择列置为null 

## 2、第二高的薪水

​	编写一个 SQL 查询，获取 `Employee` 表中第二高的薪水（Salary） 

| id   | salary |
| ---- | ------ |
| 1    | 100    |
| 2    | 200    |
| 3    | 300    |

​	例如上述 `Employee` 表，SQL查询应该返回 `200` 作为第二高的薪水。如果不存在第二高的薪水，那么查询应返回 `null`。

| SecondHighhestSalary |
| -------------------- |
| 200                  |

```mysql
# 从排除了最大值的数据中取最大值，取得就是第二高的薪水
select max(Salary) SecondHighestSalary from Employee where Employee.Salary not in (select max(Salary) from Employee )
```

## 3、第N高的薪水

```mysql
create function getNthHighestSalary(N int) returns int
begin
  # 先用declare定义类型，后通过set进行赋值。
  declare n INT;
  set n = N-1;  # 下标从0开始
  return ( 
    # distinct的只显示一次重复出更的值.不过这个值出现多少次：只显示一次.
    # order by：默认使用升序进行排列；DESC 关键字，会使用：降序进行排列
  	select distinct Salary from Employee order by Salary desc limit n,1
  );
end
```

​	思路：比如第N=2高，如果直接用N值到limit，limit 2,1，意为从第3行开始，显示一行。所以要用**N-1**=1，才能表示从第二行开始。

## 4、round与trunc函数

​	`TRUNC`：函数截取时**不进行**四舍五入

​	`round`：函数截取时**进行**四舍五入



# MySQL常见练习题

## 1、准备工作

### （1）表名和字段

​	学生表 
​		Student(s_id,s_name,s_birth,s_sex) ——学生编号,学生姓名, 出生年月,学生性别 
​	课程表 
​		Course(c_id,c_name,t_id) ——课程编号, 课程名称, 教师编号 
​	教师表 
​		Teacher(t_id,t_name) ——教师编号,教师姓名 
​	成绩表 
​		Score(s_id,c_id,s_score) ——学生编号,课程编号,分数

### （2）创建表

```mysql
--建表
--学生表
CREATE TABLE `Student`(
    `s_id` VARCHAR(20),
    `s_name` VARCHAR(20) NOT NULL DEFAULT '',
    `s_birth` VARCHAR(20) NOT NULL DEFAULT '',
    `s_sex` VARCHAR(10) NOT NULL DEFAULT '',
    PRIMARY KEY(`s_id`)
);
--课程表
CREATE TABLE `Course`(
    `c_id`  VARCHAR(20),
    `c_name` VARCHAR(20) NOT NULL DEFAULT '',
    `t_id` VARCHAR(20) NOT NULL,
    PRIMARY KEY(`c_id`)
);
--教师表
CREATE TABLE `Teacher`(
    `t_id` VARCHAR(20),
    `t_name` VARCHAR(20) NOT NULL DEFAULT '',
    PRIMARY KEY(`t_id`)
);
--成绩表
CREATE TABLE `Score`(
    `s_id` VARCHAR(20),
    `c_id`  VARCHAR(20),
    `s_score` INT(3),
    PRIMARY KEY(`s_id`,`c_id`)
);
--插入学生表测试数据
insert into Student values('01' , '赵雷' , '1990-01-01' , '男');
insert into Student values('02' , '钱电' , '1990-12-21' , '男');
insert into Student values('03' , '孙风' , '1990-05-20' , '男');
insert into Student values('04' , '李云' , '1990-08-06' , '男');
insert into Student values('05' , '周梅' , '1991-12-01' , '女');
insert into Student values('06' , '吴兰' , '1992-03-01' , '女');
insert into Student values('07' , '郑竹' , '1989-07-01' , '女');
insert into Student values('08' , '王菊' , '1990-01-20' , '女');
--课程表测试数据
insert into Course values('01' , '语文' , '02');
insert into Course values('02' , '数学' , '01');
insert into Course values('03' , '英语' , '03');
 
--教师表测试数据
insert into Teacher values('01' , '张三');
insert into Teacher values('02' , '李四');
insert into Teacher values('03' , '王五');
 
--成绩表测试数据
insert into Score values('01' , '01' , 80);
insert into Score values('01' , '02' , 90);
insert into Score values('01' , '03' , 99);
insert into Score values('02' , '01' , 70);
insert into Score values('02' , '02' , 60);
insert into Score values('02' , '03' , 80);
insert into Score values('03' , '01' , 80);
insert into Score values('03' , '02' , 80);
insert into Score values('03' , '03' , 80);
insert into Score values('04' , '01' , 50);
insert into Score values('04' , '02' , 30);
insert into Score values('04' , '03' , 20);
insert into Score values('05' , '01' , 76);
insert into Score values('05' , '02' , 87);
insert into Score values('06' , '01' , 31);
insert into Score values('06' , '03' , 34);
insert into Score values('07' , '02' , 89);
insert into Score values('07' , '03' , 98);
```

### （3）数据库表

​	course表：

| c_id | c_name | t_id |
| :--: | :----: | :--: |
|  01  |  语文  |  02  |
|  02  |  数学  |  01  |
|  03  |  英语  |  03  |

​	score表：

| s_id | c_id | s_score |
| :--: | :--: | :-----: |
|  01  |  01  |   80    |
|  02  |  02  |   90    |
|  03  |  03  |   99    |
| ...  | ...  |   ...   |

​	student表：

| s_id | s_name |  s_birth   | s_sex |
| :--: | :----: | :--------: | :---: |
|  01  |  赵雷  | 1990-01-01 |  男   |
|  02  |  钱电  | 1990-12-21 |  男   |
|  03  |  孙风  | 1990-05-20 |  男   |
| ...  |  ...   |    ...     |  ...  |

​	teacher表：

| t_id | t_name |
| :--: | :----: |
|  01  |  张三  |
|  02  |  李四  |
|  03  |  王五  |

## 2、练习题

### （1）查询"01"课程比"02"课程成绩高的学生的信息及课程分数

​	join on用法：返回两张表中**相等的数据**

​	**思路**：可以**根据**：`score`  表的字段：`s_id` 查询某个学生的全部成绩 或 指定科目成绩

- 成绩（score）之间的**对比都是需要使用：score表**，所以对score表创建**两个别名**
- 先查询出：单个学生的 ’01‘成绩
- 因为题目要求是显示所有的学生信息，但是课程分数可能有，也可能没有，所以使用：left
- 左连接，查询 单个学生’02‘的成绩，然后使用where 判断：01>02

```mysql
select stu.*,sco1.s_score as 01_score,sco1.s_score as 02_score
   from student stu 
   	 # 两张临时表，然后比较两张表的s_score字段
	 join score sco1 on stu.s_id=sco1.s_id and sco1.c_id='01' 
	 left join score sco2 on stu.s_id=sco2.s_id and sco2.c_id='02' or sco2.c_id=NULL where sco1.s_score>sco2.s_score
```

---

SQL执行过程：

​		from—>join---->left join---->where------>on------>select

### （2）查询"01"课程比**"02"**课程成绩低的学生的信息及课程分数

```mysql
select stu.*,sco1.s_score as 01_score,sco2.s_score as 02_score
	from student stu 
		join score sco1 on stu.s_id=sco1.s_id and sco1.c_id='01'	# 查询课程为：01的学生
		left join score sco2 on stu.s_id=sco2.s_id and sco2.c_id='02' or sco2.c_id=NULL # 查询课程为：02的学生
		where sco1.s_score<sco2.s_score  # 判断
```

---

SQL执行过程：

​		from—>join---->left join---->where------>on------>select

### （3）查询平均成绩大于等于**60**分的同学的学生编号和学生姓名和平均成绩

```mysql
SELECT
	stu.s_id,
	stu.s_name,
	Round( AVG( sco.s_score ), 2 ) AS scoreavg
FROM
	student stu
	JOIN score sco ON stu.s_id = sco.s_id 
GROUP BY 	# 将查询的结果按要求分组，，然后计算平均分，并大于等于60
	stu.s_id,
	stu.s_name 
HAVING
	Round( AVG( sco.s_score ), 2 ) >= 60
```

---

SQL执行过程：

​		from—>GROUP BY---->HAVING------>select

### （4）查询平均成绩小于60分的同学的学生编号和学生姓名和平均成绩

​	**附加：**包括有成绩的和无成绩的

```mysql
select stu.s_id,stu.s_name,ROUND(AVG(sco.s_score),2) as avg_score from 
    student stu 
    left join score sco on sco.s_id = stu.s_id
    GROUP BY stu.s_id,stu.s_name HAVING ROUND(AVG(sco.s_score),2)<60
    union
select stu.s_id,stu.s_name,0 as avg_score from 
    student stu 
    where stu.s_id not in ( # 查询score表中没有成绩的同学
                select distinct s_id from score);
```

---

![](/../MySQLimages/无成绩.png)

SQL执行过程：

​		from—>GROUP BY---->HAVING------>select

---

​	注意：UNION 内部的 SELECT 语句必须**拥有相同数量的列**。列也必须拥有**相似的数据类型**。同时，每条 SELECT 语句中的列的**顺序必须相同**。

![](/../MySQLimages/union2.png)

### （5）查询所有同学的学生编号、学生姓名、选课总数、所有课程的总成绩

**不加group by**

```mysql
select stu.s_id,stu.s_name,count(sco.c_id) as sum_course,sum(sco.s_score) as sum_score
	from student stu  left join score sco on stu.s_id=sco.s_id
```

**结果：**

![](/../MySQLimages/group5.png)

sum_course：所有学生的**课程总数量**；sum_score：所有学生的**课程总分**

---

**加group by**，作用：查询不重复的**记录**，并记录重复的条数数量

```mysql
select  stu.s_id,stu.s_name,count(sco.c_id) as sum_course,sum(sco.s_score) as sum_score
	from student stu left join score sco on stu.s_id=sco.s_id
	group by stu.s_id,stu.s_name	# 根据结果集按指定字段分组（查询不重复的记录）
```

![](/../MySQLimages/group55.png)

### （6）查询"李"姓老师的数量

```mysql
select count(t.t_id) from teacher t where t.t_name like '%李%'
```

### （7）查询学过"张三"老师授课的同学的信息 

```mysql
select stu.* from student stu 
	join score sco  on  stu.s_id = sco.s_id 
	# in 关键字
	where sco.c_id in( 
        # 嵌套查询
		select c_id from course where t_id=(
			select t_id  from teacher where t_name='张三'
		)
	);
```

### （8）查询没学过"张三"老师授课的同学的信息

```mysql
select stu.* from student stu 
		join score sco on stu.s_id = sco.s_id 
		# 这种方法不可取
		# 成绩表中学生是多条的，只要有一条信息不是：张三老师的课，则查询成功，不符合题意
		where sco.c_id not in(
			select c_id from course where t_id = (
				select t_id from teacher where t_name='张三'
			)
		);
```

正确的写法：

```mysql
select * from 
    student c 
	# 排除
    where c.s_id not in(
		# 查询所有被张三老师交的学生s_id
        select a.s_id from student a 
        join score b on a.s_id=b.s_id where b.c_id in(
            select c_id from course where t_id =(
                select t_id from teacher where t_name = '张三'))
		);
```

### （9）查询学过编号为"01"并且也学过编号为"02"的课程的同学的信息

```mysql
select stu.* from 
		student stu,score sco1,score sco2
		# 先把所有学生查询出来，再根据这张临时表来判断是否都学过：01,02
		where stu.s_id = sco1.s_id 
			and stu.s_id = sco2.s_id and sco1.c_id='01' and sco2.c_id='02';
```

SQL执行顺序：from——>where——> and——>select

### （10）查询没有学全所有课程的同学的信息

```mysql
select stu.* from 
	student stu where stu.s_id in(
         # 排除所有课都修过的同学，获取剩下同学的:s_id
         select s_id from score where s_id not in(
             # 查询三门课都修过的同学（三张临时表必须同时满足）
             select sco1.s_id from score sco1
             join score sco2 on sco1.s_id = sco2.s_id and sco2.c_id='03'
             join score sco3 on sco1.s_id = sco3.s_id and sco3.c_id='02'
             where sco1.c_id='01';
         )
    );
```

SQL执行顺序：内部查询——外部查询

### （11）查询至少有一门课与学号为"01"的同学所学相同课的同学的信息

```mysql
select * from student
	where s_id in(
		select distinct s_id from score where c_id in(
			select c_id from score where s_id='01'
		)
	);
```

### （12）查询和"01"号的同学学习的课程完全相同的其他同学的信息

```mysql
# 查询和"01"号的同学学习的课程完全相同的其他同学的信息
# count(1):是计算一共有多少符合条件的行(1并不是表示第一个字段,而是表示一个固定值)

select * from student 
		where s_id in(
			select distinct s_id from score where s_id!='01' and c_id in(
				select c_id from score where s_id='01'
			)
            # 按s_id 分组，计算每个同学共有多少门课：count(1)
			group by s_id
            # select count(1) from score where s_id='01'; 计算01同学有多少门课
			having count(1)=(select count(1) from score where s_id='01')
		);
```



![](/../MySQLimages/分组查询.png)

### （13）查询没学过"张三"老师讲授的任一门课程的学生姓名

```mysql
select a.s_name from student a
	# 排除
	where a.s_id not in (
		# 查询学过“张三”老师课的同学
    select s_id from score where c_id = (
			select c_id from course where t_id =(
          select t_id from teacher where t_name = '张三'
			)
		)
	# 分组，获取不重复记录
  group by s_id
	);
```

### （14）查询两门及其以上不及格课程的同学的学号，姓名及其平均成绩 

```mysql
select stu.s_id,stu.s_name,Round(avg(sco.s_score),2) as avg_score
	from student stu 
	left join score sco on stu.s_id=sco.s_id
	where stu.s_id in(
		# 查询不及格的学生以及不及格的科目大于等于2
		select s_id from score where s_score<60 group by s_id having count(1)>=2
	)
	group by s_id,s_name;
```

### （15）检索"01"课程分数小于60，并按分数降序排列的学生信息

```mysql
select stu.*,sco.s_id,sco.s_score 
		from student stu,score sco 
		where stu.s_id=sco.s_id and sco.c_id='01' and sco.s_score<60
		order by s_score desc;
```

### （16）按平均成绩从高到低显示所有学生的所有课程的成绩以及平均成绩

```mysql
 select sco.s_id,
		(select s_score from score  where  s_id = sco.s_id and c_id = '01') as '语文',
		(select s_score from score  where  s_id = sco.s_id and c_id = '02') as '数学',
		(select s_score from score  where  s_id = sco.s_id and c_id = '03') as '英语',
		Round(avg(s_score),2) as '平均分'
		from score sco 
		group by sco.s_id 
		order by '平均分' 
		desc; 
```

### （17）查询各科成绩最高分、最低分和平均分

```mysql
#	以如下形式显示：课程ID，课程name，最高分，最低分，平均分，及格率，中等率，优良率，优秀率
# 	及格为>=60，中等为：70-80，优良为：80-90，优秀为：>=90
```

```mysql
# 若成绩大于等于60，则值为：1，否则为：0
case when sco.s_score>=60 then 1 else 0 end; 
```

```mysql
select 	sco.c_id,cou.c_name,max(sco.s_score),min(sco.s_score),round(avg(sco.s_score),2) as avg_score,
		round((sum(case when sco.s_score>=60 then 1 else 0 end)/count(sco.s_score)),2) as '及格率',
		round((sum(case when sco.s_score>=70 and sco.s_score<=80 then 1 else 0 end)/count(sco.s_score)),2) as '中等率',
		round((sum(case when sco.s_score>=80 and sco.s_score<=90 then 1 else 0 end)/count(sco.s_score)),2) as '优良率',
		round((sum(case when sco.s_score>=90 then 1 else 0 end)/count(sco.s_score)),2) as '优秀率'
		from score sco left join course cou on sco.c_id=cou.c_id group by sco.c_id,cou.c_name
```

### （18）



