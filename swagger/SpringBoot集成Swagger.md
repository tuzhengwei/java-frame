---
typora-root-url: images
---

# SpringBoot集成Swagger

## 1 新建springboot项目

​	首先新建一个spirngboot项目，勾选组件时勾选`Spring-Web`

## 2 导入Swagger依赖

```xml
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger2 -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger2</artifactId>
    <version>3.0.0</version>
</dependency>
```

```xml
<!-- https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui -->
<dependency>
    <groupId>io.springfox</groupId>
    <artifactId>springfox-swagger-ui</artifactId>
    <version>3.0.0</version>
</dependency>
```

​	《注意：》若`3.0.0`访问不了，多半是《降级问题》， 直接换成`2.9.2`就可以进入

## 3 编写HelloController测试

​	编写一个controller测试一下项目是否搭建成功，在主程序同级目录下新建`controller`包，其中新建`HelloController`类

```java
@RestController
public class HelloCtroller {

        @RequestMapping("/hello")
        public String hello(){
            return "hello";
        }
}
```

​	启动，访问：`localhost:8080/hello`

## 4 编写Swagger配置类

在主程序同级目录下《新建》`config`包，其中新建`SwaggerConfig`配置类

- 记住用`@Configuration`注解注明这是配置类
- 同时用`@EnableSwagger2`注解开启Swagger2

---

```java
package com.example.swapper.config;

import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

}
```

## 5  测试进入Sawgger页面

​	启动项目，输入：`http://localhost:8080/swagger-ui.html`

> ​	注意：启动报错：`@EnableSwagger2`注解找不到，但是已经导入了对应的jar包
>
> ​    将导入的依赖包`3.0.0`降级为`2.9.2`即可使用

---

![](/../images/测试初始页面.png)

​	这个界面是Swagger为我们提供的ui界面，我们可以在源码中找到它

![](/../images/首页.png)

## 6 配置Swagger API信息

> ​	在Swagger提供的ui界面，其中的`Swagger`信息模块我们可以<button>自定义信息内容</button>
>
> ​	只需要在Swagger配置类`SwaggerConfig`中实例化`Docket`类队对象的bean实例，通过配置<button>`ApiInfo`类</button>的信息然后传入Docket的bean实例即可

---





