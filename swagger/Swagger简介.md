# Swagger简介

## 1. 前后端分离

​	**后端时代**：前端<button>只用管理</button>静态页面；html等静态资源交给后端通过<button>模板引擎：JSP</button>进行渲染

​	**前后端分离时代**：

- 后端：控制层：controller、服务层：service、数据访问层：dao

- 前端：前端控制层、视图层

- 前后端交互：通过API接口

- 前后端《相对独立，松耦合》，甚至可以部署在《不同的服务器》上

  **随之产生的问题**：

  ​	前后端联调，前端人员和后端人员<button>无法做到</button>`及时协商，尽早解决`

----

**解决方案**：

- 首先指定`schema`，实时更新最新的API，降低集成风险
- 早些年：指定word计划文档
- 前后端分离：
  - 前端测试后端接口数据是否正确：postman
  - 后端提供接口，需要实时更新最新的消息和改动

---

## 2. Swagger引入

- 号称历史上最流行的《API框架》
- RestFul Api文档在线生成工具 =》Api文档与Api定义同步更新
- 直接运行，可以在线测试Api接口
- 支持多种语言

**官网**：https://swagger.io/









